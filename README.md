# README

This repository contains the **PKM server and API source code**.

The documentation is available at [https://decoder.ow2.io/pkm-api/public/index.html](https://decoder.ow2.io/pkm-api/public/index.html).

The integration tests are available at [https://gitlab.ow2.org/decoder/integration-tests](https://gitlab.ow2.org/decoder/integration-tests).

**Note to developers**:

As this repository uses a sub-module doc_to_asfm, you should do the following after cloning that repository:

	$ git submodule init

	$ git submodule update
