const fs = require('fs');

class LineProcessor
{
	constructor(body_filenames)
	{
		this.body_filenames = body_filenames;
		this.state = 0;
		this.body_index = 0;
	}
	
	process(line)
	{
		switch(this.state)
		{
			case 0:
				if(line.endsWith('=> new Promise('))
				{
					console.log(line.slice(0, -15) + '=> {');
					this.state = 1;
				}
				else
				{
					console.log(line);
				}
				break;
				
			case 1:
				if(line == ');')
				{
					if(this.body_index < this.body_filenames.length)
					{
						const body_filename = this.body_filenames[this.body_index];
						console.warn('Opening ' + body_filename);
						const body = fs.readFileSync(body_filename, 'utf8');
						console.log(body);
						++this.body_index;
					}
					console.log('}');
					this.state = 0;
				}
				break;
		}
	}
}

function fix(body_filenames)
{
	var line_processor = new LineProcessor(body_filenames);
	
	process.stdin.setEncoding('utf8');
	process.stdin.resume();

	var buf = '';

	process.stdin.on('data', function(chunk)
	{
		var lines = chunk.split("\n");

		lines[0] = buf + lines[0];
		buf = lines.pop();

		lines.forEach((line) =>
		{
			line_processor.process(line);
		});
	});

	process.stdin.on('end', function() {
			line_processor.process(buf);
	});
}

fix(process.argv.slice(2));

