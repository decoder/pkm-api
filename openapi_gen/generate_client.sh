#!/usr/bin/env bash
API=$(cd $(dirname "$0")/..; pwd)
OPENAPI_GEN_DIR=$(cd $(dirname "${BASH_SOURCE}"); pwd)
HERE=$(pwd)

if [ -z "$1" ]; then
	DIR=$(pwd)
else
	DIR=$(cd "$1"; pwd)
fi

DEFAULT_OPENAPI_GEN_CONF="${OPENAPI_GEN_DIR}/openapi_gen.conf"
source "${DEFAULT_OPENAPI_GEN_CONF}" || exit 1
OPENAPI_GEN_CONF="${DIR}/openapi_gen.conf"
source "${OPENAPI_GEN_CONF}" || exit 1
export OPENAPI_GENERATOR_VERSION

if [ ! -e "${DIR}/${SERVER_API_SPEC}" ]; then
	echo "'${DIR}/${SERVER_API_SPEC}' not found"
	exit 1
fi

if [ ! -x "$(which java)" ]; then
	echo "java not found" 1>&2
	echo "Please install OpenJDK" 1>&2
	exit 1
fi

OPENAPI_GENERATOR=
if [ -n "${OPENAPI_GENERATOR_JAR}" ] && [ -e "${OPENAPI_GENERATOR_JAR}" ]; then
	OPENAPI_GENERATOR=( java -jar "${OPENAPI_GENERATOR_JAR}" )
elif [ -n "${OPENAPI_GENERATOR}" ]; then
  OPENAPI_GENERATOR=( "${OPENAPI_GENERATOR}" )
elif [ -x $(which openapi-generator-cli.sh) ]; then
	OPENAPI_GENERATOR=( openapi-generator-cli.sh )
else
	echo "Please install OpenAPI Generator."
	echo "Download https://github.com/OpenAPITools/openapi-generator/raw/master/bin/utils/openapi-generator-cli.sh and put it in your PATH,"
	echo "or download OpenAPI Generator JAR and set environment variable OPENAPI_GENERATOR_JAR accordingly."
	exit 1
fi

if [ -x $(which js-beautify) ]; then
	export JS_POST_PROCESS_FILE=$(which js-beautify)
else
	echo "Please install js-beautify"
	exit 1
fi

BUILD_CLIENT_SDK='no'
if [ ! -e "${DIR}/${CLIENT_SDK_DIR}/src/index.js" ]; then
	BUILD_CLIENT_SDK='yes'
else
	CLIENT_SDK_DEPS=()
	CLIENT_SDK_DEPS+=( "$0" )
	CLIENT_SDK_DEPS+=( "${DIR}/${SERVER_API_SPEC}" )
	for SERVER_API_SCHEMA in "${SERVER_API_SCHEMAS[@]}"; do
		CLIENT_SDK_DEPS+=( "${DIR}/${SERVER_API_SCHEMA}" )
	done
	for CLIENT_SDK_DEP in "${CLIENT_SDK_DEPS[@]}"; do
		if [ "${CLIENT_SDK_DEP}" -nt "${DIR}/${CLIENT_SDK_DIR}/src/index.js" ]; then
			BUILD_CLIENT_SDK='yes'
		fi
	done
fi

if [ "${BUILD_CLIENT_SDK}" = 'yes' ]; then
	# run OpenAPI Generator
	echo "Running OpenAPI Generator"
	rm -rf "${DIR}/${CLIENT_SDK_DIR}"
	"${OPENAPI_GENERATOR[@]}" \
		generate \
		-g javascript \
		-i "${DIR}/${SERVER_API_SPEC}" \
		-o "${DIR}/${CLIENT_SDK_DIR}" \
		--additional-properties modelPropertyNaming=original,usePromises=true,useES6=false \
		|| exit 1
fi

cp -f "${API}/Apache-2.0.txt" "${DIR}/${CLIENT_SDK_DIR}/LICENSE" || exit 1
echo "Success"
echo "The ${TOOL_NAME} HTTP server client SDK source code is in '${DIR}/${CLIENT_SDK_DIR}'"
cd "${HERE}"
