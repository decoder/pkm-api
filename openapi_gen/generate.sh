#!/usr/bin/env bash
DIR=$(cd $(dirname "$0"); pwd)
"${DIR}/generate_server.sh" "$@" || exit 1
"${DIR}/generate_client.sh" "$@" || exit 1

exit 0
