#! /bin/bash

set -o errexit
set -o pipefail
set -o nounset

# the directory of the script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# the temp directory used, within $DIR
# omit the -p parameter to create a temporal directory in the default location
# WORK_DIR=`mktemp -d -p "$DIR"`
WORK_DIR=`mktemp -d`

# check if tmp dir was created
if [[ ! "$WORK_DIR" || ! -d "$WORK_DIR" ]]; then
  echo "Could not create temp dir"
  exit 1
fi

# deletes the temp directory
function cleanup {
  rm -rf "$WORK_DIR"
  echo "Deleted temp working directory $WORK_DIR"
}

# register the cleanup function to be called on the EXIT signal
trap cleanup EXIT

openssl genrsa -out "${DIR}/../ssl/pkm_docker.key" 2048
openssl req -new -out "${WORK_DIR}/pkm_docker.csr" -key "${DIR}/../ssl/pkm_docker.key" -config "${DIR}/openssl.cnf" -batch
openssl x509 -req -days 3650 -in "${WORK_DIR}/pkm_docker.csr" -signkey "${DIR}/../ssl/pkm_docker.key"  -out "${DIR}/../ssl/pkm_docker.crt" -extensions v3_req -extfile "${DIR}/openssl.cnf"
rm -f "${WORK_DIR}/pkm_docker.csr"
