#!/bin/bash

echo "Frama-Clang for PKM start script."

VOLUME_CONFIG=/pkm-api/volume_config
[ -f "${VOLUME_CONFIG}/frama-clang/server_config.json" ] && \
    cp -f "${VOLUME_CONFIG}/frama-clang/server_config.json" /pkm-api/frama-clang && \
    cp -f "${VOLUME_CONFIG}/frama-clang/server_config.json" /pkm-api/frama-clang/server
  
runuser -u frama-clang -- /pkm-api/frama-clang/server/start_server.sh
