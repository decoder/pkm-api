# FramaClangRestfulApiForPkm.FramaClangApi

All URIs are relative to *http://pkm-api_frama-clang_1:8082*

Method | HTTP request | Description
------------- | ------------- | -------------
[**framaClang**](FramaClangApi.md#framaClang) | **POST** /frama_clang/{dbName} | 
[**getJob**](FramaClangApi.md#getJob) | **GET** /frama_clang/jobs/{jobId} | 



## framaClang

> Job framaClang(dbName, key, body, opts)



Runs Frama-Clang C++ parsing tool based on LLVM Clang

### Example

```javascript
var FramaClangRestfulApiForPkm = require('frama_clang_restful_api_for_pkm');

var apiInstance = new FramaClangRestfulApiForPkm.FramaClangApi();
var dbName = "dbName_example"; // String | The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations)
var key = "key_example"; // String | Access key to the PKM (optional, depending on server configuration)
var body = new FramaClangRestfulApiForPkm.InlineObject(); // InlineObject | 
var opts = {
  'asynchronous': false, // Boolean | flag to control asynchronous/synchronous execution of doc_to_asfm job
  'invocationID': "invocationID_example" // String | invocation identifier (optional)
};
apiInstance.framaClang(dbName, key, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations) | 
 **key** | **String**| Access key to the PKM (optional, depending on server configuration) | 
 **body** | [**InlineObject**](InlineObject.md)|  | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of doc_to_asfm job | [optional] [default to false]
 **invocationID** | **String**| invocation identifier (optional) | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getJob

> Job getJob(jobId, key)



Get Frama-Clang Job. Getting a finished or failed job, unpublish it (it is no longer available)

### Example

```javascript
var FramaClangRestfulApiForPkm = require('frama_clang_restful_api_for_pkm');

var apiInstance = new FramaClangRestfulApiForPkm.FramaClangApi();
var jobId = 56; // Number | Job identifier
var key = "key_example"; // String | Access key to the PKM (optional, depending on server configuration)
apiInstance.getJob(jobId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| Job identifier | 
 **key** | **String**| Access key to the PKM (optional, depending on server configuration) | 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

