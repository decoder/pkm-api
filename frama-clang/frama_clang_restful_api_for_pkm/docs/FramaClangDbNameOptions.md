# FramaClangRestfulApiForPkm.FramaClangDbNameOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**includes_system_files** | **Boolean** | Enable/Disable inclusion of system files (i.e. system headers) in the result | [optional] 


