# FramaClangRestfulApiForPkm.InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_file_paths** | **[String]** | Source Code filenames to process | 
**options** | [**FramaClangDbNameOptions**](FramaClangDbNameOptions.md) |  | [optional] 


