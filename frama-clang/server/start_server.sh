#!/usr/bin/env bash
SERVER_DIR=$(cd $(dirname "$0"); pwd)
API=$(cd "${SERVER_DIR}/../.."; pwd)
HERE=$(pwd)

export NODE_PATH="${API}:${NODE_PATH}"

trap ctrl_c INT

function ctrl_c()
{
        cd "${HERE}"
        exit 0
}

cd "${SERVER_DIR}"
# export NODE_TLS_REJECT_UNAUTHORIZED=0
# export NODE_EXTRA_CA_CERTS="${API}/ssl/pkm.crt"
npm start
cd "${HERE}"
