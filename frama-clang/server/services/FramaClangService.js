/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Runs Frama-Clang C++ parsing tool based on LLVM Clang
*
* dbName String The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations)
* key String Access key to the PKM (optional, depending on server configuration)
* body InlineObject 
* asynchronous Boolean flag to control asynchronous/synchronous execution of doc_to_asfm job (optional)
* invocationID String invocation identifier (optional) (optional)
* returns Job
* */
const frama_clang = ({ dbName, key, body, asynchronous, invocationID }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const source_file_paths = body.source_file_paths;
			const options = body.options;
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const max_parallel_jobs = (global.server_config !== undefined) && global.server_config.max_parallel_jobs;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const tmp_dir = (global.server_config !== undefined) && global.server_config.tmp_dir;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('frama-clang', { dbName : dbName, key : key, source_file_paths : source_file_paths}, { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;

				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const code_api = new PkmRestfulApi.CodeApi();
					const compile_command_api = new PkmRestfulApi.CompileCommandApi();
					const File = require('../../../util/file.js');
					const FileSystem = require('../../../util/file_system');
					const CompileCommand = require('../../../util/compile_command');
					const frama_clang = require('../../../util/frama_clang').frama_clang;
					
					// create a temporary file system
					const file_system = new FileSystem({ debug : debug, logger : job, ...((tmp_dir !== undefined) ? { tmp_dir : tmp_dir } : {}) });
					file_system.make_temporary_root_directory().then(() =>
					{
						// a temporary file system was created
						new Promise((resolve, reject) =>
						{
							// get the list of all source files in the database (without the content)
							code_api.getRawSourceCodes(dbName, key, { abbrev : true }).then((abbrev_source_files) =>
							{
								let write_source_files_promises = [];
									
								// for each source file of database
								abbrev_source_files.forEach((abbrev_source_file) =>
								{
									write_source_files_promises.push(new Promise((resolve, reject) =>
									{
									// get the source file including its content
										code_api.getRawSourceCode(dbName, abbrev_source_file.rel_path, key).then((source_code_file) =>
										{
											if(source_code_file.format != 'text')
											{
												const err_msg = 'File \'' + abbrev_source_file.rel_path + '\' is not a text file';
												job.warn(err_msg);
												reject(err_msg);
												return;
											}
											
											if(source_code_file.content === undefined)
											{
												const err_msg = 'File \'' + abbrev_source_file.rel_path + '\' has no content defined';
												job.warn(err_msg);
												reject(err_msg);
												return;
											}
											
											// replace @ROOT@ by effective root directory
											source_code_file.content = source_code_file.content.replace(/@ROOT@/g, file_system.root_directory);
											
											// write the content of the source file on the host file system
											file_system.writeFile(File.from(source_code_file)).then(() =>
											{
												// the content of the source file was written on the host file system
												resolve();
											}).catch((write_file_err) =>
											{
												// an error occurred while writting the content of the source file on the host file system
												reject(write_file_err);
											});
										}).catch((get_source_file_err) =>
										{
											// an error occurred while getting the source file including its content
											reject(get_source_file_err);
										});
									}));
								});
									
								Promise.all(write_source_files_promises).then(() =>
								{
									// all source files were written on the host file system
									resolve();
								}).catch((write_source_files_err) =>
								{
									// an error occurred while writting all source files on the host file system
									reject(write_source_files_err);
								});
							}).catch((get_abbrev_source_files_err) =>
							{
								// an error occurred while getting the list of all source files in the database (without the content)
								reject(get_abbrev_source_files_err);
							});
						}).then(() => new Promise((resolve, reject) =>
						{
							// all source files were written on the host file system
							
							// get the compile commands
							let get_compile_command_promises = [];
								
							source_file_paths.forEach((source_file_path) =>
							{
								get_compile_command_promises.push(new Promise((resolve, reject) =>
								{
									// get compile command if available
									compile_command_api.getCompileCommand(dbName, source_file_path, key).then((compile_commands) =>
									{
										if(compile_commands.length > 0)
										{
											resolve(CompileCommand.from(compile_commands[0], file_system.root_directory));
										}
									}).catch((err) =>
									{
										if(err.status == 404)
										{
											job.warn('compile command for \'' + source_file_path + '\' not found');
											const path = require('path');
											const compile_command = new CompileCommand(path.join(file_system.root_directory, path.dirname(source_file_path)), source_file_path, '/usr/bin/c++ -I.');
											job.warn('defaulting to ' + JSON.stringify(compile_command, null, 2) + ' as compile command');
											resolve(compile_command);
										}
										else
										{
											job.warn('can\'t get compile command for \'' + source_file_path + '\':' + err);
											reject(err);
										}
									});
								}));
							});
								
							Promise.all(get_compile_command_promises).then((compile_commands) =>
							{
								resolve(compile_commands);
							}).catch((err) =>
							{
								reject(err);
							});
						})).then((compile_commands) => new Promise((resolve, reject) =>
						{
							let frama_clang_options =
							{
								max_parallel_jobs : max_parallel_jobs,
								debug : debug,
								logger : job,
								...options
							};
							
							// run Frama-Clang
							frama_clang(file_system, compile_commands, frama_clang_options).then((frama_clang_result) =>
							{
								const cpp_source_code_documents = frama_clang_result.source_code_documents;
								const cpp_annotations_documents = frama_clang_result.annotations_documents;
								const cpp_comments_documents = frama_clang_result.comments_documents;
								const system_files = frama_clang_result.system_files;
								
								if(debug && cpp_source_code_documents)
								{
									job.log(cpp_source_code_documents.length + ' C++ source code documents generated');
								}
								
								if(debug && cpp_annotations_documents)
								{
									job.log(cpp_annotations_documents.length + ' C++ annotations documents generated');
								}
								
								if(debug && cpp_comments_documents)
								{
									job.log(cpp_comments_documents.length + ' C++ comments documents generated');
								}
								
								if(debug && system_files)
								{
									job.log(system_files.length + ' system files generated');
								}
								
								((!cpp_source_code_documents) ? Promise.resolve() : new Promise((resolve, reject) =>
								{
									let put_cpp_source_codes_promises = [];
									cpp_source_code_documents.forEach((cpp_source_code_document) =>
									{
										if(debug)
										{
											job.log('putting a C++ source code document for \'' + cpp_source_code_document.sourceFile + '\'');
										}
										put_cpp_source_codes_promises.push(new Promise((resolve, reject) =>
										{
											code_api.putCPPSourceCodes(dbName, key, [ cpp_source_code_document ]).then(() =>
											{
												if(invocation !== undefined) invocation.pushSourceCodes(dbName, 'cpp', 'sourcecode', [ cpp_source_code_document ]);
												
												resolve();
											}).catch((put_cpp_source_codes_err) =>
											{
												const msg = 'can\'t put C++ source code documents for \'' + cpp_source_code_document.sourceFile + '\'' + (put_cpp_source_codes_err.status ? (': PKM server returned error status ' + put_cpp_source_codes_err.status) : '');
												job.error(msg);
												reject(new Error(msg));
											});
										}));
									});
									
									Promise.all(put_cpp_source_codes_promises).then(() =>
									{
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								})).then(() =>
								{
									return (!cpp_annotations_documents) ? Promise.resolve() : new Promise((resolve, reject) =>
									{
										let put_cpp_annotations_promises = [];
										cpp_annotations_documents.forEach((cpp_annotations_document) =>
										{
											if(debug)
											{
												job.log('putting a C++ annotations document for \'' + cpp_annotations_document.sourceFile + '\'');
											}
											put_cpp_annotations_promises.push(new Promise((resolve, reject) =>
											{
												code_api.putCPPAnnotations(dbName, key, [ cpp_annotations_document ]).then(() =>
												{
													if(invocation !== undefined) invocation.pushSourceCodes(dbName, 'cpp', 'annotations', [ cpp_annotations_document ]);
												
													resolve();
												}).catch((put_cpp_annotations_err) =>
												{
													const msg = 'can\'t put C++ annotations documents for \'' + cpp_annotations_document.sourceFile + '\'' + (put_cpp_annotations_err.status ? (': PKM server returned error status ' + put_cpp_annotations_err.status) : '');
													job.error(msg);
													reject(new Error(msg));
												});
											}));
										});
										
										Promise.all(put_cpp_annotations_promises).then(() =>
										{
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									});
								}).then(() =>
								{
									return (!cpp_comments_documents) ? Promise.resolve() : new Promise((resolve, reject) =>
									{
										let put_cpp_comments_promises = [];
										cpp_comments_documents.forEach((cpp_comments_document) =>
										{
											if(debug)
											{
												job.log('putting a C++ comments document for \'' + cpp_comments_document.sourceFile + '\'');
											}
											put_cpp_comments_promises.push(new Promise((resolve, reject) =>
											{
												code_api.putCPPComments(dbName, key, [ cpp_comments_document ]).then(() =>
												{
													if(invocation !== undefined) invocation.pushSourceCodes(dbName, 'cpp', 'comments', [ cpp_comments_document ]);
												
													resolve();
												}).catch((put_cpp_comments_err) =>
												{
													const msg = 'can\'t put C++ comments documents for \'' + cpp_comments_document.sourceFile + '\'' + (put_cpp_comments_err.status ? (': PKM server returned error status ' + put_cpp_comments_err.status) : '');
													job.error(msg);
													reject(new Error(msg));
												});
											}));
										});
										
										Promise.all(put_cpp_comments_promises).then(() =>
										{
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									});
								}).then(() =>
								{
									return (!system_files) ? Promise.resolve() : new Promise((resolve, reject) =>
									{
										let put_system_file_promises = [];
										
										system_files.forEach((system_file) =>
										{
											if(debug)
											{
												job.log('putting File \'' + system_file.rel_path + '\'');
											}
											put_system_file_promises.push(new Promise((resolve, reject) =>
											{
												code_api.putRawSourceCodes(dbName, key, [ system_file ]).then(() =>
												{
													if(invocation !== undefined) invocation.pushRawSourceCodes(dbName, [ system_file ]);
												
													resolve();
												}).catch((put_raw_source_codes_err) =>
												{
													const msg = 'can\'t put File \'' + system_file.rel_path + '\'' + (put_raw_source_codes_err.status ? (': PKM server returned error status ' + put_raw_source_codes_err.status) : '');
													job.error(msg);
													reject(new Error(msg));
												});
											}));
										});
										
										Promise.all(put_system_file_promises).then(() =>
										{
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									});
								}).then(() =>
								{
									// abstract representation of the source code was updated in the database
									resolve()
								}).catch((err) =>
								{
									// an error occurred while updating the database
									reject(err);
								});
							}).catch((err) =>
							{
								// an error occurred while running Frama-Clang
								reject(err);
							});
						})).catch((err) =>
						{
							// delay the promise rejection until temporary file system has been deleted
							error = err;
							status = false;
						}).finally(() =>
						{
							// delete previously created temporary file system
							(do_not_remove_temporary_root_directory ? Promise.resolve() : file_system.remove_temporary_root_directory()).catch((err) =>
							{
								// an error occurred while deleting the temporary file system
								const { deep_copy } = require('../../../util/deep_copy');
								job.error(JSON.stringify(deep_copy(err)));
								
								if(status)
								{
									error = err;
									status = false;
								}
							}).finally(() =>
							{
								resolve();
							});
						});
					}).catch((err) =>
					{
						// an error occurred while creating the temporary file system
						reject(err);
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'Frama-Clang',
							'nature of report' : 'Parser report',
							'start running time' : job.start_date,
							'end running time': end_date.toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}
/**
* Get Frama-Clang Job. Getting a finished or failed job, unpublish it (it is no longer available)
*
* jobId Integer Job identifier
* key String Access key to the PKM (optional, depending on server configuration)
* returns Job
* */
const getJob = ({ jobId, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const Job = require('../../../util/job');
			
			const job = Job.find(jobId);
			if(job !== undefined)
			{
				if(job.parameters.key == key)
				{
					if((job.state == 'failed') || (job.state == 'finished'))
					{
						job.unpublish();
					}
					resolve(Service.successResponse(job, 200));
				}
				else
				{
					reject(Service.rejectResponse(new String('Forbidden'), 403));
				}
			}
			else
			{
				resolve(Service.rejectResponse(new String('Not Found'), 404));
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}

module.exports = {
  frama_clang,
  getJob,
};

