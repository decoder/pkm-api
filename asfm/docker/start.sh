#!/bin/bash

echo "ASFM for PKM start script."

VOLUME_CONFIG=/pkm-api/volume_config
[ -f "${VOLUME_CONFIG}/asfm/server_config.json" ] && \
    cp -f "${VOLUME_CONFIG}/asfm/server_config.json" /pkm-api/asfm && \
    cp -f "${VOLUME_CONFIG}/asfm/server_config.json" /pkm-api/asfm/server
  
runuser -u asfm -- /pkm-api/asfm/server/start_server.sh
