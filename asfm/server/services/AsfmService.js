/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Converts an Abstract Semi-Formal Model (ASFM) to a Microsoft Word .docx file
*
* dbName String The project name where to find the Abstract Semi-Formal Model (ASFM) and put the .docx file
* key String Access key to the PKM
* body InlineObject1 
* asynchronous Boolean flag to control asynchronous/synchronous execution of asfm_to_doc job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (optional)
* invocationID String invocation identifier (optional) (optional)
* returns Job
* */
const asfm_to_doc = ({ dbName, key, body, asynchronous, invocationID }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const docName = body.docName;
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('asfm_to_doc', Object.assign({ dbName : dbName, key : key }, body), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const doc_api = new PkmRestfulApi.DocApi();
					const File = require('../../../util/file.js');
					const asfm_to_doc = require('../../../util/asfm_to_doc').asfm_to_doc;
					
					if(debug)
					{
						job.log('getting \'' + docName + '\' document');
					}
					// get the ASFM documentation
					doc_api.getDocs(dbName, key, { doc : docName }).then((documents) =>
					{
						// Note: there's actually only one document even if we get an array
						const asfm_obj = documents[0];
						
						let asfm_to_doc_options =
						{
							debug : debug,
							logger : job
						};
						
						// run asfm_to_doc
						asfm_to_doc(asfm_obj, asfm_to_doc_options).then((buffer) =>
						{
							const file_path = (asfm_obj.sourceFile !== undefined) ? asfm_obj.sourceFile : (asfm_obj.name + '.docx')
							const doc_file = new File(
								file_path,  // rel_path
								buffer,     // content
								null,       // encoding
								'',         // type (let PKM guess the type)
								'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // MIME type
							);
							
							if(debug)
							{
								job.log('putting Documentation file');
							}
							doc_api.putRawDocs(dbName, key, [ doc_file.export() ]).then(() =>
							{
								// database has been updated
								if(invocation !== undefined) invocation.pushRawDocs(dbName, [ doc_file ]);
							
								resolve();
							}).catch((put_raw_docs_err) =>
							{
								const msg = 'can\'t put Documentation file' + (put_raw_docs_err.status ? (': PKM server returned error status ' + put_raw_docs_err.status) : '');
								job.error(msg);
								reject(new Error(msg));
							});
						}).catch((asfm_to_doc_err) =>
						{
							// an error occurred while running asfm_to_doc
							reject(asfm_to_doc_err);
						});
					}).catch((get_doc_err) =>
					{
						// an error occurred while getting ASFM documentation
						const msg = 'can\'t get ASFM Documentation'+ (get_doc_err.status ? (': PKM server returned error status ' + get_doc_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'asfm_to_doc',
							'nature of report' : 'ASFM to Docx conversion',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
				
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}
/**
* Converts source code Abstract Syntax trees (ASTs) to an Abstract Semi-Formal Model (ASFM)
*
* dbName String The project name where to find the source code Abstract Syntax trees (ASTs) and put the Abstract Semi-Formal Model (ASFM).
* key String Access key to the PKM
* asynchronous Boolean flag to control asynchronous/synchronous execution of code_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (optional)
* invocationID String invocation identifier (optional) (optional)
* returns Job
* */
const code_to_asfm = ({ dbName, key, asynchronous, invocationID }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const max_parallel_class_processings = ((global.server_config !== undefined) && (global.server_config.code_to_asfm !== undefined) && global.server_config.code_to_asfm.max_parallel_class_processings) || 1;
			const max_parallel_member_gets_per_class = ((global.server_config !== undefined) && (global.server_config.code_to_asfm !== undefined) && global.server_config.code_to_asfm.max_parallel_member_gets_per_class) || 1;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('code_to_asfm', Object.assign({ dbName : dbName, key : key }, {}), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const asyncPool = require('tiny-async-pool');
				const CppClass = require('../../../util/cpp_class');
				
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let id = 0;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					let doc_document =
					{
						name : dbName,
						units : []
					};
					
					let unit_doc =
					{
						name : (dbName + ' unit')
					}
					
					unit_doc.id = id++;
					const code_api = new PkmRestfulApi.CodeApi();
					
					code_api.getCPPSourceCodeArtefacts(dbName, key, { kind : '/^struct$|^union$|^class$/' }).then((cpp_class_documents) =>
					{
						let cpp_classes = cpp_class_documents
							.filter((cpp_class_document) =>
								cpp_class_document.hasOwnProperty('loc') &&
								cpp_class_document.loc.hasOwnProperty('file') &&
								!cpp_class_document.loc.file.startsWith('/'))
							.map(cpp_class_document => new CppClass(cpp_class_document))
							.filter((cpp_class) => cpp_class.completeDefinition === true);
						
						asyncPool(max_parallel_class_processings, cpp_classes, (cpp_class) => new Promise((resolve, reject) =>
						{
							let promises = [];
							
							let cpp_class_doc =
							{
								name : (cpp_class.pretty_decl !== undefined) ? cpp_class.pretty_decl : ('class #' + cpp_class.id + ' has no pretty_decl'),
								doc : '',
								parent : unit_doc.id
							};
							
							cpp_class_doc.id = ++id;
							
							promises.push(new Promise((resolve, reject) =>
							{
								code_api.getCPPCommentArtefacts(dbName, key, { id : cpp_class.id }).then((cpp_class_comments) =>
								{
									cpp_class_comments.map((comments) => comments.comments).forEach((comment) =>
									{
										if(typeof comment === 'string')
										{
											cpp_class_doc.doc += comment + '\n';
										}
										else if(Array.isArray(comment))
										{
											cpp_class_doc.doc += comment.join('\n');
										}
									});
									
									resolve();
								}).catch((err) =>
								{
									if(err.status === 404)
									{
										resolve();
									}
									else
									{
										if(!err.status) console.error(err);
										const msg = 'can\'t get C++ comments for class #' + cpp_class.id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
										job.error(msg);
										reject(new Error(msg));
									}
								});
							}));
							
							promises.push(new Promise((resolve, reject) =>
							{
								code_api.getCPPAnnotationArtefacts(dbName, key, { id : cpp_class.id }).then((cpp_class_annotations) =>
								{
									cpp_class_annotations.map((annotations) => annotations.annotations).forEach((annotation) =>
									{
										if(typeof annotation === 'string')
										{
											cpp_class_doc.doc += annotation + '\n';
										}
										else if(Array.isArray(annotation))
										{
											cpp_class_doc.doc += annotation.join('\n');
										}
									});
									
									resolve();
								}).catch((err) =>
								{
									if(err.status === 404)
									{
										resolve();
									}
									else
									{
										if(!err.status) console.error(err);
										const msg = 'can\'t get C++ annotations for class #' + cpp_class.id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
										job.error(msg);
										reject(new Error(msg));
									}
								});
							}));
							
							if(cpp_class.hasOwnProperty('fields'))
							{
								promises.push(new Promise((resolve, reject) =>
								{
									asyncPool(max_parallel_member_gets_per_class, cpp_class.fields, ({ id }) => new Promise((resolve, reject) =>
									{
										code_api.getCPPSourceCodeArtefacts(dbName, key, { id : id }).then((cpp_fields) =>
										{
											let cpp_field = cpp_fields[0];
											
											let cpp_class_field_doc =
											{
												name : (cpp_field.pretty_decl !== undefined) ? cpp_field.pretty_decl : ('field #' + cpp_field.id + ' has no pretty_decl'),
												doc : '',
												parent : cpp_class_doc.id
											};
											
											cpp_class_field_doc.id = id++;
											
											let promises = [];
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPCommentArtefacts(dbName, key, { id : id }).then((cpp_field_comments) =>
												{
													cpp_field_comments.map((comments) => comments.comments).forEach((comment) =>
													{
														if(typeof comment === 'string')
														{
															cpp_class_field_doc.doc += comment + '\n';
														}
														else if(Array.isArray(comment))
														{
															cpp_class_field_doc.doc += comment.join('\n');
														}
													});
													
													resolve(cpp_class_field_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ comments for field #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPAnnotationArtefacts(dbName, key, { id : id }).then((cpp_field_annotations) =>
												{
													cpp_field_annotations.map((annotations) => annotations.annotations).forEach((annotation) =>
													{
														if(typeof annotation === 'string')
														{
															cpp_class_field_doc.doc += annotation + '\n';
														}
														else if(Array.isArray(annotation))
														{
															cpp_class_field_doc.doc += annotation.join('\n');
														}
													});
													
													resolve(cpp_class_field_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ annotations for field #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											Promise.all(promises).then(() =>
											{
												if(cpp_class_field_doc.doc.length === 0) delete cpp_class_field_doc.doc;
												resolve(cpp_class_field_doc);
											}).catch((err) =>
											{
												reject(err);
											});
										}).catch((get_cpp_class_field_err) =>
										{
											const msg = 'can\'t get C++ class field #' + id + '\'' + (get_cpp_class_field_err.status ? (': PKM server returned error status ' + get_cpp_class_field_err.status) : '');
											job.error(msg);
											reject(new Error(msg));
										});
									})).then((cpp_class_field_docs) =>
									{
										cpp_class_doc.fields = cpp_class_field_docs;
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								}));
							}
							
							if(cpp_class.hasOwnProperty('methods'))
							{
								promises.push(new Promise((resolve, reject) =>
								{
									asyncPool(max_parallel_member_gets_per_class, cpp_class.methods, ({ id }) => new Promise((resolve, reject) =>
									{
										code_api.getCPPSourceCodeArtefacts(dbName, key, { id : id }).then((cpp_methods) =>
										{
											let cpp_method = cpp_methods[0];
											
											let cpp_class_method_doc =
											{
												name : (cpp_method.pretty_decl !== undefined) ? cpp_method.pretty_decl : ('method #' + cpp_method.id + ' has no pretty_decl'),
												doc : '',
												parent : cpp_class_doc.id
											};
											
											cpp_class_method_doc.id = id++;
											
											let promises = [];
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPCommentArtefacts(dbName, key, { id : id }).then((cpp_method_comments) =>
												{
													cpp_method_comments.map((comments) => comments.comments).forEach((comment) =>
													{
														if(typeof comment === 'string')
														{
															cpp_class_method_doc.doc += comment + '\n';
														}
														else if(Array.isArray(comment))
														{
															cpp_class_method_doc.doc += comment.join('\n');
														}
													});
													
													resolve(cpp_class_method_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ comments for method #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPAnnotationArtefacts(dbName, key, { id : id }).then((cpp_method_annotations) =>
												{
													cpp_method_annotations.map((annotations) => annotations.annotations).forEach((annotation) =>
													{
														if(typeof annotation === 'string')
														{
															cpp_class_method_doc.doc += annotation + '\n';
														}
														else if(Array.isArray(annotation))
														{
															cpp_class_method_doc.doc += annotation.join('\n');
														}
													});
													
													resolve(cpp_class_method_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ annotations for method #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											Promise.all(promises).then(() =>
											{
												if(cpp_class_method_doc.doc.length === 0) delete cpp_class_method_doc.doc;
												resolve(cpp_class_method_doc);
											}).catch((err) =>
											{
												reject(err);
											});
										}).catch((get_cpp_class_method_err) =>
										{
											const msg = 'can\'t get C++ class method #' + id + '\'' + (get_cpp_class_method_err.status ? (': PKM server returned error status ' + get_cpp_class_method_err.status) : '');
											job.error(msg);
											reject(new Error(msg));
										});
									})).then((cpp_class_method_docs) =>
									{
										cpp_class_doc.methods = cpp_class_method_docs;
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								}));
							}
							
							Promise.all(promises).then(() =>
							{
								if(cpp_class_doc.doc.length === 0) delete cpp_class_doc.doc;
								resolve(cpp_class_doc);
							}).catch((err) =>
							{
								reject(err);
							});
						})).then((cpp_class_docs) =>
						{
							unit_doc.classes = cpp_class_docs;
							doc_document.units.push(unit_doc);
							
							resolve(doc_document);
						}).catch((err) =>
						{
							reject(err);
						});
					}).catch((get_cpp_source_code_artefacts_err) =>
					{
						const msg = 'can\'t get C++ structs/unions/classes' + (get_cpp_source_code_artefacts_err.status ? (': PKM server returned error status ' + get_cpp_source_code_artefacts_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then((doc_document) =>
				{
					const doc_api = new PkmRestfulApi.DocApi();
					
					if(debug)
					{
						job.log('putting ASFM Documentation');
					}
					
					doc_api.putDocs(dbName, key, [ doc_document ]).then(() =>
					{
						if(invocation !== undefined) invocation.pushDocs(dbName, [ doc_document ]);
						
						resolve();
					}).catch((put_docs_err) =>
					{
						const msg = 'can\'t put ASFM Documentation' + (put_docs_err.status ? (': PKM server returned error status ' + put_docs_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				}).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'code_to_asfm',
							'nature of report' : 'Code to ASFM conversion',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}
/**
* Converts a Microsoft Office .docx file to an Abstract Semi-Formal Model (ASFM)
*
* dbName String The project name where to find the .docx file and put the Abstract Semi-Formal Model (ASFM)
* key String Access key to the PKM
* body InlineObject 
* asynchronous Boolean flag to control asynchronous/synchronous execution of doc_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (optional)
* invocationID String invocation identifier (optional) (optional)
* returns Job
* */
const doc_to_asfm = ({ dbName, key, body, asynchronous, invocationID }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const file_path = body.file_path;
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const tmp_dir = (global.server_config !== undefined) && global.server_config.tmp_dir;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('doc_to_asfm', Object.assign({ dbName : dbName, key : key }, body), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const doc_api = new PkmRestfulApi.DocApi();
					const File = require('../../../util/file.js');
					const FileSystem = require('../../../util/file_system');
					const doc_to_asfm = require('../../../util/doc_to_asfm').doc_to_asfm;
					
					// create a temporary file system
					const file_system = new FileSystem({ debug : debug, logger : job, ...((tmp_dir !== undefined) ? { tmp_dir : tmp_dir } : {}) });
					file_system.make_temporary_root_directory().then(() =>
					{
						// a temporary file system was created
						new Promise((resolve, reject) =>
						{
							// get the raw documentation file
							doc_api.getRawDoc(dbName, file_path, key).then((doc_file) =>
							{
								// write the content of the documentation file on the host file system
								file_system.writeFile(File.from(doc_file)).then(() =>
								{
									// the content of the documentation file was written on the host file system
									resolve();
								}).catch((write_file_err) =>
								{
									// an error occurred while writting the content of the documentation file on the host file system
									reject(write_file_err);
								});
							}).catch((get_raw_doc_err) =>
							{
								// an error occurred while getting the documentation file
								reject(get_raw_doc_err);
							});
						}).then(() => new Promise((resolve, reject) =>
						{
							// documentation file was written on the host file system
							let doc_to_asfm_options =
							{
								debug : debug,
								logger : job
							};
							
							// run doc_to_asfm
							doc_to_asfm(file_system, file_path, doc_to_asfm_options).then((doc_document) =>
							{
								doc_document.sourceFile = file_path;
								
								if(debug)
								{
									job.log('putting ASFM Documentation');
								}
								doc_api.putDocs(dbName, key, [ doc_document ]).then(() =>
								{
									if(invocation !== undefined) invocation.pushDocs(dbName, [ doc_document ]);
									
									resolve();
								}).catch((put_docs_err) =>
								{
									const msg = 'can\'t put ASFM Documentation' + (put_docs_err.status ? (': PKM server returned error status ' + put_docs_err.status) : '');
									job.error(msg);
									reject(new Error(msg));
								});
							}).catch((err) =>
							{
								// an error occurred while running doc_to_asfm
								reject(err);
							});
						})).catch((err) =>
						{
							// delay the promise rejection until temporary file system has been deleted
							error = err;
							status = false;
						}).finally(() =>
						{
							// delete previously created temporary file system
							(do_not_remove_temporary_root_directory ? Promise.resolve() : file_system.remove_temporary_root_directory()).catch((err) =>
							{
								// an error occurred while deleting the temporary file system
								const { deep_copy } = require('../../../util/deep_copy');
								job.error(JSON.stringify(deep_copy(err)));
								
								if(status)
								{
									error = err;
									status = false;
								}
							}).finally(() =>
							{
								resolve();
							});
						});
					}).catch((err) =>
					{
						// an error occurred while creating the temporary file system
						reject(err);
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'doc_to_asfm',
							'nature of report' : 'Docx to ASFM conversion',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}
/**
* Get ASFM Job (either doc_to_asfm, asfm_to_doc, or code_to_asfm). Getting a finished or failed job, unpublish it (it is no longer available)
*
* jobId Integer Job identifier
* key String Access key to the PKM
* returns Job
* */
const getJob = ({ jobId, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const Job = require('../../../util/job');
			
			const job = Job.find(jobId);
			if(job !== undefined)
			{
				if(job.parameters.key == key)
				{
					if((job.state == 'failed') || (job.state == 'finished'))
					{
						job.unpublish();
					}
					resolve(Service.successResponse(job, 200));
				}
				else
				{
					reject(Service.rejectResponse(new String('Forbidden'), 403));
				}
			}
			else
			{
				resolve(Service.rejectResponse(new String('Not Found'), 404));
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}

module.exports = {
  asfm_to_doc,
  code_to_asfm,
  doc_to_asfm,
  getJob,
};

