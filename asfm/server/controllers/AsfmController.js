/**
 * The AsfmController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/AsfmService');
const asfm_to_doc = async (request, response) => {
  await Controller.handleRequest(request, response, service.asfm_to_doc);
};

const code_to_asfm = async (request, response) => {
  await Controller.handleRequest(request, response, service.code_to_asfm);
};

const doc_to_asfm = async (request, response) => {
  await Controller.handleRequest(request, response, service.doc_to_asfm);
};

const getJob = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJob);
};


module.exports = {
  asfm_to_doc,
  code_to_asfm,
  doc_to_asfm,
  getJob,
};
