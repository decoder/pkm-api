# AsfmRestfulApiForPkm.AsfmApi

All URIs are relative to *http://pkm-api_asfm_1:8086*

Method | HTTP request | Description
------------- | ------------- | -------------
[**asfmToDoc**](AsfmApi.md#asfmToDoc) | **POST** /asfm/asfm_to_doc/{dbName} | 
[**codeToAsfm**](AsfmApi.md#codeToAsfm) | **POST** /asfm/code_to_asfm/{dbName} | 
[**docToAsfm**](AsfmApi.md#docToAsfm) | **POST** /asfm/doc_to_asfm/{dbName} | 
[**getJob**](AsfmApi.md#getJob) | **GET** /asfm/jobs/{jobId} | 



## asfmToDoc

> Job asfmToDoc(dbName, key, body, opts)



Converts an Abstract Semi-Formal Model (ASFM) to a Microsoft Word .docx file

### Example

```javascript
var AsfmRestfulApiForPkm = require('asfm_restful_api_for_pkm');

var apiInstance = new AsfmRestfulApiForPkm.AsfmApi();
var dbName = "dbName_example"; // String | The project name where to find the Abstract Semi-Formal Model (ASFM) and put the .docx file
var key = "key_example"; // String | Access key to the PKM
var body = new AsfmRestfulApiForPkm.InlineObject1(); // InlineObject1 | 
var opts = {
  'asynchronous': false, // Boolean | flag to control asynchronous/synchronous execution of asfm_to_doc job. When invocationID is specified, this flag is implicitely set to false (synchronous execution).
  'invocationID': "invocationID_example" // String | invocation identifier (optional)
};
apiInstance.asfmToDoc(dbName, key, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| The project name where to find the Abstract Semi-Formal Model (ASFM) and put the .docx file | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**InlineObject1**](InlineObject1.md)|  | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of asfm_to_doc job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). | [optional] [default to false]
 **invocationID** | **String**| invocation identifier (optional) | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## codeToAsfm

> Job codeToAsfm(dbName, key, opts)



Converts source code Abstract Syntax trees (ASTs) to an Abstract Semi-Formal Model (ASFM)

### Example

```javascript
var AsfmRestfulApiForPkm = require('asfm_restful_api_for_pkm');

var apiInstance = new AsfmRestfulApiForPkm.AsfmApi();
var dbName = "dbName_example"; // String | The project name where to find the source code Abstract Syntax trees (ASTs) and put the Abstract Semi-Formal Model (ASFM).
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'asynchronous': false, // Boolean | flag to control asynchronous/synchronous execution of code_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution).
  'invocationID': "invocationID_example" // String | invocation identifier (optional)
};
apiInstance.codeToAsfm(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| The project name where to find the source code Abstract Syntax trees (ASTs) and put the Abstract Semi-Formal Model (ASFM). | 
 **key** | **String**| Access key to the PKM | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of code_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). | [optional] [default to false]
 **invocationID** | **String**| invocation identifier (optional) | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## docToAsfm

> Job docToAsfm(dbName, key, body, opts)



Converts a Microsoft Office .docx file to an Abstract Semi-Formal Model (ASFM)

### Example

```javascript
var AsfmRestfulApiForPkm = require('asfm_restful_api_for_pkm');

var apiInstance = new AsfmRestfulApiForPkm.AsfmApi();
var dbName = "dbName_example"; // String | The project name where to find the .docx file and put the Abstract Semi-Formal Model (ASFM)
var key = "key_example"; // String | Access key to the PKM
var body = new AsfmRestfulApiForPkm.InlineObject(); // InlineObject | 
var opts = {
  'asynchronous': false, // Boolean | flag to control asynchronous/synchronous execution of doc_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution).
  'invocationID': "invocationID_example" // String | invocation identifier (optional)
};
apiInstance.docToAsfm(dbName, key, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| The project name where to find the .docx file and put the Abstract Semi-Formal Model (ASFM) | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**InlineObject**](InlineObject.md)|  | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of doc_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). | [optional] [default to false]
 **invocationID** | **String**| invocation identifier (optional) | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getJob

> Job getJob(jobId, key)



Get ASFM Job (either doc_to_asfm, asfm_to_doc, or code_to_asfm). Getting a finished or failed job, unpublish it (it is no longer available)

### Example

```javascript
var AsfmRestfulApiForPkm = require('asfm_restful_api_for_pkm');

var apiInstance = new AsfmRestfulApiForPkm.AsfmApi();
var jobId = 56; // Number | Job identifier
var key = "key_example"; // String | Access key to the PKM
apiInstance.getJob(jobId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| Job identifier | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

