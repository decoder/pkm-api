# AsfmRestfulApiForPkm.InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_path** | **String** | Filename of the Microsoft Office .docx file to convert | 


