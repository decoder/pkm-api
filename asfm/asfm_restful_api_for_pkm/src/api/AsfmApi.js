/**
 * AsfmRestfulApiForPkm
 * API of ASFM for PKM
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/InlineObject', 'model/InlineObject1', 'model/Job'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/InlineObject'), require('../model/InlineObject1'), require('../model/Job'));
  } else {
    // Browser globals (root is window)
    if (!root.AsfmRestfulApiForPkm) {
      root.AsfmRestfulApiForPkm = {};
    }
    root.AsfmRestfulApiForPkm.AsfmApi = factory(root.AsfmRestfulApiForPkm.ApiClient, root.AsfmRestfulApiForPkm.InlineObject, root.AsfmRestfulApiForPkm.InlineObject1, root.AsfmRestfulApiForPkm.Job);
  }
}(this, function(ApiClient, InlineObject, InlineObject1, Job) {
  'use strict';

  /**
   * Asfm service.
   * @module api/AsfmApi
   * @version 1.0.0
   */

  /**
   * Constructs a new AsfmApi. 
   * @alias module:api/AsfmApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Converts an Abstract Semi-Formal Model (ASFM) to a Microsoft Word .docx file
     * @param {String} dbName The project name where to find the Abstract Semi-Formal Model (ASFM) and put the .docx file
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject1} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of asfm_to_doc job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Job} and HTTP response
     */
    this.asfmToDocWithHttpInfo = function(dbName, key, body, opts) {
      opts = opts || {};
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling asfmToDoc");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling asfmToDoc");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling asfmToDoc");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'asynchronous': opts['asynchronous'],
        'invocationID': opts['invocationID'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Job;
      return this.apiClient.callApi(
        '/asfm/asfm_to_doc/{dbName}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Converts an Abstract Semi-Formal Model (ASFM) to a Microsoft Word .docx file
     * @param {String} dbName The project name where to find the Abstract Semi-Formal Model (ASFM) and put the .docx file
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject1} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of asfm_to_doc job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Job}
     */
    this.asfmToDoc = function(dbName, key, body, opts) {
      return this.asfmToDocWithHttpInfo(dbName, key, body, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Converts source code Abstract Syntax trees (ASTs) to an Abstract Semi-Formal Model (ASFM)
     * @param {String} dbName The project name where to find the source code Abstract Syntax trees (ASTs) and put the Abstract Semi-Formal Model (ASFM).
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of code_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Job} and HTTP response
     */
    this.codeToAsfmWithHttpInfo = function(dbName, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling codeToAsfm");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling codeToAsfm");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'asynchronous': opts['asynchronous'],
        'invocationID': opts['invocationID'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Job;
      return this.apiClient.callApi(
        '/asfm/code_to_asfm/{dbName}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Converts source code Abstract Syntax trees (ASTs) to an Abstract Semi-Formal Model (ASFM)
     * @param {String} dbName The project name where to find the source code Abstract Syntax trees (ASTs) and put the Abstract Semi-Formal Model (ASFM).
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of code_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Job}
     */
    this.codeToAsfm = function(dbName, key, opts) {
      return this.codeToAsfmWithHttpInfo(dbName, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Converts a Microsoft Office .docx file to an Abstract Semi-Formal Model (ASFM)
     * @param {String} dbName The project name where to find the .docx file and put the Abstract Semi-Formal Model (ASFM)
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of doc_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Job} and HTTP response
     */
    this.docToAsfmWithHttpInfo = function(dbName, key, body, opts) {
      opts = opts || {};
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling docToAsfm");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling docToAsfm");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling docToAsfm");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'asynchronous': opts['asynchronous'],
        'invocationID': opts['invocationID'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Job;
      return this.apiClient.callApi(
        '/asfm/doc_to_asfm/{dbName}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Converts a Microsoft Office .docx file to an Abstract Semi-Formal Model (ASFM)
     * @param {String} dbName The project name where to find the .docx file and put the Abstract Semi-Formal Model (ASFM)
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of doc_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution). (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Job}
     */
    this.docToAsfm = function(dbName, key, body, opts) {
      return this.docToAsfmWithHttpInfo(dbName, key, body, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get ASFM Job (either doc_to_asfm, asfm_to_doc, or code_to_asfm). Getting a finished or failed job, unpublish it (it is no longer available)
     * @param {Number} jobId Job identifier
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Job} and HTTP response
     */
    this.getJobWithHttpInfo = function(jobId, key) {
      var postBody = null;
      // verify the required parameter 'jobId' is set
      if (jobId === undefined || jobId === null) {
        throw new Error("Missing the required parameter 'jobId' when calling getJob");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getJob");
      }

      var pathParams = {
        'jobId': jobId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Job;
      return this.apiClient.callApi(
        '/asfm/jobs/{jobId}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get ASFM Job (either doc_to_asfm, asfm_to_doc, or code_to_asfm). Getting a finished or failed job, unpublish it (it is no longer available)
     * @param {Number} jobId Job identifier
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Job}
     */
    this.getJob = function(jobId, key) {
      return this.getJobWithHttpInfo(jobId, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
