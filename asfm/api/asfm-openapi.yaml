# 
# This file is part of PKM (Persistent Knowledge Monitor).
# Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
#                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#         http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License. 

openapi: 3.0.0
info:
  title: AsfmRestfulApiForPkm
  description: API of ASFM for PKM
  version: 1.0.0
  contact:
    name: DECODER project
    url: https://www.decoder-project.eu
    email: decoder@decoder-project.eu
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
servers:
  - url: http://pkm-api_asfm_1:8086
paths:
  /asfm/doc_to_asfm/{dbName}:
    post:
      tags:
        - Asfm
      operationId: doc_to_asfm
      description: Converts a Microsoft Office .docx file to an Abstract Semi-Formal Model (ASFM)
      parameters:
        - name: dbName
          description: The project name where to find the .docx file and put the Abstract Semi-Formal Model (ASFM)
          in: path
          required: true
          schema:
            type: string
        - name: asynchronous
          description: flag to control asynchronous/synchronous execution of doc_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution).
          in: query
          schema:
            type: boolean
            default: false
        - $ref: '#/components/parameters/InvocationIdParam'
        - $ref: '#/components/parameters/keyParam'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type : object
              properties:
                file_path:
                  description: Filename of the Microsoft Office .docx file to convert
                  type: string
              required:
                - file_path
      x-codegen-request-body-name: body
      responses:
        201:
          description: Created, returns a job
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
        400:
          description: Bad request
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        401:
          description: Unauthorized operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        403:
          description: Forbidden operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        500:
          description: Internal Server Error, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
  /asfm/asfm_to_doc/{dbName}:
    post:
      tags:
        - Asfm
      operationId: asfm_to_doc
      description: Converts an Abstract Semi-Formal Model (ASFM) to a Microsoft Word .docx file
      parameters:
        - name: dbName
          description: The project name where to find the Abstract Semi-Formal Model (ASFM) and put the .docx file
          in: path
          required: true
          schema:
            type: string
        - name: asynchronous
          description: flag to control asynchronous/synchronous execution of asfm_to_doc job. When invocationID is specified, this flag is implicitely set to false (synchronous execution).
          in: query
          schema:
            type: boolean
            default: false
        - $ref: '#/components/parameters/InvocationIdParam'
        - $ref: '#/components/parameters/keyParam'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type : object
              properties:
                docName:
                  description: Name of the Abstract Semi-Formal Model (ASFM) to convert
                  type: string
              required:
                - docName
      x-codegen-request-body-name: body
      responses:
        201:
          description: Created, returns a job
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
        400:
          description: Bad request
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        401:
          description: Unauthorized operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        403:
          description: Forbidden operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        500:
          description: Internal Server Error, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
  /asfm/code_to_asfm/{dbName}:
    post:
      tags:
        - Asfm
      operationId: code_to_asfm
      description: Converts source code Abstract Syntax trees (ASTs) to an Abstract Semi-Formal Model (ASFM)
      parameters:
        - name: dbName
          description: The project name where to find the source code Abstract Syntax trees (ASTs) and put the Abstract Semi-Formal Model (ASFM).
          in: path
          required: true
          schema:
            type: string
        - name: asynchronous
          description: flag to control asynchronous/synchronous execution of code_to_asfm job. When invocationID is specified, this flag is implicitely set to false (synchronous execution).
          in: query
          schema:
            type: boolean
            default: false
        - $ref: '#/components/parameters/InvocationIdParam'
        - $ref: '#/components/parameters/keyParam'
      x-codegen-request-body-name: body
      responses:
        201:
          description: Created, returns a job
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
        400:
          description: Bad request
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        401:
          description: Unauthorized operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        403:
          description: Forbidden operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        500:
          description: Internal Server Error, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
  /asfm/jobs/{jobId}:
    get:
      tags:
        - Asfm
      operationId: getJob
      description: Get ASFM Job (either doc_to_asfm, asfm_to_doc, or code_to_asfm). Getting a finished or failed job, unpublish it (it is no longer available)
      parameters:
        - name: jobId
          description: Job identifier
          in: path
          required: true
          schema:
            type: integer
            minimum: 0
        - $ref: '#/components/parameters/keyParam'
      responses:
        200:
          description: Successful operation, returns a job
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
        400:
          description: Bad request
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        401:
          description: Unauthorized operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        403:
          description: Forbidden operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        404:
          description: Not found, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
        500:
          description: Internal Server Error, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
          content:
            application/json:
              schema:
                type: string
                description: error message
components:
  parameters:
    keyParam:
      name: key
      description: Access key to the PKM
      in: header
      required: true
      schema:
        type: string
    InvocationIdParam:
      name: invocationID
      description: invocation identifier (optional)
      in: query
      schema:
        type: string
  schemas:
    Job:
      type: object
      description: a job
      properties:
        id:
          description: job identifier
          type: integer
          minimum: 0
        service_name:
          description: name of the service running the job
          type: string
        parameters:
          description: parameters of the job
          type: object
          properties:
            dbName:
              description: database name
              type: string
        state:
          description: job state
          enum: [pending, running, failed, finished]
        start_date:
          description: date (GMT) when job started
          type: string
        end_date:
          description: date (GMT) when job ended
          type: string
        logs:
          description: log messages
          type: string
        warnings:
          type: string
          description: warning messages
        err:
          description: error
          type: object
  headers:
# for CORS (Cross-Origin Resource Sharing)
    Access-Control-Allow-Origin:
      schema:
        type: string
        default: '*'
    Access-Control-Allow-Methods:
      schema:
        type: string
        default: 'GET, POST, PUT, DELETE, OPTIONS'
    Access-Control-Allow-Headers:
      schema:
        type: string
        default: 'Origin, Content-Type, Accept, key'
