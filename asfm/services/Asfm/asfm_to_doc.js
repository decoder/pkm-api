	return new Promise((resolve, reject) =>
	{
		try
		{
			const docName = body.docName;
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('asfm_to_doc', Object.assign({ dbName : dbName, key : key }, body), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const doc_api = new PkmRestfulApi.DocApi();
					const File = require('../../../util/file.js');
					const asfm_to_doc = require('../../../util/asfm_to_doc').asfm_to_doc;
					
					if(debug)
					{
						job.log('getting \'' + docName + '\' document');
					}
					// get the ASFM documentation
					doc_api.getDocs(dbName, key, { doc : docName }).then((documents) =>
					{
						// Note: there's actually only one document even if we get an array
						const asfm_obj = documents[0];
						
						let asfm_to_doc_options =
						{
							debug : debug,
							logger : job
						};
						
						// run asfm_to_doc
						asfm_to_doc(asfm_obj, asfm_to_doc_options).then((buffer) =>
						{
							const file_path = (asfm_obj.sourceFile !== undefined) ? asfm_obj.sourceFile : (asfm_obj.name + '.docx')
							const doc_file = new File(
								file_path,  // rel_path
								buffer,     // content
								null,       // encoding
								'',         // type (let PKM guess the type)
								'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // MIME type
							);
							
							if(debug)
							{
								job.log('putting Documentation file');
							}
							doc_api.putRawDocs(dbName, key, [ doc_file.export() ]).then(() =>
							{
								// database has been updated
								if(invocation !== undefined) invocation.pushRawDocs(dbName, [ doc_file ]);
							
								resolve();
							}).catch((put_raw_docs_err) =>
							{
								const msg = 'can\'t put Documentation file' + (put_raw_docs_err.status ? (': PKM server returned error status ' + put_raw_docs_err.status) : '');
								job.error(msg);
								reject(new Error(msg));
							});
						}).catch((asfm_to_doc_err) =>
						{
							// an error occurred while running asfm_to_doc
							reject(asfm_to_doc_err);
						});
					}).catch((get_doc_err) =>
					{
						// an error occurred while getting ASFM documentation
						const msg = 'can\'t get ASFM Documentation'+ (get_doc_err.status ? (': PKM server returned error status ' + get_doc_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'asfm_to_doc',
							'nature of report' : 'ASFM to Docx conversion',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
				
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});
