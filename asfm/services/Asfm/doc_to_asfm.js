	return new Promise((resolve, reject) =>
	{
		try
		{
			const file_path = body.file_path;
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const tmp_dir = (global.server_config !== undefined) && global.server_config.tmp_dir;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('doc_to_asfm', Object.assign({ dbName : dbName, key : key }, body), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const doc_api = new PkmRestfulApi.DocApi();
					const File = require('../../../util/file.js');
					const FileSystem = require('../../../util/file_system');
					const doc_to_asfm = require('../../../util/doc_to_asfm').doc_to_asfm;
					
					// create a temporary file system
					const file_system = new FileSystem({ debug : debug, logger : job, ...((tmp_dir !== undefined) ? { tmp_dir : tmp_dir } : {}) });
					file_system.make_temporary_root_directory().then(() =>
					{
						// a temporary file system was created
						new Promise((resolve, reject) =>
						{
							// get the raw documentation file
							doc_api.getRawDoc(dbName, file_path, key).then((doc_file) =>
							{
								// write the content of the documentation file on the host file system
								file_system.writeFile(File.from(doc_file)).then(() =>
								{
									// the content of the documentation file was written on the host file system
									resolve();
								}).catch((write_file_err) =>
								{
									// an error occurred while writting the content of the documentation file on the host file system
									reject(write_file_err);
								});
							}).catch((get_raw_doc_err) =>
							{
								// an error occurred while getting the documentation file
								reject(get_raw_doc_err);
							});
						}).then(() => new Promise((resolve, reject) =>
						{
							// documentation file was written on the host file system
							let doc_to_asfm_options =
							{
								debug : debug,
								logger : job
							};
							
							// run doc_to_asfm
							doc_to_asfm(file_system, file_path, doc_to_asfm_options).then((doc_document) =>
							{
								doc_document.sourceFile = file_path;
								
								if(debug)
								{
									job.log('putting ASFM Documentation');
								}
								doc_api.putDocs(dbName, key, [ doc_document ]).then(() =>
								{
									if(invocation !== undefined) invocation.pushDocs(dbName, [ doc_document ]);
									
									resolve();
								}).catch((put_docs_err) =>
								{
									const msg = 'can\'t put ASFM Documentation' + (put_docs_err.status ? (': PKM server returned error status ' + put_docs_err.status) : '');
									job.error(msg);
									reject(new Error(msg));
								});
							}).catch((err) =>
							{
								// an error occurred while running doc_to_asfm
								reject(err);
							});
						})).catch((err) =>
						{
							// delay the promise rejection until temporary file system has been deleted
							error = err;
							status = false;
						}).finally(() =>
						{
							// delete previously created temporary file system
							(do_not_remove_temporary_root_directory ? Promise.resolve() : file_system.remove_temporary_root_directory()).catch((err) =>
							{
								// an error occurred while deleting the temporary file system
								const { deep_copy } = require('../../../util/deep_copy');
								job.error(JSON.stringify(deep_copy(err)));
								
								if(status)
								{
									error = err;
									status = false;
								}
							}).finally(() =>
							{
								resolve();
							});
						});
					}).catch((err) =>
					{
						// an error occurred while creating the temporary file system
						reject(err);
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'doc_to_asfm',
							'nature of report' : 'Docx to ASFM conversion',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});
