	return new Promise((resolve, reject) =>
	{
		try
		{
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const max_parallel_class_processings = ((global.server_config !== undefined) && (global.server_config.code_to_asfm !== undefined) && global.server_config.code_to_asfm.max_parallel_class_processings) || 1;
			const max_parallel_member_gets_per_class = ((global.server_config !== undefined) && (global.server_config.code_to_asfm !== undefined) && global.server_config.code_to_asfm.max_parallel_member_gets_per_class) || 1;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('code_to_asfm', Object.assign({ dbName : dbName, key : key }, {}), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const asyncPool = require('tiny-async-pool');
				const CppClass = require('../../../util/cpp_class');
				
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let id = 0;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					let doc_document =
					{
						name : dbName,
						units : []
					};
					
					let unit_doc =
					{
						name : (dbName + ' unit')
					}
					
					unit_doc.id = id++;
					const code_api = new PkmRestfulApi.CodeApi();
					
					code_api.getCPPSourceCodeArtefacts(dbName, key, { kind : '/^struct$|^union$|^class$/' }).then((cpp_class_documents) =>
					{
						let cpp_classes = cpp_class_documents
							.filter((cpp_class_document) =>
								cpp_class_document.hasOwnProperty('loc') &&
								cpp_class_document.loc.hasOwnProperty('file') &&
								!cpp_class_document.loc.file.startsWith('/'))
							.map(cpp_class_document => new CppClass(cpp_class_document))
							.filter((cpp_class) => cpp_class.completeDefinition === true);
						
						asyncPool(max_parallel_class_processings, cpp_classes, (cpp_class) => new Promise((resolve, reject) =>
						{
							let promises = [];
							
							let cpp_class_doc =
							{
								name : (cpp_class.pretty_decl !== undefined) ? cpp_class.pretty_decl : ('class #' + cpp_class.id + ' has no pretty_decl'),
								doc : '',
								parent : unit_doc.id
							};
							
							cpp_class_doc.id = ++id;
							
							promises.push(new Promise((resolve, reject) =>
							{
								code_api.getCPPCommentArtefacts(dbName, key, { id : cpp_class.id }).then((cpp_class_comments) =>
								{
									cpp_class_comments.map((comments) => comments.comments).forEach((comment) =>
									{
										if(typeof comment === 'string')
										{
											cpp_class_doc.doc += comment + '\n';
										}
										else if(Array.isArray(comment))
										{
											cpp_class_doc.doc += comment.join('\n');
										}
									});
									
									resolve();
								}).catch((err) =>
								{
									if(err.status === 404)
									{
										resolve();
									}
									else
									{
										if(!err.status) console.error(err);
										const msg = 'can\'t get C++ comments for class #' + cpp_class.id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
										job.error(msg);
										reject(new Error(msg));
									}
								});
							}));
							
							promises.push(new Promise((resolve, reject) =>
							{
								code_api.getCPPAnnotationArtefacts(dbName, key, { id : cpp_class.id }).then((cpp_class_annotations) =>
								{
									cpp_class_annotations.map((annotations) => annotations.annotations).forEach((annotation) =>
									{
										if(typeof annotation === 'string')
										{
											cpp_class_doc.doc += annotation + '\n';
										}
										else if(Array.isArray(annotation))
										{
											cpp_class_doc.doc += annotation.join('\n');
										}
									});
									
									resolve();
								}).catch((err) =>
								{
									if(err.status === 404)
									{
										resolve();
									}
									else
									{
										if(!err.status) console.error(err);
										const msg = 'can\'t get C++ annotations for class #' + cpp_class.id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
										job.error(msg);
										reject(new Error(msg));
									}
								});
							}));
							
							if(cpp_class.hasOwnProperty('fields'))
							{
								promises.push(new Promise((resolve, reject) =>
								{
									asyncPool(max_parallel_member_gets_per_class, cpp_class.fields, ({ id }) => new Promise((resolve, reject) =>
									{
										code_api.getCPPSourceCodeArtefacts(dbName, key, { id : id }).then((cpp_fields) =>
										{
											let cpp_field = cpp_fields[0];
											
											let cpp_class_field_doc =
											{
												name : (cpp_field.pretty_decl !== undefined) ? cpp_field.pretty_decl : ('field #' + cpp_field.id + ' has no pretty_decl'),
												doc : '',
												parent : cpp_class_doc.id
											};
											
											cpp_class_field_doc.id = id++;
											
											let promises = [];
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPCommentArtefacts(dbName, key, { id : id }).then((cpp_field_comments) =>
												{
													cpp_field_comments.map((comments) => comments.comments).forEach((comment) =>
													{
														if(typeof comment === 'string')
														{
															cpp_class_field_doc.doc += comment + '\n';
														}
														else if(Array.isArray(comment))
														{
															cpp_class_field_doc.doc += comment.join('\n');
														}
													});
													
													resolve(cpp_class_field_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ comments for field #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPAnnotationArtefacts(dbName, key, { id : id }).then((cpp_field_annotations) =>
												{
													cpp_field_annotations.map((annotations) => annotations.annotations).forEach((annotation) =>
													{
														if(typeof annotation === 'string')
														{
															cpp_class_field_doc.doc += annotation + '\n';
														}
														else if(Array.isArray(annotation))
														{
															cpp_class_field_doc.doc += annotation.join('\n');
														}
													});
													
													resolve(cpp_class_field_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ annotations for field #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											Promise.all(promises).then(() =>
											{
												if(cpp_class_field_doc.doc.length === 0) delete cpp_class_field_doc.doc;
												resolve(cpp_class_field_doc);
											}).catch((err) =>
											{
												reject(err);
											});
										}).catch((get_cpp_class_field_err) =>
										{
											const msg = 'can\'t get C++ class field #' + id + '\'' + (get_cpp_class_field_err.status ? (': PKM server returned error status ' + get_cpp_class_field_err.status) : '');
											job.error(msg);
											reject(new Error(msg));
										});
									})).then((cpp_class_field_docs) =>
									{
										cpp_class_doc.fields = cpp_class_field_docs;
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								}));
							}
							
							if(cpp_class.hasOwnProperty('methods'))
							{
								promises.push(new Promise((resolve, reject) =>
								{
									asyncPool(max_parallel_member_gets_per_class, cpp_class.methods, ({ id }) => new Promise((resolve, reject) =>
									{
										code_api.getCPPSourceCodeArtefacts(dbName, key, { id : id }).then((cpp_methods) =>
										{
											let cpp_method = cpp_methods[0];
											
											let cpp_class_method_doc =
											{
												name : (cpp_method.pretty_decl !== undefined) ? cpp_method.pretty_decl : ('method #' + cpp_method.id + ' has no pretty_decl'),
												doc : '',
												parent : cpp_class_doc.id
											};
											
											cpp_class_method_doc.id = id++;
											
											let promises = [];
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPCommentArtefacts(dbName, key, { id : id }).then((cpp_method_comments) =>
												{
													cpp_method_comments.map((comments) => comments.comments).forEach((comment) =>
													{
														if(typeof comment === 'string')
														{
															cpp_class_method_doc.doc += comment + '\n';
														}
														else if(Array.isArray(comment))
														{
															cpp_class_method_doc.doc += comment.join('\n');
														}
													});
													
													resolve(cpp_class_method_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ comments for method #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											promises.push(new Promise((resolve, reject) =>
											{
												code_api.getCPPAnnotationArtefacts(dbName, key, { id : id }).then((cpp_method_annotations) =>
												{
													cpp_method_annotations.map((annotations) => annotations.annotations).forEach((annotation) =>
													{
														if(typeof annotation === 'string')
														{
															cpp_class_method_doc.doc += annotation + '\n';
														}
														else if(Array.isArray(annotation))
														{
															cpp_class_method_doc.doc += annotation.join('\n');
														}
													});
													
													resolve(cpp_class_method_doc);
												}).catch((err) =>
												{
													if(err.status === 404)
													{
														resolve();
													}
													else
													{
														if(!err.status) console.error(err);
														const msg = 'can\'t get C++ annotations for method #' + id + '\'' + (err.status ? (': PKM server returned error status ' + err.status) : '');
														job.error(msg);
														reject(new Error(msg));
													}
												});
											}));
											
											Promise.all(promises).then(() =>
											{
												if(cpp_class_method_doc.doc.length === 0) delete cpp_class_method_doc.doc;
												resolve(cpp_class_method_doc);
											}).catch((err) =>
											{
												reject(err);
											});
										}).catch((get_cpp_class_method_err) =>
										{
											const msg = 'can\'t get C++ class method #' + id + '\'' + (get_cpp_class_method_err.status ? (': PKM server returned error status ' + get_cpp_class_method_err.status) : '');
											job.error(msg);
											reject(new Error(msg));
										});
									})).then((cpp_class_method_docs) =>
									{
										cpp_class_doc.methods = cpp_class_method_docs;
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								}));
							}
							
							Promise.all(promises).then(() =>
							{
								if(cpp_class_doc.doc.length === 0) delete cpp_class_doc.doc;
								resolve(cpp_class_doc);
							}).catch((err) =>
							{
								reject(err);
							});
						})).then((cpp_class_docs) =>
						{
							unit_doc.classes = cpp_class_docs;
							doc_document.units.push(unit_doc);
							
							resolve(doc_document);
						}).catch((err) =>
						{
							reject(err);
						});
					}).catch((get_cpp_source_code_artefacts_err) =>
					{
						const msg = 'can\'t get C++ structs/unions/classes' + (get_cpp_source_code_artefacts_err.status ? (': PKM server returned error status ' + get_cpp_source_code_artefacts_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then((doc_document) =>
				{
					const doc_api = new PkmRestfulApi.DocApi();
					
					if(debug)
					{
						job.log('putting ASFM Documentation');
					}
					
					doc_api.putDocs(dbName, key, [ doc_document ]).then(() =>
					{
						if(invocation !== undefined) invocation.pushDocs(dbName, [ doc_document ]);
						
						resolve();
					}).catch((put_docs_err) =>
					{
						const msg = 'can\'t put ASFM Documentation' + (put_docs_err.status ? (': PKM server returned error status ' + put_docs_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				}).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'code_to_asfm',
							'nature of report' : 'Code to ASFM conversion',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});
