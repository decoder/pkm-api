/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Create all collections
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * 
 * @return {Promise} a promise
 */

function create_all_collections(dbName)
{
	return new Promise(function(resolve, reject)
	{
		var create_collection_promises = [];
		this.get_collection_names().forEach((collection_name) =>
		{
			const collection_info = this.get_collection_info(collection_name);
			create_collection_promises.push(this.create_collection(dbName, collection_name, collection_info));
		});
		
		Promise.all(create_collection_promises).then(() =>
		{
			resolve();
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.create_all_collections = create_all_collections;
