/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Postprocess C++ Source Codes
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} cpp_source_code_documents - C source code documents
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<Object>>} a promise
 */
function postprocess_cpp_source_codes(cpp_source_code_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const merge = (options !== undefined) && options.merge;
		
		if(merge)
		{
			let code_map = new Map();
			
			cpp_source_code_documents.forEach((cpp_source_code_document) =>
			{
				if(code_map.has(cpp_source_code_document.sourceFile))
				{
					var tmp = code_map.get(cpp_source_code_document.sourceFile);
					if(cpp_source_code_document.hasOwnProperty('inner')) tmp.inner = tmp.inner.concat(cpp_source_code_document.inner);
					tmp.manifest = tmp.manifest.concat(cpp_source_code_document.manifest);
					code_map.set(tmp.sourceFile, tmp);
					delete cpp_source_code_document.sourceFile;
					delete cpp_source_code_document.type;
				}
				else
				{
					var tmp =
					{
						type : cpp_source_code_document.type,
						sourceFile : cpp_source_code_document.sourceFile,
						manifest : cpp_source_code_document.manifest
					};
					if(cpp_source_code_document.hasOwnProperty('inner')) tmp.inner = cpp_source_code_document.inner;
					code_map.set(tmp.sourceFile, tmp);
					delete cpp_source_code_document.sourceFile;
					delete cpp_source_code_document.type;
				}
			});
			
			const code_map_iterator = code_map.values();
			let merged_cpp_source_code_documents = Array.from(code_map_iterator, (cpp_source_code_document) => cpp_source_code_document);
			merged_cpp_source_code_documents.forEach((cpp_source_code_document) =>
			{
				cpp_source_code_document.manifest = cpp_source_code_document.manifest.sort().filter((element, index, array) => !index || (element != array[index - 1]));
			});
			
			resolve(merged_cpp_source_code_documents);
		}
		else
		{
			resolve(cpp_source_code_documents);
		}
	}.bind(this));
}

module.exports.postprocess_cpp_source_codes = postprocess_cpp_source_codes;
