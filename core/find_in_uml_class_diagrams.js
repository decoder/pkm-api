/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * find in UML class diagrams
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} className - class name
 * @param {string} kind - it is either 'operations', 'attributes', or null
 * @param {string} varName - when kind is null, it is either an operation or attribute name
 * @param {string} varType - when kind is null, it is either 'parameters', 'returnType' or 'attributeType'
 * @return {Promise<Array.<Object>>} a promise that passes the result (an array) to resolve callback
 */
function find_in_uml_class_diagrams(dbName, className, kind, varName, varType)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		if(debug) console.warn("Searching UMLClasses..." );
		
		if(kind && (kind != 'operations') && (kind != 'attributes'))
		{
			reject(this.BadRequest('Bad kind'));
			return;
		}
		
		this.get_documents(dbName, 'UMLClasses', { 'modules.classes.name': className } ).then((documents) =>
		{
			let find_result = [];
			
			documents.forEach((doc) =>
			{
				if(Array.isArray(doc.modules))
				{
					doc.modules.forEach((module) =>
					{
						if(Array.isArray(module.classes))
						{
							module.classes.forEach((cls) =>
							{
								if(cls.name == className)
								{
									if(kind == 'operations')
									{
										if(Array.isArray(cls.operations))
										{
											cls.operations.forEach((operation) =>
											{
												find_result.push(operation);
											});
										}
									}
									else if((kind == 'attributes') && !varType)
									{
										if(Array.isArray(cls.attributes))
										{
											cls.attributes.forEach((attribute) =>
											{
												find_result.push(attribute);
											});
										}
									}
									else if((kind == 'attributes') && varType)
									{
										if(Array.isArray(cls.attributes))
										{
											cls.attributes.forEach((attribute) =>
											{
												if(attribute.type && (attribute.type == varType))
												{
													find_result.push(attribute);
												}
											});
										}
									}
									else if(!kind && (varType == 'parameters'))
									{
										if(debug) console.warn("looking for parameters for ",varName);
										if(Array.isArray(cls.operations))
										{
											cls.operations.forEach((operation) =>
											{
												if(operation.name && (operation.name == varName))
												{
													if(Array.isArray(operation.parameters))
													{
														operation.parameters.forEach((parameter) =>
														{
															find_result.push(parameter);
														});
													}
												}
											});
										}
									}
									else if(!kind && (varType == 'returnType'))
									{
										if(Array.isArray(cls.operations))
										{
											cls.operations.forEach((operation) =>
											{
												if(operation.name && (operation.name == varName) && operation.returnType)
												{
													find_result.push(operation.returnType);
												}
											});
										}
									}
									else if(!kind && (varType == 'attributeType'))
									{
										if(Array.isArray(cls.attributes))
										{
											cls.attributes.forEach((attribute) =>
											{
												if(attribute.name && (attribute.name == varName) && attribute.type)
												{
													find_result.push(attribute.type);
												}
											});
										}
										
									}
									else
									{
										find_result.push(cls);
									}
								}
							});
						}
					});
				}
			});
			
			resolve(find_result);
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.find_in_uml_class_diagrams = find_in_uml_class_diagrams;
