/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Getting Git files query
 * 
 * @typedef {Object} GetGitFilesQuery
 * @property {string} [filename] - filename (path relative to the Git working tree)
 */

/**
 * Delete files from a Git working tree
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} git_working_tree_directory - Directory of Git working tree
 * @param {GetGitFilesQuery} [query] - query
 * 
 * @return {Promise<Array.<File>>} a promise
 */
function delete_git_files(dbName, git_working_tree_directory, query = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const encoding = options.encoding || 'utf8';
		let git_file_system = this.get_git_file_system(dbName);
		
		const asyncPool = require('tiny-async-pool');
		const path = require('path');
		
		// Get all the Git working trees
		this.get_git_working_trees(dbName, {}).then((git_working_trees) =>
		{
			// Find the Git working tree by the Git working tree directory
			let git_working_tree = git_working_trees.find((git_working_tree) => git_working_tree.directory == git_working_tree_directory);
			
			// If found
			if(git_working_tree !== undefined)
			{
				new Promise((resolve, reject) =>
				{
					// (1) Determine the list of matching filenames
					
					// an empty query means all the files
					let all = (Object.keys(query).length == 0);
					
					// If query is empty
					if(all)
					{
						// then find all filenames in the Git working tree
						
						const find_options =
						{
							type : 'file'
						};
						git_file_system.find(git_working_tree.directory, find_options).then((matching_filepaths) =>
						{
							let filenames = [];
							
							matching_filepaths.forEach((matching_filepath) =>
							{
								const filename = git_file_system.make_rel_path(matching_filepath);
								
								// Check that the file is neither a Git link of the Git working tree nor in any Git directory
								if((filename == path.join(git_working_tree.directory, '.git')) ||
								   (!Array.isArray(git_working_tree.linked) ||
									  (git_working_tree.linked.find((linked_working_tree) => on_disk_filename == path.join(linked_working_tree.directory, '.git')) === undefined)) &&
								   (git_working_trees.find((git_working_tree) => filename.startsWith(git_working_tree.git_directory)) === undefined))
								{
									filenames.push(filename);
								}
							});
							
							// serve the found filenames
							resolve(filenames);
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}
					else if(typeof query.filename === 'string')
					{
						// Make the filename absolute
						let git_working_tree_file_system = this.get_git_working_tree_file_system(dbName, git_working_tree.directory);
						const abs_file_path = git_working_tree_file_system.make_abs_path(query.filename);
						if(git_working_tree_file_system.check_abs_file_path(abs_file_path))
						{
							const filename = git_file_system.make_rel_path(abs_file_path);
						
							// Check that the file is not in any Git directory
							if(git_working_trees.find((git_working_tree) => filename.startsWith(git_working_tree.git_directory)) === undefined)
							{
								// Check that the file exists
								git_file_system.stat(filename).then((stats) =>
								{
									if(stats.isFile())
									{
										// serve the filename
										resolve([ filename ]);
									}
									else
									{
										reject(this.NotFound());
									}
								}).catch((err) =>
								{
									reject(this.NotFound(err));
								});
							}
							else
							{
								reject(this.NotFound());
							}
						}
						else
						{
							reject(this.NotFound());
						}
					}
					else
					{
						reject(this.InternalServerError());
					}
				}).then((matching_filenames) =>
				{
					// (2) delete the on-disk matching files
					
					// For each files
					asyncPool(1, matching_filenames, (matching_filename) => new Promise((resolve, reject) =>
					{
						// delete the file
						git_file_system.unlink(matching_filename).then(() =>
						{
							resolve();
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					})).then(() =>
					{
						// job's done
						resolve();
					}).catch((err) =>
					{
						reject(this.Error(err));
					});
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				reject(this.NotFound());
			}
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.delete_git_files = delete_git_files;
