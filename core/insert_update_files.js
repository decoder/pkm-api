/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update files in a generic manner in a collection of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} collection_name - collection name
 * @param {Array.<File>} files - files
 * @param {boolean} update - flag to enable/disable replacing files
 * @param {Object} [options] - options
 * 
 * @return {Promise} a promise
 */
function insert_update_files(dbName, collection_name, files, update, options = {})
{
	return new Promise(function(resolve, reject)
	{
		let invalidate_filenames = [];
		let abbrev_existing_files_per_filenames = new Map();
		let compare_files_promises = files.map((file) => new Promise((resolve, reject) =>
		{
			this.get_files(dbName, collection_name, { filename : file.rel_path }).then((existing_files) =>
			{
				const existing_file = existing_files[0];
				if(existing_file.compare_content(file) != 0)
				{
					invalidate_filenames.push(file.rel_path);
				}
				if(existing_file.content !== undefined)
				{
					delete existing_file.content;
				}
				abbrev_existing_files_per_filenames.set(existing_file.rel_path, existing_file);
				resolve();
			}).catch((err) =>
			{
				const error = require('./error');
				if(err instanceof error.PKM_NotFound)
				{
					resolve();
				}
				else
				{
					reject(this.Error(err));
				}
			});
		}));
		
		Promise.all(compare_files_promises).then(() =>
		{
			let documents = files.map((file) =>
			{
				const abbrev_existing_file = abbrev_existing_files_per_filenames.get(file.rel_path);
				
				// If file already exists and insert/update is not from PKM itself for Git management
				if((abbrev_existing_file !== undefined) && !options.git)
				{
					// git_working_tree, git_unmerged and git_dirty properties are read-only for anyone but Git
					// when insert/update is not from Git preserve git_working_tree and git_unmerged flags,
					// set git_dirty if file content has changed
					if(abbrev_existing_file.git_working_tree !== undefined) file.git_working_tree = abbrev_existing_file.git_working_tree;
					if(abbrev_existing_file.git_unmerged !== undefined) file.git_unmerged = abbrev_existing_file.git_unmerged;
					if(invalidate_filenames.indexOf(file.rel_path) > -1) file.git_dirty = true;
				}
				
				return this.fix_file(file).database_export();
			});
		
			this.insert_update_documents_spanned(dbName, this.get_file_collection_names(), documents, update, (document) => collection_name, { filename : 1 }).then(() =>
			{
				this.invalidate_dependent_documents(dbName, invalidate_filenames).then(() =>
				{
					resolve();
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.insert_update_files = insert_update_files;
