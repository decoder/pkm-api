/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Preprocess C++ Comments
 * 
 * @memberof PKM
 * @instance
 * @param {Array.<Object>} cpp_comments_documents - C++ Comments documents
 * @param {Object} [options] - options
 * 
 * @return {Promise.<Array.<Object>>} a promise which result is the preprocessed C++ Comments documents
 */
function preprocess_cpp_comments(cpp_comments_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const dont_split = (options !== undefined) && options.dont_split;
		
		if(dont_split)
		{
			resolve(cpp_comments_documents);
		}
		else
		{
			let splitted_cpp_comments_documents = [];
			
			cpp_comments_documents.forEach((cpp_comments_document) =>
			{
				cpp_comments_document.comments.forEach((comment) =>
				{
					splitted_cpp_comments_documents.push({ type : cpp_comments_document.type, sourceFile : cpp_comments_document.sourceFile, comments : [ comment ] });
				});
			});
			
			resolve(splitted_cpp_comments_documents);
		}
	}.bind(this));
}

module.exports.preprocess_cpp_comments = preprocess_cpp_comments;
