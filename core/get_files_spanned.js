/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get files in a generic manner from several collections of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<string>} collection_names - collection names
 * @param {Object} [query] - query
 * @param {Object} [options] - options
 * 
 * @return {Promise} a promise
 */
function get_files_spanned(dbName, collection_names, query = {}, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const File = require('../util/file.js');
		
		this.get_documents_spanned(dbName, collection_names, query, options).then((documents) =>
		{
			const files = documents.map((document) => File.from_database(document));
			
			resolve(files);
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

module.exports.get_files_spanned = get_files_spanned;

