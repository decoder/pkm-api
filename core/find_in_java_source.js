/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * find in Java source
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} kind - should be 'class'
 * 
 * @return {Promise<Array.<Object>>} a promise that passes the result (an array) to resolve callback
 */
function find_in_java_source(dbName,kind)
{
	return new Promise((resolve, reject) =>
	{
		const debug = this.debug;
		
		if(kind != 'class')
		{
			reject(this.BadRequest('Bad kind'));
			return;
		}
		
		this.get_documents(dbName, 'sourcecodeJava').then((source_code_units) =>
		{
			let result = [];
			
			source_code_units.forEach((source_code_unit) =>
			{
				if (source_code_unit.CompilationUnit &&
						source_code_unit.CompilationUnit.TypeDeclaration &&
						source_code_unit.CompilationUnit.TypeDeclaration.SimpleName) 
						result.push(source_code_unit.CompilationUnit.TypeDeclaration.SimpleName.content);
			});
			
			resolve(result);
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	});
}

exports.find_in_java_source = find_in_java_source;
