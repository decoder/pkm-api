/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert files into a Git working tree
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} git_working_tree_directory - Directory of Git working tree
 * @param {Array.<File>} files - files
 * 
 * @return {Promise<Array.<File>>} a promise
 */
function insert_git_files(dbName, git_working_tree_directory, files)
{
	return this.insert_update_git_files(dbName, git_working_tree_directory, files, false);
}

exports.insert_git_files = insert_git_files;
