/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * update user role
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} user_role_name - user role name
 * @param {string} command - either 'grant' or 'revoke'
 * @param {string} user_name - user name
 * 
 * @return {Promise} a promise
 */

function update_user_role(dbName, user_role_name, command, user_name)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		const db = this.client.db(this.config.pkm_db);

		let db_operation = function(command)
		{
			if(command == 'grant') return db.command({ grantRolesToUser : user_name, roles : [ { role : user_role_name, db : dbName } ] });
			if(command == 'revoke') return db.command({ revokeRolesFromUser : user_name, roles : [ { role : user_role_name, db : dbName } ] });
			return Promise.reject(this.InternalServerError('no such command: \'' + command + '\''));
		}.bind(this);
		
		db_operation(command).then(() =>
		{
			resolve();
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.update_user_role = update_user_role;
