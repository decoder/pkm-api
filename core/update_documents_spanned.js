/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Update some documents in a generic manner into several collections of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<string>} collection_names - collection names
 * @param {Array.<Object>} documents - documents
 * @param {Function} dispatch - a function (document) => collection_name to dispatch documents to collections that shall throw when dispatching is not possible
 * @param {Object} [signature] - signature (properties in the signature are the document keys). When unspecified each document is unique whereas when an empty object {}, the collection holds only a singleton document.
 * 
 * @return {Promise} a promise
 */
function update_documents_spanned(dbName, collection_names, documents, dispatch, signature)
{
	return this.insert_update_documents_spanned(dbName, collection_names, documents, true, dispatch, signature);
}

module.exports.update_documents_spanned = update_documents_spanned;
