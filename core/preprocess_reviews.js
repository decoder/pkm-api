/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Preprocess review documents
 * 
 * @memberof PKM
 * @instance
 * @param {Array.<Object>} review_documents - review documents
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<Object>>} a promise
 */
function preprocess_reviews(review_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		try
		{
			review_documents.forEach((review_document) =>
			{
				[ 'reviewOpenDate', 'reviewCompletedDate' ].forEach((key) =>
				{
					if(review_document.hasOwnProperty(key) && !(review_document[key] instanceof Date))
					{
						let date = new Date(review_document[key]);
						if((date instanceof Date) && isNaN(date.getTime()))
						{
							throw this.BadRequest('Invalidate date format for \'' + key + '\'');
						}
						review_document[key] = date;
					}
				});
			});
		}
		catch(err)
		{
			reject(this.Error(err));
			return;
		}
		
		resolve(review_documents);
	}.bind(this));
}

module.exports.preprocess_reviews = preprocess_reviews;
