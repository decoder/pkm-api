/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A project member. Undocumented additional properties are allowed.
 * 
 * @typedef {Object} PkmProjectMember
 * @property {string} name - member name
 * @property {boolean} owner - true if member is owner of the project, otherwise false
 * @property {Array.<string>} member role names
 */

/**
 * A project. Undocumented additional properties are allowed.
 * 
 * @typedef {Object} PkmProject
 * @property {string} name - project name
 * @property {Array.<PkmProjectMember>} project members
 */

/**
 * Create a project
 * 
 * @memberof PKM
 * @instance
 * @param {PkmProject} project - project
 * @param {boolean} update - flag to enable/disable updating an existing project
 * 
 * @return {Promise} a promise
 */

function create_update_project(project, update)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		// check that project has a name
		if(project.name === undefined)
		{
			reject(this.BadRequest('project has no name'));
			return;
		}
		
		// if there are members
		if(project.members !== undefined)
		{
			// check that it is an array
			if(Array.isArray(project.members))
			{
				const members = project.members;
				
				// for each member
				for(let member_idx = 0; member_idx < members.length; ++member_idx)
				{
					const member = members[member_idx];
					
					// check that member has a name and it is a string
					if((member.name === undefined) || (typeof member.name !== 'string'))
					{
						reject(this.BadRequest('some members have no name or it is not a string'));
						return;
					}
					
					// check that member has at least one role or he has ownership flag set
					if(Array.isArray(member.roles) && member.roles.length)
					{
						const roles = member.roles;
						// for each role
						for(let role_idx = 0; role_idx < roles.length; ++role_idx)
						{
							const role = roles[role_idx];
							
							// check that role is valid
							if(!this.get_role_names().includes(role))
							{
								reject(this.BadRequest('invalid member role'));
								return;
							}
						}
					}
					else if(member.owner !== true)
					{
						reject(this.BadRequest('each member shall have at least one role or ownership flag'));
						return;
					}
				}
			}
			else
			{
				reject(this.BadRequest('project.members is not an array'));
				return;
			}
		}
		else if(!update)
		{
			// if not create a empty member array if it's a project creation
			project.members = [];
		}
		
		if(!update)
		{
			// if it's a project creation, make sure there's at least one member that is owner
			if((project.members.find(member => member.owner === true) === undefined) &&
				 (project.members.find(member => (member.roles !== undefined) && (member.roles.find(member_role => member_role === 'Owner') !== undefined)) === undefined))
			{
				// there's no owner specified
				if(project.members.find(member => member.name == this.user_name) !== undefined)
				{
					// logged user is a member
					project.members.forEach((member, member_index, members) =>
					{
						if(member.name == this.user_name)
						{
							// set ownership to logged user
							members[member_index].owner = true;
						}
					});
				}
				else
				{
					// logged user owns the project but it was not explicitely specified as an owner member
					project.members.push({ name : this.user_name, owner : true });
				}
			}
		}
		
		if(project.members !== undefined)
		{
			// check for duplicate members
			let lookup = {};
			if(project.members.filter(member => !lookup[member.name] && (lookup[member.name] = true)).length != project.members.length)
			{
				reject(this.BadRequest('duplicate members with same name while ' + (update ? 'updating' : 'creating') +' Project \'' + project.name + '\''));
				return;
			}
		}
		
		let project_is_being_created = false;
		
		((project.members === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
		{
			// check that all members are existing users
			let check_member_presence_promises = [];
			project.members.forEach((member) =>
			{
				check_member_presence_promises.push(this.get_user(member.name));
			});
			
			Promise.all(check_member_presence_promises).then(() =>
			{
				resolve();
			}).catch((err) =>
			{
				const error = require('./error');
				if(err instanceof error.PKM_NotFound)
				{
					reject(this.BadRequest('some members are not PKM users'));
				}
				else
				{
					reject(this.Error(err));
				}
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// retrieve existing project informations
			this.get_project(project.name).then((existing_project) =>
			{
				if(update)
				{
					resolve(existing_project);
				}
				else
				{
					// project already exists
					reject(this.Conflict('Project named \'' + project.name + '\' already exists'));
				}
			}).catch((err) =>
			{
				const error = require('./error');
				if(err instanceof error.PKM_NotFound)
				{
					resolve();
				}
				else
				{
					reject(this.Error(err));
				}
			});
		})).then((existing_project) => new Promise((resolve, reject) =>
		{
			if(existing_project === undefined)
			{
				this.create_db(project.name).then(() =>
				{
					project_is_being_created = true;
					resolve();
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				resolve(existing_project);
			}
		})).then((existing_project) => new Promise((resolve, reject) =>
		{
			if(project.members !== undefined)
			{
				let grant_user_promises = [];
				
				// for each member gives corresponding role to the corresponding MongoDB user's
				project.members.forEach((member) =>
				{
					grant_user_promises.push(new Promise((resolve, reject) =>
					{
						this.get_user(member.name).then((user) =>
						{
							if(member.owner)
							{
								user.grant(project.name, 'Owner');
							}
							
							if(Array.isArray(member.roles))
							{
								member.roles.forEach((member_role) =>
								{
									user.grant(project.name, member_role);
								});
							}
							
							this.update_user(user).then(() =>
							{
								resolve();
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}));
				});
				
				Promise.all(grant_user_promises).then(() =>
				{
					if((existing_project !== undefined) && Array.isArray(existing_project.members))
					{
						let revoke_user_promises = [];
						
						// revoke roles of members which are either no longer in the project or which role has disappeared
						existing_project.members.forEach((existing_member) =>
						{
							this.get_user(existing_member.name).then((user) =>
							{
								const member = project.members.find((member) => member.name === existing_member.name);
								if(member !== undefined)
								{
									existing_member.roles.forEach((existing_role) =>
									{
										if(!member.roles.includes(existing_role) || ((existing_role === 'Owner') && !member.owner))
										{
											user.revoke(project.name, existing_role);
										}
									});
								}
								else
								{
									user.revoke(project.name);
								}
								
								revoke_user_promises.push(this.update_user(user));
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						});
						
						Promise.all(revoke_user_promises).then(() =>
						{
							resolve();
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}
					else
					{
						resolve();
					}
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				resolve();
			}
		})).then(() =>
		{
			// prepare project informations for insertion or updating in the database
			if(Array.isArray(project.members))
			{
				for(let member_idx = 0; member_idx < project.members.length; ++member_idx)
				{
					const member = project.members[member_idx];
					
					if(project.members[member_idx].roles !== undefined)
					{
						// roles are not stored but instead are built on demand when getting project informations
						delete project.members[member_idx].roles;
					}
					
					if(project.members[member_idx].owner !== undefined)
					{
						// ownership is not stored but instead are built on demand when getting project informations
						delete project.members[member_idx].owner;
					}
				};
			}
			
			if(project_is_being_created)
			{
				let promises = [];
				
				// insert some project metadata into collections
				let project_metadata = Object.keys(this.config.project);
				project_metadata.forEach((metadata) =>
				{
					promises.push(new Promise((resolve, reject) =>
					{
						if(!project.hasOwnProperty(metadata))
						{
							const fs = require('fs');
							
							const file = this.config.project[metadata].file;
							
							if(typeof file === 'string')
							{
								try
								{
									const content = fs.readFileSync(this.get_shared_data_path(file), { encoding : 'utf8' });
									project[metadata] = JSON.parse(content);
								}
								catch(err)
								{
									const msg = 'Error with File \'' + file + '\', ' + err.message;
									console.warn(msg);
									reject(new Error(msg));
								}
							}
							else
							{
								const files = this.config.project[metadata].files;
								
								if(Array.isArray(files))
								{
									files.forEach((file) =>
									{
										try
										{
											const content = fs.readFileSync(this.get_shared_data_path(file), { encoding : 'utf8' });
											if(!project.hasOwnProperty(metadata)) project[metadata] = [];
											project[metadata].push(JSON.parse(content));
										}
										catch(err)
										{
											const msg = 'Error with File \'' + file + '\', ' + err.message;
											console.warn(msg);
											reject(new Error(msg));
										}
									});
								}
							}
						}
						
						if(project.hasOwnProperty(metadata))
						{
							const insert = this.config.project[metadata].insert;
							if((typeof insert === 'string') && (typeof this[insert] === 'function'))
							{
								this[insert](project.name, project[metadata]).then(() =>
								{
									delete project[metadata];
									resolve();
								}).catch((err) =>
								{
									reject(this.Error(err));
								});
							}
						}
						else
						{
							resolve();
						}
					}));
				});
				
				Promise.all(promises).then(() =>
				{
					// insert project informations
					this.insert_documents(project.name, 'Project', [ project ], { signature : {} /* singleton document */ }).then(() =>
					{
						// register project name
						this.insert_documents(this.config.pkm_db, 'Projects', [ { name : project.name } ], { signature : { name : 1 } }).then(() =>
						{
							let git_file_system = this.get_git_file_system(project.name, { debug : debug });
							
							git_file_system.make_root_directory().then(() =>
							{
								// project has been created
								if(debug)
								{
									console.log('Project \'' + project.name + '\' has been created');
								}
								
								resolve(true);
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}).catch((err) =>
					{
						reject(this.Error(err));
					});
				}).catch((err) =>
				{
					this.delete_project(project.name).catch((err) =>
					{
						console.warn(err);
					}).finally(() =>
					{
						reject(this.Error(err));
					});
				});
			}
			else
			{
				let promises = [];
				
				// update some project metadata into collections
				let project_metadata = Object.keys(this.config.project);
				project_metadata.forEach((metadata) =>
				{
					if(project.hasOwnProperty(metadata))
					{
						const update = this.config.project[metadata].update;
						if((typeof update === 'string') && (typeof this[update] === 'function'))
						{
							promises.push(this[update](project.name, project[metadata]));
							delete project[metadata];
						}
					}
				});
				
				// update project informations
				promises.push(this.update_documents(project.name, 'Project', [ project ], { signature : {} /* singleton document */ }));
				
				Promise.all(promises).then(() =>
				{
					// project has been updated
					if(debug)
					{
						console.log('Project \'' + project.name + '\' has been updated');
					}
					
					resolve(false);
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
		}).catch((err) =>
		{
			if(project_is_being_created)
			{
				this.drop_db(project.name).catch((err) =>
				{
					console.warn(err);
				}).finally(() =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				reject(this.Error(err));
			}
		});
	}.bind(this));
}

exports.create_update_project = create_update_project;
