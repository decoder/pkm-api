/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Drop all users
 * 
 * @memberof PKM
 * @instance
 * @return {Promise} a promise
 */

function drop_all_users()
{
	return new Promise(function(resolve, reject)
	{
		const db = this.client.db(this.config.pkm_db);

		db.command({ dropAllUsersFromDatabase : 1 }).then(() =>
		{
			resolve();
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.drop_all_users = drop_all_users;
