/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Resource
 * 
 * @typedef {Object} Resource
 * @property {string} collection - collection name
 */

/** Role privilege
 * 
 * @typedef {Object} RolePrivilege
 * @property {Resource} resource - resource
 * @property {Array.<string>} actions - allowed actions on the resource
 */

/** Role
 * 
 * @typedef {Object} Role
 * @property {string} role - role name
 * @property {Array.<RolePrivilege>} privileges - role privileges
 * @property {Array.<UserRole>} roles - inherited roles

/**
 * Create role
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Role} role - role
 * 
 * @return {Promise} a promise
 */

function create_role(dbName, role)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const db = this.client.db(dbName);

		const cmd =
		{
			createRole : role.role,
			privileges : role.privileges,
			roles : role.roles
		};
		
		if(debug)
		{
			console.log('Running command ', JSON.stringify(cmd, null, 2));
		}
		db.command(cmd).then(() =>
		{
			resolve();
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.create_role = create_role;
