/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update UML files
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<File>} uml_files - UML files
 * @param {boolean} update - flag to enable/disable replacing UML files
 * 
 * @return {Promise} a promise
 */
function insert_update_uml_files(dbName, uml_files, update)
{
	return this.insert_update_files(dbName, 'RawUML', uml_files, update);
}

exports.insert_update_uml_files = insert_update_uml_files;
