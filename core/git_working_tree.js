/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const GitConfig = require('../util/git_config');

/** Git Working Tree
 */
class GitWorkingTree
{
	/** constructor
	 * 
	 * @param {string} directory - Git working tree directory
	 * @param {string} git_directory - Git directory
	 * @param {string} git_branch - Tracked Git branch
	 * @param {string} git_commit_id - SHA1 ID of commit
	 * @param {GitConfig} git_config - Git config
	 * @param {string} [main] - directory of main Git working tree
	 * @param {Array.<GitWorkingTree>} [linked] - linked Git working trees
	 */
	constructor(directory, git_directory, git_branch, git_commit_id, git_config, main = undefined, linked = undefined)
	{
		this.directory = directory;
		this.git_directory = git_directory;
		this.git_branch = git_branch;
		this.git_commit_id = git_commit_id;
		this.git_config = git_config || {};
		if(main !== undefined) this.main = main;
		if(linked !== undefined) this.linked = linked;
	}
	
	/** Build a GitWorkingTree instance from an object, convenient for transportation with a transport protocol, that looks like a GitWorkingTree instance
	 * 
	 * @param {Object} obj - an object
	 * @return {GitWorkingTree} an instance of GitWorkingTree
	 */
	static from(obj)
	{
		return new GitWorkingTree(
			obj.directory,
			obj.git_directory,
			obj.git_branch,
			obj.git_commit_id,
			GitConfig.from(obj.git_config),
			obj.main,
			obj.linked
		);
	}

}

module.exports = GitWorkingTree;
