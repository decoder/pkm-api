/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get all PKM users or the PKM users that have a role in a database
 * 
 * @memberof PKM
 * @instance
 * 
 * @param {string} [dbName] - a database name
 * 
 * @return {Promise<Array.<User>>} a promise
 */

function get_users(dbName)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const db = this.client.db(this.config.pkm_db);

		const cmd = { usersInfo: 1 };
		db.command(cmd).then((result) =>
		{
			if(Array.isArray(result.users))
			{
				let users = [];
				
				result.users.forEach((u) =>
				{
					const User = require('./user');
					const GitUserCredential = require('./git_user_credential');
				
					if((dbName === undefined) || (u.roles.find(ur => ur.db == dbName) !== undefined))
					{
						const user = new User(
							u.user,                              // user's name
							undefined,                           // user's password (hidden)
							u.customData.first_name,             // first name
							u.customData.last_name,              // last name
							u.customData.email,                  // email
							u.customData.phone,                  // phone
							u.roles,                             // roles
							((this.user_name == u.user) && (u.customData.git_user_credentials !== undefined)) ? u.customData.git_user_credentials.map((obj) => GitUserCredential.decrypt(obj, this.crypt)) : undefined    // Git user's credentials
						);
						
						users.push(this.revoke_pkm_management_roles(user));
					}
				});
				
				if(debug)
				{
					const util = require('util');
					console.log(dbName + '\'s PKM users :', util.inspect(users, { showHidden: false, depth: null }));
				}
				resolve(users);
			}
			else
			{
				reject(this.InternalServerError());
			}
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_users = get_users;
