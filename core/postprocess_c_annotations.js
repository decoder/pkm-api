/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Postprocess C Annotations
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} c_annotations_documents - C annotation documents
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<Object>>} a promise
 */
function postprocess_c_annotations(c_annotations_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const merge = (options !== undefined) && options.merge;
		
		if(merge)
		{
			let annotations_map = new Map();
			
			c_annotations_documents.forEach((c_annotations_document) =>
			{
				if(annotations_map.has(c_annotations_document.sourceFile))
				{
					var tmp = annotations_map.get(c_annotations_document.sourceFile);
					tmp.annotations = tmp.annotations.concat(c_annotations_document.annotations);
					annotations_map.set(c_annotations_document.sourceFile, tmp);
				}
				else
				{
					var tmp =
					{
						type : c_annotations_document.type,
						sourceFile : c_annotations_document.sourceFile,
						annotations : c_annotations_document.annotations
					};
					annotations_map.set(c_annotations_document.sourceFile, tmp);
				}
			});
			
			const annotations_map_iterator = annotations_map.values();
			const merged_c_annotations_documents = Array.from(annotations_map_iterator, (c_annotations_document) => c_annotations_document);
			
			resolve(merged_c_annotations_documents);
		}
		else
		{
			resolve(c_annotations_documents);
		}
	}.bind(this));
}

module.exports.postprocess_c_annotations = postprocess_c_annotations;
