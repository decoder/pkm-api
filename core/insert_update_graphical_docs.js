/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update Graphical documentation documents
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<Object>} doc_documents - Documentation documents
 * @param {boolean} update - flag to enable/disable replacing Graphical documentation documents
 * 
 * @return {Promise.<Array<string> >} a promise (resolve argument is an array of document unique IDs in the collection)
 */
function insert_update_graphical_docs(dbName, graphical_doc_documents, update)
{
	return this.insert_update_documents(dbName, 'GraphicalDocumentation', graphical_doc_documents, update, { signature : { 'class' : 1, 'object': 1 } });
}

module.exports.insert_update_graphical_docs = insert_update_graphical_docs;
