/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Delete a project
 * 
 * @memberof PKM
 * @instance
 * @param {string} project_name - project name
 * 
 * @return {Promise} a promise
 */

function delete_project(project_name)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		// drop the database of the project (user grants, collections, roles)
		this.drop_db(project_name).then(() =>
		{
			// unregister project name
			this.delete_documents(this.config.pkm_db, 'Projects', { name : project_name }).then(() =>
			{
				let git_file_system = this.get_git_file_system(project_name, { debug : debug });
				
				git_file_system.remove_root_directory().then(() =>
				{
					// project has been deleted
					if(debug)
					{
						console.log('Project \'' + project_name + '\' has been deleted');
					}
					
					resolve();
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.delete_project = delete_project;
