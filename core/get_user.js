/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get user
 * 
 * @memberof PKM
 * @instance
 * @param {string} user_name - user name
 * 
 * @return {Promise<User>} a promise
 */

function get_user(user_name)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const db = this.client.db(this.config.pkm_db);

		db.command({ usersInfo: { user: user_name, db: this.config.pkm_db }, showPrivileges: true }).then((result) =>
		{
			if(Array.isArray(result.users) && (result.users.length != 0))
			{
				const User = require('./user');
				const GitUserCredential = require('./git_user_credential');
				
				const user = this.revoke_pkm_management_roles(
					new User(
						user_name,                                      // user's name
						undefined,                                      // user's password (hidden)
						result.users[0].customData.first_name,          // first name
						result.users[0].customData.last_name,           // last name
						result.users[0].customData.email,               // email
						result.users[0].customData.phone,               // phone
						result.users[0].roles,                          // roles
						((this.user_name == user_name) && (result.users[0].customData.git_user_credentials !== undefined)) ? result.users[0].customData.git_user_credentials.map((obj) => GitUserCredential.decrypt(obj, this.crypt)) : undefined // Git user's credentials
					)
				);
				
				if(debug)
				{
					console.log('get_user(\'' + user_name + '\') => ', user);
				}
				resolve(user);
			}
			else
			{
				reject(this.NotFound());
			}
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_user = get_user;
