/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Postprocess C++ Comments
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} cpp_comments_documents - C++ comment documents
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<Object>>} a promise
 */
function postprocess_cpp_comments(cpp_comments_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const merge = (options !== undefined) && options.merge;
		
		if(merge)
		{
			let comments_map = new Map();
			
			cpp_comments_documents.forEach((cpp_comments_document) =>
			{
				if(comments_map.has(cpp_comments_document.sourceFile))
				{
					var tmp = comments_map.get(cpp_comments_document.sourceFile);
					tmp.comments = tmp.comments.concat(cpp_comments_document.comments);
					comments_map.set(cpp_comments_document.sourceFile, tmp);
				}
				else
				{
					var tmp =
					{
						type : cpp_comments_document.type,
						sourceFile : cpp_comments_document.sourceFile,
						comments : cpp_comments_document.comments
					};
					comments_map.set(cpp_comments_document.sourceFile, tmp);
				}
			});
			
			const comments_map_iterator = comments_map.values();
			const merged_cpp_comments_documents = Array.from(comments_map_iterator, (cpp_comments_document) => cpp_comments_document);
			
			resolve(merged_cpp_comments_documents);
		}
		else
		{
			resolve(cpp_comments_documents);
		}
	}.bind(this));
}

module.exports.postprocess_cpp_comments = postprocess_cpp_comments;
