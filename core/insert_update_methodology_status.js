/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update Methodology status documents into a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<Object>} methodology_status_documents - Methodology status documents
 * @param {boolean} update - flag to enable/disable replacing Methodology status document
 * 
 * @return {Promise} a promise
 */
function insert_update_methodology_status(dbName, methodology_status_documents, update)
{
	return this.insert_update_documents(dbName, 'MethodologyStatus', methodology_status_documents, update, { signature : { id : 1 } });
}

exports.insert_update_methodology_status = insert_update_methodology_status;
