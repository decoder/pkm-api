/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @typedef {Object} GetProjectsOptions
 * @property {boolean} [abbrev] - enable/disable project metadata
 */

/**
 * Get projects
 * 
 * @memberof PKM
 * @instance
 * 
 * @param {GetProjectsOptions} [options] - options
 * 
 * @return {Promise.<Array.<PkmProject>>} a promise
 */
function get_projects(options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		// retrieve the current user
		this.get_user(this.user_name).then((user) =>
		{
			// retrieve project name list
			this.get_documents(this.config.pkm_db, 'Projects').then((abbrev_projects) =>
			{
				const project_names = abbrev_projects.map(abbrev_project => abbrev_project.name);
				var get_project_promises = [];
				
				project_names.forEach((project_name) =>
				{
					get_project_promises.push(new Promise((resolve, reject) =>
					{
						// check if user has a role in the project or he is administrator
						if(user.has_role(project_name) || user.is_admin())
						{
							// user has a role in the project or it is an administrator: retrieve the project information
							this.get_project(project_name, options).then((project) =>
							{
								resolve(project);
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						}
						else
						{
							// user has no role in the project and he is not an administrator: show only the project name
							resolve({ name : project_name });
						}
					}));
				});

				Promise.all(get_project_promises).then((projects) =>
				{
					resolve(projects);
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}).catch((err) =>
			{
				const error = require('./error');
				if(err instanceof error.PKM_NotFound)
				{
					resolve([]);
				}
				else
				{
					reject(this.Error(err));
				}
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_projects = get_projects;
