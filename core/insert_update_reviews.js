/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update review documents into a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<Object>} review_documents - Review documents
 * @param {boolean} update - flag to enable/disable replacing Review documents
 * 
 * @return {Promise.<Array<string> >} a promise (resolve argument is an array of document unique IDs in the collection)
 */
function insert_update_reviews(dbName, review_documents, update)
{
	return new Promise(function(resolve, reject)
	{
		// If the reviewID field is present, convert it to a MongoDB Object ID, or allocate a new one
		const ObjectID = require('mongodb').ObjectID;
		
		try
		{
			review_documents.forEach((review_document) =>
			{
				try
				{
					review_document.reviewID = (review_document.hasOwnProperty('reviewID') ? new ObjectID(review_document.reviewID) : new ObjectID()).toHexString();
				}
				catch(err)
				{
					throw this.BadRequest('reviewID field is invalid: ' + err.message);
				}
			});
		}
		catch(err)
		{
			reject(this.Error(err));
			return;
		}

		this.insert_update_documents(dbName, 'Reviews', review_documents, update, { signature : { reviewID : 1 } }).then(() =>
		{
			resolve(review_documents.map((review_document) => review_document.reviewID));
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.insert_update_reviews = insert_update_reviews;
