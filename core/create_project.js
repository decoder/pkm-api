/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A project member. Undocumented additional properties are allowed.
 * 
 * @typedef {Object} PkmProjectMember
 * @property {string} name - member name
 * @property {boolean} owner - true if member is owner of the project, otherwise false
 * @property {Array.<string>} member role names
 */

/**
 * A project. Undocumented additional properties are allowed.
 * 
 * @typedef {Object} PkmProject
 * @property {string} name - project name
 * @property {Array.<PkmProjectMember>} project members
 */

/**
 * Create a project
 * 
 * @memberof PKM
 * @instance
 * @param {PkmProject} project - project
 * 
 * @return {Promise} a promise
 */

function create_project(project)
{
	return this.create_update_project(project, false);
}

exports.create_project = create_project;
