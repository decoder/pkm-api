/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Drop database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * 
 * @return {Promise} a promise
 */

function drop_db(dbName)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const db = this.client.db(dbName);

		// get the informations about current user
		this.get_user(this.user_name).then((current_user) =>
		{
			// check that current user is administrator that will be able to drop a database
			if(current_user.is_admin())
			{
				// drop all collections
				this.drop_all_collections(dbName).then(() =>
				{
					if(debug)
					{
						console.log('All collections in Database \'' + dbName + '\' have been dropped');
					}
					
					// get all users that have a role in the database
					this.get_users(dbName).then((users) =>
					{
						// revoke all users in the project
						var update_user_promises = [];
						
						users.forEach((user) =>
						{
							user.revoke(dbName);
							update_user_promises.push(this.update_user(user));
						});
							
						Promise.all(update_user_promises).then(() =>
						{
							if(debug)
							{
								console.log('All users in Database \'' + dbName + '\' have been revoked');
							}
							
							// drop all roles in the database
							this.drop_all_roles(dbName).then(() =>
							{
								if(debug)
								{
									console.log('All roles in Database \'' + dbName + '\' have been dropped');
								}
								
								// database has been dropped
								if(debug)
								{
									console.log('Database \'' + dbName + '\' has been dropped');
								}
								
								resolve();
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}).catch((err) =>
					{
						reject(this.Error(err));
					});
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				reject(this.Forbidden('User \'' + this.user_name + '\' can\'t drop the Database \'' + dbName + '\' because he\'s not an administrator'));
			}
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.drop_db = drop_db;
