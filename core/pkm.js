/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 *
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const { insert_annotations } = require('./insert_annotations');
const { update_annotations } = require('./update_annotations');
const { get_annotations } = require('./get_annotations');
const { delete_annotations } = require('./delete_annotations');
const { create_all_collections } = require('./create_all_collections');
const { create_all_roles } = require('./create_all_roles');
const { create_collection } = require('./create_collection');
const { create_db } = require('./create_db');
const { create_role } = require('./create_role');
const { create_user } = require('./create_user');
const { delete_source_files } = require('./delete_source_files');
const { drop_all_collections } = require('./drop_all_collections');
const { drop_all_roles } = require('./drop_all_roles');
const { drop_all_users } = require('./drop_all_users');
const { drop_collection } = require('./drop_collection');
const { drop_db } = require('./drop_db');
const { drop_role } = require('./drop_role');
const { drop_user } = require('./drop_user');
const { find_in_doc } = require('./find_in_doc');
const { find_in_c_source } = require('./find_in_c_source');
const { find_in_uml_class_diagrams } = require('./find_in_uml_class_diagrams');
const { insert_source_files } = require('./insert_source_files');
const { insert_update_source_files } = require('./insert_update_source_files');
const { insert_update_uml_class_diagrams } = require('./insert_update_uml_class_diagrams');
const { insert_uml_class_diagrams } = require('./insert_uml_class_diagrams');
const { update_uml_class_diagrams } = require('./update_uml_class_diagrams');
const { update_source_files } = require('./update_source_files');
const { update_user_password } = require('./update_user_password');
const { update_user_role } = require('./update_user_role');
const { insert_testar_state_models } = require('./insert_testar_state_models');
const { insert_testar_test_results } = require('./insert_testar_test_results');
const { get_testar_test_results } = require('./get_testar_test_results');
const { get_testar_state_models } = require('./get_testar_state_models');
const { delete_testar_test_results } = require('./delete_testar_test_results');
const { delete_testar_state_models } = require('./delete_testar_state_models');
const { get_source_files } = require('./get_source_files');
const { delete_uml_class_diagrams } = require('./delete_uml_class_diagrams');
const { get_uml_class_diagrams } = require('./get_uml_class_diagrams');
const { update_c_source_code } = require('./update_c_source_code');
const { update_c_annotations } = require('./update_c_annotations');
const { update_c_comments } = require('./update_c_comments');
const { get_c_source_codes } = require('./get_c_source_codes');
const { get_c_annotations } = require('./get_c_annotations');
const { get_c_comments } = require('./get_c_comments');
const { create_update_user } = require('./create_update_user');
const { update_user } = require('./update_user');
const { get_user } = require('./get_user');
const { get_current_user } = require('./get_current_user');
const { get_users } = require('./get_users');
const { get_user_roles } = require('./get_user_roles');
const { insert_update_documents } = require('./insert_update_documents');
const { insert_documents } = require('./insert_documents');
const { update_documents } = require('./update_documents');
const { insert_update_documents_spanned } = require('./insert_update_documents_spanned');
const { insert_documents_spanned } = require('./insert_documents_spanned');
const { update_documents_spanned } = require('./update_documents_spanned');
const { get_documents } = require('./get_documents');
const { delete_documents } = require('./delete_documents');
const { delete_documents_spanned } = require('./delete_documents_spanned');
const { get_collection } = require('./get_collection');
const { insert_update_java_source_code } = require('./insert_update_java_source_code');
const { insert_java_source_code } = require('./insert_java_source_code');
const { get_java_source_codes } = require('./get_java_source_codes');
const { insert_uml_state_machines } = require('./insert_uml_state_machines');
const { update_uml_state_machines } = require('./update_uml_state_machines');
const { insert_update_uml_state_machines } = require('./insert_update_uml_state_machines');
const { get_uml_state_machines } = require('./get_uml_state_machines');
const { delete_uml_state_machines } = require('./delete_uml_state_machines');
const { insert_java_annotations } = require('./insert_java_annotations');
const { update_java_annotations } = require('./update_java_annotations');
const { insert_update_java_annotations } = require('./insert_update_java_annotations');
const { get_java_annotations } = require('./get_java_annotations');
const { insert_uml_files } = require('./insert_uml_files');
const { update_uml_files } = require('./update_uml_files');
const { insert_update_uml_files } = require('./insert_update_uml_files');
const { get_uml_files } = require('./get_uml_files');
const { delete_uml_files } = require('./delete_uml_files');
const { insert_update_files } = require('./insert_update_files');
const { get_files } = require('./get_files');
const { delete_files } = require('./delete_files');
const { delete_files_spanned } = require('./delete_files_spanned');
const { create_project } = require('./create_project');
const { update_project } = require('./update_project');
const { create_update_project } = require('./create_update_project');
const { get_project } = require('./get_project');
const { delete_project } = require('./delete_project');
const { get_user_projects } = require('./get_user_projects');
const { get_projects } = require('./get_projects');
const { invalidate_dependent_documents } = require('./invalidate_dependent_documents');
const { insert_compile_commands } = require('./insert_compile_commands');
const { update_compile_commands } = require('./update_compile_commands');
const { get_compile_commands } = require('./get_compile_commands');
const { delete_compile_commands } = require('./delete_compile_commands');
const { update_cpp_source_code } = require('./update_cpp_source_code');
const { update_cpp_annotations } = require('./update_cpp_annotations');
const { update_cpp_comments } = require('./update_cpp_comments');
const { get_cpp_source_codes } = require('./get_cpp_source_codes');
const { get_cpp_annotations } = require('./get_cpp_annotations');
const { get_cpp_comments } = require('./get_cpp_comments');
const { find_in_cpp_source } = require('./find_in_cpp_source');
const { find_in_cpp_class } = require('./find_in_cpp_class');
const { get_java_comments } = require('./get_java_comments');
const { insert_update_java_comments } = require('./insert_update_java_comments');
const { insert_java_comments } = require('./insert_java_comments');
const { update_java_comments } = require('./update_java_comments');
const { update_java_source_code } = require('./update_java_source_code');
const { count_documents } = require('./count_documents');
const { count_documents_spanned } = require('./count_documents_spanned');
const { get_documents_spanned } = require('./get_documents_spanned');
const { get_source_codes } = require('./get_source_codes');
const { get_source_code_annotations } = require('./get_source_code_annotations');
const { get_source_code_comments } = require('./get_source_code_comments');
const { insert_update_files_spanned } = require('./insert_update_files_spanned');
const { get_files_spanned } = require('./get_files_spanned');
const { insert_update_any_files } = require('./insert_update_any_files');
const { insert_any_files } = require('./insert_any_files');
const { update_any_files } = require('./update_any_files');
const { get_any_files } = require('./get_any_files');
const { delete_any_files } = require('./delete_any_files');
const { insert_update_traceability_matrix } = require('./insert_update_traceability_matrix');
const { insert_traceability_matrix } = require('./insert_traceability_matrix');
const { update_traceability_matrix } = require('./update_traceability_matrix');
const { get_traceability_matrix } = require('./get_traceability_matrix');
const { delete_traceability_matrix } = require('./delete_traceability_matrix');
const { insert_update_logs } = require('./insert_update_logs');
const { insert_logs } = require('./insert_logs');
const { update_logs } = require('./update_logs');
const { get_logs } = require('./get_logs');
const { delete_logs } = require('./delete_logs');
const { postprocess_logs } = require('./postprocess_logs');
const { insert_update_doc_files } = require('./insert_update_doc_files');
const { insert_doc_files } = require('./insert_doc_files');
const { update_doc_files } = require('./update_doc_files');
const { get_doc_files } = require('./get_doc_files');
const { delete_doc_files } = require('./delete_doc_files');
const { delete_docs } = require('./delete_docs');
const { get_docs } = require('./get_docs');
const { insert_docs } = require('./insert_docs');
const { insert_update_docs } = require('./insert_update_docs');
const { update_docs } = require('./update_docs');
const { delete_graphical_docs } = require('./delete_graphical_docs');
const { get_graphical_docs } = require('./get_graphical_docs');
const { insert_graphical_docs } = require('./insert_graphical_docs');
const { insert_update_graphical_docs } = require('./insert_update_graphical_docs');
const { update_graphical_docs } = require('./update_graphical_docs');
const { postprocess_c_source_codes } = require('./postprocess_c_source_codes');
const { postprocess_c_annotations } = require('./postprocess_c_annotations');
const { postprocess_c_comments } = require('./postprocess_c_comments');
const { postprocess_cpp_source_codes } = require('./postprocess_cpp_source_codes');
const { postprocess_cpp_annotations } = require('./postprocess_cpp_annotations');
const { postprocess_cpp_comments } = require('./postprocess_cpp_comments');
const { has_postprocess_c_source_codes } = require('./has_postprocess_c_source_codes');
const { has_postprocess_c_annotations } = require('./has_postprocess_c_annotations');
const { has_postprocess_c_comments } = require('./has_postprocess_c_comments');
const { has_postprocess_cpp_source_codes } = require('./has_postprocess_cpp_source_codes');
const { has_postprocess_cpp_annotations } = require('./has_postprocess_cpp_annotations');
const { has_postprocess_cpp_comments } = require('./has_postprocess_cpp_comments');
const { find_in_java_source } = require('./find_in_java_source');
const { find_in_java_class } = require('./find_in_java_class');
const { preprocess_c_source_codes } = require('./preprocess_c_source_codes');
const { preprocess_c_annotations } = require('./preprocess_c_annotations');
const { preprocess_c_comments } = require('./preprocess_c_comments');
const { preprocess_cpp_source_codes } = require('./preprocess_cpp_source_codes');
const { preprocess_cpp_annotations } = require('./preprocess_cpp_annotations');
const { preprocess_cpp_comments } = require('./preprocess_cpp_comments');
const { preprocess_logs } = require('./preprocess_logs');
const { insert_update_cves } = require('./insert_update_cves');
const { insert_cves } = require('./insert_cves');
const { update_cves } = require('./update_cves');
const { get_cves } = require('./get_cves');
const { delete_cves } = require('./delete_cves');
const { preprocess_files } = require('./preprocess_files');
const { postprocess_files } = require('./postprocess_files');
const { insert_update_executable_binary_files } = require('./insert_update_executable_binary_files');
const { insert_executable_binary_files } = require('./insert_executable_binary_files');
const { update_executable_binary_files } = require('./update_executable_binary_files');
const { get_executable_binary_files } = require('./get_executable_binary_files');
const { delete_executable_binary_files } = require('./delete_executable_binary_files');
const { delete_c_source_codes } = require('./delete_c_source_codes');
const { delete_c_annotations } = require('./delete_c_annotations');
const { delete_c_comments } = require('./delete_c_comments');
const { delete_cpp_source_codes } = require('./delete_cpp_source_codes');
const { delete_cpp_annotations } = require('./delete_cpp_annotations');
const { delete_cpp_comments } = require('./delete_cpp_comments');
const { delete_java_source_codes } = require('./delete_java_source_codes');
const { delete_java_annotations } = require('./delete_java_annotations');
const { delete_java_comments } = require('./delete_java_comments');
const { delete_source_codes } = require('./delete_source_codes');
const { delete_source_code_annotations } = require('./delete_source_code_annotations');
const { delete_source_code_comments } = require('./delete_source_code_comments');
const { delete_git_working_trees } = require('./delete_git_working_trees');
const { get_git_working_trees } = require('./get_git_working_trees');
const { insert_git_working_trees } = require('./insert_git_working_trees');
const { insert_update_git_working_trees } = require('./insert_update_git_working_trees');
const { update_git_working_trees } = require('./update_git_working_trees');
const { git } = require('./git');
const { get_git_config } = require('./get_git_config');
const { update_git_config } = require('./update_git_config');
const { get_git_files } = require('./get_git_files');
const { insert_update_git_files } = require('./insert_update_git_files');
const { insert_git_files } = require('./insert_git_files');
const { update_git_files } = require('./update_git_files');
const { delete_git_files } = require('./delete_git_files');
const { delete_tools } = require('./delete_tools');
const { get_tools } = require('./get_tools');
const { insert_tools } = require('./insert_tools');
const { insert_update_tools } = require('./insert_update_tools');
const { update_tools } = require('./update_tools');
const { delete_invocations } = require('./delete_invocations');
const { get_invocations } = require('./get_invocations');
const { insert_invocations } = require('./insert_invocations');
const { insert_update_invocations } = require('./insert_update_invocations');
const { update_invocations } = require('./update_invocations');
const { insert_testar_settings } = require('./insert_testar_settings');
const { update_testar_settings } = require('./update_testar_settings');
const { insert_update_testar_settings } = require('./insert_update_testar_settings');
const { get_testar_settings } = require('./get_testar_settings');
const { delete_testar_settings } = require('./delete_testar_settings');
const { insert_methodology_status } = require('./insert_methodology_status');
const { update_methodology_status } = require('./update_methodology_status');
const { insert_update_methodology_status } = require('./insert_update_methodology_status');
const { get_methodology_status } = require('./get_methodology_status');
const { delete_methodology_status } = require('./delete_methodology_status');
const { insert_reviews } = require('./insert_reviews');
const { update_reviews } = require('./update_reviews');
const { insert_update_reviews } = require('./insert_update_reviews');
const { get_reviews } = require('./get_reviews');
const { delete_reviews } = require('./delete_reviews');
const { preprocess_reviews } = require('./preprocess_reviews');

const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const MongoClient = require('mongodb').MongoClient;
const Ajv = require('ajv');
const asyncPool = require('tiny-async-pool');
const FileSystem = require('../util/file_system');
const error = require('./error');
const Crypt = require('../util/crypt');
const { deep_copy } = require('../util/deep_copy');

// configuration examples:
// const config = {
// 	db_host : 'localhost:27017',
// 	session_timeout : 0, // in milliseconds, zero means no timeout, connection only closed when explicitely requested
// 	pkm_db : 'pkm',
// 	debug : true
// };

var pkms = new Map(); // map key => pkm

/** A MongoDB client shared among several PKM user's sessions */
class Client extends MongoClient
{
	/** Constructor
	*
	* @param @param {...*} var_args - same arguments as MongoClient constructor
	*/
	constructor(...var_args)
	{
		super(...var_args);
		this.acquired = 1;
		this.connected = true;
	}
	
	/** Acquire */
	acquire()
	{
		if(this.connected) this.acquired++;
	}
	
	/** Release */
	release()
	{
		if(this.connected && (this.acquired != 0) && (--this.acquired == 0))
		{
			this.close();
			this.connected = false;
		}
	}
}

/** A generator for the PKM access keys that identify the user's sessions */
class KeyGenerator
{
	static counter = 0;
	
	/** Generate a key
	 * 
	 * @param {string} secret - a secret
	 * @return {string} a key
	 */
	static generate(secret)
	{
		const seconds_since_epoch = Math.floor(Date.now() / 1000);
		// raw key: ( MSB=32-bit counter of seconds since epoch, LSB=16-bit rollover counter ) => 12-characters hexadecimal string
		const raw_key = seconds_since_epoch.toString(16).slice(0, 8).padStart(8, '0') + KeyGenerator.counter.toString(16).padStart(4, '0');
		KeyGenerator.counter = (KeyGenerator.counter < 65535) ? (KeyGenerator.counter + 1) : 0;
		// encrypt raw key with secret
		const key = jwt.sign(raw_key, secret);
		return key;
	}
	
	/** Verify a key
	 * 
	 * @param {string} key - a key
	 * @param {string} secret - a secret
	 */
	static verify(key, secret)
	{
		jwt.verify(key, secret);
	}
}

/**
 * A PKM configuration
 *
 * @typedef {Object} PkmConfig
 * @property {boolean} debug - flag to enable/disable debug messages
 * @property {string} schemas.*.file - filename of collection (*) documents schema
 * @property {boolean} schemas.*.enable - flag to enable/disable collection (*) documents validation
 * @property {string} db_host - MongoDB host
 * @property {string} pkm_db - PKM users database
 * @property {Number} session_timeout - user's session expiration time in milliseconds
 */

/** 
 * Persistent Knowledge Monitor User's session
 */
class PKM
{
	static info =
	{
		title: 'PKM',
		description: 'Persistent Knowledge Monitor',
		contact:
		{
			name: 'DECODER project',
			url: 'https://www.decoder-project.eu',
			email: 'decoder@decoder-project.eu'
		},
		version: '1.0.0',
		license:
		{
			name: 'GNU AGPL v3.0',
			url: 'https://www.gnu.org/licenses/agpl-3.0.en.html'
		},
		copyright:
		{
			year: '2020',
			owners:
			[
				'Capgemini Group',
				'Commissariat à l\'énergie atomique et aux énergies alternatives',
				'OW2',
				'Sysgo AG',
				'Technikon',
				'Tree Technology',
				'Universitat Politècnica de València'
			]
		}
	};
	static LogChunkSizeThreshold = 4096 * 1024; // 4 Mchars
	// Note: Javascript string are UTF-16 (2 bytes per characters). UTF-8 characters may needs two UTF-16 characters.
	// Note: MongoDB has a strong limit of 16 MB for BSON documents in a collection
	
	static global_config = PKM.load_global_config();
	
	/** Load global PKM configuration. Global PKM configuration can be overriden using File 'pkm_config.json'.
	 *
	 * @return {PkmConfig} configuration
	 */
	static load_global_config()
	{
		let global_config =
		{
			debug : false,
			db_host: 'pkm-api_mongodb_1:27017',
			pkm_db: 'pkm',
			session_timeout : 24 * 60 * 60 * 1000, // one day timeout for PKM user's sessions
			secret : crypto.randomBytes(16).toString('hex'), // a secret (32 hexadecimal digits) gets generated each time server starts, can be overriden in pkm_config.json
			secret_file : 'secret',
			git_root_directory : 'git-root',
			git_max_parallel_read_files : 1,
			git_max_parallel_write_files : 1,
			git_remote_timeout : 10 * 60 * 1000, // 10 minutes timeout for Git remote operations (clone, fetch, pull and push) to avoid blocking server on a required authentication
			server_request_body_limit: '256MB', // maximum size of request body: 256MB ought to be enough for anybody
			collections :
			{
				Annotations: { index: { key : { path : 1, access : 1 }, options : { unique : true } } },
				RawSourcecode: { index: { key : { filename : 1, chunkId : 1 }, options : { unique : true } } },
				RawUML: { index: { key : { filename : 1, chunkId : 1 }, options : { unique : true } } },
				sourcecodeC: { index: { key : { sourceFile : 1 }, options : { unique : false } } },
				sourcecodeCPP: { index: { key : { sourceFile : 1 }, options : { unique : false } } },
				sourcecodeJava: { index: { key : { sourceFile : 1 }, options : { unique : true } } },
				annotationsACSL: { index: { key : { sourceFile : 1 }, options : { unique : false } } },
				annotationsACSLPP: { index: { key : { sourceFile : 1 }, options : { unique : false } } },
				annotationsJML: { index: { key : { sourceFile : 1 }, options : { unique : true } } },
				commentsC: { index: { key : { sourceFile : 1 }, options : { unique : false } } },
				commentsCPP: { index: { key : { sourceFile : 1 }, options : { unique : false } } },
				commentsjava: { index: { key : { sourceFile : 1 }, options : { unique : true } } },
				RawDocumentation: { index: { key : { filename : 1, chunkId : 1 }, options : { unique : true } } },
				Documentation: { index: { key : { name : 1 }, options : { unique : true } } },
				GraphicalDocumentation: { index: { key : { 'class' : 1, 'object': 1 }, options : { unique : true } } },
				UMLClasses: { index: { key : { name : 1 }, options : { unique : true } } },
				UMLStateMachines: { index: { key : { name : 1 }, options : { unique : true } } },
				TESTARStateModels: {},
				TESTARTestResults: {},
				Logs: { index: { key : { id : 1, chunkId : 1 }, options : { unique : true } } },
				TraceabilityMatrix: {},
				Project: { index: { key : { name : 1 }, options : { unique : true } } },
				CompileCommands: { index: { key : { file : 1 }, options : { unique : true } } },
				CVEList: { index: { key : { 'CVE_data_meta.ID': 1 }, options : { unique : true } } },
				RawBinaries: { index: { key : { filename : 1, chunkId : 1 }, options : { unique : true } } },
				GitWorkingTrees: { index: { key : { directory : 1 }, options : { unique : true } } },
				Tools: { index: { key : { toolID : 1 }, options : { unique : true } } },
				Invocations: { index: { key : { invocationID : 1 }, options : { unique : true } } },
				TESTARSettings: {},
				MethodologyStatus: { index: { key : { id : 1 }, options : { unique : true } } },
				Reviews: { index: { key : { key : 1 }, options : { unique : true } } }
			},
			file_collection_names :
			[
				'RawSourcecode',
				'RawUML',
				'RawDocumentation',
				'RawBinaries'
			],
			project:
			{
				testarSettings:
				{
					insert: 'insert_testar_settings',
					update: 'update_testar_settings',
					get: 'get_testar_settings'
				},
				tools:
				{
					insert: 'insert_tools',
					update: 'update_tools',
					get: 'get_tools',
					files:
					[
						'tools_specs/asfm_to_doc.json',
						'tools_specs/classmodelxmi2jsontransformer.json',
						'tools_specs/code-sumarization-file.json',
						'tools_specs/code-sumarization-project.json',
						'tools_specs/code_to_asfm.json',
						'tools_specs/doc_to_asfm.json',
						'tools_specs/excavator.json',
						'tools_specs/frama-c.json',
						'tools_specs/frama-clang.json',
						'tools_specs/javaparser.json',
						'tools_specs/javaparserWholeProject.json',
						'tools_specs/jmlgen.json',
						'tools_specs/NER.json',
						'tools_specs/openjml.json',
						'tools_specs/semparser.json',
						'tools_specs/rawtext_semparser.json',
						'tools_specs/SRLChunk.json',
						'tools_specs/testar.json',
						'tools_specs/TraceRecovery.json',
						'tools_specs/variable-misuse-file.json',
						'tools_specs/variable-misuse-project.json'
					]
				},
				methodologyStatus:
				{
					insert: 'insert_methodology_status',
					update: 'update_methodology_status',
					get: 'get_methodology_status',
					files:
					[
						'methodology/status/hld-phase.json',
						'methodology/status/lld-phase.json',
						'methodology/status/llv-phase.json',
						'methodology/status/hlv-phase.json'
					]
				}
			},
			roles:
			{
				// PKM management database: contains users and project name list
				pkm :
				{
					// simple user: can view project name list, view users, and change own password and custom data
					User :
					{
						privileges :
						[
							{
								resource :
								{
									collection : ''
								},
								actions :
								[
									'listIndexes',
									'listCollections',
									'viewUser',
									'changeOwnPassword',
									'changeOwnCustomData'
								]
							},
							{
								resource :
								{
									collection : 'Projects'
								},
								actions :
								[
									'find',
									'insert',
									'killCursors'
								]
							}
						],
						roles : []
					}
				},
				// Project database: contains project data
				project :
				{
					// Owner can grant/revoke role to/from to users, create/drop collections
					Owner :
					{
						privileges :
						[
							{
								resource :
								{
									collection : ''
								},
								actions :
								[
									'createCollection',
									'dropCollection',
									'grantRole',
									'revokeRole',
									'viewRole',
									'viewUser'
								]
							}
						],
						roles : []
					},
					// Developer can read/write data
					Developer :
					{
						privileges :
						[
							{
								resource :
								{
									collection : ''
								},
								actions :
								[
									'find',
									'insert',
									'killCursors',
									'listIndexes',
									'listCollections',
									'remove',
									'update'
								]
							}
						],
						roles : []
					},
					// Reviewer: can read/write project data
					Reviewer :
					{
						privileges :
						[
							{
								resource :
								{
									collection : ''
								},
								actions :
								[
									'find',
									'insert',
									'killCursors',
									'listIndexes',
									'listCollections',
									'remove',
									'update'
								]
							}
						],
						roles : []
					},
					// Reviewer: can read/write project data
					Maintainer :
					{
						privileges :
						[
							{
								resource :
								{
									collection : ''
								},
								actions :
								[
									'find',
									'insert',
									'killCursors',
									'listIndexes',
									'listCollections',
									'remove',
									'update'
								]
							}
						],
						roles : []
					}
				}
			},
			schemas :
			{
				Annotations :
				{
					file : 'api/pkm-annotations-schema.json',
					enable : true
				},
				RawSourcecode :
				{
					file :  'api/pkm-db-file-schema.json',
					enable : true
				},
				RawUML :
				{
					file :  'api/pkm-db-file-schema.json',
					enable : true
				},
				sourcecodeC :
				{
					file : 'api/pkm-c-source-code-schema.json',
					enable : true
				},
				sourcecodeCPP :
				{
					file : 'api/pkm-cpp-source-code-schema.json',
					enable : true
				},
				sourcecodeJava :
				{
					file : 'api/pkm-java-source-code-schema.json',
					enable : true
				},
				annotationsACSL :
				{
					file : 'api/pkm-acsl-schema.json',
					enable : true
				},
				commentsC :
				{
					file : 'api/pkm-c-comments-schema.json',
					enable : true
				},
				commentsCPP :
				{
					file : 'api/pkm-cpp-comments-schema.json',
					enable : true
				},
				commentsjava :
				{
					file : 'api/pkm-java-comments-schema.json',
					enable : true
				},
				annotationsACSLPP :
				{
					file : 'api/pkm-acslpp-schema.json',
					enable : true
				},
				annotationsJML :
				{
					file: 'api/jml-schema.json',
					enable : true
				},
				UMLClasses :
				{
					file : 'api/pkm-uml-class-diagram-schema.json',
					enable : true
				},
				UMLStateMachines :
				{
					file : 'api/pkm-uml-state-model-schema.json',
					enable : true
				},
				TESTARStateModels :
				{
					file : 'api/TESTAR_StateModel_Schema.json',
					enable : true
				},
				TESTARTestResults :
				{
					file : 'api/TESTAR_TestResults_Schema.json',
					enable : true
				},
				CompileCommands :
				{
					file : 'api/pkm-compile-command-schema.json',
					enable : true
				},
				TraceabilityMatrix :
				{
					file : 'api/pkm-traceability-matrix-schema.json',
					enable : true
				},
				Logs :
				{
					file : 'api/pkm-log-schema.json',
					enable : true
				},
				RawDocumentation :
				{
					file :  'api/pkm-db-file-schema.json',
					enable : true
				},
				Documentation :
				{
					file : 'api/pkm-asfm-schema.json',
					enable : true
				},
				GraphicalDocumentation :
				{
					file : 'api/pkm-gsl-schema.json',
					enable : true
				},
				CVEList :
				{
					file : 'api/pkm-cve-schema.json',
					enable : true
				},
				RawBinaries :
				{
					file :  'api/pkm-db-file-schema.json',
					enable : true
				},
				GitWorkingTree :
				{
					file : 'api/pkm-git-working-tree-schema.json',
					enable : true
				},
				Tools :
				{
					file : 'api/tools.json',
					enable : true
				},
				Invocations :
				{
					file : 'api/invocations.json',
					enable : true
				},
				TESTARSettings :
				{
					file : 'api/TESTAR_Settings_Schema.json',
					enable : true
				},
				MethodologyStatus:
				{
					file: 'api/pkm-methodology-status-schema.json',
					enable: true
				},
				Reviews:
				{
					file: 'api/pkm-review-schema.json',
					enable: true
				}
			},
			types :
			{
				Annotations           : 'Annotation',
				RawSourcecode         : 'Code',
				RawUML                : 'Diagram',
				sourcecodeC           : 'Code',
				sourcecodeCPP         : 'Code',
				sourcecodeJava        : 'Code',
				annotationsACSL       : 'Annotation',
				annotationsACSLPP     : 'Annotation',
				annotationsJML        : 'Annotation',
				commentsC             : 'Comment',
				commentsCPP           : 'Comment',
				commentsjava          : 'Comment',
				RawDocumentation      : 'Document',
				Documentation         : 'ASFM Docs',
				GraphicalDocumentation: 'GSL Docs',
				UMLClasses            : 'Diagram',
				UMLStateMachines      : 'Diagram',
				TESTARStateModels     : 'TESTAR_State_Model',
				TESTARTestResults     : 'TESTAR_Test_Results',
// 				TraceabilityMatrix    : '',
// 				Project               : '',
				CompileCommands       : 'Compile Commands',
				Logs                  : 'Log',
				CVEList               : 'CVE',
				RawBinaries           : 'Executable Binary',
// 				GitWorkingTrees       : '',
// 				Tools                 : '',
// 				TESTARSettings        : '',
// 				MethodologyStatus     : '',
// 				Reviews               : ''
			},
			file_types :
			[
				// Before adding new file types: see https://www.digipres.org/formats/mime-types
				
				// Programming languages
				{ suffixes : [ '.c' ], format : 'text', mime_types : [ 'text/x-csrc', 'text/x-c' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.h' ], format : 'text', mime_types : [ 'text/x-chdr' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.cc', '.cp', '.cxx', '.cpp', '.c++' ], format : 'text', mime_types : [ 'text/x-c++src' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.hpp' ], format : 'text', mime_types : [ 'text/x-c++hdr' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.java' ], format : 'text', mime_types : [ 'text/x-java', 'text/x-java-source' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.f', '.for', '.f77', '.f90' ], format : 'text', mime_types : [ 'text/x-fortran' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.p', '.pp', '.pas', '.dpr' ], format : 'text', mime_types : [ 'text/x-pascal' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.cs' ], format : 'text', mime_types : [ 'text/x-csharp' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.js' ], format : 'text', mime_types : [ 'text/javascript', 'application/javascript', 'application/ecmascript', 'application/x-ecmascript', 'application/x-javascript', 'text/ecmascript', 'text/javascript1.0', 'text/javascript1.1', 'text/javascript1.2', 'text/javascript1.3', 'text/javascript1.4', 'text/javascript1.5', 'text/x-ecmascript', 'text/x-javascript' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.py' ], format : 'text', mime_types : [ 'text/x-python' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.sh' ], format : 'text', mime_types : [ 'text/x-sh', 'application/x-sh' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.csh' ], format : 'text', mime_types : [ 'text/x-csh', 'application/x-csh' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.pl', '.pm', '.al', '.perl' ], format : 'text', mime_types : [ 'text/x-perl' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.php', '.php3', '.php4' ], format : 'text', mime_types : [ 'text/x-php' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.pro' ], format : 'text', mime_types : [ 'text/x-prolog' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.clj' ], format : 'text', mime_types : [ 'text/x-clojure' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.cbl', '.cob' ], format : 'text', mime_types : [ 'text/x-cobol' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.coffee', '.litcoffee' ], format : 'text', mime_types : [ 'text/x-coffeescript', 'text/coffeescript' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.cfm', '.cfml', '.cfc' ], format : 'text', mime_types : [ 'text/x-coldfusion' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.cl', '.jl', '.lisp', '.lsp' ], format : 'text', mime_types : [ 'text/x-common-lisp' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.d' ], format : 'text', mime_types : [ 'text/x-d' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.e' ], format : 'text', mime_types : [ 'text/x-eiffel' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.el' ], format : 'text', mime_types : [ 'text/x-emacs-lisp' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.erl' ], format : 'text', mime_types : [ 'text/x-erlang' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.exp' ], format : 'text', mime_types : [ 'text/x-expect' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.4th' ], format : 'text', mime_types : [ 'text/x-forth' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.go' ], format : 'text', mime_types : [ 'text/x-go' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.groovy' ], format : 'text', mime_types : [ 'text/x-groovy' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.haml' ], format : 'text', mime_types : [ 'text/x-haml' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.hs', '.lhs' ], format : 'text', mime_types : [ 'text/x-haskell' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.hx' ], format : 'text', mime_types : [ 'text/x-haxe' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.idl' ], format : 'text', mime_types : [ 'text/x-idl' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.jsp' ], format : 'text', mime_types : [ 'text/x-jsp' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.l' ], format : 'text', mime_types : [ 'text/x-lex' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.lua' ], format : 'text', mime_types : [ 'text/x-lua' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.ml' ], format : 'text', mime_types : [ 'text/x-ml' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.m3', '.i3', '.mg', '.ig' ], format : 'text', mime_types : [ 'text/x-modula' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.m' ], format : 'text', mime_types : [ 'text/x-objcsrc' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.m' ], format : 'text', mime_types : [ 'text/x-matlab' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.ocaml', '.mli' ], format : 'text', mime_types : [ 'text/x-ocaml' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.rexx' ], format : 'text', mime_types : [ 'text/x-rexx' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.r' ], format : 'text', mime_types : [ 'text/x-rsrc' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.rest', '.rst', '.restx' ], format : 'text', mime_types : [ 'text/x-rst' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.rb' ], format : 'text', mime_types : [ 'text/x-ruby' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.scala' ], format : 'text', mime_types : [ 'text/x-scala' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.scm' ], format : 'text', mime_types : [ 'text/x-scheme' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.sed' ], format : 'text', mime_types : [ 'text/x-sed' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.sql' ], format : 'text', mime_types : [ 'text/x-sql' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.st' ], format : 'text', mime_types : [ 'text/x-stsrc' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.itk', '.tcl', '.tk' ], format : 'text', mime_types : [ 'text/x-tcl', 'application/x-tcl' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.cls', '.frm' ], format : 'text', mime_types : [ 'text/x-vbasic' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.vb' ], format : 'text', mime_types : [ 'text/x-vbscript' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.v' ], format : 'text', mime_types : [ 'text/x-verilog' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.vhd', '.vhdl' ], format : 'text', mime_types : [ 'text/x-vhdl' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.y' ], format : 'text', mime_types : [ 'text/x-yacc' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.css' ], format : 'text', mime_types : [ 'text/css' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.html', '.htm', '.shtml' ], format : 'text', mime_types : [ 'text/html' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.jade' ], format : 'text', mime_types : [ 'text/jade' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.jsx' ], format : 'text', mime_types : [ 'text/jsx' ], collection : 'RawSourcecode' },
				{ suffixes : [ '.less' ], format : 'text', mime_types : [ 'text/less' ], collection : 'RawSourcecode' },
				
				// UML
				{ suffixes : [ '.uml' ], format : 'text', collection : 'RawUML' },
				
				// Documentation
				{ suffixes : [ '.pdf' ], format : 'binary', mime_types : [ 'application/pdf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.gdoc' ], format : 'binary', mime_types : [ 'application/vnd.google-apps.document' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.docm', '.dotm' ], format : 'binary', mime_types : [ 'application/vnd.ms-word.document.macroenabled.12' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xps' ], format : 'binary', mime_types : [ 'application/vnd.ms-xpsdocument' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odc' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.chart' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.otc' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.chart-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odb' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.database' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odf' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.formula' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odft' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.formula-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odg' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.graphics' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.otg' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.graphics-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odi' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.image' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.oti' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.image-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odp' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.presentation' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.otp' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.presentation-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ods' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.spreadsheet' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ots' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.spreadsheet-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odt' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.text' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.odm' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.text-master' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ott' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.text-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.oth' ], format : 'binary', mime_types : [ 'application/vnd.oasis.opendocument.text-web' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ppt', '.pps' ], format : 'binary', mime_types : [ 'application/vnd.ms-powerpoint' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ppsm' ], format : 'binary', mime_types : [ 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pptm' ], format : 'binary', mime_types : [ 'application/vnd.ms-powerpoint.presentation.macroEnabled.12' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pptx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.presentationml.presentation' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sldx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.presentationml.slide' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ppsx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.presentationml.slideshow' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pot' ], format : 'binary', mime_types : [ 'application/vnd.ms-powerpoint' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.potm' ], format : 'binary', mime_types : [ 'application/vnd.ms-powerpoint.template.macroEnabled.12' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.potx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.presentationml.template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xls' ], format : 'binary', mime_types : [ 'application/vnd.ms-excel' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xlsx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xltx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.spreadsheetml.template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.doc', '.dot' ], format : 'binary', mime_types : [ 'application/msword' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.docx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dotx' ], format : 'binary', mime_types : [ 'application/vnd.openxmlformats-officedocument.wordprocessingml.template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wps' ], format : 'binary', mime_types : [ 'application/vnd.ms-works' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.thmx' ], format : 'binary', mime_types : [ 'application/vnd.ms-officetheme' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.3gpp' ], format : 'binary', mime_types : [ 'audio/3gpp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.adp' ], format : 'binary', mime_types : [ 'audio/adpcm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.amr' ], format : 'binary', mime_types : [ 'audio/amr' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.au', '.snd' ], format : 'binary', mime_types : [ 'audio/basic' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mid', '.midi', '.kar', '.rmi' ], format : 'binary', mime_types : [ 'audio/midi' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mxmf' ], format : 'binary', mime_types : [ 'audio/mobile-xmf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mp3' ], format : 'binary', mime_types : [ 'audio/mp3' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.m4a', '.mp4a' ], format : 'binary', mime_types : [ 'audio/mp4' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mpga', '.mp2', '.mp2a', '.mp3', '.m2a', '.m3a' ], format : 'binary', mime_types : [ 'audio/mpeg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.oga', '.ogg', '.spx', '.opus' ], format : 'binary', mime_types : [ 'audio/ogg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.s3m' ], format : 'binary', mime_types : [ 'audio/s3m' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sil' ], format : 'binary', mime_types : [ 'audio/silk' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uva', '.uvva' ], format : 'binary', mime_types : [ 'audio/vnd.dece.audio' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.eol' ], format : 'binary', mime_types : [ 'audio/vnd.digital-winds' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dra' ], format : 'binary', mime_types : [ 'audio/vnd.dra' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dts' ], format : 'binary', mime_types : [ 'audio/vnd.dts' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dtshd' ], format : 'binary', mime_types : [ 'audio/vnd.dts.hd' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.lvp' ], format : 'binary', mime_types : [ 'audio/vnd.lucent.voice' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pya' ], format : 'binary', mime_types : [ 'audio/vnd.ms-playready.media.pya' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ecelp4800' ], format : 'binary', mime_types : [ 'audio/vnd.nuera.ecelp4800' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ecelp7470' ], format : 'binary', mime_types : [ 'audio/vnd.nuera.ecelp7470' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ecelp9600' ], format : 'binary', mime_types : [ 'audio/vnd.nuera.ecelp9600' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.rip' ], format : 'binary', mime_types : [ 'audio/vnd.rip' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wav' ], format : 'binary', mime_types : [ 'audio/wav' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wav' ], format : 'binary', mime_types : [ 'audio/wave' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.weba' ], format : 'binary', mime_types : [ 'audio/webm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.aac' ], format : 'binary', mime_types : [ 'audio/x-aac' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.aif', '.aiff', '.aifc' ], format : 'binary', mime_types : [ 'audio/x-aiff' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.caf' ], format : 'binary', mime_types : [ 'audio/x-caf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.flac' ], format : 'binary', mime_types : [ 'audio/x-flac' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.m4a' ], format : 'binary', mime_types : [ 'audio/x-m4a' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mka' ], format : 'binary', mime_types : [ 'audio/x-matroska' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.m3u' ], format : 'binary', mime_types : [ 'audio/x-mpegurl' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wax' ], format : 'binary', mime_types : [ 'audio/x-ms-wax' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wma' ], format : 'binary', mime_types : [ 'audio/x-ms-wma' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ram', '.ra' ], format : 'binary', mime_types : [ 'audio/x-pn-realaudio' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.rmp' ], format : 'binary', mime_types : [ 'audio/x-pn-realaudio-plugin' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ra' ], format : 'binary', mime_types : [ 'audio/x-realaudio' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wav' ], format : 'binary', mime_types : [ 'audio/x-wav' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xm' ], format : 'binary', mime_types : [ 'audio/xm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.exr' ], format : 'binary', mime_types : [ 'image/aces' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.apng' ], format : 'binary', mime_types : [ 'image/apng' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.avif' ], format : 'binary', mime_types : [ 'image/avif' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.bmp' ], format : 'binary', mime_types : [ 'image/bmp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.cgm' ], format : 'binary', mime_types : [ 'image/cgm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.drle' ], format : 'binary', mime_types : [ 'image/dicom-rle' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.emf' ], format : 'binary', mime_types : [ 'image/emf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fits' ], format : 'binary', mime_types : [ 'image/fits' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.g3' ], format : 'binary', mime_types : [ 'image/g3fax' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.gif' ], format : 'binary', mime_types : [ 'image/gif' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.heic' ], format : 'binary', mime_types : [ 'image/heic' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.heics' ], format : 'binary', mime_types : [ 'image/heic-sequence' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.heif' ], format : 'binary', mime_types : [ 'image/heif' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.heifs' ], format : 'binary', mime_types : [ 'image/heif-sequence' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.hej2' ], format : 'binary', mime_types : [ 'image/hej2k' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.hsj2' ], format : 'binary', mime_types : [ 'image/hsj2' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ief' ], format : 'binary', mime_types : [ 'image/ief' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jls' ], format : 'binary', mime_types : [ 'image/jls' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jp2', '.jpg2' ], format : 'binary', mime_types : [ 'image/jp2' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jpeg', '.jpg', '.jpe' ], format : 'binary', mime_types : [ 'image/jpeg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jph' ], format : 'binary', mime_types : [ 'image/jph' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jhc' ], format : 'binary', mime_types : [ 'image/jphc' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jpm' ], format : 'binary', mime_types : [ 'image/jpm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jpx', '.jpf' ], format : 'binary', mime_types : [ 'image/jpx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxr' ], format : 'binary', mime_types : [ 'image/jxr' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxra' ], format : 'binary', mime_types : [ 'image/jxra' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxrs' ], format : 'binary', mime_types : [ 'image/jxrs' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxs' ], format : 'binary', mime_types : [ 'image/jxs' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxsc' ], format : 'binary', mime_types : [ 'image/jxsc' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxsi' ], format : 'binary', mime_types : [ 'image/jxsi' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jxss' ], format : 'binary', mime_types : [ 'image/jxss' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ktx' ], format : 'binary', mime_types : [ 'image/ktx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ktx2' ], format : 'binary', mime_types : [ 'image/ktx2' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.png' ], format : 'binary', mime_types : [ 'image/png' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.btif' ], format : 'binary', mime_types : [ 'image/prs.btif' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pti' ], format : 'binary', mime_types : [ 'image/prs.pti' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sgi' ], format : 'binary', mime_types : [ 'image/sgi' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.svg', '.svgz' ], format : 'binary', mime_types : [ 'image/svg+xml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.t38' ], format : 'binary', mime_types : [ 'image/t38' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.tif', '.tiff' ], format : 'binary', mime_types : [ 'image/tiff' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.tfx' ], format : 'binary', mime_types : [ 'image/tiff-fx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.psd' ], format : 'binary', mime_types : [ 'image/vnd.adobe.photoshop' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.azv' ], format : 'binary', mime_types : [ 'image/vnd.airzip.accelerator.azv' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvi', '.uvvi', '.uvg', '.uvvg' ], format : 'binary', mime_types : [ 'image/vnd.dece.graphic' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.djvu', '.djv' ], format : 'binary', mime_types : [ 'image/vnd.djvu' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sub' ], format : 'binary', mime_types : [ 'image/vnd.dvb.subtitle' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dwg' ], format : 'binary', mime_types : [ 'image/vnd.dwg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dxf' ], format : 'binary', mime_types : [ 'image/vnd.dxf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fbs' ], format : 'binary', mime_types : [ 'image/vnd.fastbidsheet' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fpx' ], format : 'binary', mime_types : [ 'image/vnd.fpx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fst' ], format : 'binary', mime_types : [ 'image/vnd.fst' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mmr' ], format : 'binary', mime_types : [ 'image/vnd.fujixerox.edmics-mmr' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.rlc' ], format : 'binary', mime_types : [ 'image/vnd.fujixerox.edmics-rlc' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ico' ], format : 'binary', mime_types : [ 'image/vnd.microsoft.icon' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dds' ], format : 'binary', mime_types : [ 'image/vnd.ms-dds' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mdi' ], format : 'binary', mime_types : [ 'image/vnd.ms-modi' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wdp' ], format : 'binary', mime_types : [ 'image/vnd.ms-photo' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.npx' ], format : 'binary', mime_types : [ 'image/vnd.net-fpx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.b16' ], format : 'binary', mime_types : [ 'image/vnd.pco.b16' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.tap' ], format : 'binary', mime_types : [ 'image/vnd.tencent.tap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.vtf' ], format : 'binary', mime_types : [ 'image/vnd.valve.source.texture' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wbmp' ], format : 'binary', mime_types : [ 'image/vnd.wap.wbmp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xif' ], format : 'binary', mime_types : [ 'image/vnd.xiff' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pcx' ], format : 'binary', mime_types : [ 'image/vnd.zbrush.pcx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.webp' ], format : 'binary', mime_types : [ 'image/webp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wmf' ], format : 'binary', mime_types : [ 'image/wmf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.3ds' ], format : 'binary', mime_types : [ 'image/x-3ds' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ras' ], format : 'binary', mime_types : [ 'image/x-cmu-raster' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.cmx' ], format : 'binary', mime_types : [ 'image/x-cmx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fh', '.fhc', '.fh4', '.fh5', '.fh7' ], format : 'binary', mime_types : [ 'image/x-freehand' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ico' ], format : 'binary', mime_types : [ 'image/x-icon' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jng' ], format : 'binary', mime_types : [ 'image/x-jng' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sid' ], format : 'binary', mime_types : [ 'image/x-mrsid-image' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.bmp' ], format : 'binary', mime_types : [ 'image/x-ms-bmp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pcx' ], format : 'binary', mime_types : [ 'image/x-pcx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pic', '.pct' ], format : 'binary', mime_types : [ 'image/x-pict' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pnm' ], format : 'binary', mime_types : [ 'image/x-portable-anymap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pbm' ], format : 'binary', mime_types : [ 'image/x-portable-bitmap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pgm' ], format : 'binary', mime_types : [ 'image/x-portable-graymap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ppm' ], format : 'binary', mime_types : [ 'image/x-portable-pixmap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.rgb' ], format : 'binary', mime_types : [ 'image/x-rgb' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.tga' ], format : 'binary', mime_types : [ 'image/x-tga' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xbm' ], format : 'binary', mime_types : [ 'image/x-xbitmap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xpm' ], format : 'binary', mime_types : [ 'image/x-xpixmap' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xwd' ], format : 'binary', mime_types : [ 'image/x-xwindowdump' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.appcache', '.manifest' ], format : 'text', mime_types : [ 'text/cache-manifest' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ics', '.ifb' ], format : 'text', mime_types : [ 'text/calendar' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.csv' ], format : 'text', mime_types : [ 'text/csv' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.markdown', '.md', '.mdtext', '.mkd' ], format : 'text', mime_types : [ 'text/markdown', 'text/x-web-markdown' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mml' ], format : 'text', mime_types : [ 'text/mathml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mdx' ], format : 'text', mime_types : [ 'text/mdx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.n3' ], format : 'text', mime_types : [ 'text/n3' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.txt', '.text', '.conf', '.def', '.list', '.log', '.in', '.ini' ], format : 'text', mime_types : [ 'text/plain' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dsc' ], format : 'text', mime_types : [ 'text/prs.lines.tag' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.rtx' ], format : 'text', mime_types : [ 'text/richtext' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.rtf' ], format : 'text', mime_types : [ 'text/rtf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sgml', '.sgm' ], format : 'text', mime_types : [ 'text/sgml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.shex' ], format : 'text', mime_types : [ 'text/shex' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.slim', '.slm' ], format : 'text', mime_types : [ 'text/slim' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.spdx' ], format : 'text', mime_types : [ 'text/spdx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.stylus', '.styl' ], format : 'text', mime_types : [ 'text/stylus' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.tsv' ], format : 'text', mime_types : [ 'text/tab-separated-values' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.t', '.tr', '.roff', '.man', '.me', '.ms' ], format : 'text', mime_types : [ 'text/troff' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ttl' ], format : 'text', mime_types : [ 'text/turtle' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uri', '.uris', '.urls' ], format : 'text', mime_types : [ 'text/uri-list' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.vcard' ], format : 'text', mime_types : [ 'text/vcard' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.curl' ], format : 'text', mime_types : [ 'text/vnd.curl' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dcurl' ], format : 'text', mime_types : [ 'text/vnd.curl.dcurl' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mcurl' ], format : 'text', mime_types : [ 'text/vnd.curl.mcurl' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.scurl' ], format : 'text', mime_types : [ 'text/vnd.curl.scurl' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sub' ], format : 'text', mime_types : [ 'text/vnd.dvb.subtitle' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ged' ], format : 'text', mime_types : [ 'text/vnd.familysearch.gedcom' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fly' ], format : 'text', mime_types : [ 'text/vnd.fly' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.flx' ], format : 'text', mime_types : [ 'text/vnd.fmi.flexstor' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.gv' ], format : 'text', mime_types : [ 'text/vnd.graphviz' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.3dml' ], format : 'text', mime_types : [ 'text/vnd.in3d.3dml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.spot' ], format : 'text', mime_types : [ 'text/vnd.in3d.spot' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jad' ], format : 'text', mime_types : [ 'text/vnd.sun.j2me.app-descriptor' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wml' ], format : 'text', mime_types : [ 'text/vnd.wap.wml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wmls' ], format : 'text', mime_types : [ 'text/vnd.wap.wmlscript' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.vtt' ], format : 'text', mime_types : [ 'text/vtt' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.s', '.asm' ], format : 'text', mime_types : [ 'text/x-asm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.c', '.cc', '.cxx', '.cpp', '.h', '.hh', '.dic' ], format : 'text', mime_types : [ 'text/x-c' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.htc' ], format : 'text', mime_types : [ 'text/x-component' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.f', '.for', '.f77', '.f90' ], format : 'text', mime_types : [ 'text/x-fortran' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.hbs' ], format : 'text', mime_types : [ 'text/x-handlebars-template' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.java' ], format : 'text', mime_types : [ 'text/x-java-source' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.lua' ], format : 'text', mime_types : [ 'text/x-lua' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mkd' ], format : 'text', mime_types : [ 'text/x-markdown' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.nfo' ], format : 'text', mime_types : [ 'text/x-nfo' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.opml' ], format : 'text', mime_types : [ 'text/x-opml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.org' ], format : 'text', mime_types : [ 'text/x-org' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.p', '.pas' ], format : 'text', mime_types : [ 'text/x-pascal' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pde' ], format : 'text', mime_types : [ 'text/x-processing' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sass' ], format : 'text', mime_types : [ 'text/x-sass' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.scss' ], format : 'text', mime_types : [ 'text/x-scss' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.etx' ], format : 'text', mime_types : [ 'text/x-setext' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.sfv' ], format : 'text', mime_types : [ 'text/x-sfv' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ymp' ], format : 'text', mime_types : [ 'text/x-suse-ymp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uu' ], format : 'text', mime_types : [ 'text/x-uuencode' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.vcs' ], format : 'text', mime_types : [ 'text/x-vcalendar' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.vcf' ], format : 'text', mime_types : [ 'text/x-vcard' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.xml' ], format : 'text', mime_types : [ 'text/xml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.yaml', '.yml' ], format : 'text', mime_types : [ 'text/x-yaml', 'text/yaml' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.3gp', '.3gpp' ], format : 'binary', mime_types : [ 'video/3gpp' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.3g2' ], format : 'binary', mime_types : [ 'video/3gpp2' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.h261' ], format : 'binary', mime_types : [ 'video/h261' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.h263' ], format : 'binary', mime_types : [ 'video/h263' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.h264' ], format : 'binary', mime_types : [ 'video/h264' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.m4s' ], format : 'binary', mime_types : [ 'video/iso.segment' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jpgv' ], format : 'binary', mime_types : [ 'video/jpeg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.jpm', '.jpgm' ], format : 'binary', mime_types : [ 'video/jpm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mj2', '.mjp2' ], format : 'binary', mime_types : [ 'video/mj2' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ts' ], format : 'binary', mime_types : [ 'video/mp2t' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mp4', '.mp4v', '.mpg4' ], format : 'binary', mime_types : [ 'video/mp4' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mpeg', '.mpg', '.mpe', '.m1v', '.m2v' ], format : 'binary', mime_types : [ 'video/mpeg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.ogv' ], format : 'binary', mime_types : [ 'video/ogg' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.qt', '.mov' ], format : 'binary', mime_types : [ 'video/quicktime' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvh', '.uvvh' ], format : 'binary', mime_types : [ 'video/vnd.dece.hd' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvm', '.uvvm' ], format : 'binary', mime_types : [ 'video/vnd.dece.mobile' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvp', '.uvvp' ], format : 'binary', mime_types : [ 'video/vnd.dece.pd' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvs', '.uvvs' ], format : 'binary', mime_types : [ 'video/vnd.dece.sd' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvv', '.uvvv' ], format : 'binary', mime_types : [ 'video/vnd.dece.video' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.dvb' ], format : 'binary', mime_types : [ 'video/vnd.dvb.file' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fvt' ], format : 'binary', mime_types : [ 'video/vnd.fvt' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mxu', '.m4u' ], format : 'binary', mime_types : [ 'video/vnd.mpegurl' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.pyv' ], format : 'binary', mime_types : [ 'video/vnd.ms-playready.media.pyv' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.uvu', '.uvvu' ], format : 'binary', mime_types : [ 'video/vnd.uvvu.mp4' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.viv' ], format : 'binary', mime_types : [ 'video/vnd.vivo' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.webm' ], format : 'binary', mime_types : [ 'video/webm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.f4v' ], format : 'binary', mime_types : [ 'video/x-f4v' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.fli' ], format : 'binary', mime_types : [ 'video/x-fli' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.flv' ], format : 'binary', mime_types : [ 'video/x-flv' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.m4v' ], format : 'binary', mime_types : [ 'video/x-m4v' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mkv', '.mk3d', '.mks' ], format : 'binary', mime_types : [ 'video/x-matroska' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.mng' ], format : 'binary', mime_types : [ 'video/x-mng' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.asf', '.asx' ], format : 'binary', mime_types : [ 'video/x-ms-asf' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.vob' ], format : 'binary', mime_types : [ 'video/x-ms-vob' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wm' ], format : 'binary', mime_types : [ 'video/x-ms-wm' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wmv' ], format : 'binary', mime_types : [ 'video/x-ms-wmv' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wmx' ], format : 'binary', mime_types : [ 'video/x-ms-wmx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.wvx' ], format : 'binary', mime_types : [ 'video/x-ms-wvx' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.avi' ], format : 'binary', mime_types : [ 'video/x-msvideo' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.movie' ], format : 'binary', mime_types : [ 'video/x-sgi-movie' ], collection : 'RawDocumentation' },
				{ suffixes : [ '.smv' ], format : 'binary', mime_types : [ 'video/x-smv' ], collection : 'RawDocumentation' }
			]
		};
		
		const custom_config_filename = path.resolve(path.join(__dirname, '..', 'pkm_config.json'));

		const custom_config_stats = fs.statSync(custom_config_filename, { throwIfNoEntry : false });

		if(custom_config_stats !== undefined)
		{
			if(!custom_config_stats.isFile() && !custom_config_stats.isSymbolicLink())
			{
				const msg = custom_config_filename + ' is neither a regular file nor a symbolic link';
				console.error(msg);
				throw new Error(msg);
			}
			
			let custom_config_content;
			try
			{
				custom_config_content = fs.readFileSync(custom_config_filename, { encoding : 'utf8' });
				console.warn('PKM configuration read from ' + custom_config_filename);
			}
			catch(err)
			{
				const msg = 'Reading PKM configuration from ' + custom_config_filename + ',' + err.message;
				console.error(msg);
				throw new Error(msg);
			}

			let custom_config;
			try
			{
				custom_config = JSON.parse(custom_config_content);
			}
			catch(err)
			{
				const msg = 'In PKM configuration read from ' + custom_config_filename + ', ' + err.message;
				console.error(msg);
				throw new Error(msg);
			}
			
			if(custom_config.hasOwnProperty('schemas'))
			{
				const collection_names = Object.keys(global_config.collections);
				collection_names.forEach((collection_name) =>
				{
					if(custom_config.schemas.hasOwnProperty(collection_name))
					{
						const custom_schema = custom_config.schemas[collection_name];
						if(!global_config.schemas.hasOwnProperty(collection_name))
						{
							console.warn('WARNING! Global configuration has no schema for collection \'' + collection_name + '\'');
							global_config.schemas[collection_name] = {};
						}
						if(custom_schema.hasOwnProperty('file')) global_config.schemas[collection_name].file = custom_schema.file;
						if(custom_schema.hasOwnProperty('enable')) global_config.schemas[collection_name].enable = custom_schema.enable;
					}
				});
				
				delete custom_config.schemas;
			}

			if(custom_config.hasOwnProperty('project'))
			{
				if((typeof custom_config.project === 'object') && !Array.isArray(custom_config.project))
				{
					const project_metadata = Object.keys(global_config.project);
					project_metadata.forEach((metadata) =>
					{
						if(custom_config.project.hasOwnProperty(metadata))
						{
							const file = custom_config.project[metadata].file;
							
							if(typeof file === 'string')
							{
								global_config.project[metadata].file = file;
							}
							
							const files = custom_config.project[metadata].files;
							
							if(Array.isArray(files))
							{
								global_config.project[metadata].files = files;
							}
						}
					});
				}
				
				delete custom_config.project;
			}
			
			[ 'roles', 'project' ].forEach((key) =>
			{
				if(custom_config.hasOwnProperty(key))
				{
					delete custom_config[key];
				}
			});
			
			[ 'session_timeout', 'git_remote_timeout' ].forEach((key) =>
			{
				if(custom_config.hasOwnProperty(key))
				{
					if((typeof custom_config[key] !== 'string') && ((typeof custom_config[key] !== 'number') || (custom_config[key] < 0)))
					{
						const msg = 'In \'' + custom_config_filename + '\', ' + key + ' shall be either a number >= 0 or a string (e.g. "1 d", "1 h", "10 m" or "60 s")';
						console.error(msg);
						throw Error(msg);
					}
					
					if(typeof custom_config[key] === 'string')
					{
						// convert duration string to milliseconds
						let milliseconds = 0;
						let days = custom_config[key].match(/^(\d+)\s*[dD]$/);
						let hours = custom_config[key].match(/^(\d+)\s*[hH]$/);
						let minutes = custom_config[key].match(/^(\d+)\s*m$/);
						let secs = custom_config[key].match(/^(\d+)\s*s$/);
						if(days) milliseconds += parseInt(days[1]) * 86400000;
						if(hours) milliseconds += parseInt(hours[1]) * 3600000;
						if(minutes) milliseconds += parseInt(minutes[1]) * 60000;
						if(secs) milliseconds += parseInt(secs[1]) * 1000;
						custom_config[key] = milliseconds;
					}
				}
			});

			Object.assign(global_config, custom_config);
		}
		else
		{
			console.warn('WARNING! ' + custom_config_filename + ' does not exist');
				
			// configuration file is missing: create a configuration file with the built-in PKM configuration
			let custom_config = deep_copy(global_config);
			delete custom_config.secret;
			delete custom_config.collections;
			delete custom_config.file_collection_names;
			delete custom_config.roles;
			const project_metadata = Object.keys(custom_config.project);
			project_metadata.forEach((metadata) =>
			{
				Object.keys(custom_config.project[metadata]).forEach((key) =>
				{
					if(![ 'file', 'files' ].includes(key))
					{
						delete custom_config.project[metadata][key];
					}
				});
				if(!Object.keys(custom_config.project[metadata]).length)
				{
					delete custom_config.project[metadata];
				}
			});
			[ 'session_timeout', 'git_remote_timeout' ].forEach((key) =>
			{
				let secs = Math.floor(custom_config[key] / 1000);
				const days = Math.floor(secs / (24 * 60 * 60));
				if(days)
				{
					custom_config[key] = days + ' d';
					return;
				}
				const hours = Math.floor(secs / (60 * 60));
				if(hours)
				{
					custom_config[key] = hours + ' h';
					return;
				}
				const minutes = Math.floor(secs / 60);
				if(minutes)
				{
					custom_config[key] = minutes + ' m';
					return;
				}
				custom_config[key] = secs + ' s';
			});
			
			try
			{
				fs.writeFileSync(custom_config_filename, JSON.stringify(custom_config, null, 2).replace(/  /g, '\t'), { encoding : 'utf8' });
			}
			catch(custom_config_write_file_err)
			{
				console.warn('WARNING! Writing ' + custom_config_filename + ', got an error: ' + custom_config_write_file_err.message);
			}
			
			console.warn('WARNING! Using built-in configuration');
		}
		
		global_config.git_root_directory = path.resolve(path.isAbsolute(global_config.git_root_directory) ? global_config.git_root_directory : path.join(__dirname, '..', global_config.git_root_directory));
		
		if(global_config.secret_file !== undefined)
		{
			const secret_file = path.isAbsolute(global_config.secret_file) ? global_config.secret_file : path.join(__dirname, '..', global_config.secret_file);
			
			let secret_file_exists;
			try
			{
				fs.accessSync(secret_file, fs.constants.R_OK);
				secret_file_exists = true;
			}
			catch(err)
			{
				console.warn(err.message);
				secret_file_exists = false;
			}
			
			if(secret_file_exists)
			{
				try
				{
					const secret = fs.readFileSync(secret_file, { encoding : 'utf8' });
					console.warn('PKM secret read from ' + secret_file);
					global_config.secret = secret;
				}
				catch(err)
				{
					const msg = 'Reading PKM secret from ' + secret_file + ', ' + err.message;
					console.error(msg);
					throw new Error(msg);
				}
			}
			else
			{
				try
				{
					fs.writeFileSync(secret_file, global_config.secret, { encoding : 'utf8', mode : 0o600 });
					console.log('PKM secret written to ' + secret_file);
				}
				catch(err)
				{
					const msg = 'Writing PKM secret to ' + secret_file + ', ' + err.message;
					console.error(msg);
					throw new Error(msg);
				}
			}
		}
		if(global_config.secret.length < 32)
		{
			const msg = 'PKM secret is too short (shall be at least 32 bytes)';
			console.error(msg);
			throw new Error(msg);
		}

		return global_config;
	}

	static copyright_notice()
	{
		return 'Copyright (c) ' + PKM.info.copyright.year + ' ' + PKM.info.copyright.owners.join(', ') + '. License: ' + PKM.info.license.name + '.';
	}

	/** Get dummy document validator
	*
	* @return {function({Object})} a validator
	*/
	static get_dummy_document_validator()
	{
		const dummy_validator = (document) => true;
		dummy_validator.errors = new Error('this document is always valid');
		return dummy_validator;
	}

	/** Get Shared data path
	* @param {string} filename - filename
	*
	* @return {string} absolute path of shared data
	*/
	static get_shared_data_path(filename)
	{
		return path.isAbsolute(filename) ? filename : path.resolve(__dirname + '/../' + filename);
	}

	/** Get Shared data path
	* @param {string} filename - filename
	*
	* @return {string} absolute path of shared data
	*/
	get_shared_data_path(filename)
	{
		return PKM.get_shared_data_path(filename);
	}
	
	/** Get document validator
	*
	* @param {string} schema_path - path of schema file
	* @return {function({Object})} a validator
	*/
	static get_document_validator(schema_path)
	{
		try
		{
			const schema_content = fs.readFileSync(schema_path);
			let schema = JSON.parse(schema_content);
			//const ajv = new Ajv({ allErrors : true });
			let ajv = new Ajv.default({ allErrors: true, strict: false });
			// console.log("schema:", schema);
			return ajv.compile(schema);
		}
		catch(err)
		{
			console.warn('In Schema File \'' + schema_path + '\':' + err.message);
			const error = require('./error');
			throw new error.PKM_InternalServerError('In Schema File \'' + schema_path + '\':' + err.message)
		}
	}

	/** Validate a C document.
	*
	* If validation is enabled and a valid schema is present, these methods throw an error when document is not valid.
	*
	* @param {string} collection_name - collection name
	* @param {Object} document a document
	*/
	validate_document(collection_name, document)
	{
		if(!(this.document_validators[collection_name] || (this.document_validators[collection_name] = (((this.config.schemas[collection_name] !== undefined) && this.config.schemas[collection_name].enable && this.config.schemas[collection_name].file) ? PKM.get_document_validator(PKM.get_shared_data_path(this.config.schemas[collection_name].file)) : PKM.get_dummy_document_validator())))(document))
		{
			throw this.document_validators[collection_name].errors;
		}
	}

	/** Get predefined collection names
	 *
	 * @return Array.<string>
	 */
	static get_collection_names()
	{
		const collection_names = Object.keys(PKM.global_config.collections);
		return collection_names;
	}

	/** Get predefined collection names of PKM instance
	 *
	 * @return Array.<string>
	 */
	get_collection_names()
	{
		return PKM.get_collection_names();
	}

	/** Get predefined collection info
	 *
	 * @return Array.<string>
	 */
	static get_collection_info(collection_name)
	{
		return PKM.global_config.collections[collection_name];
	}
	
	/** Get predefined collection info
	 *
	 * @return Array.<string>
	 */
	get_collection_info(collection_name)
	{
		return PKM.get_collection_info(collection_name);
	}
	
	/** Get predefined file collection names
	 *
	 * @return Array.<string>
	 */
	static get_file_collection_names()
	{
		return PKM.global_config.file_collection_names;
	}

	/** Get predefined file collection names of PKM instance
	 *
	 * @return Array.<string>
	 */
	get_file_collection_names()
	{
		return PKM.get_file_collection_names();
	}
	
	/** Get predefined role names
	 *
	 * @return Array.<string>
	 */
	static get_role_names()
	{
		return Object.keys(PKM.global_config.roles.project);
	}

	/** Get predefined role names of PKM instance
	 *
	 * @return Array.<string>
	 */
	get_role_names()
	{
		return PKM.get_role_names();
	}
	
	/** get_git_file_system options
	 * 
	 * @typedef {Object} GetGitFileSystemOptions
	 * @property {boolean} debug - enable/disable debugging messages
	 * @property {Logger} logger - a logger (default: none)
	 */
	
	/** Get Git file system of a project
	 * @param {string} project_name - project name
	 * @param {GetGitFileSystemOptions} options - options
	 * @return {FileSystem} the Git file system for the project
	 */
	get_git_file_system(project_name, options)
	{
		return new FileSystem(path.join(this.config.git_root_directory, project_name), { ...options, debug : this.debug });
	}

	/** get_git_working_tree_file_system options
	 * 
	 * @typedef {Object} GetGitWorkingTreeFileSystemOptions
	 * @property {boolean} debug - enable/disable debugging messages
	 * @property {Logger} logger - a logger (default: none)
	 */
	
	/** Get Git working tree file system of a project
	 * @param {string} project_name - project name
	 * @param {GetGitWorkingTreeFileSystemOptions} options - options
	 * @return {FileSystem} the Git file system for the project
	 */
	get_git_working_tree_file_system(project_name, git_working_tree_directory, options)
	{
		return new FileSystem(path.join(this.config.git_root_directory, project_name, git_working_tree_directory), { ...options, debug : this.debug });
	}

/** Constructor
 *
 * @param {string} key - a key
 * @param {string} user_name - user's name
 * @param {Client} client - connected client instance
 * @param {PkmConfig} [config] - a Configuration (optional)
 */
	constructor(key, user_name, client, config)
	{
		this.LogChunkSizeThreshold = PKM.LogChunkSizeThreshold;
		this.key = key;
		this.user_name = user_name;
		this.client = client;
		this.session_timeout = false;
		this.acquired = 1;
		this.config = PKM.global_config;
		if(typeof config === 'object')
		{
			Object.keys(config).forEach((key) =>
			{
				if((config[key] !== undefined) && (key !== 'collections')) this.config[key] = config[key];
			});
		}
		this.crypt = new Crypt(this.config.secret);
		this.debug = this.config.debug;
		this.insert_annotations = insert_annotations;
		this.update_annotations = update_annotations;
		this.get_annotations = get_annotations;
		this.delete_annotations = delete_annotations;
		this.create_all_collections = create_all_collections;
		this.create_all_roles = create_all_roles;
		this.create_collection = create_collection;
		this.create_db = create_db;
		this.create_role = create_role;
		this.create_user = create_user;
		this.delete_source_files = delete_source_files;
		this.drop_all_collections = drop_all_collections;
		this.drop_all_roles = drop_all_roles;
		this.drop_all_users = drop_all_users;
		this.drop_collection = drop_collection;
		this.drop_db = drop_db;
		this.drop_role = drop_role;
		this.drop_user = drop_user;
		this.find_in_doc = find_in_doc;
		this.find_in_c_source = find_in_c_source;
		this.find_in_uml_class_diagrams = find_in_uml_class_diagrams;
		this.insert_source_files = insert_source_files;
		this.insert_update_source_files = insert_update_source_files;
		this.update_source_files = update_source_files;
		this.update_user_password = update_user_password;
		this.update_user_role = update_user_role;
		this.insert_testar_state_models = insert_testar_state_models;
		this.insert_testar_test_results = insert_testar_test_results;
		this.get_testar_test_results = get_testar_test_results;
		this.get_testar_state_models = get_testar_state_models;
		this.delete_testar_test_results = delete_testar_test_results;
		this.delete_testar_state_models = delete_testar_state_models;
		this.get_source_files = get_source_files;
		this.insert_update_uml_class_diagrams = insert_update_uml_class_diagrams;
		this.insert_uml_class_diagrams = insert_uml_class_diagrams;
		this.update_uml_class_diagrams = update_uml_class_diagrams;
		this.delete_uml_class_diagrams = delete_uml_class_diagrams;
		this.get_uml_class_diagrams = get_uml_class_diagrams;
		this.update_c_source_code = update_c_source_code;
		this.update_c_annotations = update_c_annotations;
		this.update_c_comments = update_c_comments;
		this.get_c_source_codes = get_c_source_codes;
		this.get_c_annotations = get_c_annotations;
		this.get_c_comments = get_c_comments;
		this.create_update_user = create_update_user;
		this.update_user = update_user;
		this.get_user = get_user;
		this.get_current_user = get_current_user;
		this.get_users = get_users;
		this.get_user_roles = get_user_roles;
		this.insert_update_documents = insert_update_documents;
		this.insert_documents = insert_documents;
		this.update_documents = update_documents;
		this.insert_update_documents_spanned = insert_update_documents_spanned;
		this.insert_documents_spanned = insert_documents_spanned;
		this.update_documents_spanned = update_documents_spanned;
		this.get_documents = get_documents;
		this.delete_documents = delete_documents;
		this.delete_documents_spanned = delete_documents_spanned;
		this.get_collection = get_collection;
		this.insert_update_java_source_code = insert_update_java_source_code;
		this.insert_java_source_code = insert_java_source_code;
		this.get_java_source_codes = get_java_source_codes;
		this.insert_uml_state_machines = insert_uml_state_machines;
		this.update_uml_state_machines = update_uml_state_machines;
		this.insert_update_uml_state_machines = insert_update_uml_state_machines;
		this.delete_uml_state_machines = delete_uml_state_machines;
		this.get_uml_state_machines = get_uml_state_machines;
		this.insert_java_annotations = insert_java_annotations;
		this.update_java_annotations = update_java_annotations;
		this.insert_update_java_annotations = insert_update_java_annotations;
		this.get_java_annotations = get_java_annotations;
		this.insert_uml_files = insert_uml_files;
		this.update_uml_files = update_uml_files;
		this.insert_update_uml_files = insert_update_uml_files;
		this.get_uml_files = get_uml_files;
		this.delete_uml_files = delete_uml_files;
		this.insert_update_files = insert_update_files;
		this.get_files = get_files;
		this.delete_files = delete_files;
		this.delete_files_spanned = delete_files_spanned;
		this.create_project = create_project;
		this.update_project = update_project;
		this.create_update_project = create_update_project;
		this.get_project = get_project;
		this.delete_project = delete_project;
		this.get_user_projects = get_user_projects;
		this.get_projects = get_projects;
		this.invalidate_dependent_documents = invalidate_dependent_documents;
		this.insert_compile_commands = insert_compile_commands;
		this.update_compile_commands = update_compile_commands;
		this.get_compile_commands = get_compile_commands;
		this.delete_compile_commands = delete_compile_commands;
		this.update_cpp_source_code = update_cpp_source_code;
		this.update_cpp_annotations = update_cpp_annotations;
		this.update_cpp_comments = update_cpp_comments;
		this.get_cpp_source_codes = get_cpp_source_codes;
		this.get_cpp_annotations = get_cpp_annotations;
		this.get_cpp_comments = get_cpp_comments;
		this.find_in_cpp_source = find_in_cpp_source;
		this.find_in_cpp_class = find_in_cpp_class;
		this.document_validators = {};
		this.get_java_comments = get_java_comments;
		this.insert_update_java_comments = insert_update_java_comments;
		this.insert_java_comments = insert_java_comments;
		this.update_java_comments = update_java_comments;
		this.update_java_source_code = update_java_source_code;
		this.count_documents = count_documents;
		this.count_documents_spanned = count_documents_spanned;
		this.get_documents_spanned = get_documents_spanned;
		this.get_source_codes = get_source_codes;
		this.get_source_code_annotations = get_source_code_annotations;
		this.get_source_code_comments = get_source_code_comments;
		this.insert_update_files_spanned = insert_update_files_spanned;
		this.get_files_spanned = get_files_spanned;
		this.insert_update_any_files = insert_update_any_files;
		this.insert_any_files = insert_any_files;
		this.update_any_files = update_any_files;
		this.get_any_files = get_any_files;
		this.delete_any_files = delete_any_files;
		this.insert_update_traceability_matrix = insert_update_traceability_matrix;
		this.insert_traceability_matrix = insert_traceability_matrix;
		this.update_traceability_matrix = update_traceability_matrix;
		this.get_traceability_matrix = get_traceability_matrix;
		this.delete_traceability_matrix = delete_traceability_matrix;
		this.insert_update_logs = insert_update_logs;
		this.insert_logs = insert_logs;
		this.update_logs = update_logs;
		this.get_logs = get_logs;
		this.delete_logs = delete_logs;
		this.insert_update_doc_files = insert_update_doc_files;
		this.insert_doc_files = insert_doc_files;
		this.update_doc_files = update_doc_files;
		this.get_doc_files = get_doc_files;
		this.delete_doc_files = delete_doc_files;
		this.delete_docs = delete_docs;
		this.get_docs = get_docs;
		this.insert_docs = insert_docs;
		this.insert_update_docs = insert_update_docs;
		this.update_docs = update_docs;
		this.delete_graphical_docs = delete_graphical_docs;
		this.get_graphical_docs = get_graphical_docs;
		this.insert_graphical_docs = insert_graphical_docs;
		this.insert_update_graphical_docs = insert_update_graphical_docs;
		this.update_graphical_docs = update_graphical_docs;
		this.find_in_java_source = find_in_java_source;
		this.find_in_java_class = find_in_java_class;
		this.insert_update_cves = insert_update_cves;
		this.insert_cves = insert_cves;
		this.update_cves = update_cves;
		this.get_cves = get_cves;
		this.delete_cves = delete_cves;
		this.insert_update_executable_binary_files = insert_update_executable_binary_files;
		this.insert_executable_binary_files = insert_executable_binary_files;
		this.update_executable_binary_files = update_executable_binary_files;
		this.get_executable_binary_files = get_executable_binary_files;
		this.delete_executable_binary_files = delete_executable_binary_files;
		this.delete_c_source_codes = delete_c_source_codes;
		this.delete_c_annotations = delete_c_annotations;
		this.delete_c_comments = delete_c_comments;
		this.delete_cpp_source_codes = delete_cpp_source_codes;
		this.delete_cpp_annotations = delete_cpp_annotations;
		this.delete_cpp_comments = delete_cpp_comments;
		this.delete_java_source_codes = delete_java_source_codes;
		this.delete_java_annotations = delete_java_annotations;
		this.delete_java_comments = delete_java_comments;
		this.delete_source_codes = delete_source_codes;
		this.delete_source_code_annotations = delete_source_code_annotations;
		this.delete_source_code_comments = delete_source_code_comments;
		this.delete_git_working_trees = delete_git_working_trees;
		this.get_git_working_trees = get_git_working_trees;
		this.insert_git_working_trees = insert_git_working_trees;
		this.insert_update_git_working_trees = insert_update_git_working_trees;
		this.update_git_working_trees = update_git_working_trees;
		this.git = git;
		this.get_git_config = get_git_config;
		this.update_git_config = update_git_config;
		this.get_git_files = get_git_files;
		this.insert_update_git_files = insert_update_git_files;
		this.update_git_files = update_git_files;
		this.insert_git_files = insert_git_files;
		this.delete_git_files = delete_git_files;
		this.delete_tools = delete_tools;
		this.get_tools = get_tools;
		this.insert_tools = insert_tools;
		this.insert_update_tools = insert_update_tools;
		this.update_tools = update_tools;
		this.delete_invocations = delete_invocations;
		this.get_invocations = get_invocations;
		this.insert_invocations = insert_invocations;
		this.insert_update_invocations = insert_update_invocations;
		this.update_invocations = update_invocations;
		this.insert_testar_settings = insert_testar_settings;
		this.update_testar_settings = update_testar_settings;
		this.insert_update_testar_settings = insert_update_testar_settings;
		this.get_testar_settings = get_testar_settings;
		this.delete_testar_settings = delete_testar_settings;
		this.insert_methodology_status = insert_methodology_status;
		this.update_methodology_status = update_methodology_status;
		this.insert_update_methodology_status = insert_update_methodology_status;
		this.get_methodology_status = get_methodology_status;
		this.delete_methodology_status = delete_methodology_status;
		this.insert_reviews = insert_reviews;
		this.update_reviews = update_reviews;
		this.insert_update_reviews = insert_update_reviews;
		this.get_reviews = get_reviews;
		this.delete_reviews = delete_reviews;
		this.postprocess = {};
		this.postprocess['sourcecodeC'] = postprocess_c_source_codes.bind(this);
		this.postprocess['annotationsACSL'] = postprocess_c_annotations.bind(this);
		this.postprocess['commentsC'] = postprocess_c_comments.bind(this);
		this.postprocess['sourcecodeCPP'] = postprocess_cpp_source_codes.bind(this);
		this.postprocess['annotationsACSLPP'] = postprocess_cpp_annotations.bind(this);
		this.postprocess['commentsCPP'] = postprocess_cpp_comments.bind(this);
		this.get_file_collection_names().forEach((collection_name) =>
		{
			this.postprocess[collection_name] = postprocess_files.bind(this);
		});
		this.postprocess['Logs'] = postprocess_logs.bind(this);
		this.has_postprocess = {};
		this.has_postprocess['sourcecodeC'] = has_postprocess_c_source_codes.bind(this);
		this.has_postprocess['annotationsACSL'] = has_postprocess_c_annotations.bind(this);
		this.has_postprocess['commentsC'] = has_postprocess_c_comments.bind(this);
		this.has_postprocess['sourcecodeCPP'] = has_postprocess_cpp_source_codes.bind(this);
		this.has_postprocess['annotationsACSLPP'] = has_postprocess_cpp_annotations.bind(this);
		this.has_postprocess['commentsCPP'] = has_postprocess_cpp_comments.bind(this);
		this.get_file_collection_names().forEach((collection_name) =>
		{
			this.has_postprocess[collection_name] = () => true;
		});
		this.has_postprocess['Logs'] = () => true;
		this.preprocess = {};
		this.preprocess['sourcecodeC'] = preprocess_c_source_codes.bind(this);
		this.preprocess['annotationsACSL'] = preprocess_c_annotations.bind(this);
		this.preprocess['commentsC'] = preprocess_c_comments.bind(this);
		this.preprocess['sourcecodeCPP'] = preprocess_cpp_source_codes.bind(this);
		this.preprocess['annotationsACSLPP'] = preprocess_cpp_annotations.bind(this);
		this.preprocess['commentsCPP'] = preprocess_cpp_comments.bind(this);
		this.preprocess['Logs'] = preprocess_logs.bind(this);
		this.preprocess['Reviews'] = preprocess_reviews.bind(this);
		this.get_file_collection_names().forEach((collection_name) =>
		{
			this.preprocess[collection_name] = preprocess_files.bind(this);
		});
		if(this.key)
		{
			pkms.set(this.key, this);
			this.keep_alive_until_session_timeout();
		}
	}

/** Acquire a PKM instance
 *
 */
	acquire()
	{
		if(this.key && pkms.has(this.key))
		{
			if(this.debug)
			{
				console.log('acquired PKM with key \'' + this.key + '\'');
			}
			var pkm = pkms.get(this.key);
			++pkm.acquired;
		}
	}

/** Release a PKM instance
 *
 */
	release()
	{
		if(this.key && pkms.has(this.key))
		{
			var pkm = pkms.get(this.key);
			if(pkm.acquired)
			{
				--pkm.acquired;
				if(this.debug)
				{
					console.log('released PKM with key \'' + this.key + '\'');
				}
			}
			else
			{
				console.warn('can\'t release an unacquired PKM with key \'' + this.key + '\'');
			}
			if(pkm.session_timeout)
			{
				console.log('session timeout for PKM with key \'' + this.key + '\': closing connection');
				pkm.close();
			}
		}
	}

/** Close a PKM instance
 */
	close()
	{
		if(this.key && pkms.has(this.key))
		{
			if(this.session_timeout_id !== undefined)
			{
				clearTimeout(this.session_timeout_id);
				this.session_timeout_id = undefined;
			}
			if(this.debug)
			{
				console.log('closing PKM for key \'' + this.key + '\'');
			}
			pkms.delete(this.key);
			this.client.release();
		}
	}

/** Initialize
 *
 * @param {string} superuser_name - MongoDB "superuser" that can create a "root"
 * @param {string} superuser_auth_db - superuser authentication database
 * @param {string} superuser_password - superuser password
 * @param {User} pkm_admin_user - PKM administrator user
 * @param {PkmConfig} [config] - a Configuration (optional)
 * @return {Promise} a promise
 */
	static initialize(superuser_name, superuser_auth_db, superuser_password, pkm_admin_user, config)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = (!(config === undefined) && !(config.debug === undefined)) ? config.debug : PKM.global_config.debug;
			const db_host = (config && config.db_host) || PKM.global_config.db_host;
			const git_root_directory = (config && config.git_root_directory && path.resolve(config.git_root_directory)) || PKM.global_config.git_root_directory;
			const url = 'mongodb://' + encodeURIComponent(superuser_name) + ':' + encodeURIComponent(superuser_password) + '@' + db_host;
			const options =
			{
				authSource : superuser_auth_db,
				authMechanism : 'SCRAM-SHA-1',
				useUnifiedTopology: true,
				useNewUrlParser: true
			};

			const client = new Client(url, options);

			if(debug)
			{
				console.log('connecting to \'' + url + '\' authenticating with Database \'' + superuser_auth_db + '\'');
			}

			client.connect().then(() =>
			{
				const pkm = new PKM(null, superuser_name, client, config);

				if(debug)
				{
					console.log('successful login');
				}

				if(debug) console.log('Creating Git root directory at ' + git_root_directory);
				fs.mkdir(git_root_directory, { recursive : true }, (mkdir_err) =>
				{
					if(mkdir_err)
					{
						reject(mkdir_err);
					}
					else
					{
						if(debug) console.log('Created Git root Directory at ' + git_root_directory);
							
						pkm.get_collection(pkm.config.pkm_db, 'magic', { strict : true }).then((magic) =>
						{
							if(debug)
							{
								console.log('PKM already initialized');
							}
							resolve();
						}).catch((err) =>
						{
							if(debug)
							{
								console.log('PKM not yet initialized');
							}
							
							var create_role_promises = [];
							Object.keys(pkm.config.roles.pkm).forEach((role_name) =>
							{
								const role = deep_copy(pkm.config.roles.pkm[role_name]);

								role.role = role_name;
								role.privileges.forEach((privilege, privilege_index, privileges) =>
								{
									if((privilege.resource.collection !== undefined) && (privilege.resource.db === undefined))
									{
										privileges[privilege_index].resource.db = pkm.config.pkm_db;
									}
								});

								create_role_promises.push(pkm.create_role(pkm.config.pkm_db, role));
							});

							Promise.all(create_role_promises).then(() =>
							{
								if(debug)
								{
									console.log('Roles ' + Object.keys(pkm.config.roles.pkm).map(role_name => '\'' + role_name + '\'').join(', ') + ' created in \'' + pkm.config.pkm_db + '\'');
								}
								pkm_admin_user.grant('admin', 'root');
								pkm.create_user(pkm_admin_user).then(() =>
								{
									if(debug)
									{
										console.log('\'' + pkm_admin_user.name + '\' user created in \'' + pkm.config.pkm_db + '\'');
									}
									pkm.create_collection(pkm.config.pkm_db, 'Projects').then((projects) =>
									{
										if(debug)
										{
											console.log('\'Projects\' collection created in \'' + pkm.config.pkm_db + '\'');
										}
										pkm.create_collection(pkm.config.pkm_db, 'magic').then((magic) =>
										{
											if(debug)
											{
												console.log('PKM initialized');
											}
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									}).catch((err) =>
									{
										reject(err);
									});
								}).catch((err) =>
								{
									reject(err);
								});
							}).catch((err) =>
							{
								reject(err);
							});
						});
					}
				});
			}).catch((err) =>
			{
				reject(err);
			});
		});
	}

/** Finalize
 *
 * @param {string} superuser_name - MongoDB "superuser" that can create a "root"
 * @param {string} superuser_auth_db - superuser authentication database
 * @param {string} superuser_password - superuser password
 * @param {PkmConfig} [config] - a Configuration (optional)
 * @return {Promise} a promise
 */
	static finalize(superuser_name, superuser_auth_db, superuser_password, config)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = (!(config === undefined) && !(config.debug === undefined)) ? config.debug : PKM.global_config.debug;
			const db_host = (config && config.db_host) || PKM.global_config.db_host;
			const git_root_directory = (config && config.git_root_directory && path.resolve(config.git_root_directory)) || PKM.global_config.git_root_directory;
			const url = 'mongodb://' + encodeURIComponent(superuser_name) + ':' + encodeURIComponent(superuser_password) + '@' + db_host;
			const options =
			{
				authSource : superuser_auth_db,
				authMechanism : 'SCRAM-SHA-1',
				useUnifiedTopology: true,
				useNewUrlParser: true
			};

			const client = new Client(url, options);

			if(debug)
			{
				console.log('connecting to \'' + url + '\' authenticating with Database \'' + superuser_auth_db + '\'');
			}

			client.connect().then(() =>
			{
				const pkm = new PKM(null, superuser_name, client, config);

				if(debug)
				{
					console.log('successful login');
				}

				pkm.get_collection(pkm.config.pkm_db, 'magic').then((magic) =>
				{
					if(debug)
					{
						console.log('PKM found');
					}

					pkm.get_projects().then((projects) =>
					{
						var delete_project_promises = [];

						projects.forEach((project) =>
						{
							if(debug)
							{
								console.log('Deleting Project \'' + project.name + '\'');
							}
							projects.push(pkm.delete_project(project.name));
						});

						Promise.all(delete_project_promises).then(() =>
						{
							if(debug)
							{
								console.log('All projects deleted');
							}
							pkm.drop_db(pkm.config.pkm_db).then(() =>
							{
								if(debug)
								{
									console.log('Dropping all users');
								}
								pkm.drop_all_users(pkm.config.pkm_db).then(() =>
								{
									if(debug)
									{
										console.log('All users dropped');
									}
									
									if(debug)
									{
										console.log('Deleting Git root directory at ' + git_root_directory);
									}
									
									fs.rmdir(git_root_directory, { maxRetries : 5, recursive : true }, (rmdir_err) =>
									{
										if(rmdir_err)
										{
											reject(rmdir_err);
										}
										else
										{
											if(debug)
											{
												console.log('Deleted Git root file directory at ' + git_root_directory);
											}
											
											if(debug)
											{
												console.log('PKM finalized');
											}
											resolve();
										}
									});
								}).catch((err) =>
								{
									reject(err);
								});
							}).catch((err) =>
							{
								reject(err);
							});
						}).catch((err) =>
						{
							reject(err);
						});
					}).catch((err) =>
					{
						reject(err);
					});
				}).catch((err) =>
				{
					reject(err);
				});
			}).catch((err) =>
			{
				reject(err);
			});
		});
	}

/** Login
 *
 * @param {string} user_name - User name
 * @param {string} user_password - User password
 * @param {PkmConfig} config - a Configuration
 * @return {Promise<PKM>} a promise
 */
	static login(user_name, user_password, config)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = ((config !== undefined) && (config.debug !== undefined)) ? config.debug : PKM.global_config.debug;
			const secret = ((config !== undefined) && config.secret) || PKM.global_config.secret;
			const key = KeyGenerator.generate(secret);
			const pkm_db = (config && config.pkm_db) || PKM.global_config.pkm_db;
			const db_host = (config && config.db_host) || PKM.global_config.db_host;

			const url = 'mongodb://' + encodeURIComponent(user_name) + ':' + encodeURIComponent(user_password) + '@' + db_host;
			const options =
			{
				authSource : pkm_db,
				authMechanism : 'SCRAM-SHA-1',
				useUnifiedTopology: true,
				useNewUrlParser: true
			};

			const client = new Client(url, options);

			if(debug)
			{
				console.log('connecting to \'' + url + '\' authenticating with Database \'' + pkm_db + '\'');
			}

			client.connect().then(() =>
			{
				if(debug)
				{
					console.log('successful login');
					console.log('key is \'' + key + '\`');
				}
				resolve(new PKM(key, user_name, client, config));
			}).catch((err) =>
			{
				const error = require('./error');
				reject(new error.PKM_Unauthorized(debug ? err : null));
			});
		});
	}

/** Logout
 *
 * @param {string} key - a key (valid or invalid)
 * @param {PkmConfig} [config] - a Configuration (optional)
 * @return {Promise} a promise
 */
	static logout(key, config)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = (!(config === undefined) && !(config.debug === undefined)) ? config.debug : PKM.global_config.debug;
			const secret = ((config !== undefined) && config.secret) || PKM.global_config.secret;

			try
			{
				KeyGenerator.verify(key, secret);
				// good key
				if(pkms.has(key))
				{
					var pkm = pkms.get(key);
					pkm.close();
					resolve();
					return;
				}

				const error = require('./error');
				reject(new error.PKM_Unauthorized(debug ? 'invalid key' : null));
			}
			catch(err)
			{
				// bad key
				const error = require('./error');
				reject(new error.PKM_Unauthorized(debug ? err : null));
			}
		});
	}

/** Access
 *
 * @param {string} key - a key (valid or invalid)
 * @param {PkmConfig} [config] - a Configuration (optional)
 * @return {Promise<PKM>} a promise
 */
	static access(key, config)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = (!(config === undefined) && !(config.debug === undefined)) ? config.debug : PKM.global_config.debug;
			const secret = ((config !== undefined) && config.secret) || PKM.global_config.secret;

			try
			{
				if(debug)
				{
					console.log('trying access with key \'' + key + '\`');
				}
				KeyGenerator.verify(key, secret);
				// good key
				if(pkms.has(key))
				{
					var pkm = pkms.get(key);
					pkm.acquire();
					pkms.set(key, pkm);
					pkm.keep_alive_until_session_timeout();
					if(debug)
					{
						console.log('Access on behalf of \'' + pkm.user_name + '\'');
					}
					resolve(pkm);
					return;
				}

				const error = require('./error');
				reject(new error.PKM_Unauthorized(debug ? 'invalid key' : null));
			}
			catch(err)
			{
				// bad key
				const error = require('./error');
				reject(new error.PKM_Unauthorized(debug ? err : null));
			}
		});
	}

/** Duplicate PKM user's session
 *
 * @param {PKM} pkm - an instance of class PKM
 * @return {PKM} a new instance of class PKM
 */
	static from(pkm)
	{
		const key = KeyGenerator.generate(pkm.config.secret);
		pkm.client.acquire();
		return new PKM(key, pkm.user_name, pkm.client, pkm.config);
	}
	
	/** Revoke the PKM management roles from a user.
	 * This function hides PKM management roles that are internal to the implementation.
	 *
	 * @param {User} user - a user
	 * @return {User} a user
	 */
	revoke_pkm_management_roles(user)
	{
		user.revoke(this.config.pkm_db);

		return user;
	}

	/** Grant the PKM management roles to a user.
	 * This function restores PKM management roles that are internal to the implementation.
	 * The returned user instance is ready for creating/updating user in the PKM management database.
	 *
	 * @param {User} user - a user
	 * @return {User} a user ready for creating/updating in the PKM management database
	 */
	grant_pkm_management_roles(user)
	{
		// set 'User' role in PKM management database
		user.grant_user_role(this.config.pkm_db, 'User');

		return user;
	}

	/** Create an instance of PKM_Error from a foreign error or a bare string, and log the error
	 *
	 * @param {(Object|string)} err - an error
	 * @return {PKM_Error} a PKM_Error instance
	 */
	Error(err)
	{
		if(err instanceof error.PKM_Error)
		{
			return err;
		}

		const pkm_err = error.PKM_Error.from(err, this.debug);
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	BadRequest(err)
	{
		const pkm_err = this.Error(error.PKM_BadRequest.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	Unauthorized(err)
	{
		const pkm_err = this.Error(error.PKM_Unauthorized.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	Forbidden(err)
	{
		const pkm_err = this.Error(error.PKM_Forbidden.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	NotFound(err)
	{
		const pkm_err = this.Error(error.PKM_NotFound.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	Conflict(err)
	{
		const pkm_err = this.Error(error.PKM_Conflict.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	InternalServerError(err)
	{
		const pkm_err = this.Error(error.PKM_InternalServerError.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}

	NotImplemented(err)
	{
		const pkm_err = this.Error(error.PKM_NotImplemented.from(err, this.debug));
		if(this.debug)
		{
			console.warn(pkm_err);
		}
		return pkm_err;
	}
	
	/** Get type attribute key of a document according to the collection
	 * 
	 * @param {string} collection_name - collection name
	 * @return {string} a type key value
	 */
	static get_type_key(collection_name)
	{
		return (PKM.get_file_collection_names().find(name => collection_name === name) !== undefined) ? 'fileType' : 'type';
	}
	
	/** Get type attribute value of a document according to the collection
	 * 
	 * @param {string} collection_name - collection name
	 * @return {string} a type attribute value
	 */
	static get_type_value(collection_name)
	{
		return PKM.global_config.types[collection_name];
	}
	
	/** Guess file attribute (type, MIME type, collection) based on MIME type, filename suffix and magic number
	 * 
	 * @param {File} file - a file look-alike (either an instance of class File or a file from the database)
	 * @param {string} attribute - attribute to guess (either mime_type, type or collection)
	 * @return {string} the guessed attribute 
	 */
	guess_file_attribute(file, attribute)
	{
		if(Array.isArray(this.config.file_types))
		{
			for(let i = 0; i < this.config.file_types.length; ++i)
			{
				const file_type = this.config.file_types[i];
				if(Array.isArray(file_type.mime_types) && file_type.mime_types.length)
				{
					if((file.mime_type && file_type.mime_types.includes(file.mime_type)) || (file.fileMimeType && file_type.mime_types.includes(file.fileMimeType)))
					{
						const attribute_value = (attribute == 'mime_type') ? file_type.mime_types[0] : file_type[attribute];
						if((attribute != 'collection') || this.get_collection_names().includes(attribute_value))
						{
							return attribute_value;
						}
					}
				}
				
				if((attribute != 'mime_type') || (Array.isArray(file_type.mime_types) && file_type.mime_types.length))
				{
					if(Array.isArray(file_type.suffixes))
					{
						for(let j = 0; j < file_type.suffixes.length; ++j)
						{
							const suffix = file_type.suffixes[j];
							if(typeof suffix === 'string')
							{
								if((file.rel_path && file.rel_path.toLowerCase().endsWith(suffix.toLowerCase())) || (file.filename && file.filename.toLowerCase().endsWith(suffix.toLowerCase())))
								{
									const attribute_value = (attribute == 'mime_type') ? file_type.mime_types[0] : file_type[attribute];
									if((attribute != 'collection') || this.get_collection_names().includes(attribute_value))
									{
										return attribute_value;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(file.fileFormat)
		{
			if(file.fileFormat == 'binary')
			{
				if(attribute == 'collection')
				{
					const buf = Buffer.from(file.filecontent, 'base64');
					if((buf.length >= 4) &&
					   (buf[0] == 0x7f) &&
					   (buf[1] == 'E') &&
					   (buf[2] == 'L') &&
					   (buf[3] == 'F'))
					{
						return 'RawBinaries';
					}
				
					if(attribute == 'mime_type')
					{
						return 'application/octet-stream';
					}
				}
			}
			else
			{
				if(attribute == 'mime_type')
				{
					return 'plain/text';
				}
			}
		}
		
		if(Buffer.isBuffer(file.content))
		{
			if((attribute == 'collection') &&
			   (file.content.length >= 4) &&
			   (file.content[0] == 0x7f) &&
			   (file.content[1] == 'E') &&
			   (file.content[2] == 'L') &&
			   (file.content[3] == 'F'))
			{
				return 'RawBinaries';
			}
			
			if(attribute == 'mime_type')
			{
				return 'application/octet-stream';
			}
		}
		else
		{
			if(attribute == 'mime_type')
			{
				return 'plain/text';
			}
		}
		
		return '';
	}
	
	/** Fix some attributes (type and mime_type) of a file
	 * 
	 * @param {File} a file (an instance of class File)
	 * @return {File} the fixed file
	 */
	fix_file(file)
	{
		if(Buffer.isBuffer(file.content))
		{
			if(this.guess_file_attribute(file, 'format') == 'text')
			{
				// binary -> text
				if(this.debug)
				{
					console.log('convert ' + file.rel_path + ' to text format');
				}
				file.content = file.content.toString('utf8');
				file.encoding = 'utf8';
			}
		}
		if(!file.type)
		{
			const collection = this.guess_file_attribute(file, 'collection');
			if(collection)
			{
				file.type = PKM.get_type_value(collection);
			}
		}
		if(!file.mime_type) file.mime_type = this.guess_file_attribute(file, 'mime_type');
		return file;
	}
	
	/** Dispatch a file based on MIME type, filename suffix and magic number
	 * 
	 * @param {File} file - a file look-alike
	 * @return {string} collection name
	 */
	dispatch_file(file)
	{
		const collection = this.guess_file_attribute(file, 'collection');
		if(!collection)
		{
			throw 'cannot dispatch \'' + (file.rel_path || file.filename) + '\' to collections ' + this.get_file_collection_names().join(', ');
		}
		
		return collection;
	}
	
	/** Test file compatibility with PKM based on MIME type, filename suffix and magic number
	 * 
	 * @param {File} file - a file look-alike
	 * @return {boolean} test result
	 */
	test_file_compatibility(file)
	{
		return !!this.guess_file_attribute(file, 'collection');
	}
	
	/** Fix document
	 * 
	 * @param {Object} document - document
	 * @param {string} collection_name - collection name
	 * @return {Object} fixed document
	 */
	static fix_document(document, collection_name)
	{
		const type_key = PKM.get_type_key(collection_name);
		const type_value = PKM.get_type_value(collection_name);
		if((type_value !== undefined) && (!document.hasOwnProperty(type_key) || !document[type_key]))
		{
			document[type_key] = type_value;
		}
		
		return document;
	}
	
	/** Fix document
	 * 
	 * @param {Object} document - document
	 * @param {string} collection_name - collection name
	 * @return {Object} fixed document
	 */
	fix_document(document, collection_name)
	{
		return PKM.fix_document(document, collection_name);
	}
	
	/** Keep alive PKM instance until session timeout
	 */
	keep_alive_until_session_timeout()
	{
		if(this.session_timeout_id !== undefined)
		{
			clearTimeout(this.session_timeout_id);
			this.session_timeout_id = undefined;
			if(this.debug)
			{
				console.log('session timeout handler removed');
			}
		}
		
		if(this.config.session_timeout)
		{
			this.session_timeout_id = setTimeout(function(key)
			{
				if(pkms.has(key))
				{
					var pkm = pkms.get(key);
					if(pkm.acquired)
					{
						if(pkm.debug)
						{
							console.log('got session timeout for PKM with key \'' + pkm.key + '\': deferring connection close until pending access is finished');
						}
						pkm.session_timeout = true;
					}
					else
					{
						if(pkm.debug)
						{
							console.log('session timeout for PKM with key \'' + pkm.key + '\': closing connection');
						}
						pkm.close();
					}
				}
			}, this.config.session_timeout, this.key);
			if(this.debug)
			{
				console.log('session timeout handler set');
			}
		}
	}
}

module.exports = PKM;
