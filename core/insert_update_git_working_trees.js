/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update Git Working Tree documents
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<Object>} git_working_trees_documents - Git Working Tree documents
 * @param {boolean} update - flag to enable/disable replacing Git Working Tree documents
 * 
 * @return {Promise} a promise
 */
function insert_update_git_working_trees(dbName, git_working_trees_documents, update)
{
	return this.insert_update_documents(dbName, 'GitWorkingTrees', git_working_trees_documents, update, { signature : { directory : 1 } });
}

module.exports.insert_update_git_working_trees = insert_update_git_working_trees;
