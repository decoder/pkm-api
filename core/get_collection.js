/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** get_collection options
 * 
 * @typedef {Object} GetCollectionOptions
 * @property {boolean} strict - returns an error if the collection does not exist
 */

/**
 * Get a collection of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} collection_name - collection name
 * @param {GetCollectionOptions} [options] - options
 * 
 * @return {Promise<Object>} a promise
 */

function get_collection(dbName, collection_name, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;

		const db = this.client.db(dbName);
		
		((!options.strict) ? Promise.resolve() : new Promise((resolve, reject) =>
		{
			let collection_cursor = db.listCollections({ name : collection_name });
			
			let collection_exists = false;
			collection_cursor.forEach((document) =>
			{
				collection_exists = true;
			}, (err) =>
			{
				if(err)
				{
					reject(this.Error(err));
				}
				else if(collection_exists)
				{
					resolve();
				}
				else
				{
					reject(this.NotFound());
				}
			});
		})).then(() =>
		{
			db.collection(collection_name, {}, (err, collection) =>
			{
				if(err)
				{
					reject(this.Error(err));
				}
				else
				{
					resolve(collection);
				}
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_collection = get_collection;
