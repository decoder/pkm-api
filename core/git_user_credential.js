/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

/** Git User's credential
 */
class GitUserCredential
{
	/** constructor
	 * 
	 * @param {string} git_remote_url - Git remote URL
	 * @param {string} git_user_name - Git user's name
	 * @param {string} git_password - Git password (HTTPS password/token or SSH passphrase to unlock SSH private key)
	 * @param {string} git_ssh_private_key - Git SSH private key
	 */
	constructor(git_remote_url, git_user_name, git_password, git_ssh_private_key)
	{
		this.git_remote_url = git_remote_url;
		if(git_user_name !== undefined) this.git_user_name = git_user_name;
		if(git_password !== undefined) this.git_password = git_password;
		if(git_ssh_private_key !== undefined) this.git_ssh_private_key = git_ssh_private_key;
	}
	
	/** Build a GitUserCredential instance from an object, convenient for transportation with a transport protocol, that looks like a Git user's credential
	 * 
	 * @param {Object} obj - an object
	 * @return {GitUserCredential} an instance of GitUserCredential
	 */
	static from(obj)
	{
		return new GitUserCredential(
			obj.git_remote_url,
			obj.git_user_name,
			obj.git_password,
			obj.git_ssh_private_key
		);
	}
	
	/** Encrypt Git user credential (password and SSH private key)
	 * 
	 * @param {Crypt} crypt - a crypter
	 * @return {GitUserCredential} the encrypted Git user credential
	 */
	encrypt(crypt)
	{
		return new GitUserCredential(
			this.git_remote_url,
			this.git_user_name,
			this.git_password && crypt.encrypt(this.git_password),
			this.git_ssh_private_key && crypt.encrypt(this.git_ssh_private_key)
		);
	}
	
	/** Decrypt Git user credential (password and SSH private key)
	 * 
	 * @param {Crypt} crypt - a crypter
	 * @return {GitUserCredential} the decrypted Git user credential
	 */
	decrypt(crypt)
	{
		return new GitUserCredential(
			this.git_remote_url,
			this.git_user_name,
			this.git_password && crypt.decrypt(this.git_password),
			this.git_ssh_private_key && crypt.decrypt(this.git_ssh_private_key)
		);
	}
	
	/** Build a GitUserCredential instance from an object (with encrypted password and SSH private key), convenient for transportation with a transport protocol, that looks like a Git user's credential
	 * 
	 * @param {Object} obj - an object
	 * @param {Crypt} crypt - a crypter
	 * @return {GitUserCredential} the encrypted Git user credential
	 */
	static decrypt(obj, crypt)
	{
		return new GitUserCredential(
			obj.git_remote_url,
			obj.git_user_name,
			obj.git_password && crypt.decrypt(obj.git_password),
			obj.git_ssh_private_key && crypt.decrypt(obj.git_ssh_private_key)
		);
	}

	/** Merge with another instance of GitUserCredential provided that the other instance is about the same Git remote URL
	 * 
	 * @param {GitUserCredential} other - another instance of GitUserCredential
	 * @return {GitUserCredential} this
	 */
	merge(other)
	{
		if(this.git_remote_url == other.git_remote_url)
		{
			if(other.git_user_name !== undefined) this.git_user_name = other.git_user_name;
			if(other.git_password !== undefined) this.git_password = other.git_password;
			if(other.git_ssh_private_key !== undefined) this.git_ssh_private_key = other.git_ssh_private_key;
		}
		
		return this;
	}
}

module.exports = GitUserCredential;
