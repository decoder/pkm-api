/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Postprocess C++ Annotations
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} cpp_annotations_documents - C++ annotation documents
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<Object>>} a promise
 */
function postprocess_cpp_annotations(cpp_annotations_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const merge = (options !== undefined) && options.merge;
		
		if(merge)
		{
			let annotations_map = new Map();
			
			cpp_annotations_documents.forEach((cpp_annotations_document) =>
			{
				if(annotations_map.has(cpp_annotations_document.sourceFile))
				{
					var tmp = annotations_map.get(cpp_annotations_document.sourceFile);
					tmp.annotations = tmp.annotations.concat(cpp_annotations_document.annotations);
					annotations_map.set(cpp_annotations_document.sourceFile, tmp);
				}
				else
				{
					var tmp =
					{
						type : cpp_annotations_document.type,
						sourceFile : cpp_annotations_document.sourceFile,
						annotations : cpp_annotations_document.annotations
					};
					annotations_map.set(cpp_annotations_document.sourceFile, tmp);
				}
			});
			
			const annotations_map_iterator = annotations_map.values();
			const merged_cpp_annotations_documents = Array.from(annotations_map_iterator, (cpp_annotations_document) => cpp_annotations_document);
			
			resolve(merged_cpp_annotations_documents);
		}
		else
		{
			resolve(cpp_annotations_documents);
		}
	}.bind(this));
}

module.exports.postprocess_cpp_annotations = postprocess_cpp_annotations;
