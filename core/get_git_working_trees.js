/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get Git Working Trees
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Object} [query] - query
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<GitWorkingTree>>} a promise
 */
function get_git_working_trees(dbName, query = {}, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const GitWorkingTree = require('./git_working_tree');
		
		this.get_documents(dbName, 'GitWorkingTrees', query, options).then((documents) =>
		{
			const git_working_trees = documents.map((document) => GitWorkingTree.from(document));
			
			resolve(git_working_trees);
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_git_working_trees = get_git_working_trees;
