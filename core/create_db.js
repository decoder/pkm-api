/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Create a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * 
 * @return {Promise} a promise
 */

function create_db(dbName)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		// get the informations about current user
		this.get_user(this.user_name).then((current_user) =>
		{
			// check that current user is administrator that will be able to create a new database
			if(current_user.is_admin())
			{
				// create all predefined collections
				this.create_all_collections(dbName).then(() =>
				{
					if(debug)
					{
						console.log('All predefined collections in Database \'' + dbName + '\' have been created');
					}
					// create all predefined roles
					this.create_all_roles(dbName).then(() =>
					{
						if(debug)
						{
							console.log('All predefined roles in Database \'' + dbName + '\' have been created');
						}
						
						// database has been created
						if(debug)
						{
							console.log('Database \'' + dbName + '\' has been created');
						}
						
						resolve();
					}).catch((err) =>
					{
						reject(this.Error(err));
					});
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				reject(this.Forbidden('User \'' + this.user_name + '\' can\'t create Database \'' + dbName + '\' because he\'s not an administrator'));
			}
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.create_db = create_db;
