/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Create all predefined roles
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * 
 * @return {Promise} a promise
 */

function create_all_roles(dbName)
{
	return new Promise(function(resolve, reject)
	{
		var create_role_promises = [];
		Object.keys(this.config.roles.project).forEach((role_name) =>
		{
			const { deep_copy } = require('../util/deep_copy');
			const role = deep_copy(this.config.roles.project[role_name]);
			
			role.role = role_name;
			role.privileges.forEach((privilege, privilege_index, privileges) =>
			{
				if(privilege.resource.db === undefined)
				{
					privileges[privilege_index].resource.db = dbName;
				}
			});
			
			create_role_promises.push(this.create_role(dbName, role));
		});
		
		Promise.all(create_role_promises).then(() =>
		{
			resolve();
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.create_all_roles = create_all_roles;
