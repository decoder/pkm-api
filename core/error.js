/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

const MongoError = require('mongodb').MongoError;
const { deep_copy } = require('../util/deep_copy');

/** A PKM error
 */
class PKM_Error extends Error
{
	/** constructor
	 * 
	 * @param {Number} code - error code
	 * @param {*} error - error data
	 */
	constructor(code, error)
	{
		super(error);
		this.code = code;
	}
	
	/** Create an instance of PKM_Error from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_Error} a PKM_Error instance
	 */
	static from(error, debug)
	{
		let error_copy = debug ? deep_copy(error) : null;
		if((error_copy !== null) && (error.message !== undefined))
		{
			error_copy.message = error.message;
		}
		
		if(error instanceof MongoError)
		{
			if(error.code !== undefined)
			{
				// see https://github.com/mongodb/mongo/blob/master/src/mongo/base/error_codes.yml
				switch(error.code)
				{
					case 13 /* Unauthorized */: return new PKM_Forbidden(error_copy);
					case 11 /* UserNotFound */:
					case 26 /* NamespaceNotFound */: return new PKM_NotFound(error_copy);
					case 48 /* NamespaceExists */:
					case 11000 /* DuplicateKey */:  return new PKM_Conflict(error_copy);
				}
			}
			
			if(typeof error.message === 'string')
			{
				if(error.message.includes('already exists')) return new PKM_Conflict(error_copy);
			}
		}
		
		return new PKM_InternalServerError(error_copy);
	}
}

/** A PKM bad request error
 */
class PKM_BadRequest extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(400, 'Bad Request' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}
	
	/** Create an instance of PKM_BadRequest from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_BadRequest} a PKM_BadRequest instance
	 */
	static from(error, debug)
	{
		return new PKM_BadRequest(debug ? deep_copy(error) : null);
	}
}

/** A PKM unauthorized access error
 */
class PKM_Unauthorized extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(401, 'Unauthorized' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}
	
	/** Create an instance of PKM_Unauthorized from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_Unauthorized} a PKM_Unauthorized instance
	 */
	static from(error, debug)
	{
		return new PKM_Unauthorized(debug ? deep_copy(error) : null);
	}
}

/** A PKM forbidden access error
 */
class PKM_Forbidden extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(403, 'Forbidden' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}
	
	/** Create an instance of PKM_Forbidden from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_Forbidden} a PKM_Forbidden instance
	 */
	static from(error, debug)
	{
		return new PKM_Forbidden(debug ? deep_copy(error) : null);
	}
}

/** A PKM not found error
 */
class PKM_NotFound extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(404, 'Not Found' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}
	
	/** Create an instance of PKM_NotFound from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_NotFound} a PKM_NotFound instance
	 */
	static from(error, debug)
	{
		return new PKM_NotFound(debug ? deep_copy(error) : null);
	}
}

/** A PKM conflict error
 */
class PKM_Conflict extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(409, 'Conflict' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}
	
	/** Create an instance of PKM_Conflict from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_Conflict} a PKM_Conflict instance
	 */
	static from(error, debug)
	{
		return new PKM_Conflict(debug ? deep_copy(error) : null);
	}
}

/** A PKM internal server error
 */
class PKM_InternalServerError extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(500, 'Internal Server Error' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}
	
	/** Create an instance of PKM_InternalServerError from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_InternalServerError} a PKM_InternalServerError instance
	 */
	static from(error, debug)
	{
		return new PKM_InternalServerError(debug ? deep_copy(error) : null);
	}
}

/** A PKM not implemented error
 */
class PKM_NotImplemented extends PKM_Error
{
	/** constructor
	 * 
	 * @param {*} error - error data
	 */
	constructor(error)
	{
		super(501, 'Not Implemented' + (error ? (':' + JSON.stringify(error, null, 2)) : ''));
	}

	/** Create an instance of PKM_NotImplemented from a foreign error or a bare string
	 * 
	 * @param {(Object|string)} error - an error
	 * @return {PKM_NotImplemented} a PKM_NotImplemented instance
	 */
	static from(error, debug)
	{
		return new PKM_NotImplemented(debug ? deep_copy(error) : null);
	}
}

module.exports =
{
	PKM_Error,
	PKM_BadRequest,
	PKM_Unauthorized,
	PKM_Forbidden,
	PKM_NotFound,
	PKM_Conflict,
	PKM_InternalServerError,
	PKM_NotImplemented
}
