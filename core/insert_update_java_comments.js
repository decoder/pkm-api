/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 *
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Insert/Update Java Annotations
 *
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} java_comments_documents - Java comments documents
 * @param {boolean} update - flag to enable/disable replacing Java comments documents
 *
 * @return {Promise} a promise
 */
function insert_update_java_comments(dbName, java_comments_documents, update)
{
	return this.insert_update_documents(dbName, 'commentsjava', java_comments_documents, update, { signature : { sourceFile : 1 } });
}

module.exports.insert_update_java_comments = insert_update_java_comments;
