/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get roles
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * 
 * @return {Promise<Array.<UserRole>>} a promise
 */

function get_user_roles(dbName)
{
	return new Promise(function(resolve, reject)
	{
		const db = this.client.db(dbName);

		db.command({ rolesInfo : 1, showBuiltinRoles: false }).then((result) =>
		{
			const UserRole = require('./user_role');
			var roles = [];
			result.roles.forEach((role_info) =>
			{
				roles.push(new UserRole(role_info.db, role_info.role));
			});
			resolve(roles);
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_user_roles = get_user_roles;
