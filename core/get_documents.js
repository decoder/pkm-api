/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get Documents in a generic manner from a collection of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} collection_name - collection name
 * @param {Object} [query] - query
 * @param {Object} [options] - options
 * 
 * @return {(Promise<Array.<Object>>|Promise<GetDocumentsCountMatchesResult>)} a promise
 */
function get_documents(dbName, collection_name, query = {}, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		if(debug)
		{
			console.log('PKM.get_documents(' + JSON.stringify(dbName) + ',' + JSON.stringify(collection_name) + ',' + JSON.stringify(query) + ',' + JSON.stringify(options) + ')');
		}
		const has_postprocess = (this.has_postprocess[collection_name] !== undefined) && this.has_postprocess[collection_name](options);
		
		const query_is_empty = Object.keys(query).length == 0;
		
		this.get_collection(dbName, collection_name).then((documents_collection) =>
		{
			let documents = [];
			
			let find_options = Object.assign({}, options);
			if(has_postprocess)
			{
				if(options.skip !== undefined) delete find_options.skip;
				if(options.limit !== undefined) delete find_options.limit;
				if(options.sort !== undefined) delete find_options.sort;
			}
			else
			{
				if((options.skip !== undefined) && !options.skip) delete find_options.skip;
				if((options.limit !== undefined) && !options.limit) delete find_options.limit;
			}
			if(debug)
			{
				const util = require('util');
				console.log('searching for '+ util.inspect(query, { showHidden: false, depth: null }) + ' with options ' + util.inspect(find_options, { showHidden: false, depth: null }) + ' in Collection \'' + collection_name + '\' of Database \'' + dbName + '\'');
			}
			const driver_slicing = find_options.skip || find_options.limit;
			
			var document_cursor = documents_collection.find(query, find_options);
			
			document_cursor.forEach((document) =>
			{
				documents.push(this.fix_document(document, collection_name));
			}, (err) =>
			{
				if(err)
				{
					reject(this.Error(err));
				}
				else
				{
					if(documents.length)
					{
						if(debug)
						{
							if(driver_slicing)
							{
								const start = options.skip || 0;
								const end = start + documents.length;
								console.log('After driver slicing [' + start + ',' + end + '[, found ' + documents.length + ' document(s) from collection \'' + collection_name + '\' of database \'' + dbName + '\'');
							}
							else
							{
								console.log('Found ' + documents.length + ' document(s) from collection \'' + collection_name + '\' of database \'' + dbName + '\'');
							}
						}
						
						const postprocess = this.postprocess[collection_name];
						const postprocess_slicing = has_postprocess && (options.skip || options.limit);
						((postprocess === undefined) ? Promise.resolve(documents) : new Promise((resolve, reject) =>
						{
							postprocess(documents, options).then((documents) =>
							{
								if(debug)
								{
									console.log('After postprocessing, found ' + documents.length + ' document(s) from collection \'' + collection_name + '\' of database \'' + dbName + '\'');
								}
								if(postprocess_slicing)
								{
									const start = options.skip || 0;
									const end = start + ((options.limit && (options.limit < documents.length)) ? options.limit : documents.length);
									documents = documents.slice(start, end);
									if(debug)
									{
										console.log('After slicing [' + start + ',' + end + '[, found ' + (end - start) + ' document(s) from collection \'' + collection_name + '\' of database \'' + dbName + '\'');
									}
								}
								
								resolve(documents);
								
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						})).then((documents) =>
						{
							if(debug && ((postprocess !== undefined) || postprocess_slicing))
							{
								console.log('Overall found ' + documents.length + ' document(s) from collection \'' + collection_name + '\' of database \'' + dbName + '\'');
							}
							
							if(documents.length || query_is_empty)
							{
								resolve(documents);
							}
							else
							{
								reject(this.NotFound());
							}
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}
					else if(query_is_empty)
					{
						resolve(documents);
					}
					else
					{
						reject(this.NotFound());
					}
				}
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_documents = get_documents;
