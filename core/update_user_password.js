/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Update user password
 * 
 * @memberof PKM
 * @instance
 * @param {string} new_password - new user password
 * 
 * @return {Promise} a promise
 */

function update_user_password(new_password)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const db = this.client.db(this.config.pkm_db);
		db.command({updateUser : this.user_name, pwd: new_password}).then(() =>
		{
			resolve();
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.update_user_password = update_user_password;
