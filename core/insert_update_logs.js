/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert/Update some Log documents into a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<Object>} log_documents - Log documents
 * @param {boolean} update - flag to enable/disable replacing Log documents
 * 
 * @return {Promise.<Array<string> >} a promise (resolve argument is an array of document unique IDs in the collection)
 */
function insert_update_logs(dbName, log_documents, update)
{
	return new Promise(function(resolve, reject)
	{
		// If the id field is present, convert it to a MongoDB Object ID, or allocate a new one
		const ObjectID = require('mongodb').ObjectID;
		
		try
		{
			log_documents.forEach((log_document) =>
			{
				try
				{
					log_document.id = (log_document.hasOwnProperty('id') ? new ObjectID(log_document.id) : new ObjectID()).toHexString();
				}
				catch(err)
				{
					throw this.BadRequest('id field is invalid: ' + err.message);
				}
			});
		}
		catch(err)
		{
			reject(this.Error(err));
			return;
		}

		this.insert_update_documents(dbName, 'Logs', log_documents, update, { signature : { id : 1 } }).then(() =>
		{
			resolve(log_documents.map((log_document) => log_document.id));
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.insert_update_logs = insert_update_logs;
