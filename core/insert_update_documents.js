/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** insert_update_documents options
 * 
 * @typedef {Object} InsertUpdateDocumentsOptions
 * @property {Object} [signature] - signature (properties in the signature are the document keys). When unspecified each document is unique whereas when an empty object {}, the collection holds only a singleton document.
 * @property {string} [custom_id_key] - custom ID key replicating the MongoDB Object ID field _id. This option applies only when the signature is undefined.
 */

/**
 * Insert/Update some documents in a generic manner into a collection of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} collection_name - collection name
 * @param {Array.<Object>} documents - documents
 * @param {boolean} update - flag to enable/disable replacing documents when signature is specified, otherwise ignored
 * @param {InsertUpdateDocumentsOptions} [options] - options
 * 
 * @return {Promise.<Array<string> >} a promise (resolve argument is an array of document unique IDs in the collection)
 */
function insert_update_documents(dbName, collection_name, documents, update, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const signature = options.signature;
		const custom_id_key = options.custom_id_key;
		
		let convert_id = function(document, id_key)
		{
			// Convert the ID in the document with the given name id_key to a MongoDB Object ID
			const ObjectID = require('mongodb').ObjectID;
			try
			{
				document._id = new ObjectID(document[id_key]);
			}
			catch(err)
			{
				throw this.BadRequest('invalid ' + id_key + ' field: ' + err.message);
			}
		}.bind(this);
		
		let find_field = function(document, path, callback)
		{
			if(path.length == 0)
			{
				return (typeof callback === 'function') ? callback(document) : true;
			}
			
			if(typeof document === 'object')
			{
				if(Array.isArray(document)) return false;
				
				let delim_pos = path.indexOf('.');
				const key = (delim_pos >= 0) ? path.slice(0, delim_pos) : path;
				if(document.hasOwnProperty(key))
				{
					return (delim_pos >= 0) ? find_field(document[key], path.slice(delim_pos + 1), callback)
					                        : find_field(document[key], '', callback);
				}
			}
			
			return false;
		};
		
		let get_query = function(signature, document)
		{
			if(signature !== undefined)
			{
				let query = {}
				Object.keys(signature).forEach((path) =>
				{
					query[path] = find_field(document, path, (value) => value);
				});
				return query;
			}
			else if(document.hasOwnProperty('_id'))
			{
				let query =
				{
					_id : document._id
				};
				
				return query;
			}
			
			return undefined;
		};

		// Check input documents and fix document type if missing
		try
		{
			documents.forEach((document) =>
			{
				if((signature === undefined) && document.hasOwnProperty(custom_id_key))
				{
					convert_id(document, custom_id_key);
				}
				else if(document.hasOwnProperty('_id'))
				{
					convert_id(document, '_id');
				}
				
				document = this.fix_document(document, collection_name);
				
				if(signature !== undefined)
				{
					Object.keys(signature).forEach((path) =>
					{
						if(!find_field(document, path))
						{
							throw this.BadRequest('Document has no ' + path + ' field');
						}
					});
				}
				
				try
				{
					this.validate_document(collection_name, document);
				}
				catch(err)
				{
					throw this.BadRequest(err);
				}
			});
		}
		catch(err)
		{
			reject(this.Error(err));
			return;
		}
		
		// Check for duplicate Documents in the request
		if(signature !== undefined)
		{
			let lookup = new Map();
				
			for(let i = 0; i < documents.length; ++i)
			{
				const document = documents[i];
				const query = get_query(signature, document);
				
				if(query !== undefined)
				{
					const key = JSON.stringify(query);
					
					if(lookup.has(key))
					{
						reject(this.BadRequest('duplicate Documents in ' + (update ? "update" : "insert") + ' request of collection \'' + collection_name + '\''));
						return;
					}
					else
					{
						lookup.set(key, query);
					}
				}
			}
		}
		else
		{
			let lookup = new Set();
				
			for(let i = 0; i < documents.length; ++i)
			{
				const document = documents[i];
				if(document.hasOwnProperty('_id'))
				{
					const key = document._id.str();
					
					if(lookup.has(key))
					{
						reject(this.BadRequest('duplicate Documents in ' + (update ? "update" : "insert") + ' request of collection \'' + collection_name + '\''));
						return;
					}
					else
					{
						lookup.add(key);
					}
				}
			}
		}
		
		// Get the MongoDB collection
		this.get_collection(dbName, collection_name).then((documents_collection) =>
		{
			// check that Documents do not exist already in the database
			let check_for_absence = function(documents_collection, documents)
			{
				return new Promise((resolve, reject) =>
				{
					// Check that none of the Documents already exist in the database
					let checking_promises = [];
				
					documents.forEach((document) =>
					{
						let query = get_query(signature, document);
						
						if(query !== undefined)
						{
							let document_cursor = documents_collection.find(query);
							
							if(debug)
							{
								if(signature !== undefined)
								{
									console.log('Counting documents which key is '+ JSON.stringify(query) + ' in Collection \'' + collection_name + '\' of Database \'' + dbName + '\'');
								}
								else
								{
									console.log('Counting documents in Collection \'' + collection_name + '\' of Database \'' + dbName + '\'');
								}
							}
							checking_promises.push(document_cursor.count(true, { limit : 1 }).then((count) =>
							{
								if(count >= 1)
								{
									if(signature !== undefined)
									{
										return Promise.reject(this.Conflict('Document which key is ' + JSON.stringify(query) + ' already exists in collection \'' + collection_name + '\''));
									}
									else
									{
										return Promise.reject(this.Conflict('Document already exists in collection \'' + collection_name + '\''));
									}
								}
								else
								{
									if(debug)
									{
										if(signature !== undefined)
										{
											console.log('Document which key is ' + JSON.stringify(query) + ' is missing in collection \'' + collection_name + '\'');
										}
										else
										{
											console.log('Document is missing in collection \'' + collection_name + '\'');
										}
									}
									return Promise.resolve();
								}
							}));
						}
					});
				
					Promise.all(checking_promises).then(() =>
					{
						resolve();
					}).catch((err) =>
					{
						reject(this.Error(err));
					});
				});
			}.bind(this);
			
			// Preprocess the documents
			const preprocess = this.preprocess[collection_name];
			((preprocess !== undefined) ? preprocess(documents, options) : Promise.resolve(documents)).then((documents) =>
			{
				// When inserting, check if documents are already present in the database
				(update ? Promise.resolve() : check_for_absence(documents_collection, documents)).then(() =>
				{
					// When updating, delete the documents which are already present in the database
					((!update) ? Promise.resolve() : new Promise((resolve, reject) =>
					{
						// compute unique delete queries
						let delete_queries = new Map();
						documents.forEach((document) =>
						{
							let delete_query = get_query(signature, document);
							if(delete_query !== undefined)
							{
								delete_queries.set(JSON.stringify(delete_query), delete_query);
							}
						});
						
						// delete documents
						let delete_promises = [];
						
						delete_queries.forEach((delete_query) =>
						{
							if(debug)
							{
								console.log('Deleting documents which match ' + JSON.stringify(delete_query) + ' in collection \'' + collection_name + '\'');
							}
							delete_promises.push(documents_collection.deleteMany(delete_query));
						});
						
						Promise.all(delete_promises).then(() =>
						{
							resolve();
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					})).then(() =>
					{
						// Insert the documents in the database
						
						let insert_promises = [];
						
						documents.forEach((document) =>
						{
							if(debug)
							{
								console.log('Inserting one document in collection \'' + collection_name + '\'');
							}
							
							insert_promises.push(new Promise((resolve, reject) =>
							{
								documents_collection.insertOne(document, { checkKeys : false }).then(() =>
								{
									resolve();
								}).catch((err) =>
								{
									const query = get_query(signature, document);
									
									if(query !== undefined)
									{
										const key = JSON.stringify(query);
										console.error('While inserting document with key ' + key + 'in collection \'' + collection_name + '\': ' + err.message); 
									}
									reject(this.Error(err));
								});
							}));
						});
						
						Promise.all(insert_promises).then(() =>
						{
							if(debug) console.log('All Documents have been ' + (update ? 'updated' : 'inserted') + ' in collection \'' + collection_name + '\'');
							resolve(documents.map((document) => document._id.toString()));
						}).catch((err) =>
						{
							reject(this.Error(err));
						});
					}).catch((err) =>
					{
						reject(this.Error(err));
					});
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

module.exports.insert_update_documents = insert_update_documents;
