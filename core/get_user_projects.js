/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @typedef {Object} GetUserProjectsOptions
 * @property {string} [project_name] - project name
 * @property {boolean} [abbrev] - enable/disable project metadata
 */

/**
 * Get user's projects
 * 
 * @memberof PKM
 * @instance
 * @param {PkmUser} user - a user
 * @param {GetUserProjectsOptions} [options] - options 
 * 
 * @return {Promise.<Array.<PkmProject>>} a promise
 */
function get_user_projects(user, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		if(options.project_name === undefined)
		{
			// get all user's projects
			
			// first determine the database where user has a role
			user_db_names = user.get_db_names().filter(db_name => (db_name != this.config.pkm_db));
			
			// get all projects
			this.get_projects().then((projects) =>
			{
				const user_projects = projects.filter((project) => user_db_names.includes(project.name));
				
				resolve(user_projects);
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}
		else
		{
			if(user.has_role(options.project_name))
			{
				// get project
				this.get_project(options.project_name).then((project) =>
				{
					resolve([ project ]);
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}
			else
			{
				reject(this.NotFound());
			}
		}
	}.bind(this));
}

exports.get_user_projects = get_user_projects;
