/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Delete some documents in a generic manner from a collection of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} collection_name - collection name
 * @param {Object} [query] - query
 * 
 * @return {Promise} a promise
 */
function delete_documents(dbName, collection_name, query = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const query_is_empty = Object.keys(query).length == 0;

		this.get_collection(dbName, collection_name).then((documents_collection) =>
		{
			documents_collection.deleteMany(query).then((delete_many_result) =>
			{
				if(debug) console.log(delete_many_result.deletedCount + ' Document(s) deleted in collection \'' + collection_name + '\': ', query);
				if(query_is_empty || delete_many_result.deletedCount)
				{
					resolve();
				}
				else
				{
					reject(this.NotFound());
				}
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.delete_documents = delete_documents;
