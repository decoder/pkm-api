/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get documents in a generic manner from several collections of a database
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<string>} collection_names - collection names
 * @param {Object} [query] - query
 * @param {Object} [options] - options
 * 
 * @return {Promise} a promise
 */
function get_documents_spanned(dbName, collection_names, query = {}, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		let pkm = this;
		
		if(debug)
		{
			console.log('PKM.get_documents_spanned(' + JSON.stringify(dbName) + ',' + JSON.stringify(collection_names) + ',' + JSON.stringify(query) + ',' + JSON.stringify(options) + ')');
		}
		
		const query_is_empty = Object.keys(query).length == 0;
		
		function get_documents_spanned_rec(dbName, collection_names, index, query, options)
		{
			return (index >= collection_names.length) ? Promise.resolve([]) : new Promise(function(resolve, reject)
			{
				let skip;
				let limit;
				if(options.skip) skip = options.skip;
				if(options.limit) limit = options.limit;
				
				const collection_name = collection_names[index];
				let result;
				pkm.get_documents(dbName, collection_name, query, options).then((documents) =>
				{
					result = documents;
				}).catch((err) =>
				{
					const error = require('./error');
					if(!error instanceof error.PKM_NotFound)
					{
						reject(pkm.Error(err));
					}
					else
					{
						result = [];
					}
				}).finally(() =>
				{
					if(result !== undefined)
					{
						let new_options = Object.assign({}, options);
						if(skip !== undefined) new_options.skip = skip = (skip >= result.length) ? (skip - result.length) : 0;
						if(limit !== undefined) new_options.limit = limit = (limit >= result.length) ? (limit - result.length) : 0;
						if((index < collection_names.length) && ((limit === undefined) || limit))
						{
							get_documents_spanned_rec(dbName, collection_names, index + 1, query, new_options).then((documents) =>
							{
								resolve(result.concat(documents));
							}).catch((err) =>
							{
								reject(err);
							});
						}
						else
						{
							resolve(result);
						}
					}
				});
			}.bind(this));
		}
		
		get_documents_spanned_rec(dbName, collection_names, 0, query, Object.assign({}, options)).then((documents) =>
		{
			if(documents.length || query_is_empty)
			{
				if(debug)
				{
					console.log('Found ' + documents.length + ' document(s) from collection(s) ' + collection_names.join(', ') + ' of database \'' + dbName + '\'');
				}
				resolve(documents);
			}
			else
			{
				reject(this.NotFound());
			}
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

module.exports.get_documents_spanned = get_documents_spanned;

