/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Update Annotations
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} annotations - Annotations
 * 
 * @return {Promise} a promise
 */
function update_annotations(dbName, annotations)
{
	return new Promise((resolve, reject) =>
	{
		let merge_promises = annotations.map((new_annotation) => new Promise((resolve, reject) =>
		{
			this.get_documents(dbName, 'Annotations', { path : new_annotation.path, access : new_annotation.access }).then((existing_annotations) =>
			{
				let existing_annotation = existing_annotations[0];
				
				let merged_annotation = { ...existing_annotation, ...new_annotation };
				resolve(merged_annotation);
			}).catch((err) =>
			{
				const error = require('./error');
				if(err instanceof error.PKM_NotFound)
				{
					resolve(new_annotation);
				}
				else
				{
					reject(this.Error(err));
				}
			});
		}));
		
		Promise.all(merge_promises).then((annotations) =>
		{
			this.update_documents(dbName, 'Annotations', annotations, { signature : { path : 1, access : 1 } }).then(() =>
			{
				resolve();
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	});
}

module.exports.update_annotations = update_annotations;

