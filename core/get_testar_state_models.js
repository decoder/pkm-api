/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Get one or all TESTAR State Model JSON Documents
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} [query] - query
 * @param {Object} [options] - options
 * 
 * @return {Promise.<Array<Object> >} a promise
 */
function get_testar_state_models(dbName, query = {}, options = {})
{
	return this.get_documents(dbName, 'TESTARStateModels', query, options);
}

exports.get_testar_state_models = get_testar_state_models;
