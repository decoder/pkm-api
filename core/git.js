/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const { deep_copy } = require('../util/deep_copy');

/** PKM Git options
 * 
 * @typedef {Object} PKMGitOptions
 * @property {Logger} logger - a logger (default: none)
*/

/** Run a set of Git commands
 * 
 * @param {string} dbName - database name
 * @param {Array.<Array.<string>>} git_commands - some Git commands
 * @param {PKMGitOptions} [options] - options
 * @return {Promise} a promise
 */
function git(dbName, git_commands, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const git_remote_timeout = this.config.git_remote_timeout;
		const max_parallel_write_files = this.config.git_max_parallel_write_files;
		const max_parallel_read_files = this.config.git_max_parallel_read_files;
		const logger = (options.logger !== undefined) ? options.logger : console;
		const dont_delete_pkm_files = options.dont_delete_pkm_files;
		const dont_delete_git_working_tree_files = options.dont_delete_git_working_tree_files;
		const tmp_dir = this.config.tmp_dir;
		
		const User = require('./user.js');
		const File = require('../util/file.js');
		const FileSystem = require('../util/file_system');
		const { git } = require('../util/git');
		const { scan_and_fix_git_working_trees } = require('../util/git');
		const { get_git_working_tree } = require('../util/git');
		const { list_git_versioned_files } = require('../util/git');
		const { list_git_unmerged_files } = require('../util/git');
		const asyncPool = require('tiny-async-pool');
		const path = require('path');
		
		let git_file_system = this.get_git_file_system(dbName, { logger : logger });
		
		let git_user_credentials = [];
		let git_working_trees;
		let pkm_filenames = [];
		let on_disk_filenames = [];
		let git_versioned_filenames = [];
		let git_unversioned_filenames = [];
		let git_unmerged_filenames = [];
		let git_error;
		
		new Promise((resolve, reject) =>
		{
			git_file_system.make_root_directory().then(() =>
			{
				if(debug)
				{
					logger.log('Git root directory of Project \'' + dbName + '\' has been created');
				}
				
				resolve(true);
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).then(() => new Promise((resolve, reject) =>
		{
			// (1) scan and fix the git working trees
			scan_and_fix_git_working_trees(git_file_system, { debug : debug, logger : logger }).then((_git_working_trees) =>
			{
				git_working_trees = _git_working_trees;
				resolve();
			}).catch((err) =>
			{
				reject(err);
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (2) Get the list of all on-disk filenames
			on_disk_filenames = [];
			
			const find_options =
			{
				type : 'file'
			};
			git_file_system.find(git_file_system.root_directory, find_options).then((matching_filepaths) =>
			{
				matching_filepaths.forEach((matching_filepath) =>
				{
					const on_disk_filename = git_file_system.make_rel_path(matching_filepath);
					
					// Check that the file is neither a Git link in any working trees nor in any Git directory
					if(git_working_trees.find((git_working_tree) =>
						(on_disk_filename == path.join(git_working_tree.directory, '.git')) ||
						(Array.isArray(git_working_tree.linked) &&
						(git_working_tree.linked.find((linked_working_tree) => on_disk_filename == path.join(linked_working_tree.directory, '.git')) !== undefined)) ||
						on_disk_filename.startsWith(git_working_tree.git_directory)) === undefined)
					{
						on_disk_filenames.push(on_disk_filename);
					}
				});
				resolve();
			}).catch((err) =>
			{
				reject(err);
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (3) Get Git user's credentials
			
			// Get current user
			this.get_current_user().then((current_user) =>
			{
				// Get Git user's credentials from user's "wallet" and make it the current Git user's credential set
				git_user_credentials = (current_user.git_user_credentials !== undefined) ? current_user.git_user_credentials : [];
				
				// Merge them with Git user's credentials from the Git service options
				if(options.git_user_credentials !== undefined)
				{
					// For each Git user's credential from the Git service options
					options.git_user_credentials.forEach((options_git_user_credential) =>
					{
						// Find the corresponding Git user's credential in the user's wallet (with same Git remote URL)
						let git_user_credential = git_user_credentials.find((git_user_credential) => git_user_credential.git_remote_url == options_git_user_credential.git_remote_url);
						
						// If found
						if(git_user_credential !== undefined)
						{
							// Merge the current Git user's credential with the Git user's credential from the service options
							git_user_credential.merge(options_git_user_credential);
						}
						else
						{
							// otherwise, add Git user's credential in the service options to the set of Git user's credentials
							git_user_credentials.push(options_git_user_credential);
						}
					});
				}
				
				resolve();
				
			}).catch((err) =>
			{
				logger.error('Error getting current, ' + JSON.stringify(deep_copy(err)));
				reject(err);
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (4) Synchronize on-disk files with PKM:
			// - get all files from the database and write them on the file system
			// - delete files on the file system supported by the PKM which are not in the database
			
			// get the list of all PKM files in the database (without the content)
			this.get_any_files(dbName, {}, { projection : { filecontent : 0 } }).then((pkm_abbrev_files) =>
			{
				// for each PKM file
				asyncPool(max_parallel_write_files, pkm_abbrev_files, (pkm_abbrev_file) => new Promise((resolve, reject) =>
				{
					if((pkm_abbrev_file.git_dirty === undefined) || pkm_abbrev_file.git_dirty)
					{
						// get the PKM file including its content
						this.get_any_files(dbName, { filename : pkm_abbrev_file.rel_path }).then((pkm_files) =>
						{
							const pkm_file = pkm_files[0];
							
							// write the content of the PKM file on the host file system
							git_file_system.writeFile(pkm_file).then(() =>
							{
								// the content of the PKM file was written on the host file system
								pkm_filenames.push(pkm_file.rel_path);
								// the file is not to be deleted on the file system
								let index = on_disk_filenames.indexOf(pkm_file.rel_path);
								if(index > -1)
								{
									on_disk_filenames.splice(index, 1);
								}
								
								// clear Git dirty flag
								pkm_file.git_dirty = false;
								this.update_any_files(dbName, [ pkm_file ], { git : true }).then(() =>
								{
									resolve();
								}).catch((err) =>
								{
									logger.error('Error updating file \'' + pkm_file.rel_path + '\', ' + JSON.stringify(deep_copy(err)));
									reject(err);
								});
							}).catch((write_file_err) =>
							{
								// an error occurred while writing the content of the file on the host file system
								reject(write_file_err);
							});
						}).catch((get_file_err) =>
						{
							// an error occurred while getting the file including its content
							logger.error('Error getting file \'' + pkm_abbrev_file.rel_path + '\', ' + JSON.stringify(deep_copy(get_file_err)));
							reject(get_file_err);
						});
					}
					else
					{
						pkm_filenames.push(pkm_abbrev_file.rel_path);
						// the file is not to be deleted on the file system
						let index = on_disk_filenames.indexOf(pkm_abbrev_file.rel_path);
						if(index > -1)
						{
							on_disk_filenames.splice(index, 1);
						}
						resolve();
					}
				})).then(() =>
				{
					// all files were written on the host file system
					
					if(dont_delete_git_working_tree_files)
					{
						resolve();
					}
					else
					{
						// delete all files on the file system supported by the PKM which are not in the database
						
						// for each file
						asyncPool(max_parallel_read_files, on_disk_filenames, (on_disk_filename) => new Promise((resolve, reject) =>
						{
							// read the file
							git_file_system.readFile(on_disk_filename).then((on_disk_file) =>
							{
								// test PKM support for that file
								if(this.test_file_compatibility(on_disk_file))
								{
									// delete the file
									git_file_system.unlink(on_disk_filename).then(() =>
									{
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								}
								else
								{
									// leave the file intact on disk
									resolve();
								}
							}).catch((err) =>
							{
								reject(err);
							});
						})).then(() =>
						{
							on_disk_filenames = [];
							
							resolve();
						}).catch((err) =>
						{
							reject(err);
						});
					}
				}).catch((write_files_err) =>
				{
					// an error occurred while writing all files on the host file system
					reject(write_files_err);
				});
			}).catch((get_abbrev_files_err) =>
			{
				const error = require('./error');
				if(get_abbrev_files_err instanceof error.PKM_NotFound)
				{
					logger.warn('No files found in PKM');
					resolve();
				}
				else
				{
					logger.error('Error getting files (abbrev), ' + JSON.stringify(deep_copy(get_abbrev_files_err)));
					reject(get_abbrev_files_err);
				}
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (5) run all Git commands on the file system
			git(git_file_system, git_commands, { git_remote_timeout : git_remote_timeout, git_user_credentials : git_user_credentials, debug : debug, logger : logger, ...((tmp_dir !== undefined) ? { tmp_dir : tmp_dir } : {}) }).then((git_outputs) =>
			{
				resolve();
			}).catch((git_err) =>
			{
				// delay promise rejection until PKM update is finished
				git_error = git_err;
				logger.error('Something went wrong while the execution of the Git commands: inspect the warning messages.');
				resolve();
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (6) scan and fix the Git working trees
			scan_and_fix_git_working_trees(git_file_system, { debug : debug, logger : logger }).then((_git_working_trees) =>
			{
				git_working_trees = _git_working_trees;
				resolve();
			}).catch((err) =>
			{
				reject(err);
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (7) update the Git working trees in the PKM
			this.update_git_working_trees(dbName, git_working_trees).then(() =>
			{
				if(debug)
				{
					git_working_trees.forEach((git_working_tree) =>
					{
						logger.log('Put Git working tree ' + JSON.stringify(git_working_tree));
					});
				}
				resolve();
			}).catch((err) =>
			{
				logger.error('Error updating Git working trees, ' + JSON.stringify(deep_copy(err)));
				reject(err);
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (8) Get the list of all on-disk filenames
			on_disk_filenames = [];
			
			const find_options =
			{
				type : 'file'
			};
			git_file_system.find(git_file_system.root_directory, find_options).then((matching_filepaths) =>
			{
				matching_filepaths.forEach((matching_filepath) =>
				{
					const on_disk_filename = git_file_system.make_rel_path(matching_filepath);
					
					// Check that the file is neither a Git link in any working trees nor in any Git directory
					if(git_working_trees.find((git_working_tree) =>
						(on_disk_filename == path.join(git_working_tree.directory, '.git')) ||
						(Array.isArray(git_working_tree.linked) &&
						(git_working_tree.linked.find((linked_working_tree) => on_disk_filename == path.join(linked_working_tree.directory, '.git')) !== undefined)) ||
						on_disk_filename.startsWith(git_working_tree.git_directory)) === undefined)
					{
						on_disk_filenames.push(on_disk_filename);
					}
				});
				resolve();
			}).catch((err) =>
			{
				reject(err);
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (9) Get the list of versioned filenames
			//       - starting with the files of the linked Git working trees
			//       - then finishing with the files of the main Git working tree
			git_versioned_filenames = [];
			asyncPool(1, git_working_trees, (git_working_tree) => new Promise((resolve, reject) =>
			{
				((git_working_tree.linked === undefined) ? Promise.resolve() : asyncPool(1, git_working_tree.linked, (linked_git_working_tree) => new Promise((resolve, reject) =>
				{
					list_git_versioned_files({ ...options, working_directory : git_file_system.make_abs_path(linked_git_working_tree.directory) }).then((filenames) =>
					{
						filenames.forEach((filename) =>
						{
							const git_versioned_filename = git_file_system.make_rel_path(filename);
							git_versioned_filenames.push(git_versioned_filename);
						});
					}).catch((err) =>
					{
					}).finally(() =>
					{
						resolve();
					});
				}))).then(() =>
				{
					list_git_versioned_files({ ...options, working_directory : git_file_system.make_abs_path(git_working_tree.directory) }).then((filenames) =>
					{
						filenames.forEach((filename) =>
						{
							if(i > -1) git_unversioned_filenames.splice(i, 1);
							git_versioned_filenames.push(git_versioned_filename);
						});
					}).catch((err) =>
					{
					}).finally(() =>
					{
						resolve();
					});
				});
			})).then(() =>
			{
				resolve();
			}).catch((err) =>
			{
				reject();
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (10) Get the list of unmerged filenames
			//       - starting with the files of the linked Git working trees
			//       - then finishing with the files of the main Git working tree
			git_unmerged_filenames = [];
			asyncPool(1, git_working_trees, (git_working_tree) => new Promise((resolve, reject) =>
			{
				((git_working_tree.linked === undefined) ? Promise.resolve() : asyncPool(1, git_working_tree.linked, (linked_git_working_tree) => new Promise((resolve, reject) =>
				{
					list_git_unmerged_files({ ...options, working_directory : git_file_system.make_abs_path(linked_git_working_tree.directory) }).then((filenames) =>
					{
						filenames.forEach((filename) =>
						{
							const git_unmerged_filename = git_file_system.make_rel_path(filename);
							git_unmerged_filenames.push(git_unmerged_filename);
						});
					}).catch((err) =>
					{
					}).finally(() =>
					{
						resolve();
					});
				}))).then(() =>
				{
					list_git_unmerged_files({ ...options, working_directory : git_file_system.make_abs_path(git_working_tree.directory) }).then((filenames) =>
					{
						filenames.forEach((filename) =>
						{
							const git_unmerged_filename = git_file_system.make_rel_path(filename);
							git_unmerged_filenames.push(git_unmerged_filename);
						});
					}).catch((err) =>
					{
					}).finally(() =>
					{
						resolve();
					});
				});
			})).then(() =>
			{
				resolve();
			}).catch((err) =>
			{
				reject();
			});
		})).then(() => new Promise((resolve, reject) =>
		{
			// (11) Synchronize PKM with on-disk files:
			// - put all files from the Git directory on the file system into the database
			// - delete files in the database which are no longer in the Git working directory
			
			let git_working_tree_dirs_per_dir_path = new Map(); // this is a cache for quickly retrieving the Git working tree from an absolute directory path
			
			asyncPool(max_parallel_read_files, on_disk_filenames, (on_disk_filename) => new Promise((resolve, reject) =>
			{
				// read the file
				git_file_system.readFile(on_disk_filename).then((on_disk_file) =>
				{
					// if file is supported by the PKM
					if(this.test_file_compatibility(on_disk_file))
					{
						// If file is Git versioned
						let i = git_versioned_filenames.indexOf(on_disk_file.rel_path);
						((i > -1) ? Promise.resolve() : new Promise((resolve, reject) =>
						{
							// Bind the file to a Git working tree

							// Get the Git working tree of the file
							new Promise((resolve, reject) =>
							{
								const abs_file_path = git_file_system.make_abs_path(on_disk_file.rel_path);
								const abs_dir_path = path.dirname(abs_file_path);
								
								// lookup the cache for this directory
								let git_working_tree_directory = git_working_tree_dirs_per_dir_path.get(abs_dir_path);
								if(git_working_tree_directory !== undefined)
								{
									// cache hit
									resolve(git_working_tree_directory);
								}
								else
								{
									// cache miss
									get_git_working_tree({ ...options, working_directory : abs_dir_path }).then((git_working_tree_directory) =>
									{
										// fill in the cache for this directory
										git_working_tree_dirs_per_dir_path.set(abs_dir_path, git_working_tree_directory);
										
										resolve(git_working_tree_directory);
									}).catch((err) =>
									{
										reject(err);
									});
								}
							}).then((git_working_tree_directory) =>
							{
								// Ignore working trees outside of the root directory (in case the root file system is itself within a Git working tree)
								if(git_file_system.check_abs_directory_path(git_working_tree_directory))
								{
									// Bind file to the Git working tree
									on_disk_file.git_working_tree = git_file_system.make_rel_path(git_working_tree_directory);
									
									// Update unmerge flag
									on_disk_file.git_unmerged = (git_unmerged_filenames.indexOf(on_disk_file.rel_path) > -1);
								}
								
								resolve();
							}).catch((err) =>
							{
								reject(err);
							});
						})).then(() =>
						{
							if(on_disk_file.git_unmerged)
							{
								logger.warn('File \'' + on_disk_file.rel_path + '\' is unmerged');
							}
							
							// Put the file (with Git dirty flag cleared) in the PKM
							on_disk_file.git_dirty = false;
							this.update_any_files(dbName, [ on_disk_file ], { git : true }).then(() =>
							{
								if(debug)
								{
									logger.log('Put File \'' + on_disk_file.rel_path + '\'');
								}
								let index = pkm_filenames.indexOf(on_disk_file.rel_path);
								if(index > -1)
								{
									pkm_filenames.splice(index, 1);
								}
								resolve();
							}).catch((err) =>
							{
								logger.error('Can\'t put File \'' + on_disk_file.rel_path + '\': ' + JSON.stringify(deep_copy(err)));
								reject(err);
							});
						}).catch((err) =>
						{
							reject(err);
						});
					}
					else
					{
						logger.warn('File \'' + on_disk_file.rel_path + '\' is not supported');
						resolve();
					}
				}).catch((err) =>
				{
					reject(err);
				});
			})).then(() =>
			{
				let unbind_or_delete_promises = [];
				
				// For each PKM files which are no longer in the Git working directory
				pkm_filenames.forEach((pkm_filename) =>
				{
					unbind_or_delete_promises.push(new Promise((resolve, reject) =>
					{
						if(dont_delete_pkm_files)
						{
							// For files which were deleted on the file system, don't delete the corresponding files in the PKM,
							// but instead unbind these corresponding PKM files from any Git working trees
					
							// get the PKM file including its content
							this.get_any_files(dbName, { filename : pkm_filename }).then((pkm_files) =>
							{
								const pkm_file = pkm_files[0];
								
								// unbind PKM file from any Git working tree
								delete pkm_file.git_working_tree;
								delete pkm_file.git_unmerged;
								
								// update the PKM file
								this.update_any_files(dbName, [ pkm_file ], { git : true }).then(() =>
								{
									if(debug)
									{
										logger.log('Unbinding File \'' + pkm_filename + '\'');
									}
									resolve();
								}).catch((err) =>
								{
									logger.error('Error updating file \'' + pkm_filename + '\', ' + JSON.stringify(deep_copy(err)));
									reject(err);
								});
							}).catch((err) =>
							{
								logger.warn('File \'' + pkm_filename + '\' can\'t be updated (it is no longer in the PKM)');
								resolve();
							});
						}
						else
						{
							// delete the PKM file
							this.delete_any_files(dbName, { filename : pkm_filename }).then(() =>
							{
								if(debug)
								{
									logger.log('Deleted File \'' + pkm_filename + '\'');
								}
								resolve();
							}).catch((err) =>
							{
								logger.error('Error deleting file \'' + pkm_filename + '\', ' + JSON.stringify(deep_copy(err))); 
								reject(err);
							});
						}
					}));
				});
				
				Promise.all(unbind_or_delete_promises).then(() =>
				{
					// files in the database were either unbound from any Git working trees or deleted
					
					resolve();
				}).catch((err) =>
				{
					reject(err);
				});
			}).catch((err) =>
			{
				reject(err);
			});
		})).then(() =>
		{
			// every files have been updated in the database
			
			if(git_error === undefined)
			{
				resolve();
			}
			else
			{
				reject(git_error);
			}
		}).catch((err) =>
		{
			console.warn(err);
			reject(err);
		});
	}.bind(this));
}

exports.git = git;

