/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Insert testar test results
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<Object>} testar_test_results_documents - TESTAR test results documents
 * 
 * @return {Promise} a promise
 */
function insert_testar_test_results(dbName, testar_test_results_documents)
{
	return new Promise(function(resolve, reject)
	{
		this.insert_documents(dbName, 'TESTARTestResults', testar_test_results_documents).then((artefactIds) =>
		{
			resolve(artefactIds.map((artefactId) => { return { 'TESTARTestResults artefactId': artefactId }; }));
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.insert_testar_test_results = insert_testar_test_results;
