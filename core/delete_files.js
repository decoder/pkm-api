/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Delete files in a generic manner in a collection of a database, together with documents on other collections which originate from these files
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} collection_name - collection name
 * @param {Object} [query] - query
 * 
 * @return {Promise} a promise
 */
function delete_files(dbName, collection_name, query = {})
{
	return new Promise(function(resolve, reject)
	{
		var pkm = this;
		
		function get_filenames(collection_name, query)
		{
			return new Promise(function(resolve, reject)
			{
				pkm.get_documents(dbName, collection_name, query, { projection : { filename : 1 } }).then((files) =>
				{
					resolve(files.map(file => file.filename));
				}).catch((err) =>
				{
					const error = require('./error');
					if(err instanceof error.PKM_NotFound)
					{
						resolve([]);
					}
					else
					{
						reject(pkm.Error(err));
					}
				});
			}.bind(this));
		}
		
		get_filenames(collection_name, query).then((filenames) =>
		{
			this.delete_documents(dbName, collection_name, query).then(() =>
			{
				this.invalidate_dependent_documents(dbName, filenames).then(() =>
				{
					resolve();
				}).catch((err) =>
				{
					reject(this.Error(err));
				});
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.delete_files = delete_files;
