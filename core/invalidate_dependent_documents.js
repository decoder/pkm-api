/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Invalidate dependent documents that originated from other documents such as files
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {Array.<string>} identifiers - identifiers (id or filenames)
 * 
 * @return {Promise} a promise
 */
function invalidate_dependent_documents(dbName, identifiers)
{
	// Remove duplicate ids in request
	let lookup = {};
	identifiers = identifiers.filter(id => !lookup[id] && (lookup[id] = true));

	return new Promise(function(resolve, reject)
	{
		let promises = [];
		
		identifiers.forEach((id) =>
		{
			promises.push(this.delete_documents(dbName, 'sourcecodeC', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'annotationsACSL', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'commentsC', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'sourcecodeCPP', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'annotationsACSLPP', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'commentsCPP', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'sourcecodeJava', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'annotationsJML', { sourceFile : id }));
			promises.push(this.delete_documents(dbName, 'commentsjava', { sourceFile : id }));
		});

		Promise.allSettled(promises).then((promise_values) =>
		{
			// if there are no rejected promises
			if(promise_values.find((promise_value) => promise_value.status === 'rejected') === undefined)
			{
				// then resolve
				resolve();
			}
			else
			{
				// otherwise
				const error = require('./error');
				
				// if there are no failures, i.e. there are no rejected promises which are not 404 not found errors
				const failure = promise_values.find((promise_value) => (promise_value.status === 'rejected') && !(promise_value.reason instanceof error.PKM_NotFound));
				if(failure === undefined)
				{
					// then lazy resolve (even if there are 404 not found errors) because it's just an invalidation 
					resolve();
				}
				else
				{
					// otherwise, reject with the first failure
					reject(failure.reason);
				}
			}
		});
	}.bind(this));
}

exports.invalidate_dependent_documents = invalidate_dependent_documents;
