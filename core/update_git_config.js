/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Update the Git configuration file of the Git directory associated (usually .git/config) to a Git working tree
 * 
 * @memberof PKM
 * @instance
 * @param {string} project_name - project name
 * @param {string} git_working_tree_directory - Directory of Git working tree
 * @param {string} git_config_file_content - Git config file content
 * 
 * @return {Promise.<string>} a promise
 */
function update_git_config(project_name, git_working_tree_directory, git_config_file_content)
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		const path = require('path');
		const File = require('../util/file.js');
		
		this.get_git_working_trees(project_name, { directory : git_working_tree_directory }).then((git_working_trees) =>
		{
			const git_working_tree = git_working_trees[0];
			
			let git_file_system = this.get_git_file_system(project_name, { debug : debug });
			
			const git_config_file = new File(
				path.posix.join(git_working_tree.git_directory, 'config'),
				git_config_file_content
			);
			
			git_file_system.writeFile(git_config_file).then(() =>
			{
				resolve();
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.update_git_config = update_git_config;
