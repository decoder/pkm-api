/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Postprocess C Source Codes
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - Database name
 * @param {Array.<Object>} c_source_code_documents - C source code documents
 * @param {Object} [options] - options
 * 
 * @return {Promise<Array.<Object>>} a promise
 */
function postprocess_c_source_codes(c_source_code_documents, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const merge = (options !== undefined) && options.merge;
		
		if(merge)
		{
			let code_map = new Map();
			
			c_source_code_documents.forEach((c_source_code_document) =>
			{
				if(code_map.has(c_source_code_document.sourceFile))
				{
					var tmp = code_map.get(c_source_code_document.sourceFile);
					tmp.globals = tmp.globals.concat(c_source_code_document.globals);
					code_map.set(c_source_code_document.sourceFile, tmp);
				}
				else
				{
					var tmp =
					{
						type : c_source_code_document.type,
						sourceFile : c_source_code_document.sourceFile,
						globals : c_source_code_document.globals,
						globinit : { option : "None" },
						globinitcalled : false
					};
					code_map.set(c_source_code_document.sourceFile, tmp);
				}
			});
			
			const code_map_iterator = code_map.values();
			const merged_c_source_code_documents = Array.from(code_map_iterator, (c_source_code_document) => c_source_code_document);
			
			resolve(merged_c_source_code_documents);
		}
		else
		{
			resolve(c_source_code_documents);
		}
	}.bind(this));
}

module.exports.postprocess_c_source_codes = postprocess_c_source_codes;
