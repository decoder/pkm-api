/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * find in Java class
 * 
 * @memberof PKM
 * @instance
 * @param {string} dbName - database name
 * @param {string} className - class name
 * @param {string} kind - either 'field' or 'method'
 * @param {string} value - either a field or method name [optional]
 * 
 * @return {Promise<Array.<Object>>} a promise that passes the result (an array) to resolve callback
 */
function find_in_java_class(dbName, className, kind, value)
{
	return new Promise( (resolve, reject) =>
	{
		const debug = this.debug;
		
		if((kind != 'field') && (kind != 'method'))
		{
			reject(this.BadRequest('Bad kind: ' , kind));
			return;
		}
		
		this.get_documents(dbName, 'sourcecodeJava', { 'CompilationUnit.TypeDeclaration.SimpleName.content': className }).then((units) =>
		{
			let result = [];
			units.forEach((unit) =>
			{
				if (kind == 'method')
				{
						// Traverse class_ast and collect methods
						if (unit.CompilationUnit.TypeDeclaration.MethodDeclaration)
						{
								var l = unit.CompilationUnit.TypeDeclaration.MethodDeclaration.length;
								for (let i=0; i<l; i++) {
									const m = unit.CompilationUnit.TypeDeclaration.MethodDeclaration[i];
									const mname = m.SimpleName.content;
									console.log("found in list method ", mname);
									if (m.PrimitiveType) {
										const mtype = m.PrimitiveType.content;
										result.push({"method_name":mname, "method_type":mtype});
									}
									else if (m.SimpleType && m.SimpleType.SimpleName.content) {
										const mtype = m.SimpleType.SimpleName.content;
										result.push({"method_name":mname, "method_type":mtype});
									}
									else // We have a constructor probably
										result.push({"method_name":mname, "method_type":""});
								}
						}
				}
				else if (kind == 'field')
				{
					// Traverse class ast and collect fields
					if (unit.CompilationUnit.TypeDeclaration.FieldDeclaration)
					{
						for (let i=0; i<unit.CompilationUnit.TypeDeclaration.FieldDeclaration.length; i++) {
							const f = unit.CompilationUnit.TypeDeclaration.FieldDeclaration[i];
							if (f.VariableDeclarationFragment)
							{
								const fname = f.VariableDeclarationFragment.SimpleName.content;
								if (f.PrimitiveType)
								{
									const ftype = f.PrimitiveType.content;
									result.push({"field_name":fname, "field_type":ftype});
								}
								else if (f.SimpleType && f.SimpleType.SimpleName)
								{
									const ftype = f.SimpleType.SimpleName.content;
									result.push({"field_name":fname, "field_type":ftype});
								}
							}
						}
					}
				}
			});
			
			resolve(result);
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	});
}

exports.find_in_java_class = find_in_java_class;
