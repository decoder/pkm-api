/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @typedef {Object} GetProjectOptions
 * @property {boolean} [abbrev] - enable/disable project metadata
 */

/**
 * Get a project
 * 
 * @memberof PKM
 * @instance
 * @param {string} project_name - project name
 * @param {GetProjectOptions} [options] - options 
 * 
 * @return {Promise.<PkmProject>} a promise
 */
function get_project(project_name, options = {})
{
	return new Promise(function(resolve, reject)
	{
		const debug = this.debug;
		
		// retrieve the current user
		this.get_user(this.user_name).then((user) =>
		{
			
			// search for project name in project name list
			this.get_documents(this.config.pkm_db, 'Projects', { name : project_name }).then(() =>
			{
				// retrieve project informations
				(options.abbrev ? Promise.resolve([ { name : project_name } ]) : this.get_documents(project_name, 'Project')).then((documents) =>
				{
					if(documents.length)
					{
						// check if user has a role in the project or he is administrator
						if(user.has_role(project_name) || user.is_admin())
						{
							let project = documents[0];
							
							// make sure there's a member array in the project
							if(project.members === undefined)
							{
								project.members = [];
							}
							
							new Promise((resolve, reject) =>
							{
								// get all users that have a role in the project
								this.get_users(project_name).then((users) =>
								{
									// build member role and ownership from user's roles
									users.forEach((user) =>
									{
										let member_idx = project.members.findIndex(member => member.name == user.name);
										let member = (member_idx == -1) ? { name : user.name } : project.members[member_idx];
										member.owner = user.has_role(project_name, 'Owner');
										member.roles = user.roles.filter(user_role => user_role.db == project_name).map(user_role => user_role.role);
										if(member_idx == -1)
										{
											project.members.push(member);
										}
										else
										{
											project.members[member_idx] = member;
										}
									});
									
									resolve();
								}).catch((err) =>
								{
									console.warn(err);
									reject(this.Error(err));
								});
							}).then(() =>
							{
								let promises = [];
								
								if(!options.abbrev)
								{
									// get some project metadata from collections
									let project_metadata = Object.keys(this.config.project);
									project_metadata.forEach((metadata) =>
									{
										const get = this.config.project[metadata].get;
										if((typeof get === 'string') && (typeof this[get] === 'function'))
										{
											promises.push(new Promise((resolve, reject) =>
											{
												this[get](project.name).then((metadata_value) =>
												{
													// add metadata to the project
													project[metadata] = metadata_value;
													resolve();
												}).catch((err) =>
												{
													const error = require('./error');
													if(err instanceof error.PKM_NotFound)
													{
														resolve();
													}
													else
													{
														reject(this.Error(err));
													}
												});
											}));
										}
									});
								}
								
								return Promise.all(promises);
							}).then(() =>
							{
								if(debug)
								{
									const util = require('util');
									console.log('get_project(\'' + project_name + '\') => ', util.inspect(project, { showHidden: false, depth: null }));
								}
								
								// serve the project informations
								resolve(project);
							}).catch((err) =>
							{
								reject(this.Error(err));
							});
						}
						else
						{
							// user has no role in the project and he is not an administrator: show only the project name
							resolve({ name : project_name });
						}
					}
					else
					{
						reject(this.NotFound());
					}
				}).catch((err) =>
				{
					console.warn(err);
					reject(this.Error(err));
				});
			}).catch((err) =>
			{
				reject(this.Error(err));
			});
		}).catch((err) =>
		{
			reject(this.Error(err));
		});
	}.bind(this));
}

exports.get_project = get_project;
