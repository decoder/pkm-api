/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new PkmRestfulApi.PkmLog();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('PkmLog', function() {
    it('should create an instance of PkmLog', function() {
      // uncomment below and update the code to test PkmLog
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be.a(PkmRestfulApi.PkmLog);
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property tool (base name: "tool")', function() {
      // uncomment below and update the code to test the property tool
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property nature_of_report (base name: "nature of report")', function() {
      // uncomment below and update the code to test the property nature_of_report
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property start_running_time (base name: "start running time")', function() {
      // uncomment below and update the code to test the property start_running_time
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property end_running_time (base name: "end running time")', function() {
      // uncomment below and update the code to test the property end_running_time
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property messages (base name: "messages")', function() {
      // uncomment below and update the code to test the property messages
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property warnings (base name: "warnings")', function() {
      // uncomment below and update the code to test the property warnings
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property errors (base name: "errors")', function() {
      // uncomment below and update the code to test the property errors
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property status (base name: "status")', function() {
      // uncomment below and update the code to test the property status
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

    it('should have the property details (base name: "details")', function() {
      // uncomment below and update the code to test the property details
      //var instance = new PkmRestfulApi.PkmLog();
      //expect(instance).to.be();
    });

  });

}));
