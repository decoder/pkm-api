/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new PkmRestfulApi.TermOffset();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('TermOffset', function() {
    it('should create an instance of TermOffset', function() {
      // uncomment below and update the code to test TermOffset
      //var instance = new PkmRestfulApi.TermOffset();
      //expect(instance).to.be.a(PkmRestfulApi.TermOffset);
    });

    it('should have the property TNoOffset (base name: "TNoOffset")', function() {
      // uncomment below and update the code to test the property TNoOffset
      //var instance = new PkmRestfulApi.TermOffset();
      //expect(instance).to.be();
    });

    it('should have the property TField (base name: "TField")', function() {
      // uncomment below and update the code to test the property TField
      //var instance = new PkmRestfulApi.TermOffset();
      //expect(instance).to.be();
    });

    it('should have the property TModel (base name: "TModel")', function() {
      // uncomment below and update the code to test the property TModel
      //var instance = new PkmRestfulApi.TermOffset();
      //expect(instance).to.be();
    });

    it('should have the property TIndex (base name: "TIndex")', function() {
      // uncomment below and update the code to test the property TIndex
      //var instance = new PkmRestfulApi.TermOffset();
      //expect(instance).to.be();
    });

  });

}));
