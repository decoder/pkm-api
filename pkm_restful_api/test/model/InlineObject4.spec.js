/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new PkmRestfulApi.InlineObject4();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('InlineObject4', function() {
    it('should create an instance of InlineObject4', function() {
      // uncomment below and update the code to test InlineObject4
      //var instance = new PkmRestfulApi.InlineObject4();
      //expect(instance).to.be.a(PkmRestfulApi.InlineObject4);
    });

    it('should have the property superuser_name (base name: "superuser_name")', function() {
      // uncomment below and update the code to test the property superuser_name
      //var instance = new PkmRestfulApi.InlineObject4();
      //expect(instance).to.be();
    });

    it('should have the property superuser_auth_db (base name: "superuser_auth_db")', function() {
      // uncomment below and update the code to test the property superuser_auth_db
      //var instance = new PkmRestfulApi.InlineObject4();
      //expect(instance).to.be();
    });

    it('should have the property superuser_password (base name: "superuser_password")', function() {
      // uncomment below and update the code to test the property superuser_password
      //var instance = new PkmRestfulApi.InlineObject4();
      //expect(instance).to.be();
    });

  });

}));
