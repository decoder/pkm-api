/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new PkmRestfulApi.PkmJavaCommentsSchema();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('PkmJavaCommentsSchema', function() {
    it('should create an instance of PkmJavaCommentsSchema', function() {
      // uncomment below and update the code to test PkmJavaCommentsSchema
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be.a(PkmRestfulApi.PkmJavaCommentsSchema);
    });

    it('should have the property fileEncoding (base name: "fileEncoding")', function() {
      // uncomment below and update the code to test the property fileEncoding
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be();
    });

    it('should have the property comments (base name: "comments")', function() {
      // uncomment below and update the code to test the property comments
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be();
    });

    it('should have the property fileMimeType (base name: "fileMimeType")', function() {
      // uncomment below and update the code to test the property fileMimeType
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be();
    });

    it('should have the property sourceFile (base name: "sourceFile")', function() {
      // uncomment below and update the code to test the property sourceFile
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be();
    });

    it('should have the property fileFormat (base name: "fileFormat")', function() {
      // uncomment below and update the code to test the property fileFormat
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be();
    });

    it('should have the property type (base name: "type")', function() {
      // uncomment below and update the code to test the property type
      //var instance = new PkmRestfulApi.PkmJavaCommentsSchema();
      //expect(instance).to.be();
    });

  });

}));
