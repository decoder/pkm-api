/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new PkmRestfulApi.PkmInvocation();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('PkmInvocation', function() {
    it('should create an instance of PkmInvocation', function() {
      // uncomment below and update the code to test PkmInvocation
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be.a(PkmRestfulApi.PkmInvocation);
    });

    it('should have the property invocationID (base name: "invocationID")', function() {
      // uncomment below and update the code to test the property invocationID
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property tool (base name: "tool")', function() {
      // uncomment below and update the code to test the property tool
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property invocationConfiguration (base name: "invocationConfiguration")', function() {
      // uncomment below and update the code to test the property invocationConfiguration
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property user (base name: "user")', function() {
      // uncomment below and update the code to test the property user
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property timestampRequest (base name: "timestampRequest")', function() {
      // uncomment below and update the code to test the property timestampRequest
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property timestampStart (base name: "timestampStart")', function() {
      // uncomment below and update the code to test the property timestampStart
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property timestampCompleted (base name: "timestampCompleted")', function() {
      // uncomment below and update the code to test the property timestampCompleted
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property invocationStatus (base name: "invocationStatus")', function() {
      // uncomment below and update the code to test the property invocationStatus
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

    it('should have the property invocationResults (base name: "invocationResults")', function() {
      // uncomment below and update the code to test the property invocationResults
      //var instance = new PkmRestfulApi.PkmInvocation();
      //expect(instance).to.be();
    });

  });

}));
