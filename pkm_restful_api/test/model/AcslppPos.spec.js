/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new PkmRestfulApi.AcslppPos();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('AcslppPos', function() {
    it('should create an instance of AcslppPos', function() {
      // uncomment below and update the code to test AcslppPos
      //var instance = new PkmRestfulApi.AcslppPos();
      //expect(instance).to.be.a(PkmRestfulApi.AcslppPos);
    });

    it('should have the property pos_path (base name: "pos_path")', function() {
      // uncomment below and update the code to test the property pos_path
      //var instance = new PkmRestfulApi.AcslppPos();
      //expect(instance).to.be();
    });

    it('should have the property pos_lnum (base name: "pos_lnum")', function() {
      // uncomment below and update the code to test the property pos_lnum
      //var instance = new PkmRestfulApi.AcslppPos();
      //expect(instance).to.be();
    });

    it('should have the property pos_bol (base name: "pos_bol")', function() {
      // uncomment below and update the code to test the property pos_bol
      //var instance = new PkmRestfulApi.AcslppPos();
      //expect(instance).to.be();
    });

    it('should have the property pos_cnum (base name: "pos_cnum")', function() {
      // uncomment below and update the code to test the property pos_cnum
      //var instance = new PkmRestfulApi.AcslppPos();
      //expect(instance).to.be();
    });

  });

}));
