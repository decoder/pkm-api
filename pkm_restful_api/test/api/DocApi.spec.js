/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PkmRestfulApi);
  }
}(this, function(expect, PkmRestfulApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new PkmRestfulApi.DocApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DocApi', function() {
    describe('deleteDocs', function() {
      it('should call deleteDocs successfully', function(done) {
        //uncomment below and update the code to test deleteDocs
        //instance.deleteDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteGraphicalDocs', function() {
      it('should call deleteGraphicalDocs successfully', function(done) {
        //uncomment below and update the code to test deleteGraphicalDocs
        //instance.deleteGraphicalDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteRawDoc', function() {
      it('should call deleteRawDoc successfully', function(done) {
        //uncomment below and update the code to test deleteRawDoc
        //instance.deleteRawDoc(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteRawDocs', function() {
      it('should call deleteRawDocs successfully', function(done) {
        //uncomment below and update the code to test deleteRawDocs
        //instance.deleteRawDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getDocArtefacts', function() {
      it('should call getDocArtefacts successfully', function(done) {
        //uncomment below and update the code to test getDocArtefacts
        //instance.getDocArtefacts(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getDocs', function() {
      it('should call getDocs successfully', function(done) {
        //uncomment below and update the code to test getDocs
        //instance.getDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getGraphicalDocs', function() {
      it('should call getGraphicalDocs successfully', function(done) {
        //uncomment below and update the code to test getGraphicalDocs
        //instance.getGraphicalDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getRawDoc', function() {
      it('should call getRawDoc successfully', function(done) {
        //uncomment below and update the code to test getRawDoc
        //instance.getRawDoc(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getRawDocs', function() {
      it('should call getRawDocs successfully', function(done) {
        //uncomment below and update the code to test getRawDocs
        //instance.getRawDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('postDocs', function() {
      it('should call postDocs successfully', function(done) {
        //uncomment below and update the code to test postDocs
        //instance.postDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('postGraphicalDocs', function() {
      it('should call postGraphicalDocs successfully', function(done) {
        //uncomment below and update the code to test postGraphicalDocs
        //instance.postGraphicalDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('postRawDocs', function() {
      it('should call postRawDocs successfully', function(done) {
        //uncomment below and update the code to test postRawDocs
        //instance.postRawDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putDocs', function() {
      it('should call putDocs successfully', function(done) {
        //uncomment below and update the code to test putDocs
        //instance.putDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putGraphicalDocs', function() {
      it('should call putGraphicalDocs successfully', function(done) {
        //uncomment below and update the code to test putGraphicalDocs
        //instance.putGraphicalDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putRawDocs', function() {
      it('should call putRawDocs successfully', function(done) {
        //uncomment below and update the code to test putRawDocs
        //instance.putRawDocs(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
