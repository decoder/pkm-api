# PkmRestfulApi.Pat

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StmtLabel** | **String** |  | [optional] 
**FormalLabel** | **String** |  | [optional] 
**BuiltinLabel** | [**BuiltinLabel**](BuiltinLabel.md) |  | [optional] 
**predicate** | [**Predicate**](Predicate.md) |  | [optional] 


