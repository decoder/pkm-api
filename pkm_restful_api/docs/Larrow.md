# PkmRestfulApi.Larrow

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Larrow_logic_types** | [**[LvType]**](LvType.md) |  | [optional] 
**Larrow_logic_type** | [**LvType**](LvType.md) |  | [optional] 


