# PkmRestfulApi.TheStateModelDifferenceSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**existsPreviousStateModel** | **Boolean** |  | [default to false]
**previousStateModelAppName** | **String** |  | [default to &#39;&#39;]
**previousStateModelAppVersion** | **String** |  | [default to &#39;&#39;]
**numberDisappearedAbstractStates** | **Number** |  | [default to 0]
**numberNewAbstractStates** | **Number** |  | [default to 0]
**stateModelDifferenceReport** | **String** |  | [default to &#39;&#39;]


