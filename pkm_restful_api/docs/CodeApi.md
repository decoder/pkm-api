# PkmRestfulApi.CodeApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCAnnotations**](CodeApi.md#deleteCAnnotations) | **DELETE** /code/c/annotations/{dbName} | 
[**deleteCAnnotationsBySourceCodeFilename**](CodeApi.md#deleteCAnnotationsBySourceCodeFilename) | **DELETE** /code/c/annotations/{dbName}/{filename} | 
[**deleteCComments**](CodeApi.md#deleteCComments) | **DELETE** /code/c/comments/{dbName} | 
[**deleteCCommentsBySourceCodeFilename**](CodeApi.md#deleteCCommentsBySourceCodeFilename) | **DELETE** /code/c/comments/{dbName}/{filename} | 
[**deleteCPPAnnotations**](CodeApi.md#deleteCPPAnnotations) | **DELETE** /code/cpp/annotations/{dbName} | 
[**deleteCPPAnnotationsBySourceCodeFilename**](CodeApi.md#deleteCPPAnnotationsBySourceCodeFilename) | **DELETE** /code/cpp/annotations/{dbName}/{filename} | 
[**deleteCPPComments**](CodeApi.md#deleteCPPComments) | **DELETE** /code/cpp/comments/{dbName} | 
[**deleteCPPCommentsBySourceCodeFilename**](CodeApi.md#deleteCPPCommentsBySourceCodeFilename) | **DELETE** /code/cpp/comments/{dbName}/{filename} | 
[**deleteCPPSourceCodes**](CodeApi.md#deleteCPPSourceCodes) | **DELETE** /code/cpp/sourcecode/{dbName} | 
[**deleteCPPSourceCodesBySourceCodeFilename**](CodeApi.md#deleteCPPSourceCodesBySourceCodeFilename) | **DELETE** /code/cpp/sourcecode/{dbName}/{filename} | 
[**deleteCSourceCodes**](CodeApi.md#deleteCSourceCodes) | **DELETE** /code/c/sourcecode/{dbName} | 
[**deleteCSourceCodesBySourceCodeFilename**](CodeApi.md#deleteCSourceCodesBySourceCodeFilename) | **DELETE** /code/c/sourcecode/{dbName}/{filename} | 
[**deleteJavaAnnotations**](CodeApi.md#deleteJavaAnnotations) | **DELETE** /code/java/annotations/{dbName} | 
[**deleteJavaAnnotationsBySourceCodeFilename**](CodeApi.md#deleteJavaAnnotationsBySourceCodeFilename) | **DELETE** /code/java/annotations/{dbName}/{filename} | 
[**deleteJavaComments**](CodeApi.md#deleteJavaComments) | **DELETE** /code/java/comments/{dbName} | 
[**deleteJavaCommentsBySourceCodeFilename**](CodeApi.md#deleteJavaCommentsBySourceCodeFilename) | **DELETE** /code/java/comments/{dbName}/{filename} | 
[**deleteJavaSourceCodes**](CodeApi.md#deleteJavaSourceCodes) | **DELETE** /code/java/sourcecode/{dbName} | 
[**deleteJavaSourceCodesBySourceCodeFilename**](CodeApi.md#deleteJavaSourceCodesBySourceCodeFilename) | **DELETE** /code/java/sourcecode/{dbName}/{filename} | 
[**deleteRawSourceCode**](CodeApi.md#deleteRawSourceCode) | **DELETE** /code/rawsourcecode/{dbName}/{filename} | 
[**deleteRawSourceCodes**](CodeApi.md#deleteRawSourceCodes) | **DELETE** /code/rawsourcecode/{dbName} | 
[**deleteSourceCodeAnnotations**](CodeApi.md#deleteSourceCodeAnnotations) | **DELETE** /code/annotations/{dbName} | 
[**deleteSourceCodeAnnotationsBySourceCodeFilename**](CodeApi.md#deleteSourceCodeAnnotationsBySourceCodeFilename) | **DELETE** /code/annotations/{dbName}/{filename} | 
[**deleteSourceCodeComments**](CodeApi.md#deleteSourceCodeComments) | **DELETE** /code/comments/{dbName} | 
[**deleteSourceCodeCommentsBySourceCodeFilename**](CodeApi.md#deleteSourceCodeCommentsBySourceCodeFilename) | **DELETE** /code/comments/{dbName}/{filename} | 
[**deleteSourceCodes**](CodeApi.md#deleteSourceCodes) | **DELETE** /code/sourcecode/{dbName} | 
[**deleteSourceCodesBySourceCodeFilename**](CodeApi.md#deleteSourceCodesBySourceCodeFilename) | **DELETE** /code/sourcecode/{dbName}/{filename} | 
[**getCAnnotations**](CodeApi.md#getCAnnotations) | **GET** /code/c/annotations/{dbName} | 
[**getCAnnotationsBySourceCodeFilename**](CodeApi.md#getCAnnotationsBySourceCodeFilename) | **GET** /code/c/annotations/{dbName}/{filename} | 
[**getCComments**](CodeApi.md#getCComments) | **GET** /code/c/comments/{dbName} | 
[**getCCommentsBySourceCodeFilename**](CodeApi.md#getCCommentsBySourceCodeFilename) | **GET** /code/c/comments/{dbName}/{filename} | 
[**getCFunctions**](CodeApi.md#getCFunctions) | **GET** /code/c/functions/{dbName} | 
[**getCFunctionsByName**](CodeApi.md#getCFunctionsByName) | **GET** /code/c/functions/{dbName}/{funcname} | 
[**getCPPAnnotationArtefacts**](CodeApi.md#getCPPAnnotationArtefacts) | **GET** /code/cpp/artefacts/annotations/{dbName} | 
[**getCPPAnnotations**](CodeApi.md#getCPPAnnotations) | **GET** /code/cpp/annotations/{dbName} | 
[**getCPPAnnotationsBySourceCodeFilename**](CodeApi.md#getCPPAnnotationsBySourceCodeFilename) | **GET** /code/cpp/annotations/{dbName}/{filename} | 
[**getCPPClassFields**](CodeApi.md#getCPPClassFields) | **GET** /code/cpp/class/fields/{dbName}/{className} | 
[**getCPPClassMethods**](CodeApi.md#getCPPClassMethods) | **GET** /code/cpp/class/methods/{dbName}/{className} | 
[**getCPPCommentArtefacts**](CodeApi.md#getCPPCommentArtefacts) | **GET** /code/cpp/artefacts/comments/{dbName} | 
[**getCPPComments**](CodeApi.md#getCPPComments) | **GET** /code/cpp/comments/{dbName} | 
[**getCPPCommentsBySourceCodeFilename**](CodeApi.md#getCPPCommentsBySourceCodeFilename) | **GET** /code/cpp/comments/{dbName}/{filename} | 
[**getCPPSourceCodeArtefacts**](CodeApi.md#getCPPSourceCodeArtefacts) | **GET** /code/cpp/artefacts/sourcecode/{dbName} | 
[**getCPPSourceCodes**](CodeApi.md#getCPPSourceCodes) | **GET** /code/cpp/sourcecode/{dbName} | 
[**getCPPSourceCodesBySourceCodeFilename**](CodeApi.md#getCPPSourceCodesBySourceCodeFilename) | **GET** /code/cpp/sourcecode/{dbName}/{filename} | 
[**getCSourceCodes**](CodeApi.md#getCSourceCodes) | **GET** /code/c/sourcecode/{dbName} | 
[**getCSourceCodesBySourceCodeFilename**](CodeApi.md#getCSourceCodesBySourceCodeFilename) | **GET** /code/c/sourcecode/{dbName}/{filename} | 
[**getCTypes**](CodeApi.md#getCTypes) | **GET** /code/c/types/{dbName} | 
[**getCTypesByName**](CodeApi.md#getCTypesByName) | **GET** /code/c/types/{dbName}/{typename} | 
[**getCVariables**](CodeApi.md#getCVariables) | **GET** /code/c/variables/{dbName} | 
[**getCVariablesByName**](CodeApi.md#getCVariablesByName) | **GET** /code/c/variables/{dbName}/{varname} | 
[**getJavaAnnotations**](CodeApi.md#getJavaAnnotations) | **GET** /code/java/annotations/{dbName} | 
[**getJavaAnnotationsBySourceCodeFilename**](CodeApi.md#getJavaAnnotationsBySourceCodeFilename) | **GET** /code/java/annotations/{dbName}/{filename} | 
[**getJavaClassFields**](CodeApi.md#getJavaClassFields) | **GET** /code/java/class/fields/{dbName}/{className} | 
[**getJavaClassMethods**](CodeApi.md#getJavaClassMethods) | **GET** /code/java/class/methods/{dbName}/{className} | 
[**getJavaClasses**](CodeApi.md#getJavaClasses) | **GET** /code/java/classes/{dbName} | 
[**getJavaComments**](CodeApi.md#getJavaComments) | **GET** /code/java/comments/{dbName} | 
[**getJavaCommentsBySourceCodeFilename**](CodeApi.md#getJavaCommentsBySourceCodeFilename) | **GET** /code/java/comments/{dbName}/{filename} | 
[**getJavaSourceCodes**](CodeApi.md#getJavaSourceCodes) | **GET** /code/java/sourcecode/{dbName} | 
[**getJavaSourceCodesBySourceCodeFilename**](CodeApi.md#getJavaSourceCodesBySourceCodeFilename) | **GET** /code/java/sourcecode/{dbName}/{filename} | 
[**getRawSourceCode**](CodeApi.md#getRawSourceCode) | **GET** /code/rawsourcecode/{dbName}/{filename} | 
[**getRawSourceCodes**](CodeApi.md#getRawSourceCodes) | **GET** /code/rawsourcecode/{dbName} | 
[**getSourceCodeAnnotations**](CodeApi.md#getSourceCodeAnnotations) | **GET** /code/annotations/{dbName} | 
[**getSourceCodeAnnotationsBySourceCodeFilename**](CodeApi.md#getSourceCodeAnnotationsBySourceCodeFilename) | **GET** /code/annotations/{dbName}/{filename} | 
[**getSourceCodeComments**](CodeApi.md#getSourceCodeComments) | **GET** /code/comments/{dbName} | 
[**getSourceCodeCommentsBySourceCodeFilename**](CodeApi.md#getSourceCodeCommentsBySourceCodeFilename) | **GET** /code/comments/{dbName}/{filename} | 
[**getSourceCodes**](CodeApi.md#getSourceCodes) | **GET** /code/sourcecode/{dbName} | 
[**getSourceCodesBySourceCodeFilename**](CodeApi.md#getSourceCodesBySourceCodeFilename) | **GET** /code/sourcecode/{dbName}/{filename} | 
[**postJavaSourceCodes**](CodeApi.md#postJavaSourceCodes) | **POST** /code/java/sourcecode/{dbName} | 
[**postRawSourceCodes**](CodeApi.md#postRawSourceCodes) | **POST** /code/rawsourcecode/{dbName} | 
[**putCAnnotations**](CodeApi.md#putCAnnotations) | **PUT** /code/c/annotations/{dbName} | 
[**putCComments**](CodeApi.md#putCComments) | **PUT** /code/c/comments/{dbName} | 
[**putCPPAnnotations**](CodeApi.md#putCPPAnnotations) | **PUT** /code/cpp/annotations/{dbName} | 
[**putCPPComments**](CodeApi.md#putCPPComments) | **PUT** /code/cpp/comments/{dbName} | 
[**putCPPSourceCodes**](CodeApi.md#putCPPSourceCodes) | **PUT** /code/cpp/sourcecode/{dbName} | 
[**putCSourceCodes**](CodeApi.md#putCSourceCodes) | **PUT** /code/c/sourcecode/{dbName} | 
[**putJavaAnnotations**](CodeApi.md#putJavaAnnotations) | **PUT** /code/java/annotations/{dbName} | 
[**putJavaComments**](CodeApi.md#putJavaComments) | **PUT** /code/java/comments/{dbName} | 
[**putJavaSourceCodes**](CodeApi.md#putJavaSourceCodes) | **PUT** /code/java/sourcecode/{dbName} | 
[**putRawSourceCodes**](CodeApi.md#putRawSourceCodes) | **PUT** /code/rawsourcecode/{dbName} | 



## deleteCAnnotations

> deleteCAnnotations(dbName, key)



Delete all C annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCAnnotations(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCAnnotationsBySourceCodeFilename

> deleteCAnnotationsBySourceCodeFilename(dbName, filename, key)



Delete the C annotations documents related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCAnnotationsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCComments

> deleteCComments(dbName, key)



Delete all C comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCComments(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCCommentsBySourceCodeFilename

> deleteCCommentsBySourceCodeFilename(dbName, filename, key)



Delete the C comments documents related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCCommentsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCPPAnnotations

> deleteCPPAnnotations(dbName, key)



Delete all C++ annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCPPAnnotations(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCPPAnnotationsBySourceCodeFilename

> deleteCPPAnnotationsBySourceCodeFilename(dbName, filename, key)



Delete the C++ annotations documents related to a C++ source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C++ Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCPPAnnotationsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C++ Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCPPComments

> deleteCPPComments(dbName, key)



Delete all C++ comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCPPComments(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCPPCommentsBySourceCodeFilename

> deleteCPPCommentsBySourceCodeFilename(dbName, filename, key)



Delete the C++ comments documents related to a C++ source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C++ Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCPPCommentsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C++ Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCPPSourceCodes

> deleteCPPSourceCodes(dbName, key)



Delete all C++ source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCPPSourceCodes(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCPPSourceCodesBySourceCodeFilename

> deleteCPPSourceCodesBySourceCodeFilename(dbName, filename, key)



Delete the C++ source code documents (Abstract Syntax Tree) related to a C++ source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C++ Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCPPSourceCodesBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C++ Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCSourceCodes

> deleteCSourceCodes(dbName, key)



Delete all C source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCSourceCodes(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCSourceCodesBySourceCodeFilename

> deleteCSourceCodesBySourceCodeFilename(dbName, filename, key)



Delete the C source code documents (Abstract Syntax Tree) related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCSourceCodesBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteJavaAnnotations

> deleteJavaAnnotations(dbName, key)



Delete all Java annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteJavaAnnotations(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteJavaAnnotationsBySourceCodeFilename

> deleteJavaAnnotationsBySourceCodeFilename(dbName, filename, key)



Delete the Java annotations documents related to a Java source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Java Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteJavaAnnotationsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Java Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteJavaComments

> deleteJavaComments(dbName, key)



Delete all Java comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteJavaComments(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteJavaCommentsBySourceCodeFilename

> deleteJavaCommentsBySourceCodeFilename(dbName, filename, key)



Delete the Java comments documents related to a Java source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Java Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteJavaCommentsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Java Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteJavaSourceCodes

> deleteJavaSourceCodes(dbName, key)



Delete all Java source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteJavaSourceCodes(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteJavaSourceCodesBySourceCodeFilename

> deleteJavaSourceCodesBySourceCodeFilename(dbName, filename, key)



Delete the Java source code documents (Abstract Syntax Tree) related to a Java source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Java Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteJavaSourceCodesBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Java Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteRawSourceCode

> deleteRawSourceCode(dbName, filename, key)



delete a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteRawSourceCode(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteRawSourceCodes

> deleteRawSourceCodes(dbName, key)



Delete all source code files present in the database together with source code ASTs, annotations and comments documents which originate from these source code files

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteRawSourceCodes(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteSourceCodeAnnotations

> deleteSourceCodeAnnotations(dbName, key)



Delete all source code annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteSourceCodeAnnotations(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteSourceCodeAnnotationsBySourceCodeFilename

> deleteSourceCodeAnnotationsBySourceCodeFilename(dbName, filename, key)



Delete the source code annotations documents related to a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteSourceCodeAnnotationsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteSourceCodeComments

> deleteSourceCodeComments(dbName, key)



Delete all source code comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteSourceCodeComments(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteSourceCodeCommentsBySourceCodeFilename

> deleteSourceCodeCommentsBySourceCodeFilename(dbName, filename, key)



Delete the source code comments documents related to a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteSourceCodeCommentsBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteSourceCodes

> deleteSourceCodes(dbName, key)



Delete all source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteSourceCodes(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteSourceCodesBySourceCodeFilename

> deleteSourceCodesBySourceCodeFilename(dbName, filename, key)



Delete the source code documents (Abstract Syntax Tree) related to a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteSourceCodesBySourceCodeFilename(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCAnnotations

> [Object] getCAnnotations(dbName, key, opts)



Get all C annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCAnnotations(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCAnnotationsBySourceCodeFilename

> [Object] getCAnnotationsBySourceCodeFilename(dbName, filename, key, opts)



Get the C annotations documents related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCAnnotationsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCComments

> [PkmCComments] getCComments(dbName, key, opts)



Get all C comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCComments(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCComments]**](PkmCComments.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCCommentsBySourceCodeFilename

> [PkmCComments] getCCommentsBySourceCodeFilename(dbName, filename, key, opts)



Get the C comments documents related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCCommentsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCComments]**](PkmCComments.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCFunctions

> [Object] getCFunctions(dbName, key)



Get all C functions

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCFunctions(dbName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCFunctionsByName

> [Object] getCFunctionsByName(dbName, funcname, key)



Get C functions by name

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var funcname = "funcname_example"; // String | Function name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCFunctionsByName(dbName, funcname, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **funcname** | **String**| Function name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPAnnotationArtefacts

> Object getCPPAnnotationArtefacts(dbName, key, opts)



Get a C++ annotation artefacts

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': 1234, // Number | artefact identifier
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getCPPAnnotationArtefacts(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **Number**| artefact identifier | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPAnnotations

> [PkmCppAnnotations] getCPPAnnotations(dbName, key, opts)



Get all C++ annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCPPAnnotations(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCppAnnotations]**](PkmCppAnnotations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPAnnotationsBySourceCodeFilename

> [PkmCppAnnotations] getCPPAnnotationsBySourceCodeFilename(dbName, filename, key, opts)



Get the C++ annotations documents related to a C++ source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCPPAnnotationsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCppAnnotations]**](PkmCppAnnotations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPClassFields

> [Object] getCPPClassFields(dbName, className, key)



Get fields from a C++ class

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var className = "className_example"; // String | class name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCPPClassFields(dbName, className, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **className** | **String**| class name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPClassMethods

> [Object] getCPPClassMethods(dbName, className, key)



Get methods from a C++ class

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var className = "className_example"; // String | class name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCPPClassMethods(dbName, className, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **className** | **String**| class name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPCommentArtefacts

> Object getCPPCommentArtefacts(dbName, key, opts)



Get a C++ comment artefacts

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': 1234, // Number | artefact identifier
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getCPPCommentArtefacts(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **Number**| artefact identifier | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPComments

> [PkmCppComments] getCPPComments(dbName, key, opts)



Get all C++ comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCPPComments(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCppComments]**](PkmCppComments.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPCommentsBySourceCodeFilename

> [PkmCppComments] getCPPCommentsBySourceCodeFilename(dbName, filename, key, opts)



Get the C++ comments documents related to a C++ source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C++ Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCPPCommentsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C++ Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCppComments]**](PkmCppComments.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPSourceCodeArtefacts

> Object getCPPSourceCodeArtefacts(dbName, key, opts)



Get a C++ source code artefacts

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': 1234, // Number | artefact identifier
  'kind': /^function$|^variable$|^typedef$|^struct$|^union$|^class$|^field$|^method$/, // String | kind of C++ artefacts as a bare character string or a /regular expression/
  'path': /^std::log[fl]?$/, // String | abstract path of C++ artefacts as a bare character string or a /regular expression/
  'name': /log[fl]?/, // String | name of C++ artefacts as a bare character string or a /regular expression/
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getCPPSourceCodeArtefacts(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **Number**| artefact identifier | [optional] 
 **kind** | **String**| kind of C++ artefacts as a bare character string or a /regular expression/ | [optional] 
 **path** | **String**| abstract path of C++ artefacts as a bare character string or a /regular expression/ | [optional] 
 **name** | **String**| name of C++ artefacts as a bare character string or a /regular expression/ | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPSourceCodes

> [PkmCppSourceCode] getCPPSourceCodes(dbName, key, opts)



Get all C++ source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCPPSourceCodes(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCppSourceCode]**](PkmCppSourceCode.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCPPSourceCodesBySourceCodeFilename

> [PkmCppSourceCode] getCPPSourceCodesBySourceCodeFilename(dbName, filename, key, opts)



Get the C++ source code documents (Abstract Syntax Tree) related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCPPSourceCodesBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCppSourceCode]**](PkmCppSourceCode.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCSourceCodes

> [PkmCSourceCode] getCSourceCodes(dbName, key, opts)



Get all C source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCSourceCodes(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCSourceCode]**](PkmCSourceCode.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCSourceCodesBySourceCodeFilename

> [PkmCSourceCode] getCSourceCodesBySourceCodeFilename(dbName, filename, key, opts)



Get the C source code documents (Abstract Syntax Tree) related to a C source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | C Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getCSourceCodesBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| C Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmCSourceCode]**](PkmCSourceCode.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCTypes

> [Object] getCTypes(dbName, key)



Get all C types

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCTypes(dbName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCTypesByName

> [Object] getCTypesByName(dbName, typename, key)



Get C types by name

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var typename = "typename_example"; // String | Type name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCTypesByName(dbName, typename, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **typename** | **String**| Type name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCVariables

> [Object] getCVariables(dbName, key)



Get all C variables

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCVariables(dbName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCVariablesByName

> [Object] getCVariablesByName(dbName, varname, key)



Get C variables by name

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var varname = "varname_example"; // String | Variable name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCVariablesByName(dbName, varname, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **varname** | **String**| Variable name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaAnnotations

> [Object] getJavaAnnotations(dbName, key, opts)



Get all Java annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getJavaAnnotations(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaAnnotationsBySourceCodeFilename

> [Object] getJavaAnnotationsBySourceCodeFilename(dbName, filename, key, opts)



Get the Java annotations documents related to a Java source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Java Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getJavaAnnotationsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Java Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaClassFields

> [Object] getJavaClassFields(dbName, className, key)



Get all fields from a Java class

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var className = "className_example"; // String | class name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getJavaClassFields(dbName, className, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **className** | **String**| class name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaClassMethods

> [Object] getJavaClassMethods(dbName, className, key)



Get all methods from a Java class

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var className = "className_example"; // String | class name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getJavaClassMethods(dbName, className, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **className** | **String**| class name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaClasses

> [Object] getJavaClasses(dbName, key)



Get all Java classes

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getJavaClasses(dbName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaComments

> [PkmJavaComments] getJavaComments(dbName, key, opts)



Get all Java comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getJavaComments(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmJavaComments]**](PkmJavaComments.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaCommentsBySourceCodeFilename

> [PkmJavaComments] getJavaCommentsBySourceCodeFilename(dbName, filename, key, opts)



Get the Java comments documents related to a Java source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Java Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getJavaCommentsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Java Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmJavaComments]**](PkmJavaComments.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaSourceCodes

> [Object] getJavaSourceCodes(dbName, key, opts)



Get all Java source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getJavaSourceCodes(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getJavaSourceCodesBySourceCodeFilename

> [Object] getJavaSourceCodesBySourceCodeFilename(dbName, filename, key, opts)



Get the Java source code documents (Abstract Syntax Tree) related to a Java source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Java Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getJavaSourceCodesBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Java Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRawSourceCode

> PkmFile getRawSourceCode(dbName, filename, key, opts)



Get a single source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
};
apiInstance.getRawSourceCode(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRawSourceCodes

> [PkmFile] getRawSourceCodes(dbName, key, opts)



Get all source code files from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getRawSourceCodes(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmFile]**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSourceCodeAnnotations

> [PkmSourceCodeAnnotations] getSourceCodeAnnotations(dbName, key, opts)



Get all source code annotations documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getSourceCodeAnnotations(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmSourceCodeAnnotations]**](PkmSourceCodeAnnotations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSourceCodeAnnotationsBySourceCodeFilename

> [PkmSourceCodeAnnotations] getSourceCodeAnnotationsBySourceCodeFilename(dbName, filename, key, opts)



Get the source code annotations documents related to a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getSourceCodeAnnotationsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmSourceCodeAnnotations]**](PkmSourceCodeAnnotations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSourceCodeComments

> [PkmSourceCodeComment] getSourceCodeComments(dbName, key, opts)



Get all source code comments documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getSourceCodeComments(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmSourceCodeComment]**](PkmSourceCodeComment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSourceCodeCommentsBySourceCodeFilename

> [PkmSourceCodeComment] getSourceCodeCommentsBySourceCodeFilename(dbName, filename, key, opts)



Get the source code comments documents related to a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getSourceCodeCommentsBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmSourceCodeComment]**](PkmSourceCodeComment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSourceCodes

> [PkmSourceCode] getSourceCodes(dbName, key, opts)



Get all source code documents (Abstract Syntax Tree) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getSourceCodes(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmSourceCode]**](PkmSourceCode.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSourceCodesBySourceCodeFilename

> [PkmSourceCode] getSourceCodesBySourceCodeFilename(dbName, filename, key, opts)



Get the source code documents (Abstract Syntax Tree) related to a source code file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Source code filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'merge': true // Boolean | enable/disable merging according to file
};
apiInstance.getSourceCodesBySourceCodeFilename(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Source code filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **merge** | **Boolean**| enable/disable merging according to file | [optional] [default to true]

### Return type

[**[PkmSourceCode]**](PkmSourceCode.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postJavaSourceCodes

> postJavaSourceCodes(dbName, key, body)



Insert some Java source code (AST)

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [null]; // [Object] | Java source code documents
apiInstance.postJavaSourceCodes(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[Object]**](Object.md)| Java source code documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postRawSourceCodes

> String postRawSourceCodes(dbName, key, body)



Insert some source code files (which are not yet present) into the database. The related documents (source code AST, comments and annotations) which originate from the source code files are invalidated.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Source code files
apiInstance.postRawSourceCodes(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Source code files | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCAnnotations

> putCAnnotations(dbName, key, body)



Put some C annotations documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [null]; // [Object] | 
apiInstance.putCAnnotations(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[Object]**](Object.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCComments

> putCComments(dbName, key, body)



Put some C comments documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCComments()]; // [PkmCComments] | C comments documents
apiInstance.putCComments(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCComments]**](PkmCComments.md)| C comments documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCPPAnnotations

> putCPPAnnotations(dbName, key, body)



Put some C++ annotations documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCppAnnotations()]; // [PkmCppAnnotations] | 
apiInstance.putCPPAnnotations(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCppAnnotations]**](PkmCppAnnotations.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCPPComments

> putCPPComments(dbName, key, body)



Put some C++ comments documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCppComments()]; // [PkmCppComments] | C++ comments documents
apiInstance.putCPPComments(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCppComments]**](PkmCppComments.md)| C++ comments documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCPPSourceCodes

> putCPPSourceCodes(dbName, key, body)



Put some C++ source code documents (Abstract Syntax Tree) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCppSourceCode()]; // [PkmCppSourceCode] | C++ source code documents
apiInstance.putCPPSourceCodes(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCppSourceCode]**](PkmCppSourceCode.md)| C++ source code documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCSourceCodes

> putCSourceCodes(dbName, key, body)



Put some C source code documents (Abstract Syntax Tree) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCSourceCode()]; // [PkmCSourceCode] | C source code documents
apiInstance.putCSourceCodes(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCSourceCode]**](PkmCSourceCode.md)| C source code documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putJavaAnnotations

> putJavaAnnotations(dbName, key, body)



Put some Java annotations documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [null]; // [Object] | 
apiInstance.putJavaAnnotations(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[Object]**](Object.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putJavaComments

> putJavaComments(dbName, key, body)



Put some Java comments documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmJavaComments()]; // [PkmJavaComments] | Java comments documents
apiInstance.putJavaComments(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmJavaComments]**](PkmJavaComments.md)| Java comments documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putJavaSourceCodes

> putJavaSourceCodes(dbName, key, body)



Put some Java source code (AST)

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [null]; // [Object] | Java source code documents
apiInstance.putJavaSourceCodes(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[Object]**](Object.md)| Java source code documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putRawSourceCodes

> putRawSourceCodes(dbName, key, body)



Insert (if not yet present) or update (if already present) some source code files into the database. The related documents (source code AST, comments and annotations) which originate from the source code files are invalidated.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CodeApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Source code files
apiInstance.putRawSourceCodes(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Source code files | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

