# PkmRestfulApi.PkmUmlStateMachine

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**id** | **String** |  | 
**diagram** | **String** |  | [optional] 
**regions** | [**[PkmUmlStateMachineRegions]**](PkmUmlStateMachineRegions.md) |  | 


