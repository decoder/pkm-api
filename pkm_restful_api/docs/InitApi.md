# PkmRestfulApi.InitApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postInitPkm**](InitApi.md#postInitPkm) | **POST** /init | 



## postInitPkm

> String postInitPkm(body)



Initialize PKM. Create Role &#39;user&#39; in users database, then create a PKM administrator in users database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InitApi();
var body = new PkmRestfulApi.InlineObject3(); // InlineObject3 | 
apiInstance.postInitPkm(body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**InlineObject3**](InlineObject3.md)|  | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

