# PkmRestfulApi.SRLChunk

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**len** | **Number** |  | 
**pos** | **Number** |  | 
**text** | **String** |  | 


