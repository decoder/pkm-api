# PkmRestfulApi.TCompBitssize

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scache** | **String** |  | [optional] 
**Computed_value** | **Number** |  | [optional] 
**Not_Computable_string** | **String** |  | [optional] 
**Not_Computable_typ** | [**TCompBitssizeNotComputableTyp**](TCompBitssizeNotComputableTyp.md) |  | [optional] 



## Enum: ScacheEnum


* `Not_Computed` (value: `"Not_Computed"`)

* `Computed` (value: `"Computed"`)

* `Not_Computable` (value: `"Not_Computable"`)




