# PkmRestfulApi.PkmCveCnaContainerReferences

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**refsource** | **String** |  | 
**url** | **String** |  | 


