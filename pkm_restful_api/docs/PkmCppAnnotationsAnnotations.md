# PkmRestfulApi.PkmCppAnnotationsAnnotations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loc** | [**AcslppLoc**](AcslppLoc.md) |  | [optional] 
**annotations** | **[String]** |  | [optional] 
**id** | **Number** | id of the annotated entity | [optional] 


