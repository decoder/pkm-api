# PkmRestfulApi.PkmLogSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | ID of Log (automatically generated when missing) | [optional] 
**tool** | **String** | tool name/tag | 
**nature_of_report** | **String** | e.g \&quot;Proof report\&quot;, \&quot;Modeling report\&quot;, \&quot;Testing report\&quot;, \&quot;GUI report\&quot;, \&quot;NER report\&quot;, \&quot;Summarization report\&quot;, etc. | 
**start_running_time** | **String** | start running time | 
**end_running_time** | **String** | end running time | 
**messages** | [**Messages**](Messages.md) |  | [optional] 
**warnings** | [**Warnings**](Warnings.md) |  | [optional] 
**errors** | [**Errors**](Errors.md) |  | [optional] 
**status** | **Boolean** | flag of failure of the process: true&#x3D;not failed | 
**details** | **Object** | e.g. parameters of the tool, analysis reports, etc. | [optional] 


