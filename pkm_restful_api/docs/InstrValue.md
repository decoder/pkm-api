# PkmRestfulApi.InstrValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**varinfo** | [**Varinfo**](Varinfo.md) |  | [optional] 
**AssignInit** | [**AssignInit**](AssignInit.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


