# PkmRestfulApi.TESTARTestResultsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **String** |  | [default to &#39;&#39;]
**url** | **String** |  | [default to &#39;&#39;]
**sequencesResult** | **[String]** |  | 
**htmlsResult** | **[String]** |  | 
**logsResult** | **[String]** |  | 
**sequencesVerdicts** | **[String]** |  | 
**sut** | [**TheSutSchema**](TheSutSchema.md) |  | 
**tool** | [**TheToolSchema**](TheToolSchema.md) |  | 
**settings** | [**TheSettingsSchema**](TheSettingsSchema.md) |  | 


