# PkmRestfulApi.LvType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Ctype** | [**Typ**](Typ.md) |  | [optional] 
**Ltype** | [**Ltype**](Ltype.md) |  | [optional] 
**Lvar** | **String** |  | [optional] 
**Larrow** | [**Larrow**](Larrow.md) |  | [optional] 


