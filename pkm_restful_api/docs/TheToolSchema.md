# PkmRestfulApi.TheToolSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | [default to &#39;&#39;]
**name** | **String** |  | [default to &#39;&#39;]
**isOpenSource** | **Boolean** |  | [default to false]
**license** | **String** |  | [default to &#39;&#39;]
**url** | **String** |  | [default to &#39;&#39;]
**version** | **String** |  | [default to &#39;&#39;]
**runtimeOS** | **String** |  | [default to &#39;&#39;]


