# PkmRestfulApi.PkmUserRoles

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**db** | **String** | database name | 
**role** | **String** | role name | 



## Enum: RoleEnum


* `Owner` (value: `"Owner"`)

* `Developer` (value: `"Developer"`)

* `Reviewer` (value: `"Reviewer"`)

* `Maintainer` (value: `"Maintainer"`)




