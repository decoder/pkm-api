# PkmRestfulApi.Spec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spec_behavior** | **[String]** |  | 
**spec_complete_behavior** | **[String]** |  | [optional] 
**spec_disjoint_behaviors** | **[String]** |  | 


