# PkmRestfulApi.TESTARApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteTESTARSettings**](TESTARApi.md#deleteTESTARSettings) | **DELETE** /testar/settings/{dbName} | 
[**deleteTESTARStateModel**](TESTARApi.md#deleteTESTARStateModel) | **DELETE** /testar/state_model/{dbName}/{artefactId} | 
[**deleteTESTARTestResults**](TESTARApi.md#deleteTESTARTestResults) | **DELETE** /testar/test_results/{dbName}/{artefactId} | 
[**getAllTESTARStateModel**](TESTARApi.md#getAllTESTARStateModel) | **GET** /testar/state_model/{dbName} | 
[**getAllTESTARTestResults**](TESTARApi.md#getAllTESTARTestResults) | **GET** /testar/test_results/{dbName} | 
[**getTESTARSettings**](TESTARApi.md#getTESTARSettings) | **GET** /testar/settings/{dbName} | 
[**getTESTARStateModel**](TESTARApi.md#getTESTARStateModel) | **GET** /testar/state_model/{dbName}/{artefactId} | 
[**getTESTARTestResults**](TESTARApi.md#getTESTARTestResults) | **GET** /testar/test_results/{dbName}/{artefactId} | 
[**postTESTARSettings**](TESTARApi.md#postTESTARSettings) | **POST** /testar/settings/{dbName} | 
[**postTESTARStateModel**](TESTARApi.md#postTESTARStateModel) | **POST** /testar/state_model/{dbName} | 
[**postTESTARTestResults**](TESTARApi.md#postTESTARTestResults) | **POST** /testar/test_results/{dbName} | 
[**putTESTARSettings**](TESTARApi.md#putTESTARSettings) | **PUT** /testar/settings/{dbName} | 



## deleteTESTARSettings

> deleteTESTARSettings(dbName, key)



Delete TESTAR settings

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTESTARSettings(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteTESTARStateModel

> deleteTESTARStateModel(dbName, artefactId, key)



Delete Specific TESTAR StateModel JSON Artefact from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | ArtefactId
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTESTARStateModel(dbName, artefactId, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| ArtefactId | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteTESTARTestResults

> deleteTESTARTestResults(dbName, artefactId, key)



Delete Specific TESTAR TestResults JSON Artefact from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | ArtefactId
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTESTARTestResults(dbName, artefactId, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| ArtefactId | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAllTESTARStateModel

> [TestarStateModel] getAllTESTARStateModel(dbName, key, opts)



Get All TESTAR StateModel JSON Artefacts from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getAllTESTARStateModel(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[TestarStateModel]**](TestarStateModel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAllTESTARTestResults

> [TestarTestResults] getAllTESTARTestResults(dbName, key, opts)



Get All TESTAR TestResults JSON Artefacts from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getAllTESTARTestResults(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[TestarTestResults]**](TestarTestResults.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTESTARSettings

> Object getTESTARSettings(dbName, key)



Get TESTAR settings

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getTESTARSettings(dbName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTESTARStateModel

> TestarStateModel getTESTARStateModel(dbName, artefactId, key)



Get Specific TESTAR StateModel JSON Artefact from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | ArtefactId
var key = "key_example"; // String | Access key to the PKM
apiInstance.getTESTARStateModel(dbName, artefactId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| ArtefactId | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**TestarStateModel**](TestarStateModel.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTESTARTestResults

> TestarTestResults getTESTARTestResults(dbName, artefactId, key)



Get Specific TESTAR TestResults JSON Artefact from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | ArtefactId
var key = "key_example"; // String | Access key to the PKM
apiInstance.getTESTARTestResults(dbName, artefactId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| ArtefactId | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**TestarTestResults**](TestarTestResults.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postTESTARSettings

> String postTESTARSettings(dbName, key, body)



Insert TESTAR settings

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = {key: null}; // Object | TESTAR settings
apiInstance.postTESTARSettings(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | **Object**| TESTAR settings | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postTESTARStateModel

> PkmTestarResponse postTESTARStateModel(dbName, key, body)



Insert some StateModel JSON Artefacts into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.TestarStateModel(); // TestarStateModel | TESTAR StateModel JSON Artefact
apiInstance.postTESTARStateModel(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**TestarStateModel**](TestarStateModel.md)| TESTAR StateModel JSON Artefact | 

### Return type

[**PkmTestarResponse**](PkmTestarResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postTESTARTestResults

> PkmTestarResponse postTESTARTestResults(dbName, key, body)



Insert some TestResults JSON Artefacts into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.TestarTestResults(); // TestarTestResults | TESTAR TestResults JSON Artefact
apiInstance.postTESTARTestResults(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**TestarTestResults**](TestarTestResults.md)| TESTAR TestResults JSON Artefact | 

### Return type

[**PkmTestarResponse**](PkmTestarResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putTESTARSettings

> putTESTARSettings(dbName, key, body)



Insert TESTAR settings or update existing ones

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TESTARApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = {key: null}; // Object | TESTAR settings
apiInstance.putTESTARSettings(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | **Object**| TESTAR settings | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

