# PkmRestfulApi.PkmCppAnnotations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceFile** | **String** |  | [optional] 
**annotations** | [**[PkmCppAnnotationsAnnotations]**](PkmCppAnnotationsAnnotations.md) |  | [optional] 


