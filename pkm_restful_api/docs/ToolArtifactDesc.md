# PkmRestfulApi.ToolArtifactDesc

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**allowedExtensions** | **[String]** |  | [optional] 


