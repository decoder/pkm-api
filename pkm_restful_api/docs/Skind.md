# PkmRestfulApi.Skind

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**skind_kind** | **String** |  | [optional] 
**skind_value** | [**SkindValue**](SkindValue.md) |  | [optional] 



## Enum: SkindKindEnum


* `Instr` (value: `"Instr"`)

* `Return` (value: `"Return"`)

* `Goto` (value: `"Goto"`)

* `Break` (value: `"Break"`)

* `Continue` (value: `"Continue"`)

* `If` (value: `"If"`)

* `Switch` (value: `"Switch"`)

* `Loop` (value: `"Loop"`)

* `Block` (value: `"Block"`)

* `UnspecifiedSequence` (value: `"UnspecifiedSequence"`)

* `Throw` (value: `"Throw"`)

* `TryCatch` (value: `"TryCatch"`)

* `TryFinally` (value: `"TryFinally"`)

* `TryExcept` (value: `"TryExcept"`)




