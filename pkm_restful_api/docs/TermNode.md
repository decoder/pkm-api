# PkmRestfulApi.TermNode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TLval** | [**TermLval**](TermLval.md) |  | [optional] 
**Tat** | [**Tat**](Tat.md) |  | [optional] 


