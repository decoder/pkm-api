# PkmRestfulApi.Kind

## Enum


* `AbiTagAttr` (value: `"AbiTagAttr"`)

* `AccessSpecDecl` (value: `"AccessSpecDecl"`)

* `AliasAttr` (value: `"AliasAttr"`)

* `AlignedAttr` (value: `"AlignedAttr"`)

* `AllocSizeAttr` (value: `"AllocSizeAttr"`)

* `AlwaysInlineAttr` (value: `"AlwaysInlineAttr"`)

* `ArrayInitIndexExpr` (value: `"ArrayInitIndexExpr"`)

* `ArrayInitLoopExpr` (value: `"ArrayInitLoopExpr"`)

* `ArraySubscriptExpr` (value: `"ArraySubscriptExpr"`)

* `AsmLabelAttr` (value: `"AsmLabelAttr"`)

* `BinaryOperator` (value: `"BinaryOperator"`)

* `BlockCommandComment` (value: `"BlockCommandComment"`)

* `BuiltinAttr` (value: `"BuiltinAttr"`)

* `BuiltinTemplateDecl` (value: `"BuiltinTemplateDecl"`)

* `BuiltinType` (value: `"BuiltinType"`)

* `CallExpr` (value: `"CallExpr"`)

* `CharacterLiteral` (value: `"CharacterLiteral"`)

* `ClassTemplateDecl` (value: `"ClassTemplateDecl"`)

* `ClassTemplatePartialSpecializationDecl` (value: `"ClassTemplatePartialSpecializationDecl"`)

* `ClassTemplateSpecializationDecl` (value: `"ClassTemplateSpecializationDecl"`)

* `ComplexType` (value: `"ComplexType"`)

* `ConditionalOperator` (value: `"ConditionalOperator"`)

* `ConstantArrayType` (value: `"ConstantArrayType"`)

* `ConstantExpr` (value: `"ConstantExpr"`)

* `ConstAttr` (value: `"ConstAttr"`)

* `CStyleCastExpr` (value: `"CStyleCastExpr"`)

* `CXX11NoReturnAttr` (value: `"CXX11NoReturnAttr"`)

* `CXXBindTemporaryExpr` (value: `"CXXBindTemporaryExpr"`)

* `CXXBoolLiteralExpr` (value: `"CXXBoolLiteralExpr"`)

* `CXXConstructExpr` (value: `"CXXConstructExpr"`)

* `CXXConstructorDecl` (value: `"CXXConstructorDecl"`)

* `CXXConversionDecl` (value: `"CXXConversionDecl"`)

* `CXXCtorInitializer` (value: `"CXXCtorInitializer"`)

* `CXXDefaultArgExpr` (value: `"CXXDefaultArgExpr"`)

* `CXXDefaultInitExpr` (value: `"CXXDefaultInitExpr"`)

* `CXXDependentScopeMemberExpr` (value: `"CXXDependentScopeMemberExpr"`)

* `CXXDestructorDecl` (value: `"CXXDestructorDecl"`)

* `CXXFunctionalCastExpr` (value: `"CXXFunctionalCastExpr"`)

* `CXXMemberCallExpr` (value: `"CXXMemberCallExpr"`)

* `CXXMethodDecl` (value: `"CXXMethodDecl"`)

* `CXXNewExpr` (value: `"CXXNewExpr"`)

* `CXXNullPtrLiteralExpr` (value: `"CXXNullPtrLiteralExpr"`)

* `CXXOperatorCallExpr` (value: `"CXXOperatorCallExpr"`)

* `CXXPseudoDestructorExpr` (value: `"CXXPseudoDestructorExpr"`)

* `CXXRecordDecl` (value: `"CXXRecordDecl"`)

* `CXXScalarValueInitExpr` (value: `"CXXScalarValueInitExpr"`)

* `CXXStaticCastExpr` (value: `"CXXStaticCastExpr"`)

* `CXXTemporaryObjectExpr` (value: `"CXXTemporaryObjectExpr"`)

* `CXXThisExpr` (value: `"CXXThisExpr"`)

* `CXXUnresolvedConstructExpr` (value: `"CXXUnresolvedConstructExpr"`)

* `DeclRefExpr` (value: `"DeclRefExpr"`)

* `DecltypeType` (value: `"DecltypeType"`)

* `DependentNameType` (value: `"DependentNameType"`)

* `DependentScopeDeclRefExpr` (value: `"DependentScopeDeclRefExpr"`)

* `DependentSizedArrayType` (value: `"DependentSizedArrayType"`)

* `DependentTemplateSpecializationType` (value: `"DependentTemplateSpecializationType"`)

* `DeprecatedAttr` (value: `"DeprecatedAttr"`)

* `ElaboratedType` (value: `"ElaboratedType"`)

* `EnumConstantDecl` (value: `"EnumConstantDecl"`)

* `EnumDecl` (value: `"EnumDecl"`)

* `EnumType` (value: `"EnumType"`)

* `ExprWithCleanups` (value: `"ExprWithCleanups"`)

* `FieldDecl` (value: `"FieldDecl"`)

* `FinalAttr` (value: `"FinalAttr"`)

* `FloatingLiteral` (value: `"FloatingLiteral"`)

* `FormatAttr` (value: `"FormatAttr"`)

* `FriendDecl` (value: `"FriendDecl"`)

* `FullComment` (value: `"FullComment"`)

* `FunctionDecl` (value: `"FunctionDecl"`)

* `FunctionProtoType` (value: `"FunctionProtoType"`)

* `FunctionTemplateDecl` (value: `"FunctionTemplateDecl"`)

* `GNUNullExpr` (value: `"GNUNullExpr"`)

* `HTMLEndTagComment` (value: `"HTMLEndTagComment"`)

* `HTMLStartTagComment` (value: `"HTMLStartTagComment"`)

* `ImplicitCastExpr` (value: `"ImplicitCastExpr"`)

* `ImplicitValueInitExpr` (value: `"ImplicitValueInitExpr"`)

* `IncompleteArrayType` (value: `"IncompleteArrayType"`)

* `IndirectFieldDecl` (value: `"IndirectFieldDecl"`)

* `InitListExpr` (value: `"InitListExpr"`)

* `InjectedClassNameType` (value: `"InjectedClassNameType"`)

* `InlineCommandComment` (value: `"InlineCommandComment"`)

* `IntegerLiteral` (value: `"IntegerLiteral"`)

* `LinkageSpecDecl` (value: `"LinkageSpecDecl"`)

* `LValueReferenceType` (value: `"LValueReferenceType"`)

* `MaterializeTemporaryExpr` (value: `"MaterializeTemporaryExpr"`)

* `MayAliasAttr` (value: `"MayAliasAttr"`)

* `MemberExpr` (value: `"MemberExpr"`)

* `MemberPointerType` (value: `"MemberPointerType"`)

* `MinVectorWidthAttr` (value: `"MinVectorWidthAttr"`)

* `ModeAttr` (value: `"ModeAttr"`)

* `NamespaceDecl` (value: `"NamespaceDecl"`)

* `NoDebugAttr` (value: `"NoDebugAttr"`)

* `NonNullAttr` (value: `"NonNullAttr"`)

* `NonTypeTemplateParmDecl` (value: `"NonTypeTemplateParmDecl"`)

* `NoThrowAttr` (value: `"NoThrowAttr"`)

* `OpaqueValueExpr` (value: `"OpaqueValueExpr"`)

* `OverrideAttr` (value: `"OverrideAttr"`)

* `OwnerAttr` (value: `"OwnerAttr"`)

* `PackExpansionExpr` (value: `"PackExpansionExpr"`)

* `PackExpansionType` (value: `"PackExpansionType"`)

* `ParagraphComment` (value: `"ParagraphComment"`)

* `ParamCommandComment` (value: `"ParamCommandComment"`)

* `ParenExpr` (value: `"ParenExpr"`)

* `ParenListExpr` (value: `"ParenListExpr"`)

* `ParenType` (value: `"ParenType"`)

* `ParmVarDecl` (value: `"ParmVarDecl"`)

* `PointerAttr` (value: `"PointerAttr"`)

* `PointerType` (value: `"PointerType"`)

* `PureAttr` (value: `"PureAttr"`)

* `QualType` (value: `"QualType"`)

* `RecordType` (value: `"RecordType"`)

* `RestrictAttr` (value: `"RestrictAttr"`)

* `ReturnsNonNullAttr` (value: `"ReturnsNonNullAttr"`)

* `ReturnsTwiceAttr` (value: `"ReturnsTwiceAttr"`)

* `RValueReferenceType` (value: `"RValueReferenceType"`)

* `SizeOfPackExpr` (value: `"SizeOfPackExpr"`)

* `StaticAssertDecl` (value: `"StaticAssertDecl"`)

* `StringLiteral` (value: `"StringLiteral"`)

* `SubstNonTypeTemplateParmExpr` (value: `"SubstNonTypeTemplateParmExpr"`)

* `SubstTemplateTypeParmType` (value: `"SubstTemplateTypeParmType"`)

* `TargetAttr` (value: `"TargetAttr"`)

* `TemplateArgument` (value: `"TemplateArgument"`)

* `TemplateSpecializationType` (value: `"TemplateSpecializationType"`)

* `TemplateTemplateParmDecl` (value: `"TemplateTemplateParmDecl"`)

* `TemplateTypeParmDecl` (value: `"TemplateTypeParmDecl"`)

* `TemplateTypeParmType` (value: `"TemplateTypeParmType"`)

* `TextComment` (value: `"TextComment"`)

* `TParamCommandComment` (value: `"TParamCommandComment"`)

* `TypeAliasDecl` (value: `"TypeAliasDecl"`)

* `TypeAliasTemplateDecl` (value: `"TypeAliasTemplateDecl"`)

* `TypedefDecl` (value: `"TypedefDecl"`)

* `TypedefType` (value: `"TypedefType"`)

* `TypeOfExprType` (value: `"TypeOfExprType"`)

* `TypeTraitExpr` (value: `"TypeTraitExpr"`)

* `UnaryExprOrTypeTraitExpr` (value: `"UnaryExprOrTypeTraitExpr"`)

* `UnaryOperator` (value: `"UnaryOperator"`)

* `UnaryTransformType` (value: `"UnaryTransformType"`)

* `UnresolvedLookupExpr` (value: `"UnresolvedLookupExpr"`)

* `UnresolvedMemberExpr` (value: `"UnresolvedMemberExpr"`)

* `UnresolvedUsingValueDecl` (value: `"UnresolvedUsingValueDecl"`)

* `UnusedAttr` (value: `"UnusedAttr"`)

* `UsingDecl` (value: `"UsingDecl"`)

* `UsingDirectiveDecl` (value: `"UsingDirectiveDecl"`)

* `UsingShadowDecl` (value: `"UsingShadowDecl"`)

* `VarDecl` (value: `"VarDecl"`)

* `VarTemplateDecl` (value: `"VarTemplateDecl"`)

* `VectorType` (value: `"VectorType"`)

* `VerbatimBlockComment` (value: `"VerbatimBlockComment"`)

* `VerbatimBlockLineComment` (value: `"VerbatimBlockLineComment"`)

* `VerbatimLineComment` (value: `"VerbatimLineComment"`)

* `VisibilityAttr` (value: `"VisibilityAttr"`)

* `WarnUnusedResultAttr` (value: `"WarnUnusedResultAttr"`)

* `WeakRefAttr` (value: `"WeakRefAttr"`)


