# PkmRestfulApi.IOApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**download**](IOApi.md#download) | **GET** /io/{dbName}/{filename} | 



## download

> File download(dbName, filename, key)



### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.IOApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.download(dbName, filename, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

**File**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf, text/markdown, text/plain, application/json

