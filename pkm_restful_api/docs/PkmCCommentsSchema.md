# PkmRestfulApi.PkmCCommentsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceFile** | **String** |  | [optional] 
**comments** | [**[PkmCCommentsComments]**](PkmCCommentsComments.md) |  | [optional] 


