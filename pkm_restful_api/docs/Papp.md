# PkmRestfulApi.Papp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logic_info** | [**LogicInfo**](LogicInfo.md) |  | [optional] 
**Papp_labels** | [**[LogicLabels]**](LogicLabels.md) |  | [optional] 
**Papp_terms** | [**[TermClosed]**](TermClosed.md) |  | [optional] 


