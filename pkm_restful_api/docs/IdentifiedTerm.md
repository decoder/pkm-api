# PkmRestfulApi.IdentifiedTerm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**it_id** | **Number** |  | [optional] 
**it_content** | [**Term**](Term.md) |  | [optional] 


