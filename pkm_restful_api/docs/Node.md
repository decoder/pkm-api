# PkmRestfulApi.Node

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | identifier of the C++ artefact | [optional] 
**kind** | [**Kind**](Kind.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 
**range** | [**Range**](Range.md) |  | [optional] 
**isImplicit** | **Boolean** |  | [optional] 
**language** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**mangledName** | **String** |  | [optional] 
**type** | [**Type**](Type.md) |  | [optional] 
**storageClass** | **String** |  | [optional] 
**inline** | **Boolean** |  | [optional] 
**pretty_decl** | **String** |  | [optional] 
**full_pretty_decl** | **String** |  | [optional] 
**full_name** | **String** |  | [optional] 
**hasBraces** | **Boolean** |  | [optional] 
**isUsed** | **Boolean** |  | [optional] 
**isReferenced** | **Boolean** |  | [optional] 
**tagUsed** | **String** |  | [optional] 
**completeDefinition** | **Boolean** |  | [optional] 
**definitionData** | [**DefinitionData**](DefinitionData.md) |  | [optional] 
**previousDecl** | **Number** |  | [optional] 
**originalNamespace** | [**Namespace**](Namespace.md) |  | [optional] 
**target** | [**Target**](Target.md) |  | [optional] 
**parentDeclContextId** | **Number** |  | [optional] 
**nominatedNamespace** | [**Namespace**](Namespace.md) |  | [optional] 
**inner** | [**[Node]**](Node.md) | List of child C++ artefacts | [optional] 


