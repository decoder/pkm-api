# PkmRestfulApi.CompileCommandApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCompileCommand**](CompileCommandApi.md#deleteCompileCommand) | **DELETE** /compile_command/{dbName}/{filename} | 
[**deleteCompileCommands**](CompileCommandApi.md#deleteCompileCommands) | **DELETE** /compile_command/{dbName} | 
[**getCompileCommand**](CompileCommandApi.md#getCompileCommand) | **GET** /compile_command/{dbName}/{filename} | 
[**getCompileCommands**](CompileCommandApi.md#getCompileCommands) | **GET** /compile_command/{dbName} | 
[**postCompileCommands**](CompileCommandApi.md#postCompileCommands) | **POST** /compile_command/{dbName} | 
[**putCompileCommands**](CompileCommandApi.md#putCompileCommands) | **PUT** /compile_command/{dbName} | 



## deleteCompileCommand

> deleteCompileCommand(dbName, filename, key)



delete the compile command document related to a file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CompileCommandApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | the filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCompileCommand(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| the filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCompileCommands

> deleteCompileCommands(dbName, key)



Delete all compile command documents in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CompileCommandApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCompileCommands(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCompileCommand

> [PkmCompileCommand] getCompileCommand(dbName, filename, key, opts)



Get the compile commands documents related to a file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CompileCommandApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | the filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getCompileCommand(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| the filename | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmCompileCommand]**](PkmCompileCommand.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCompileCommands

> [PkmCompileCommand] getCompileCommands(dbName, key, opts)



Get all compile command documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CompileCommandApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getCompileCommands(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmCompileCommand]**](PkmCompileCommand.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postCompileCommands

> postCompileCommands(dbName, key, body)



Post some compile commands into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CompileCommandApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCompileCommand()]; // [PkmCompileCommand] | Compile command documents
apiInstance.postCompileCommands(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCompileCommand]**](PkmCompileCommand.md)| Compile command documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCompileCommands

> putCompileCommands(dbName, key, body)



Put some compile commands into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CompileCommandApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmCompileCommand()]; // [PkmCompileCommand] | Compile command documents
apiInstance.putCompileCommands(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCompileCommand]**](PkmCompileCommand.md)| Compile command documents | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

