# PkmRestfulApi.GCompTag

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**compinfo** | [**Compinfo**](Compinfo.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


