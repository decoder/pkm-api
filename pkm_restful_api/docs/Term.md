# PkmRestfulApi.Term

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**term_node** | [**TermNode**](TermNode.md) |  | [optional] 
**term_loc** | [**Loc**](Loc.md) |  | [optional] 
**term_type** | [**TermType**](TermType.md) |  | [optional] 
**term_name** | **[Object]** |  | [optional] 


