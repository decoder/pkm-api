# PkmRestfulApi.PkmCveCnaContainerAffectedVendors

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**[PkmCveCnaContainerAffectedProducts]**](PkmCveCnaContainerAffectedProducts.md) |  | 
**vendor_name** | **String** |  | 


