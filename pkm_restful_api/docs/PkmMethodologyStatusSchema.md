# PkmRestfulApi.PkmMethodologyStatusSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phaseNumber** | **Number** |  | 
**name** | **String** |  | 
**id** | **String** |  | 
**description** | **String** |  | 
**completed** | **Boolean** |  | 
**tasks** | [**[Task]**](Task.md) |  | 


