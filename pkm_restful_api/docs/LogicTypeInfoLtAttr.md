# PkmRestfulApi.LogicTypeInfoLtAttr

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Attr** | [**Attr**](Attr.md) |  | [optional] 
**AttrAnnot** | **String** |  | [optional] 


