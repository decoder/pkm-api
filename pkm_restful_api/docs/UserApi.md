# PkmRestfulApi.UserApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**access**](UserApi.md#access) | **POST** /user/access | 
[**deleteUser**](UserApi.md#deleteUser) | **DELETE** /user/{userName} | 
[**dup**](UserApi.md#dup) | **POST** /user/dup | 
[**getCurrentUser**](UserApi.md#getCurrentUser) | **GET** /user/current | 
[**getUser**](UserApi.md#getUser) | **GET** /user/{userName} | 
[**getUserProject**](UserApi.md#getUserProject) | **GET** /user/{userName}/project/{projectName} | 
[**getUserProjects**](UserApi.md#getUserProjects) | **GET** /user/{userName}/project | 
[**login**](UserApi.md#login) | **POST** /user/login | 
[**logout**](UserApi.md#logout) | **POST** /user/logout | 
[**postUser**](UserApi.md#postUser) | **POST** /user | 
[**putUser**](UserApi.md#putUser) | **PUT** /user | 



## access

> access(key)



User&#39;s access. PKM access key validity is checked and session lifetime is extended until another session timeout (see session_timeout in PKM configuration)

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var key = "key_example"; // String | Access key to the PKM
apiInstance.access(key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteUser

> deleteUser(userName, key)



delete user

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var userName = "userName_example"; // String | user's name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteUser(userName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| user&#39;s name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## dup

> Object dup(key)



Duplicate user&#39;s session, returns a new access key to the PKM.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var key = "key_example"; // String | Access key to the PKM
apiInstance.dup(key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCurrentUser

> PkmUser getCurrentUser(key)



Get current user

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var key = "key_example"; // String | Access key to the PKM
apiInstance.getCurrentUser(key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmUser**](PkmUser.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUser

> PkmUser getUser(userName, key)



Get user. WARNING! user can only view own Git user&#39;s credentials even if he&#39;s an administrator.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var userName = "userName_example"; // String | user's name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getUser(userName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| user&#39;s name | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmUser**](PkmUser.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUserProject

> PkmProject getUserProject(userName, projectName, key, opts)



Get a user project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var userName = "userName_example"; // String | user's name
var projectName = "projectName_example"; // String | project name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint
};
apiInstance.getUserProject(userName, projectName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| user&#39;s name | 
 **projectName** | **String**| project name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmProject**](PkmProject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUserProjects

> [PkmProject] getUserProjects(userName, key, opts)



Get user&#39;s projects

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var userName = "userName_example"; // String | user's name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint
};
apiInstance.getUserProjects(userName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userName** | **String**| user&#39;s name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**[PkmProject]**](PkmProject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## login

> Object login(body)



User&#39;s login, returns an access key to the PKM

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var body = new PkmRestfulApi.PkmLoginInfo(); // PkmLoginInfo | user's login informations
apiInstance.login(body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PkmLoginInfo**](PkmLoginInfo.md)| user&#39;s login informations | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## logout

> logout(key)



User&#39;s logout, invalidates an access key to the PKM

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var key = "key_example"; // String | Access key to the PKM
apiInstance.logout(key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postUser

> String postUser(key, body)



Create a new user. WARNING! an administrator can set Git user&#39;s credentials at user creation but cannot alter them with later putUser operation.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.PkmUser(); // PkmUser | user's information
apiInstance.postUser(key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 
 **body** | [**PkmUser**](PkmUser.md)| user&#39;s information | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putUser

> String putUser(key, body)



Update a user. WARNING! user can only change own Git user&#39;s credentials even if he&#39;s an administrator.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UserApi();
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.PkmUser(); // PkmUser | user's information
apiInstance.putUser(key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 
 **body** | [**PkmUser**](PkmUser.md)| user&#39;s information | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

