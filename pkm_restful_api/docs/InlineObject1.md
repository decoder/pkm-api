# PkmRestfulApi.InlineObject1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**git_config** | **String** | Git configuration file content (same content as .git/config) | [optional] 


