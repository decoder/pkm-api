# PkmRestfulApi.PkmReviewReviewArtifacts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** |  | 
**type** | **String** |  | 



## Enum: TypeEnum


* `Diagram` (value: `"Diagram"`)

* `Code` (value: `"Code"`)

* `Document` (value: `"Document"`)

* `Doc` (value: `"Doc"`)

* `Annotation` (value: `"Annotation"`)

* `Log` (value: `"Log"`)

* `Comment` (value: `"Comment"`)

* `TESTAR_State_Model` (value: `"TESTAR_State_Model"`)

* `UML Model` (value: `"UML Model"`)

* `NER` (value: `"NER"`)

* `SRL` (value: `"SRL"`)




