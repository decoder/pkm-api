# PkmRestfulApi.PkmServerInfoServer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | title | [optional] 
**description** | **String** | description | [optional] 
**version** | **String** | server version | [optional] 
**contact** | [**PkmServerInfoServerContact**](PkmServerInfoServerContact.md) |  | [optional] 
**license** | [**PkmServerInfoServerLicense**](PkmServerInfoServerLicense.md) |  | [optional] 
**copyright** | [**PkmServerInfoServerCopyright**](PkmServerInfoServerCopyright.md) |  | [optional] 


