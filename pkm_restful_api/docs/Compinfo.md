# PkmRestfulApi.Compinfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cstruct** | **Boolean** |  | [optional] 
**corig_name** | **String** |  | [optional] 
**cname** | **String** |  | [optional] 
**ckey** | **Number** |  | [optional] 
**cattr** | [**[CompinfoCattr]**](CompinfoCattr.md) |  | [optional] 


