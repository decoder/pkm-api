# PkmRestfulApi.Stmt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **Number** |  | [optional] 
**labels** | **[Object]** |  | [optional] 
**skind** | [**Skind**](Skind.md) |  | [optional] 
**ghost** | **Boolean** |  | [optional] 


