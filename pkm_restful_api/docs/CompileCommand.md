# PkmRestfulApi.CompileCommand

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**directory** | **String** | working directory of the compilation relative to the virtual root directory of a project | 
**file** | **String** | path of main translation unit source processed by this compilation step relative to the virtual root directory of a project | 
**command** | **String** | compile command executed; preprocessor include directives are relative to \&quot;directory\&quot; property (mutually exclusive with \&quot;arguments\&quot;) | 
**output** | **String** | name of the output created by this compilation step | [optional] 


