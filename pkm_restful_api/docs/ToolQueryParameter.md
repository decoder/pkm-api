# PkmRestfulApi.ToolQueryParameter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**required** | **Boolean** |  | [optional] 
**type** | **String** |  | [optional] 
**allowedValues** | **[String]** |  | [optional] 


