# PkmRestfulApi.UmlOperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**returnType** | **String** |  | [optional] 
**parameters** | [**[UmlParameter]**](UmlParameter.md) |  | [optional] 


