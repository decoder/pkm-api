# PkmRestfulApi.TComp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Tcomp_compinfo** | [**Compinfo**](Compinfo.md) |  | [optional] 
**TComp_bitssize** | [**TCompBitssize**](TCompBitssize.md) |  | [optional] 
**TComp_attrs** | [**[Attr]**](Attr.md) |  | [optional] 


