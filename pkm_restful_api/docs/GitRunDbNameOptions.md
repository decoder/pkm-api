# PkmRestfulApi.GitRunDbNameOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dont_delete_pkm_files** | **Boolean** | a flag to control deletion of files in the PKM missing in any the Git working trees. This prevents to accidentally delete files in the PKM because they were deleted on the remote Git server. | [optional] [default to false]
**dont_delete_git_working_tree_files** | **Boolean** | a flag to control deletion of files on Git working trees missing in the PKM project. This prevents to accidentally delete files on a remote Git server because they were deleted in the PKM. | [optional] [default to false]
**git_user_credentials** | [**[PkmGitUserCredential]**](PkmGitUserCredential.md) | Git user&#39;s credentials (in complement to the User&#39;s wallet for Git credentials) | [optional] 


