# PkmRestfulApi.TheItemsSchema1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sequenceId** | **String** |  | [default to &#39;&#39;]
**numberSequenceNodes** | **Number** |  | [default to 0]
**startDateTime** | **String** |  | [default to &#39;&#39;]
**verdict** | **String** |  | [default to &#39;&#39;]
**foundErrors** | **Boolean** |  | [default to false]
**numberErrors** | **Number** |  | [default to 0]
**sequenceDeterministic** | **Boolean** |  | [default to false]
**sequenceActionSteps** | [**[TheItemsSchema]**](TheItemsSchema.md) |  | 


