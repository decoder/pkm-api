# PkmRestfulApi.PkmCveCVEDataMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ASSIGNER** | **String** |  | 
**ID** | **String** |  | 
**STATE** | **String** |  | 
**UPDATED** | **String** |  | 


