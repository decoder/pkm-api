# PkmRestfulApi.Fieldinfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**forig_name** | **String** |  | [optional] 
**fname** | **String** |  | [optional] 
**ftype** | [**Typ**](.md) |  | [optional] 
**fbitfield** | **Number** |  | [optional] 
**fattr** | [**[Attr]**](Attr.md) |  | [optional] 
**floc** | [**Loc**](Loc.md) |  | [optional] 
**faddrof** | **Boolean** |  | [optional] 


