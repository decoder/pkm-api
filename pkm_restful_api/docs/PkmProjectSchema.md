# PkmRestfulApi.PkmProjectSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | project name | 
**members** | [**[PkmProjectMembers]**](PkmProjectMembers.md) |  | [optional] 
**tools** | [**[Tools]**](Tools.md) |  | [optional] 
**methodologyStatus** | [**[PkmMethodologyStatusSchema]**](PkmMethodologyStatusSchema.md) |  | [optional] 
**testarSettings** | **Object** | TESTAR settings | [optional] 


