# PkmRestfulApi.Pfreeable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StmtLabel** | **String** |  | [optional] 
**FormalLabel** | **String** |  | [optional] 
**BuiltinLabel** | [**BuiltinLabel**](BuiltinLabel.md) |  | [optional] 
**term** | [**Term**](Term.md) |  | [optional] 


