# PkmRestfulApi.TermLval

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TVar** | [**LVarInfo**](LVarInfo.md) |  | [optional] 
**TMem** | [**TMem**](TMem.md) |  | [optional] 
**TResult** | [**TResult**](TResult.md) |  | [optional] 
**TNoOffset** | **String** |  | [optional] 
**TField** | [**TField**](TField.md) |  | [optional] 
**TModel** | **Object** | TBD | [optional] 
**TIndex** | **Object** | TBD | [optional] 


