# PkmRestfulApi.SkindValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instr_kind** | **String** |  | [optional] 
**instr_value** | [**InstrValue**](InstrValue.md) |  | [optional] 



## Enum: InstrKindEnum


* `Set` (value: `"Set"`)

* `Call` (value: `"Call"`)

* `Asm` (value: `"Asm"`)

* `Skip` (value: `"Skip"`)

* `Code_annot` (value: `"Code_annot"`)

* `Local_init` (value: `"Local_init"`)




