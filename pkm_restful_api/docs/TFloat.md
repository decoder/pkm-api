# PkmRestfulApi.TFloat

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TFloat_kind** | **String** |  | [optional] 
**TFloat_attrs** | [**[Attr]**](Attr.md) |  | [optional] 



## Enum: TFloatKindEnum


* `FDouble` (value: `"FDouble"`)

* `FFloat` (value: `"FFloat"`)

* `FLongDouble` (value: `"FLongDouble"`)




