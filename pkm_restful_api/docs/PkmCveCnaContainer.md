# PkmRestfulApi.PkmCveCnaContainer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affected** | [**PkmCveCnaContainerAffected**](PkmCveCnaContainerAffected.md) |  | 
**descriptions** | [**[PkmCveCnaContainerDescriptions]**](PkmCveCnaContainerDescriptions.md) |  | 
**problemtypes** | [**PkmCveCnaContainerProblemtypes**](PkmCveCnaContainerProblemtypes.md) |  | 
**references** | [**[PkmCveCnaContainerReferences]**](PkmCveCnaContainerReferences.md) |  | 


