# PkmRestfulApi.PkmAsfm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | [optional] 
**sourceFile** | **String** |  | [optional] 
**name** | **String** |  | 
**units** | [**[PkmAsfmUnits]**](PkmAsfmUnits.md) |  | [optional] 


