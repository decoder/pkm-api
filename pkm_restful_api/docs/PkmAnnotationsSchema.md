# PkmRestfulApi.PkmAnnotationsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** | pkm path to query to retrieve json data containing the text | 
**access** | **String** | json path allowing to retrieve the text to analyze in the retrieved data | 
**srl** | [**[SRLResult]**](SRLResult.md) |  | [optional] 
**ner** | [**[NERResult]**](NERResult.md) |  | [optional] 


