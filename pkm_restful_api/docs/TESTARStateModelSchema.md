# PkmRestfulApi.TESTARStateModelSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **String** |  | [default to &#39;&#39;]
**url** | **String** |  | [default to &#39;&#39;]
**sut** | [**TheSutSchema**](TheSutSchema.md) |  | 
**tool** | [**TheToolSchema**](TheToolSchema.md) |  | 
**stateModelDataStore** | **String** |  | [default to &#39;&#39;]
**stateModelDataStoreType** | **String** |  | [default to &#39;&#39;]
**stateModelDataStoreServer** | **String** |  | [default to &#39;&#39;]
**stateModelDataStoreDirectory** | **String** |  | [default to &#39;&#39;]
**stateModelDataStoreDB** | **String** |  | [default to &#39;&#39;]
**stateModelDataStoreUser** | **String** |  | [default to &#39;&#39;]
**stateModelDataStorePassword** | **String** |  | [default to &#39;&#39;]
**stateModelIdentifier** | **String** |  | [default to &#39;&#39;]
**stateModelAppName** | **String** |  | [default to &#39;&#39;]
**stateModelAppVersion** | **String** |  | [default to &#39;&#39;]
**stateModelDifference** | [**TheStateModelDifferenceSchema**](TheStateModelDifferenceSchema.md) |  | 
**abstractionId** | **String** |  | [default to &#39;&#39;]
**deterministic** | **Boolean** |  | [default to false]
**unvisitedAbstractActions** | **Number** |  | [default to 0]
**numberAbstractStates** | **Number** |  | [default to 0]
**numberAbstractActions** | **Number** |  | [default to 0]
**numberConcreteStates** | **Number** |  | [default to 0]
**numberConcreteActions** | **Number** |  | [default to 0]
**storeWidgets** | **Boolean** |  | [default to false]
**numberWidgets** | **Number** |  | [default to 0]
**numberTestSequences** | **Number** |  | [default to 0]
**testSequences** | [**[TheItemsSchema1]**](TheItemsSchema1.md) |  | 


