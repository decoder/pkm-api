# PkmRestfulApi.PkmInvocationInvocationResults

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** |  | 
**type** | **String** |  | [optional] 


