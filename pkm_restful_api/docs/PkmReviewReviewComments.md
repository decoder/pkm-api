# PkmRestfulApi.PkmReviewReviewComments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **String** |  | 
**author** | **String** |  | 
**datetime** | **String** |  | 


