# PkmRestfulApi.PredContent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Ptrue** | **Object** |  | [optional] 
**Pfalse** | **Object** |  | [optional] 
**Pforall** | [**Pforall**](Pforall.md) |  | [optional] 
**Pexists** | [**Pexists**](Pexists.md) |  | [optional] 
**Pimplies** | [**Pimplies**](Pimplies.md) |  | [optional] 
**Pseparated** | [**[TermClosed]**](TermClosed.md) |  | [optional] 
**Prel** | [**Prel**](Prel.md) |  | [optional] 
**Pnot** | [**Pnot**](Pnot.md) |  | [optional] 
**Papp** | [**Papp**](Papp.md) |  | [optional] 
**Pand** | [**Pand**](Pand.md) |  | [optional] 
**Por** | [**Por**](Por.md) |  | [optional] 
**Pxor** | [**Pxor**](Pxor.md) |  | [optional] 
**Piff** | [**Piff**](Piff.md) |  | [optional] 
**Pif** | [**Pif**](Pif.md) |  | [optional] 
**Plet** | [**Plet**](Plet.md) |  | [optional] 
**Pat** | [**Pat**](Pat.md) |  | [optional] 
**Pvalid** | [**Pvalid**](Pvalid.md) |  | [optional] 
**Pvalid_read** | [**PvalidRead**](PvalidRead.md) |  | [optional] 
**Pvalid_function** | [**PvalidFunction**](PvalidFunction.md) |  | [optional] 
**Pinitialized** | [**Pinitialized**](Pinitialized.md) |  | [optional] 
**Pdangling** | [**Pdangling**](Pdangling.md) |  | [optional] 
**Pallocable** | [**Pallocable**](Pallocable.md) |  | [optional] 
**Pfreeable** | [**Pfreeable**](Pfreeable.md) |  | [optional] 
**Pfresh** | [**Pfresh**](Pfresh.md) |  | [optional] 
**Psubtype** | [**Psubtype**](Psubtype.md) |  | [optional] 


