# PkmRestfulApi.GlobalKind

## Enum


* `GType` (value: `"GType"`)

* `GCompTag` (value: `"GCompTag"`)

* `GCompTagDecl` (value: `"GCompTagDecl"`)

* `GEnumTag` (value: `"GEnumTag"`)

* `GEnumTagDecl` (value: `"GEnumTagDecl"`)

* `GVarDecl` (value: `"GVarDecl"`)

* `GFunDecl` (value: `"GFunDecl"`)

* `GVar` (value: `"GVar"`)

* `GFun` (value: `"GFun"`)

* `GAsm` (value: `"GAsm"`)

* `GPragma` (value: `"GPragma"`)

* `GText` (value: `"GText"`)

* `GAnnot` (value: `"GAnnot"`)


