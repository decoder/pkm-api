# PkmRestfulApi.ManifestKind

## Enum


* `function` (value: `"function"`)

* `variable` (value: `"variable"`)

* `enum` (value: `"enum"`)

* `typedef` (value: `"typedef"`)

* `struct` (value: `"struct"`)

* `union` (value: `"union"`)

* `class` (value: `"class"`)

* `field` (value: `"field"`)

* `method` (value: `"method"`)


