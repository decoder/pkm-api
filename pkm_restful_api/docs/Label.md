# PkmRestfulApi.Label

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Label_string** | **String** |  | [optional] 
**Label_loc** | [**Loc**](Loc.md) |  | [optional] 
**Label_bool** | **Boolean** |  | [optional] 


