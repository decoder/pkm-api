# PkmRestfulApi.DbApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDatabase**](DbApi.md#deleteDatabase) | **DELETE** /db/{dbName} | 
[**postDatabase**](DbApi.md#postDatabase) | **POST** /db | 



## deleteDatabase

> deleteDatabase(dbName, key)



drop database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DbApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteDatabase(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postDatabase

> String postDatabase(key, body)



create a new database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DbApi();
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.InlineObject2(); // InlineObject2 | 
apiInstance.postDatabase(key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 
 **body** | [**InlineObject2**](InlineObject2.md)|  | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

