# PkmRestfulApi.MoveAssign

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exists** | **Boolean** |  | [optional] 
**needsImplicit** | **Boolean** |  | [optional] 
**simple** | **Boolean** |  | [optional] 
**trivial** | **Boolean** |  | [optional] 


