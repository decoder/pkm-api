# PkmRestfulApi.PkmGitUserCredentialSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**git_remote_url** | **String** | Git remote URL | 
**git_user_name** | **String** | Git user&#39;s name | [optional] 
**git_password** | **String** | Git password (HTTPS password/token or SSH passphrase to unlock SSH private key) | [optional] 
**git_ssh_private_key** | **String** | Git SSH private key | [optional] 


