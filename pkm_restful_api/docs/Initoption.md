# PkmRestfulApi.Initoption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**option** | [**Option**](Option.md) |  | [optional] 
**value** | [**Value**](Value.md) |  | [optional] 


