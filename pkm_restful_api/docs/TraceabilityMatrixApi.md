# PkmRestfulApi.TraceabilityMatrixApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteTraceabilityMatrix**](TraceabilityMatrixApi.md#deleteTraceabilityMatrix) | **DELETE** /traceability_matrix/{dbName} | 
[**deleteTraceabilityMatrixCell**](TraceabilityMatrixApi.md#deleteTraceabilityMatrixCell) | **DELETE** /traceability_matrix/{dbName}/{artefactId} | 
[**getTraceabilityMatrix**](TraceabilityMatrixApi.md#getTraceabilityMatrix) | **GET** /traceability_matrix/{dbName} | 
[**getTraceabilityMatrixCell**](TraceabilityMatrixApi.md#getTraceabilityMatrixCell) | **GET** /traceability_matrix/{dbName}/{artefactId} | 
[**postTraceabilityMatrix**](TraceabilityMatrixApi.md#postTraceabilityMatrix) | **POST** /traceability_matrix/{dbName} | 
[**putTraceabilityMatrix**](TraceabilityMatrixApi.md#putTraceabilityMatrix) | **PUT** /traceability_matrix/{dbName} | 



## deleteTraceabilityMatrix

> deleteTraceabilityMatrix(dbName, key)



Delete traceability matrix (all traceability matrix cell documents) from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TraceabilityMatrixApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTraceabilityMatrix(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteTraceabilityMatrixCell

> deleteTraceabilityMatrixCell(dbName, artefactId, key)



Delete a traceability matrix cell document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TraceabilityMatrixApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | the ID of traceability matrix cell
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTraceabilityMatrixCell(dbName, artefactId, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| the ID of traceability matrix cell | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTraceabilityMatrix

> [PkmTraceabilityMatrix] getTraceabilityMatrix(dbName, key, opts)



Get traceability matrix (all traceability matrix cell documents) or a part of it with row number in range [ fromRow, toRow ] and column number in range [ fromColumn, toColumn ] from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TraceabilityMatrixApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'fromRow': 0, // Number | number of rows to skip, useful for lowering response footprint
  'fromColumn': 0, // Number | number of columns to skip, useful for lowering response footprint
  'toRow': 56, // Number | last row (should be >= fromRow otherwise resulting matrix is empty), useful for lowering response footprint
  'toColumn': 56, // Number | last column (should be >= fromColumn otherwise resulting matrix is empty), useful for lowering response footprint
  'rowRole': "'source'" // String | role of row
};
apiInstance.getTraceabilityMatrix(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **fromRow** | **Number**| number of rows to skip, useful for lowering response footprint | [optional] [default to 0]
 **fromColumn** | **Number**| number of columns to skip, useful for lowering response footprint | [optional] [default to 0]
 **toRow** | **Number**| last row (should be &gt;&#x3D; fromRow otherwise resulting matrix is empty), useful for lowering response footprint | [optional] 
 **toColumn** | **Number**| last column (should be &gt;&#x3D; fromColumn otherwise resulting matrix is empty), useful for lowering response footprint | [optional] 
 **rowRole** | **String**| role of row | [optional] [default to &#39;source&#39;]

### Return type

[**[PkmTraceabilityMatrix]**](PkmTraceabilityMatrix.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTraceabilityMatrixCell

> PkmTraceabilityMatrix getTraceabilityMatrixCell(dbName, artefactId, key)



Get a traceability matrix cell document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TraceabilityMatrixApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | the ID of traceability matrix cell
var key = "key_example"; // String | Access key to the PKM
apiInstance.getTraceabilityMatrixCell(dbName, artefactId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| the ID of traceability matrix cell | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmTraceabilityMatrix**](PkmTraceabilityMatrix.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postTraceabilityMatrix

> [String] postTraceabilityMatrix(dbName, key, body)



Post one or more traceability matrix cells into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TraceabilityMatrixApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmTraceabilityMatrix()]; // [PkmTraceabilityMatrix] | Traceability Matrix cell documents
apiInstance.postTraceabilityMatrix(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmTraceabilityMatrix]**](PkmTraceabilityMatrix.md)| Traceability Matrix cell documents | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putTraceabilityMatrix

> [String] putTraceabilityMatrix(dbName, key, body)



Insert (if not yet present) or update (if already present) one or more traceability matrix cells into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.TraceabilityMatrixApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmTraceabilityMatrix()]; // [PkmTraceabilityMatrix] | Traceability Matrix cell documents
apiInstance.putTraceabilityMatrix(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmTraceabilityMatrix]**](PkmTraceabilityMatrix.md)| Traceability Matrix cell documents | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

