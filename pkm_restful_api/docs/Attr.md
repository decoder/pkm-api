# PkmRestfulApi.Attr

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Attr_string** | **String** |  | [optional] 
**Attrparam_list** | [**[AttrparamList]**](AttrparamList.md) |  | [optional] 


