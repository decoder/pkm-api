# PkmRestfulApi.ServerInfoApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getServerInfo**](ServerInfoApi.md#getServerInfo) | **GET** /server_info | 



## getServerInfo

> PkmServerInfo getServerInfo()



Returns information about the PKM instance

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ServerInfoApi();
apiInstance.getServerInfo().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PkmServerInfo**](PkmServerInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

