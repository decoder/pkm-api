# PkmRestfulApi.GAnnot

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Dfun_or_pred** | [**DfunOrPred**](DfunOrPred.md) |  | [optional] 
**Dlemma** | [**Dlemma**](Dlemma.md) |  | [optional] 
**Dvolatile** | **Object** | TBD | [optional] 
**Daxiomatic** | [**Daxiomatic**](Daxiomatic.md) |  | [optional] 
**Dtype** | **Object** | TBD | [optional] 
**Dinvariant** | **Object** | TBD | [optional] 
**Dtype_annot** | **Object** | TBD | [optional] 
**Dmodel_annot** | **Object** | TBD | [optional] 
**Dcustom_annot** | **Object** | TBD | [optional] 
**Dextended** | **Object** | TBD | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


