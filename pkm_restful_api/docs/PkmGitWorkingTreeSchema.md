# PkmRestfulApi.PkmGitWorkingTreeSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**directory** | **String** | Directory of the (main) Git working tree | [optional] 
**git_directory** | **String** | Git directory | [optional] 
**git_branch** | **String** | Tracked Git branch | 
**git_commit_id** | **String** | SHA1 ID of current commit | 
**git_config** | **Object** | Git configuration of the Git directory (usually .git/config) associated to a Git working tree | [optional] 
**linked** | [**[PkmGitWorkingTreeLinked]**](PkmGitWorkingTreeLinked.md) | Linked Git working trees created with &#39;git worktree add&#39; | [optional] 


