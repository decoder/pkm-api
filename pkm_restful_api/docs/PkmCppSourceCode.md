# PkmRestfulApi.PkmCppSourceCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | type of document for the GUI, typically &#39;Code&#39; | [optional] 
**sourceFile** | **String** | related source code file | 
**manifest** | [**[ManifestItem]**](ManifestItem.md) | Index of C++ artefacts in the document to simplify the search for C++ artefacts | 
**inner** | [**[Node]**](Node.md) | List of child C++ artefacts | [optional] 


