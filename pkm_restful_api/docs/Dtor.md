# PkmRestfulApi.Dtor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**irrelevant** | **Boolean** |  | [optional] 
**needsImplicit** | **Boolean** |  | [optional] 
**simple** | **Boolean** |  | [optional] 
**trivial** | **Boolean** |  | [optional] 


