# PkmRestfulApi.Loc1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pos_start** | [**Pos**](Pos.md) |  | [optional] 
**pos_end** | [**Pos**](Pos.md) |  | [optional] 


