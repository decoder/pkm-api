# PkmRestfulApi.LBterm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**term** | [**Term**](Term.md) |  | [optional] 


