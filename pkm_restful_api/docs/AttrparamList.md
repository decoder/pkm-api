# PkmRestfulApi.AttrparamList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AStr** | **String** |  | [optional] 
**AInt** | **Number** |  | [optional] 
**ACons** | **String** |  | [optional] 


