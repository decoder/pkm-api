# PkmRestfulApi.PkmUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | user&#39;s name | 
**password** | **String** | user&#39;s password | [optional] 
**first_name** | **String** | first name | [optional] 
**last_name** | **String** | last name | [optional] 
**email** | **String** | email address | [optional] 
**phone** | **String** | phone number | [optional] 
**roles** | [**[PkmUserRoles]**](PkmUserRoles.md) |  | [optional] 
**git_user_credentials** | [**[PkmGitUserCredentialSchema]**](PkmGitUserCredentialSchema.md) |  | [optional] 


