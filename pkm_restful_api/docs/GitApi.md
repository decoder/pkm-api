# PkmRestfulApi.GitApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteGitFile**](GitApi.md#deleteGitFile) | **DELETE** /git/files/{dbName}/{gitWorkingTree}/{filename} | 
[**deleteGitFiles**](GitApi.md#deleteGitFiles) | **DELETE** /git/files/{dbName}/{gitWorkingTree} | 
[**deleteGitWorkingTree**](GitApi.md#deleteGitWorkingTree) | **DELETE** /git/working_trees/{dbName}/{gitWorkingTree} | 
[**deleteGitWorkingTrees**](GitApi.md#deleteGitWorkingTrees) | **DELETE** /git/working_trees/{dbName} | 
[**getGitConfig**](GitApi.md#getGitConfig) | **GET** /git/config/{dbName}/{gitWorkingTree} | 
[**getGitFile**](GitApi.md#getGitFile) | **GET** /git/files/{dbName}/{gitWorkingTree}/{filename} | 
[**getGitFiles**](GitApi.md#getGitFiles) | **GET** /git/files/{dbName}/{gitWorkingTree} | 
[**getGitJob**](GitApi.md#getGitJob) | **GET** /git/jobs/{jobId} | 
[**getGitWorkingTree**](GitApi.md#getGitWorkingTree) | **GET** /git/working_trees/{dbName}/{gitWorkingTree} | 
[**getGitWorkingTrees**](GitApi.md#getGitWorkingTrees) | **GET** /git/working_trees/{dbName} | 
[**postGitFiles**](GitApi.md#postGitFiles) | **POST** /git/files/{dbName}/{gitWorkingTree} | 
[**postGitRun**](GitApi.md#postGitRun) | **POST** /git/run/{dbName} | 
[**putGitConfig**](GitApi.md#putGitConfig) | **PUT** /git/config/{dbName}/{gitWorkingTree} | 
[**putGitFiles**](GitApi.md#putGitFiles) | **PUT** /git/files/{dbName}/{gitWorkingTree} | 



## deleteGitFile

> deleteGitFile(dbName, gitWorkingTree, filename, key)



delete a file in the Git working tree associated to the database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var filename = "filename_example"; // String | filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteGitFile(dbName, gitWorkingTree, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **filename** | **String**| filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteGitFiles

> deleteGitFiles(dbName, gitWorkingTree, key)



delete files in the Git working tree associated to the database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteGitFiles(dbName, gitWorkingTree, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteGitWorkingTree

> deleteGitWorkingTree(dbName, gitWorkingTree, key, opts)



delete a Git Working Tree document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'dontDeletePkmFiles': false // Boolean | a flag to control deletion of files in the PKM together with the Git working trees.
};
apiInstance.deleteGitWorkingTree(dbName, gitWorkingTree, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 
 **dontDeletePkmFiles** | **Boolean**| a flag to control deletion of files in the PKM together with the Git working trees. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteGitWorkingTrees

> deleteGitWorkingTrees(dbName, key, opts)



Delete all Git Working Tree documents present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'dontDeletePkmFiles': false // Boolean | a flag to control deletion of files in the PKM together with the Git working trees.
};
apiInstance.deleteGitWorkingTrees(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **dontDeletePkmFiles** | **Boolean**| a flag to control deletion of files in the PKM together with the Git working trees. | [optional] [default to false]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGitConfig

> InlineResponse200 getGitConfig(dbName, gitWorkingTree, key)



Get the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
apiInstance.getGitConfig(dbName, gitWorkingTree, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGitFile

> PkmFile getGitFile(dbName, gitWorkingTree, filename, key, opts)



get a file in the Git working tree associated to the database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var filename = "filename_example"; // String | filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'encoding': "'utf8'" // String | text encoding
};
apiInstance.getGitFile(dbName, gitWorkingTree, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **filename** | **String**| filename | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **encoding** | **String**| text encoding | [optional] [default to &#39;utf8&#39;]

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGitFiles

> [PkmFile] getGitFiles(dbName, gitWorkingTree, key, opts)



get files in the Git working tree associated to the database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'encoding': "'utf8'" // String | text encoding
};
apiInstance.getGitFiles(dbName, gitWorkingTree, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **encoding** | **String**| text encoding | [optional] [default to &#39;utf8&#39;]

### Return type

[**[PkmFile]**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGitJob

> GitJob getGitJob(jobId, key)



Get Git Job. Getting a finished or failed job, unpublish it (it is no longer available))

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var jobId = 56; // Number | Job identifier
var key = "key_example"; // String | Access key to the PKM
apiInstance.getGitJob(jobId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| Job identifier | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**GitJob**](GitJob.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGitWorkingTree

> PkmGitWorkingTree getGitWorkingTree(dbName, gitWorkingTree, key)



Get Git Working Tree documents by Git working tree from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
apiInstance.getGitWorkingTree(dbName, gitWorkingTree, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmGitWorkingTree**](PkmGitWorkingTree.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGitWorkingTrees

> [PkmGitWorkingTree] getGitWorkingTrees(dbName, key, opts)



Get all Git Working Tree documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getGitWorkingTrees(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmGitWorkingTree]**](PkmGitWorkingTree.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postGitFiles

> String postGitFiles(dbName, gitWorkingTree, key, body)



post some files in the Git working tree associated to the database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Files
apiInstance.postGitFiles(dbName, gitWorkingTree, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Files | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postGitRun

> GitJob postGitRun(dbName, key, body, opts)



Run Git commands

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.InlineObject(); // InlineObject | 
var opts = {
  'asynchronous': false // Boolean | flag to control asynchronous/synchronous execution of Git job
};
apiInstance.postGitRun(dbName, key, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**InlineObject**](InlineObject.md)|  | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of Git job | [optional] [default to false]

### Return type

[**GitJob**](GitJob.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putGitConfig

> putGitConfig(dbName, gitWorkingTree, key, body)



Update the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.InlineObject1(); // InlineObject1 | 
apiInstance.putGitConfig(dbName, gitWorkingTree, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**InlineObject1**](InlineObject1.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putGitFiles

> putGitFiles(dbName, gitWorkingTree, key, body)



put some files in the Git working tree associated to the database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.GitApi();
var dbName = "dbName_example"; // String | Database name
var gitWorkingTree = "gitWorkingTree_example"; // String | Git working tree
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Files
apiInstance.putGitFiles(dbName, gitWorkingTree, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **gitWorkingTree** | **String**| Git working tree | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Files | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

