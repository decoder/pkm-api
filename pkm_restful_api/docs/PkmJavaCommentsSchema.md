# PkmRestfulApi.PkmJavaCommentsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileEncoding** | **String** |  | 
**comments** | [**[JavaComment]**](JavaComment.md) |  | 
**fileMimeType** | **String** |  | 
**sourceFile** | **String** |  | 
**fileFormat** | **String** |  | 
**type** | **String** | type of document for the GUI, typically &#39;Comment&#39; | [optional] 


