# PkmRestfulApi.CVEApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCVE**](CVEApi.md#deleteCVE) | **DELETE** /cve/{dbName}/{id} | 
[**deleteCVEs**](CVEApi.md#deleteCVEs) | **DELETE** /cve/{dbName} | 
[**getCVEs**](CVEApi.md#getCVEs) | **GET** /cve/{dbName} | 
[**postCVEs**](CVEApi.md#postCVEs) | **POST** /cve/{dbName} | 
[**putCVEs**](CVEApi.md#putCVEs) | **PUT** /cve/{dbName} | 



## deleteCVE

> deleteCVE(dbName, id, key)



Delete a CVE document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CVEApi();
var dbName = "dbName_example"; // String | Database name
var id = "id_example"; // String | the ID of the CVE document
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCVE(dbName, id, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **id** | **String**| the ID of the CVE document | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteCVEs

> deleteCVEs(dbName, key)



Delete all CVE documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CVEApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteCVEs(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCVEs

> [PkmCve] getCVEs(dbName, key, opts)



Get all CVE documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CVEApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': "id_example", // String | CVE document ID
  'state': "state_example", // String | CVE state
  'assigner': "assigner_example", // String | CVE assigner
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getCVEs(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **String**| CVE document ID | [optional] 
 **state** | **String**| CVE state | [optional] 
 **assigner** | **String**| CVE assigner | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmCve]**](PkmCve.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postCVEs

> postCVEs(dbName, key, opts)



Post one or more CVE documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CVEApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'body': [new PkmRestfulApi.PkmCve()] // [PkmCve] | CVE documents
};
apiInstance.postCVEs(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCve]**](PkmCve.md)| CVE documents | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putCVEs

> putCVEs(dbName, key, opts)



Insert (if not yet present) or update (if already present) one or more CVE documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.CVEApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'body': [new PkmRestfulApi.PkmCve()] // [PkmCve] | CVE documents
};
apiInstance.putCVEs(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmCve]**](PkmCve.md)| CVE documents | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

