# PkmRestfulApi.GEnumTagDecl

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enuminfo** | [**Enuminfo**](Enuminfo.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


