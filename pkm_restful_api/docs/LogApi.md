# PkmRestfulApi.LogApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteLog**](LogApi.md#deleteLog) | **DELETE** /log/{dbName}/{artefactId} | 
[**deleteLogs**](LogApi.md#deleteLogs) | **DELETE** /log/{dbName} | 
[**getLog**](LogApi.md#getLog) | **GET** /log/{dbName}/{artefactId} | 
[**getLogs**](LogApi.md#getLogs) | **GET** /log/{dbName} | 
[**postLogs**](LogApi.md#postLogs) | **POST** /log/{dbName} | 
[**putLogs**](LogApi.md#putLogs) | **PUT** /log/{dbName} | 



## deleteLog

> deleteLog(dbName, artefactId, key)



Delete a Log document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.LogApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | the ID of the Log document
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteLog(dbName, artefactId, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| the ID of the Log document | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteLogs

> deleteLogs(dbName, key)



Delete all Log documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.LogApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteLogs(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getLog

> PkmLog getLog(dbName, artefactId, key, opts)



Get a Log document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.LogApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | the ID of the Log document
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint
};
apiInstance.getLog(dbName, artefactId, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| the ID of the Log document | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmLog**](PkmLog.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getLogs

> [PkmLog] getLogs(dbName, key, opts)



Get all Log documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.LogApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getLogs(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint | [optional] [default to false]
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmLog]**](PkmLog.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postLogs

> [String] postLogs(dbName, key, body)



Post one or more Log documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.LogApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmLog()]; // [PkmLog] | Log documents
apiInstance.postLogs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmLog]**](PkmLog.md)| Log documents | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putLogs

> [String] putLogs(dbName, key, body)



Insert (if not yet present) or update (if already present) one or more Log documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.LogApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmLog()]; // [PkmLog] | Log documents
apiInstance.putLogs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmLog]**](PkmLog.md)| Log documents | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

