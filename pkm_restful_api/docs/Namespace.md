# PkmRestfulApi.Namespace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | identifier of the C++ artefact | 
**kind** | [**Kind**](Kind.md) |  | 
**name** | **String** |  | 


