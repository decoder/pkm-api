# PkmRestfulApi.PkmServerInfoServerLicense

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The license name used for the API | [optional] 
**url** | **String** | A URL to the license used for the API. | [optional] 


