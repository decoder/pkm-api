# PkmRestfulApi.TEnum

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enuminfo** | [**Enuminfo**](Enuminfo.md) |  | [optional] 
**TEnum_attrs** | [**[Attr]**](Attr.md) |  | [optional] 


