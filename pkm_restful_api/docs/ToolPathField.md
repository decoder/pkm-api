# PkmRestfulApi.ToolPathField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**required** | **Boolean** |  | [optional] 
**type** | **String** |  | [optional] 
**isDBName** | **Boolean** |  | [optional] 
**artifactDesc** | [**ToolArtifactDesc**](ToolArtifactDesc.md) |  | [optional] 


