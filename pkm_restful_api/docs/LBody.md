# PkmRestfulApi.LBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LBreads** | [**[LBread]**](LBread.md) |  | [optional] 
**LBterm** | [**LBterm**](LBterm.md) |  | [optional] 
**LBpred** | [**LBpred**](LBpred.md) |  | [optional] 
**LBinductive** | **[Object]** |  | [optional] 


