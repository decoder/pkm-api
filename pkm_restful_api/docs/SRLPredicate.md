# PkmRestfulApi.SRLPredicate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predicate** | [**SRLChunk**](SRLChunk.md) |  | 
**roles** | [**{String: SRLChunk}**](SRLChunk.md) |  | 


