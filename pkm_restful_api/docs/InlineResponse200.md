# PkmRestfulApi.InlineResponse200

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**git_config** | **String** | The Git configuration file content (same content as .git/config) | [optional] 


