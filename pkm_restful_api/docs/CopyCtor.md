# PkmRestfulApi.CopyCtor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hasConstParam** | **Boolean** |  | [optional] 
**implicitHasConstParam** | **Boolean** |  | [optional] 
**needsImplicit** | **Boolean** |  | [optional] 
**simple** | **Boolean** |  | [optional] 
**trivial** | **Boolean** |  | [optional] 


