# PkmRestfulApi.InvocationsApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteInvocation**](InvocationsApi.md#deleteInvocation) | **DELETE** /invocations/{dbName}/{invocationID} | 
[**deleteInvocations**](InvocationsApi.md#deleteInvocations) | **DELETE** /invocations/{dbName} | 
[**getInvocation**](InvocationsApi.md#getInvocation) | **GET** /invocations/{dbName}/{invocationID} | 
[**getInvocations**](InvocationsApi.md#getInvocations) | **GET** /invocations/{dbName} | 
[**postInvocations**](InvocationsApi.md#postInvocations) | **POST** /invocations/{dbName} | 
[**putInvocations**](InvocationsApi.md#putInvocations) | **PUT** /invocations/{dbName} | 



## deleteInvocation

> deleteInvocation(dbName, invocationID, key)



delete a Invocation document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InvocationsApi();
var dbName = "dbName_example"; // String | Database name
var invocationID = "invocationID_example"; // String | Invocation ID
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteInvocation(dbName, invocationID, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **invocationID** | **String**| Invocation ID | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteInvocations

> deleteInvocations(dbName, key)



Delete all Invocation documents present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InvocationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteInvocations(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getInvocation

> PkmInvocation getInvocation(dbName, invocationID, key)



Get Invocation documents by Invocation ID from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InvocationsApi();
var dbName = "dbName_example"; // String | Database name
var invocationID = "invocationID_example"; // String | Invocation ID
var key = "key_example"; // String | Access key to the PKM
apiInstance.getInvocation(dbName, invocationID, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **invocationID** | **String**| Invocation ID | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmInvocation**](PkmInvocation.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getInvocations

> [PkmInvocation] getInvocations(dbName, key, opts)



Get all Invocation documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InvocationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'invocationStatus': "invocationStatus_example", // String | invocation status
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getInvocations(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **invocationStatus** | **String**| invocation status | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmInvocation]**](PkmInvocation.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postInvocations

> String postInvocations(dbName, key, body)



Insert some Invocation documents (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InvocationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmInvocation()]; // [PkmInvocation] | Invocation documents
apiInstance.postInvocations(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmInvocation]**](PkmInvocation.md)| Invocation documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putInvocations

> String putInvocations(dbName, key, body)



Insert (if not yet present) or update (if already present) some Invocation documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.InvocationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmInvocation()]; // [PkmInvocation] | Invocation documents
apiInstance.putInvocations(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmInvocation]**](PkmInvocation.md)| Invocation documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

