# PkmRestfulApi.LogicVar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lv_name** | **String** |  | [optional] 
**lv_id** | **Number** |  | [optional] 
**lv_type** | [**LvType**](LvType.md) |  | [optional] 
**lv_kind** | [**LogicVarKind**](LogicVarKind.md) |  | [optional] 
**lv_origin** | [**LvOrigin**](LvOrigin.md) |  | [optional] 
**lv_attr** | **[Object]** |  | [optional] 


