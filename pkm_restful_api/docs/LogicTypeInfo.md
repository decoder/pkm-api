# PkmRestfulApi.LogicTypeInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lt_name** | **String** |  | [optional] 
**lt_params** | **[String]** |  | [optional] 
**lt_def** | **Object** |  | [optional] 
**lt_attr** | [**[LogicTypeInfoLtAttr]**](LogicTypeInfoLtAttr.md) |  | [optional] 


