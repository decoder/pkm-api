# PkmRestfulApi.PkmAsfmClasses

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**id** | **Number** |  | [optional] 
**parent** | **Number** |  | [optional] 
**doc** | **String** |  | [optional] 
**invariants** | **[String]** |  | [optional] 
**fields** | [**[PkmAsfmFields]**](PkmAsfmFields.md) |  | [optional] 
**methods** | [**[PkmAsfmFields]**](PkmAsfmFields.md) |  | [optional] 
**types** | [**[PkmAsfmFields]**](PkmAsfmFields.md) |  | [optional] 
**macros** | [**[PkmAsfmFields]**](PkmAsfmFields.md) |  | [optional] 
**constants** | [**[PkmAsfmFields]**](PkmAsfmFields.md) |  | [optional] 


