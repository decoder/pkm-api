# PkmRestfulApi.DefaultCtor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exists** | **Boolean** |  | [optional] 
**needsImplicit** | **Boolean** |  | [optional] 
**trivial** | **Boolean** |  | [optional] 
**defaultedIsConstexpr** | **Boolean** |  | [optional] 
**isConstexpr** | **Boolean** |  | [optional] 


