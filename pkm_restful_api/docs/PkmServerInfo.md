# PkmRestfulApi.PkmServerInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**server** | [**PkmServerInfoServer**](PkmServerInfoServer.md) |  | [optional] 
**api** | [**PkmServerInfoApi**](PkmServerInfoApi.md) |  | [optional] 


