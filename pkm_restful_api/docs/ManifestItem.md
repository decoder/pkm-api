# PkmRestfulApi.ManifestItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | identifier of the C++ artefact | 
**path** | **String** | simplified hierarchical name (without template arguments) of the artefact (e.g. std::max) | 
**name** | **String** | short name of the artefact | [optional] 
**kind** | [**ManifestKind**](ManifestKind.md) |  | [optional] 


