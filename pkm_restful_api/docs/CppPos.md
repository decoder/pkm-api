# PkmRestfulApi.CppPos

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pos_path** | **String** | Path where the file is located | [optional] 
**pos_lnum** | **Number** |  | [optional] 
**pos_bol** | **Number** |  | [optional] 
**pos_cnum** | **Number** |  | [optional] 


