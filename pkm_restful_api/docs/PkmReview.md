# PkmRestfulApi.PkmReview

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reviewID** | **String** |  | [optional] 
**reviewTitle** | **String** |  | 
**reviewStatus** | **String** |  | 
**reviewOpenDate** | **String** |  | 
**reviewCompletedDate** | **String** |  | [optional] 
**reviewAuthor** | **String** |  | 
**reviewComments** | [**[PkmReviewReviewComments]**](PkmReviewReviewComments.md) |  | [optional] 
**reviewArtifacts** | [**[PkmReviewReviewArtifacts]**](PkmReviewReviewArtifacts.md) |  | [optional] 



## Enum: ReviewStatusEnum


* `OPEN` (value: `"OPEN"`)

* `CLOSED` (value: `"CLOSED"`)




