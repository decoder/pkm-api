# PkmRestfulApi.GType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**typeinfo** | [**Typeinfo**](Typeinfo.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


