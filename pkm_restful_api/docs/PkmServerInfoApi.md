# PkmRestfulApi.PkmServerInfoApi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | title | [optional] 
**description** | **String** | description | [optional] 
**version** | **String** | API version | [optional] 
**contact** | [**PkmServerInfoServerContact**](PkmServerInfoServerContact.md) |  | [optional] 
**license** | [**PkmServerInfoServerLicense**](PkmServerInfoServerLicense.md) |  | [optional] 


