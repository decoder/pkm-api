# PkmRestfulApi.PkmTraceabilityMatrixTrace

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**similarity** | **Number** | The similarity between source and target texts | [optional] 
**role** | **String** | The kind of trace link between the source and the target. Can be empty. | [optional] 


