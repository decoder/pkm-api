# PkmRestfulApi.Exp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eid** | **Number** |  | [optional] 
**enode** | [**Enode**](Enode.md) |  | [optional] 
**eloc** | [**Loc**](Loc.md) |  | [optional] 


