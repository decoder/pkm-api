# PkmRestfulApi.Sbody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**battrs** | **[Object]** |  | [optional] 
**bscoping** | **Boolean** |  | [optional] 
**blocals** | **[Object]** |  | [optional] 
**bstatics** | **[Object]** |  | [optional] 
**bstmts** | **[Object]** |  | [optional] 


