# PkmRestfulApi.Predicate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pred_loc** | [**Loc**](Loc.md) |  | [optional] 
**pred_content** | [**PredContent**](PredContent.md) |  | [optional] 


