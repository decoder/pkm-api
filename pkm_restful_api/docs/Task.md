# PkmRestfulApi.Task

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**taskNumber** | **Number** |  | 
**name** | **String** |  | 
**id** | **String** |  | 
**completed** | **Boolean** |  | 


