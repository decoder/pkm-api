# PkmRestfulApi.CompileCommandWithArguments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**directory** | **String** | working directory of the compilation relative to the virtual root directory of a project | 
**file** | **String** | path of main translation unit source processed by this compilation step relative to the virtual root directory of a project | 
**_arguments** | **[String]** |  | 
**output** | **String** | name of the output created by this compilation step | [optional] 


