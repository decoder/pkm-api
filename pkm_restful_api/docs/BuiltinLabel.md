# PkmRestfulApi.BuiltinLabel

## Enum


* `Here` (value: `"Here"`)

* `Old` (value: `"Old"`)

* `Pre` (value: `"Pre"`)

* `Post` (value: `"Post"`)

* `LoopEntry` (value: `"LoopEntry"`)

* `LoopCurrent` (value: `"LoopCurrent"`)

* `Init` (value: `"Init"`)


