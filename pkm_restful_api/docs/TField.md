# PkmRestfulApi.TField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TField_info** | [**Fieldinfo**](Fieldinfo.md) |  | [optional] 
**TField_term** | [**TermOffset**](TermOffset.md) |  | [optional] 


