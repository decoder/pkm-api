# PkmRestfulApi.PkmCCommentsComments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global_kind** | [**GlobalKind**](GlobalKind.md) |  | [optional] 
**loc** | [**Loc1**](Loc1.md) |  | [optional] 
**comments** | **[String]** |  | [optional] 


