# PkmRestfulApi.PkmFileSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rel_path** | **String** | The relative path (relative to a virtual root directory for the project or the Git working tree) of the file (POSIX naming scheme) | 
**content** | **String** | The content of the file. The original file content shall be encoded as &#39;base64&#39; (server accepts RFC 4648 §4/§5, and exports RFC 4648 §4) when &#39;format&#39; field value is &#39;binary&#39; | [optional] 
**format** | **String** | The format of content | [optional] 
**encoding** | **String** | The text encoding of the original file if the original file is a text according to &#39;format&#39; field value | [optional] 
**type** | **String** | The type of the original file (e.g. Code, Annotation, Comment, or Diagram). When no type is specified, a type may be assigned based on MIME type or filename extension | [optional] 
**mime_type** | **String** | The MIME type of the original file (e.g. &#39;plain/text&#39;, &#39;text/markdown&#39; or &#39;application/vnd.openxmlformats-officedocument.wordprocessingml.document&#39;) | [optional] 
**git_working_tree** | **String** | The Git working tree path from which the file originates | [optional] 
**git_dirty** | **Boolean** | A dirty flag intended for the PKM Git service to indicate that a PKM File has been modified since the last synchronization with the Git working tree regardeless the file is versioned or not | [optional] 
**git_unmerged** | **Boolean** | A flag intended for the user provided by the PKM Git service to indicate that a PKM File is unmerged (merge conflict ?) since the last synchronization with the Git working tree | [optional] 



## Enum: FormatEnum


* `text` (value: `"text"`)

* `binary` (value: `"binary"`)




