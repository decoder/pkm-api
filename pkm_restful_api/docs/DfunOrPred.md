# PkmRestfulApi.DfunOrPred

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logic_info** | [**LogicInfo**](LogicInfo.md) |  | [optional] 
**Dfun_or_pred_loc** | [**Loc**](Loc.md) |  | [optional] 


