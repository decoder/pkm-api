# PkmRestfulApi.JavaComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loc** | [**JavaLoc**](JavaLoc.md) |  | 
**commentInEnvironment** | **String** |  | 
**comments** | **[String]** |  | 


