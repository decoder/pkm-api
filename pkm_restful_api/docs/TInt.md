# PkmRestfulApi.TInt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TInt_kind** | **String** |  | [optional] 
**TInt_attributes** | [**[Attr]**](Attr.md) |  | [optional] 



## Enum: TIntKindEnum


* `IBool` (value: `"IBool"`)

* `IChar` (value: `"IChar"`)

* `ISChar` (value: `"ISChar"`)

* `IUChar` (value: `"IUChar"`)

* `IInt` (value: `"IInt"`)

* `IUInt` (value: `"IUInt"`)

* `IShort` (value: `"IShort"`)

* `IUShort` (value: `"IUShort"`)

* `ILong` (value: `"ILong"`)

* `IULong` (value: `"IULong"`)

* `ILongLong` (value: `"ILongLong"`)

* `IULongLong` (value: `"IULongLong"`)




