# PkmRestfulApi.PkmCSourceCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceFile** | **String** |  | [optional] 
**globals** | **[{String: Object}]** |  | [optional] 
**globinit** | [**PkmCSourceCodeGlobinit**](PkmCSourceCodeGlobinit.md) |  | [optional] 
**globinitcalled** | **Boolean** |  | [optional] 


