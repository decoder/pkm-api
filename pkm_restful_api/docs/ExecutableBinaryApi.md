# PkmRestfulApi.ExecutableBinaryApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteExecutableBinaries**](ExecutableBinaryApi.md#deleteExecutableBinaries) | **DELETE** /bin/executable/{dbName} | 
[**deleteExecutableBinary**](ExecutableBinaryApi.md#deleteExecutableBinary) | **DELETE** /bin/executable/{dbName}/{filename} | 
[**getExecutableBinaries**](ExecutableBinaryApi.md#getExecutableBinaries) | **GET** /bin/executable/{dbName} | 
[**getExecutableBinary**](ExecutableBinaryApi.md#getExecutableBinary) | **GET** /bin/executable/{dbName}/{filename} | 
[**postExecutableBinaries**](ExecutableBinaryApi.md#postExecutableBinaries) | **POST** /bin/executable/{dbName} | 
[**putExecutableBinaries**](ExecutableBinaryApi.md#putExecutableBinaries) | **PUT** /bin/executable/{dbName} | 



## deleteExecutableBinaries

> deleteExecutableBinaries(dbName, key)



Delete all Executable Binary files present in the database together with documents which originate from these Executable Binary files

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ExecutableBinaryApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteExecutableBinaries(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteExecutableBinary

> deleteExecutableBinary(dbName, filename, key)



delete a Executable Binary file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ExecutableBinaryApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Executable Binary filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteExecutableBinary(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Executable Binary filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getExecutableBinaries

> [PkmFile] getExecutableBinaries(dbName, key, opts)



Get all Executable Binary files from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ExecutableBinaryApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getExecutableBinaries(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmFile]**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getExecutableBinary

> PkmFile getExecutableBinary(dbName, filename, key, opts)



Get a single Executable Binary file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ExecutableBinaryApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Executable Binary filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
};
apiInstance.getExecutableBinary(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Executable Binary filename | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postExecutableBinaries

> String postExecutableBinaries(dbName, key, opts)



Insert some Executable Binary files (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ExecutableBinaryApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'body': [new PkmRestfulApi.PkmFile()] // [PkmFile] | Executable Binary files
};
apiInstance.postExecutableBinaries(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Executable Binary files | [optional] 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putExecutableBinaries

> putExecutableBinaries(dbName, key, opts)



Insert (if not yet present) or update (if already present) some Executable Binary files into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ExecutableBinaryApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'body': [new PkmRestfulApi.PkmFile()] // [PkmFile] | Executable Binary files
};
apiInstance.putExecutableBinaries(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Executable Binary files | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

