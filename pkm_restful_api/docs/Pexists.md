# PkmRestfulApi.Pexists

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Pforall_quantifiers** | [**[Quantifiers]**](Quantifiers.md) |  | [optional] 
**predicate** | [**Predicate**](Predicate.md) |  | [optional] 


