# PkmRestfulApi.Enode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exp_node_kind** | **String** |  | [optional] 
**exp_node_value** | [**ExpNodeValue**](ExpNodeValue.md) |  | [optional] 



## Enum: ExpNodeKindEnum


* `Const` (value: `"Const"`)

* `Lval` (value: `"Lval"`)

* `SizeOf` (value: `"SizeOf"`)

* `SizeOfStr` (value: `"SizeOfStr"`)

* `AlignOf` (value: `"AlignOf"`)

* `AlignOfE` (value: `"AlignOfE"`)

* `UnOp` (value: `"UnOp"`)

* `BinOp` (value: `"BinOp"`)

* `CastE` (value: `"CastE"`)

* `AddrOf` (value: `"AddrOf"`)

* `StartOf` (value: `"StartOf"`)

* `Info` (value: `"Info"`)




