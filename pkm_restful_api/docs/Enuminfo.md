# PkmRestfulApi.Enuminfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eorig_name** | **String** |  | [optional] 
**ename** | **String** |  | [optional] 
**eattr** | [**[CompinfoCattr]**](CompinfoCattr.md) |  | [optional] 
**ekind** | **String** |  | [optional] 



## Enum: EkindEnum


* `IBool` (value: `"IBool"`)

* `IChar` (value: `"IChar"`)

* `ISChar` (value: `"ISChar"`)

* `IUChar` (value: `"IUChar"`)

* `IInt` (value: `"IInt"`)

* `IUInt` (value: `"IUInt"`)

* `IShort` (value: `"IShort"`)

* `IUShort` (value: `"IUShort"`)

* `ILong` (value: `"ILong"`)

* `IULong` (value: `"IULong"`)

* `ILongLong` (value: `"ILongLong"`)

* `IULongLong` (value: `"IULongLong"`)




