# PkmRestfulApi.DocApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDocs**](DocApi.md#deleteDocs) | **DELETE** /doc/asfm/docs/{dbName} | 
[**deleteGraphicalDocs**](DocApi.md#deleteGraphicalDocs) | **DELETE** /doc/gsl/docs/{dbName} | 
[**deleteRawDoc**](DocApi.md#deleteRawDoc) | **DELETE** /doc/rawdoc/{dbName}/{filename} | 
[**deleteRawDocs**](DocApi.md#deleteRawDocs) | **DELETE** /doc/rawdoc/{dbName} | 
[**getDocArtefacts**](DocApi.md#getDocArtefacts) | **GET** /doc/asfm/artefacts/{dbName} | 
[**getDocs**](DocApi.md#getDocs) | **GET** /doc/asfm/docs/{dbName} | 
[**getGraphicalDocs**](DocApi.md#getGraphicalDocs) | **GET** /doc/gsl/docs/{dbName} | 
[**getRawDoc**](DocApi.md#getRawDoc) | **GET** /doc/rawdoc/{dbName}/{filename} | 
[**getRawDocs**](DocApi.md#getRawDocs) | **GET** /doc/rawdoc/{dbName} | 
[**postDocs**](DocApi.md#postDocs) | **POST** /doc/asfm/docs/{dbName} | 
[**postGraphicalDocs**](DocApi.md#postGraphicalDocs) | **POST** /doc/gsl/docs/{dbName} | 
[**postRawDocs**](DocApi.md#postRawDocs) | **POST** /doc/rawdoc/{dbName} | 
[**putDocs**](DocApi.md#putDocs) | **PUT** /doc/asfm/docs/{dbName} | 
[**putGraphicalDocs**](DocApi.md#putGraphicalDocs) | **PUT** /doc/gsl/docs/{dbName} | 
[**putRawDocs**](DocApi.md#putRawDocs) | **PUT** /doc/rawdoc/{dbName} | 



## deleteDocs

> deleteDocs(dbName, key, opts)



Delete ASFM documents present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'doc': "doc_example", // String | ASFM document name as a bare character string or a /regular expression/
  'filename': "filename_example" // String | related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/
};
apiInstance.deleteDocs(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **doc** | **String**| ASFM document name as a bare character string or a /regular expression/ | [optional] 
 **filename** | **String**| related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/ | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteGraphicalDocs

> deleteGraphicalDocs(dbName, key, opts)



Delete GSL documents present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  '_class': "_class_example", // String | GSL class name
  'object': "object_example" // String | GSL object name
};
apiInstance.deleteGraphicalDocs(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **_class** | **String**| GSL class name | [optional] 
 **object** | **String**| GSL object name | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteRawDoc

> deleteRawDoc(dbName, filename, key)



delete a Documentation file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Documentation filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteRawDoc(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Documentation filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteRawDocs

> deleteRawDocs(dbName, key)



Delete all Documentation files present in the database together with documents which originate from these Documentation files

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteRawDocs(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDocArtefacts

> [Object] getDocArtefacts(dbName, key, opts)



Get ASFM artefacts from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': 1234, // Number | artefact identifier
  'doc': "doc_example", // String | ASFM document name as bare character string or a /regular expression/
  'filename': "filename_example", // String | related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/
  'unit': "unit_example", // String | unit name as bare character string or a /regular expression/
  '_class': "_class_example", // String | class name as bare character string or a /regular expression/
  'invariant': "invariant_example", // String | invariant name as bare character string or a /regular expression/
  'field': "field_example", // String | field name as bare character string or a /regular expression/
  'method': "method_example", // String | method name as bare character string or a /regular expression/
  'type': "type_example", // String | type name as bare character string or a /regular expression/
  'macro': "macro_example", // String | macro name as bare character string or a /regular expression/
  'constant': "constant_example", // String | constant name as bare character string or a /regular expression/
  'member': "member_example", // String | member name (either invariant, field, method, type, macro, or constant name) as bare character string or a /regular expression/
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getDocArtefacts(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **Number**| artefact identifier | [optional] 
 **doc** | **String**| ASFM document name as bare character string or a /regular expression/ | [optional] 
 **filename** | **String**| related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/ | [optional] 
 **unit** | **String**| unit name as bare character string or a /regular expression/ | [optional] 
 **_class** | **String**| class name as bare character string or a /regular expression/ | [optional] 
 **invariant** | **String**| invariant name as bare character string or a /regular expression/ | [optional] 
 **field** | **String**| field name as bare character string or a /regular expression/ | [optional] 
 **method** | **String**| method name as bare character string or a /regular expression/ | [optional] 
 **type** | **String**| type name as bare character string or a /regular expression/ | [optional] 
 **macro** | **String**| macro name as bare character string or a /regular expression/ | [optional] 
 **constant** | **String**| constant name as bare character string or a /regular expression/ | [optional] 
 **member** | **String**| member name (either invariant, field, method, type, macro, or constant name) as bare character string or a /regular expression/ | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDocs

> [PkmAsfm] getDocs(dbName, key, opts)



Get ASFM documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'doc': "doc_example", // String | ASFM document name as a bare character string or a /regular expression/
  'filename': "filename_example", // String | related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getDocs(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **doc** | **String**| ASFM document name as a bare character string or a /regular expression/ | [optional] 
 **filename** | **String**| related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/ | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmAsfm]**](PkmAsfm.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getGraphicalDocs

> [Object] getGraphicalDocs(dbName, key, opts)



Get GSL documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  '_class': "_class_example", // String | GSL class name
  'object': "object_example", // String | GSL object name
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getGraphicalDocs(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **_class** | **String**| GSL class name | [optional] 
 **object** | **String**| GSL object name | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRawDoc

> PkmFile getRawDoc(dbName, filename, key, opts)



Get a single Documentation file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | Documentation filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
};
apiInstance.getRawDoc(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| Documentation filename | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRawDocs

> [PkmFile] getRawDocs(dbName, key, opts)



Get all Documentation files from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getRawDocs(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmFile]**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postDocs

> String postDocs(dbName, key, body)



Insert some ASFM documents (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmAsfm()]; // [PkmAsfm] | ASFM documents
apiInstance.postDocs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmAsfm]**](PkmAsfm.md)| ASFM documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postGraphicalDocs

> String postGraphicalDocs(dbName, key, body)



Insert some GSL documents (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [null]; // [Object] | GSL documents
apiInstance.postGraphicalDocs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[Object]**](Object.md)| GSL documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postRawDocs

> String postRawDocs(dbName, key, body)



Insert some Documentation files (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Documentation files
apiInstance.postRawDocs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Documentation files | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putDocs

> String putDocs(dbName, key, body)



Insert (if not yet present) or update (if already present) some ASFM documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmAsfm()]; // [PkmAsfm] | ASFM documents
apiInstance.putDocs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmAsfm]**](PkmAsfm.md)| ASFM documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putGraphicalDocs

> String putGraphicalDocs(dbName, key, body)



Insert (if not yet present) or update (if already present) some GSL documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [null]; // [Object] | GSL documents
apiInstance.putGraphicalDocs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[Object]**](Object.md)| GSL documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putRawDocs

> putRawDocs(dbName, key, body)



Insert (if not yet present) or update (if already present) some Documentation files into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.DocApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Documentation files
apiInstance.putRawDocs(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Documentation files | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

