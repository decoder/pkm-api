# PkmRestfulApi.CompoundInit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TVoid** | [**[Attr]**](Attr.md) |  | [optional] 
**TInt** | [**TInt**](TInt.md) |  | [optional] 
**TFloat** | [**TFloat**](TFloat.md) |  | [optional] 
**TPtr** | [**TPtr**](TPtr.md) |  | [optional] 
**TArray** | **Object** | TBD | [optional] 
**TFun** | **Object** | TBD | [optional] 
**TNamed** | [**TNamed**](TNamed.md) |  | [optional] 
**TComp** | [**TComp**](TComp.md) |  | [optional] 
**TEnum** | [**TEnum**](TEnum.md) |  | [optional] 
**TBuiltin** | [**TBuiltin**](TBuiltin.md) |  | [optional] 
**CompoundInit_offset_inits** | **[Object]** |  | [optional] 


