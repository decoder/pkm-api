# PkmRestfulApi.PkmUmlStateMachineRegions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subvertexes** | [**[PkmUmlStateMachineSubvertexes]**](PkmUmlStateMachineSubvertexes.md) |  | 
**name** | **String** |  | 
**id** | **String** |  | 
**transitions** | [**[PkmUmlStateMachineTransitions]**](PkmUmlStateMachineTransitions.md) |  | 


