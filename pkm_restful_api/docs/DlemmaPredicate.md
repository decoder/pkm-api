# PkmRestfulApi.DlemmaPredicate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predicate** | [**Predicate**](Predicate.md) |  | [optional] 


