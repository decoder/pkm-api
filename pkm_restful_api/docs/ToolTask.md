# PkmRestfulApi.ToolTask

## Enum


* `requirements` (value: `"requirements"`)

* `systemDesign` (value: `"systemDesign"`)

* `architectureDesign` (value: `"architectureDesign"`)

* `moduleDesign` (value: `"moduleDesign"`)

* `systemImplementation` (value: `"systemImplementation"`)

* `unitTesting` (value: `"unitTesting"`)

* `integrationTesting` (value: `"integrationTesting"`)

* `systemTesting` (value: `"systemTesting"`)

* `acceptanceTesting` (value: `"acceptanceTesting"`)


