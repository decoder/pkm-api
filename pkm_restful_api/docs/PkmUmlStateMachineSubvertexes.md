# PkmRestfulApi.PkmUmlStateMachineSubvertexes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isSourceOfTransition** | **Boolean** |  | 
**previousSubvertexes** | **[String]** |  | 
**isTargetOfTransition** | **Boolean** |  | 
**name** | **String** |  | 
**nextSubvertexes** | **[String]** |  | 
**id** | **String** |  | 
**type** | **String** |  | 


