# PkmRestfulApi.PkmCppCommentsComments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loc** | [**CppLoc**](CppLoc.md) |  | [optional] 
**comments** | [**[CppComment]**](CppComment.md) |  | [optional] 
**id** | **Number** | id of the commented entity | [optional] 


