# PkmRestfulApi.ToolsApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteTool**](ToolsApi.md#deleteTool) | **DELETE** /tools/{dbName}/{toolID} | 
[**deleteTools**](ToolsApi.md#deleteTools) | **DELETE** /tools/{dbName} | 
[**getTool**](ToolsApi.md#getTool) | **GET** /tools/{dbName}/{toolID} | 
[**getTools**](ToolsApi.md#getTools) | **GET** /tools/{dbName} | 
[**postTools**](ToolsApi.md#postTools) | **POST** /tools/{dbName} | 
[**putTools**](ToolsApi.md#putTools) | **PUT** /tools/{dbName} | 



## deleteTool

> deleteTool(dbName, toolID, key)



delete a Tool document from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ToolsApi();
var dbName = "dbName_example"; // String | Database name
var toolID = "toolID_example"; // String | Tool ID
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTool(dbName, toolID, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **toolID** | **String**| Tool ID | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteTools

> deleteTools(dbName, key)



Delete all Tool documents present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ToolsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteTools(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTool

> PkmTool getTool(dbName, toolID, key)



Get Tool documents by Tool ID from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ToolsApi();
var dbName = "dbName_example"; // String | Database name
var toolID = "toolID_example"; // String | Tool ID
var key = "key_example"; // String | Access key to the PKM
apiInstance.getTool(dbName, toolID, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **toolID** | **String**| Tool ID | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmTool**](PkmTool.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getTools

> [PkmTool] getTools(dbName, key, opts)



Get all Tool documents from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ToolsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'phase': "phase_example", // String | phase
  'task': "task_example", // String | task
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getTools(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **phase** | **String**| phase | [optional] 
 **task** | **String**| task | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmTool]**](PkmTool.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postTools

> String postTools(dbName, key, opts)



Insert some Tool documents (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ToolsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'body': [new PkmRestfulApi.PkmTool()] // [PkmTool] | Tool documents
};
apiInstance.postTools(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmTool]**](PkmTool.md)| Tool documents | [optional] 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putTools

> String putTools(dbName, key, opts)



Insert (if not yet present) or update (if already present) some Tool documents into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ToolsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'body': [new PkmRestfulApi.PkmTool()] // [PkmTool] | Tool documents
};
apiInstance.putTools(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmTool]**](PkmTool.md)| Tool documents | [optional] 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

