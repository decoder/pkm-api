# PkmRestfulApi.InlineObject4

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**superuser_name** | **String** | MongoDB \&quot;superuser\&quot; that can drop roles and users in MongoDB PKM users database | [optional] 
**superuser_auth_db** | **String** | MongoDB authentication database for superuser | [optional] 
**superuser_password** | **String** | superuser password | [optional] 


