# PkmRestfulApi.PkmServerInfoServerCopyright

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**year** | **String** | inception year | [optional] 
**owners** | **[String]** | Copyright owners | [optional] 


