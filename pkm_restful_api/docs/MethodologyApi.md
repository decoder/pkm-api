# PkmRestfulApi.MethodologyApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteMethodologyStatus**](MethodologyApi.md#deleteMethodologyStatus) | **DELETE** /methodology/status/{dbName} | 
[**getMethodologyStatus**](MethodologyApi.md#getMethodologyStatus) | **GET** /methodology/status/{dbName} | 
[**postMethodologyStatus**](MethodologyApi.md#postMethodologyStatus) | **POST** /methodology/status/{dbName} | 
[**putMethodologyStatus**](MethodologyApi.md#putMethodologyStatus) | **PUT** /methodology/status/{dbName} | 



## deleteMethodologyStatus

> deleteMethodologyStatus(dbName, key, opts)



Delete Methodology status

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.MethodologyApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': "id_example", // String | phase identifier
  'name': "name_example", // String | phase name
  'phaseNumber': 56 // Number | phase number
};
apiInstance.deleteMethodologyStatus(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **String**| phase identifier | [optional] 
 **name** | **String**| phase name | [optional] 
 **phaseNumber** | **Number**| phase number | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMethodologyStatus

> [PkmMethodologyStatus] getMethodologyStatus(dbName, key, opts)



Get Methodology status

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.MethodologyApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'id': "id_example", // String | phase identifier
  'name': "name_example", // String | phase name
  'phaseNumber': 56, // Number | phase number
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getMethodologyStatus(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **id** | **String**| phase identifier | [optional] 
 **name** | **String**| phase name | [optional] 
 **phaseNumber** | **Number**| phase number | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmMethodologyStatus]**](PkmMethodologyStatus.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postMethodologyStatus

> String postMethodologyStatus(dbName, key, body)



Insert Methodology status

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.MethodologyApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmMethodologyStatus()]; // [PkmMethodologyStatus] | Methodology status
apiInstance.postMethodologyStatus(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmMethodologyStatus]**](PkmMethodologyStatus.md)| Methodology status | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putMethodologyStatus

> putMethodologyStatus(dbName, key, body)



Insert Methodology status or update existing one

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.MethodologyApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmMethodologyStatus()]; // [PkmMethodologyStatus] | Methodology status
apiInstance.putMethodologyStatus(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmMethodologyStatus]**](PkmMethodologyStatus.md)| Methodology status | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

