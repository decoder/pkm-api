# PkmRestfulApi.FiniApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postFiniPkm**](FiniApi.md#postFiniPkm) | **POST** /fini | 



## postFiniPkm

> postFiniPkm(body)



Finalize PKM. Drop Role &#39;user&#39; in users database, then drop all users in users database.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.FiniApi();
var body = new PkmRestfulApi.InlineObject4(); // InlineObject4 | 
apiInstance.postFiniPkm(body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**InlineObject4**](InlineObject4.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

