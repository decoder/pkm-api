# PkmRestfulApi.AssignInit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SingleInit** | [**SingleInit**](SingleInit.md) |  | [optional] 
**CompoundInit** | [**CompoundInit**](CompoundInit.md) |  | [optional] 


