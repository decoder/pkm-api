# PkmRestfulApi.GFun

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fundec** | [**Fundec**](Fundec.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


