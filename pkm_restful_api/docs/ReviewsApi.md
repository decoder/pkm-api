# PkmRestfulApi.ReviewsApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteReviews**](ReviewsApi.md#deleteReviews) | **DELETE** /reviews/{dbName} | 
[**getReviews**](ReviewsApi.md#getReviews) | **GET** /reviews/{dbName} | 
[**postReviews**](ReviewsApi.md#postReviews) | **POST** /reviews/{dbName} | 
[**putReviews**](ReviewsApi.md#putReviews) | **PUT** /reviews/{dbName} | 



## deleteReviews

> deleteReviews(dbName, key, opts)



Delete Reviews

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ReviewsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'reviewID': "reviewID_example", // String | review identifier
  'reviewAuthor': "reviewAuthor_example", // String | review author
  'reviewStatus': "reviewStatus_example" // String | review status
};
apiInstance.deleteReviews(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **reviewID** | **String**| review identifier | [optional] 
 **reviewAuthor** | **String**| review author | [optional] 
 **reviewStatus** | **String**| review status | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getReviews

> [PkmReview] getReviews(dbName, key, opts)



Get Reviews

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ReviewsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'reviewID': "reviewID_example", // String | review identifier
  'reviewAuthor': "reviewAuthor_example", // String | review author
  'reviewStatus': "reviewStatus_example", // String | review status
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getReviews(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **reviewID** | **String**| review identifier | [optional] 
 **reviewAuthor** | **String**| review author | [optional] 
 **reviewStatus** | **String**| review status | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmReview]**](PkmReview.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postReviews

> [String] postReviews(dbName, key, body)



Insert Reviews

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ReviewsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmReview()]; // [PkmReview] | Reviews
apiInstance.postReviews(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmReview]**](PkmReview.md)| Reviews | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putReviews

> [String] putReviews(dbName, key, body)



Insert Reviews or update existing ones

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ReviewsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmReview()]; // [PkmReview] | Reviews
apiInstance.putReviews(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmReview]**](PkmReview.md)| Reviews | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

