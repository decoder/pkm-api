# PkmRestfulApi.PkmAsfmFields

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**id** | **Number** |  | [optional] 
**parent** | **Number** |  | [optional] 
**doc** | **String** |  | [optional] 


