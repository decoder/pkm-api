# PkmRestfulApi.UMLApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteRawUML**](UMLApi.md#deleteRawUML) | **DELETE** /uml/rawuml/{dbName}/{filename} | 
[**deleteRawUMLs**](UMLApi.md#deleteRawUMLs) | **DELETE** /uml/rawuml/{dbName} | 
[**deleteUMLClassDiagrams**](UMLApi.md#deleteUMLClassDiagrams) | **DELETE** /uml/uml_class/{dbName} | 
[**deleteUMLStateMachines**](UMLApi.md#deleteUMLStateMachines) | **DELETE** /uml/uml_state_machine/{dbName} | 
[**getRawUML**](UMLApi.md#getRawUML) | **GET** /uml/rawuml/{dbName}/{filename} | 
[**getRawUMLs**](UMLApi.md#getRawUMLs) | **GET** /uml/rawuml/{dbName} | 
[**getUMLClass**](UMLApi.md#getUMLClass) | **GET** /uml/uml_class/class/{dbName}/{className} | 
[**getUMLClassDiagram**](UMLApi.md#getUMLClassDiagram) | **GET** /uml/uml_class/{dbName}/{diagramName} | 
[**getUMLClassDiagrams**](UMLApi.md#getUMLClassDiagrams) | **GET** /uml/uml_class/{dbName} | 
[**getUMLStateMachine**](UMLApi.md#getUMLStateMachine) | **GET** /uml/uml_state_machine/{dbName}/{diagramName} | 
[**getUMLStateMachines**](UMLApi.md#getUMLStateMachines) | **GET** /uml/uml_state_machine/{dbName} | 
[**postRawUMLs**](UMLApi.md#postRawUMLs) | **POST** /uml/rawuml/{dbName} | 
[**postUMLClassDiagrams**](UMLApi.md#postUMLClassDiagrams) | **POST** /uml/uml_class/{dbName} | 
[**postUMLStateMachines**](UMLApi.md#postUMLStateMachines) | **POST** /uml/uml_state_machine/{dbName} | 
[**putRawUMLs**](UMLApi.md#putRawUMLs) | **PUT** /uml/rawuml/{dbName} | 
[**putUMLClassDiagrams**](UMLApi.md#putUMLClassDiagrams) | **PUT** /uml/uml_class/{dbName} | 
[**putUMLStateMachines**](UMLApi.md#putUMLStateMachines) | **PUT** /uml/uml_state_machine/{dbName} | 



## deleteRawUML

> deleteRawUML(dbName, filename, key)



delete a UML file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | UML filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteRawUML(dbName, filename, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| UML filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteRawUMLs

> deleteRawUMLs(dbName, key)



Delete all UML files present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteRawUMLs(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteUMLClassDiagrams

> deleteUMLClassDiagrams(dbName, key)



Delete all UML class diagrams present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteUMLClassDiagrams(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteUMLStateMachines

> deleteUMLStateMachines(dbName, key)



Delete all UML state machines present in the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteUMLStateMachines(dbName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRawUML

> PkmFile getRawUML(dbName, filename, key, opts)



Get a single UML file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | UML filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
};
apiInstance.getRawUML(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| UML filename | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRawUMLs

> [PkmFile] getRawUMLs(dbName, key, opts)



Get all UML files from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getRawUMLs(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmFile]**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUMLClass

> [Object] getUMLClass(dbName, className, key)



Get UML classes by name from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var className = "className_example"; // String | Class name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getUMLClass(dbName, className, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **className** | **String**| Class name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUMLClassDiagram

> [Object] getUMLClassDiagram(dbName, diagramName, key)



Get UML class diagram by name from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var diagramName = "diagramName_example"; // String | Class diagram name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getUMLClassDiagram(dbName, diagramName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **diagramName** | **String**| Class diagram name | 
 **key** | **String**| Access key to the PKM | 

### Return type

**[Object]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUMLClassDiagrams

> [PkmUmlClassDiagram] getUMLClassDiagrams(dbName, key, opts)



Get all UML class diagrams from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getUMLClassDiagrams(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmUmlClassDiagram]**](PkmUmlClassDiagram.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUMLStateMachine

> [PkmUmlStateMachine] getUMLStateMachine(dbName, diagramName, key)



Get UML state machine by name from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var diagramName = "diagramName_example"; // String | Class diagram name
var key = "key_example"; // String | Access key to the PKM
apiInstance.getUMLStateMachine(dbName, diagramName, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **diagramName** | **String**| Class diagram name | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**[PkmUmlStateMachine]**](PkmUmlStateMachine.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUMLStateMachines

> [PkmUmlStateMachine] getUMLStateMachines(dbName, key, opts)



Get all UML state machines from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getUMLStateMachines(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmUmlStateMachine]**](PkmUmlStateMachine.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postRawUMLs

> String postRawUMLs(dbName, key, body)



Insert some UML files (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | UML files
apiInstance.postRawUMLs(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| UML files | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postUMLClassDiagrams

> String postUMLClassDiagrams(dbName, key, body)



Insert some UML class diagrams (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmUmlClassDiagram()]; // [PkmUmlClassDiagram] | UML class diagram documents
apiInstance.postUMLClassDiagrams(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmUmlClassDiagram]**](PkmUmlClassDiagram.md)| UML class diagram documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## postUMLStateMachines

> String postUMLStateMachines(dbName, key, body)



Insert some UML state machines (which are not yet present) into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmUmlStateMachine()]; // [PkmUmlStateMachine] | UML state machine documents
apiInstance.postUMLStateMachines(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmUmlStateMachine]**](PkmUmlStateMachine.md)| UML state machine documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putRawUMLs

> putRawUMLs(dbName, key, body)



Insert (if not yet present) or update (if already present) some UML files into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | UML files
apiInstance.putRawUMLs(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| UML files | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putUMLClassDiagrams

> String putUMLClassDiagrams(dbName, key, body)



Insert (if not yet present) or update (if already present) some UML class diagrams into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmUmlClassDiagram()]; // [PkmUmlClassDiagram] | UML class diagram documents
apiInstance.putUMLClassDiagrams(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmUmlClassDiagram]**](PkmUmlClassDiagram.md)| UML class diagram documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putUMLStateMachines

> String putUMLStateMachines(dbName, key, body)



Insert (if not yet present) or update (if already present) some UML state machines into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.UMLApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmUmlStateMachine()]; // [PkmUmlStateMachine] | UML state machine documents
apiInstance.putUMLStateMachines(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmUmlStateMachine]**](PkmUmlStateMachine.md)| UML state machine documents | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

