# PkmRestfulApi.Type

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qualType** | **String** |  | 
**desugaredQualType** | **String** |  | [optional] 
**typeAliasDeclId** | **Number** |  | [optional] 


