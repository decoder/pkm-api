# PkmRestfulApi.Tat

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**term** | [**Term**](Term.md) |  | [optional] 
**StmtLabel** | **String** |  | [optional] 
**FormalLabel** | **String** |  | [optional] 
**BuiltinLabel** | [**BuiltinLabel**](BuiltinLabel.md) |  | [optional] 


