# PkmRestfulApi.GVar

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**varinfo** | [**Varinfo**](Varinfo.md) |  | [optional] 
**initinfo** | [**Initinfo**](Initinfo.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


