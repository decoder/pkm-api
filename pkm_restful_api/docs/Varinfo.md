# PkmRestfulApi.Varinfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vname** | **String** |  | [optional] 
**vid** | **Number** |  | [optional] 


