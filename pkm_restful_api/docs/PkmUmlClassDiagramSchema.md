# PkmRestfulApi.PkmUmlClassDiagramSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**name** | **String** |  | 
**diagram** | **String** |  | [optional] 
**modules** | [**[UmlModule]**](UmlModule.md) |  | 


