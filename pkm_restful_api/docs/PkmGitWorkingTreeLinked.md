# PkmRestfulApi.PkmGitWorkingTreeLinked

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**main** | **String** | Directory of the main Git working tree | [optional] 
**directory** | **String** | Directory of the linked Git working tree | [optional] 
**git_directory** | **String** | Git directory | [optional] 
**git_branch** | **String** | Tracked Git branch | [optional] 
**git_commit_id** | **String** | SHA1 ID of current commit | [optional] 
**git_config** | **Object** | Git configuration of the Git directory (usually .git/config) associated to a linked Git working tree | [optional] 


