# PkmRestfulApi.Tools

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**toolID** | **String** |  | 
**minutesToStuck** | **Number** |  | [optional] 
**toolName** | **String** |  | 
**description** | **String** |  | [optional] 
**server** | **String** |  | 
**phases** | [**[ToolPhase]**](ToolPhase.md) |  | 
**tasks** | [**[ToolTask]**](ToolTask.md) |  | 
**endpoint** | [**ToolEndpoint**](ToolEndpoint.md) |  | 


