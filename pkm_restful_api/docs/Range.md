# PkmRestfulApi.Range

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**begin** | [**Loc**](Loc.md) |  | 
**end** | [**Loc**](Loc.md) |  | 


