# PkmRestfulApi.LogicInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**l_var_info** | [**LVarInfo**](LVarInfo.md) |  | [optional] 
**l_labels** | [**[LogicLabels]**](LogicLabels.md) |  | [optional] 
**l_tparams** | **[Object]** | TODO | [optional] 
**logic_type** | [**LogicType**](LogicType.md) |  | [optional] 
**l_profile** | **[Object]** |  | [optional] 
**l_body** | [**LBody**](LBody.md) |  | [optional] 


