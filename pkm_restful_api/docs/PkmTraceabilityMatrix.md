# PkmRestfulApi.PkmTraceabilityMatrix

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**src_path** | **String** | Url-encoded path in the pkm e.g. (not encoded for readability)  code/c/comments/mydb | [optional] 
**src_access** | **String** | Url-encoded json path (in JMESPath query language) to access the text in json pointed to by the src_path. E.g. (not encoded for readability): &#39;[?global_kind &#x3D;&#x3D; &#39;GFun&#39;] | [?loc.pos_start.pos_path &#x3D;&#x3D; &#39;examples/vector2.c&#39;].comments | [0]&#39; | [optional] 
**tgt_path** | **String** | Url-encoded path in the pkm e.g. (not encoded for readability)  code/c/comments/mydb | [optional] 
**tgt_access** | **String** | Url-encoded json path (in JMESPath query language) to access the text in json pointed to by the src_path. E.g. (not encoded for readability): &#39;[?global_kind &#x3D;&#x3D; &#39;GFun&#39;] | [?loc.pos_start.pos_path &#x3D;&#x3D; &#39;examples/vector2.c&#39;].comments | [1]&#39; | [optional] 
**trace** | [**PkmTraceabilityMatrixTrace**](PkmTraceabilityMatrixTrace.md) |  | [optional] 


