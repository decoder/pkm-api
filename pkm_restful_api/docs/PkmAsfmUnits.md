# PkmRestfulApi.PkmAsfmUnits

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**id** | **Number** |  | [optional] 
**classes** | [**[PkmAsfmClasses]**](PkmAsfmClasses.md) |  | [optional] 


