# PkmRestfulApi.Pif

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predicate** | [**Predicate**](Predicate.md) |  | [optional] 
**term** | [**Term**](Term.md) |  | [optional] 


