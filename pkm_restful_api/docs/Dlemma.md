# PkmRestfulApi.Dlemma

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dlemma_string** | **String** |  | [optional] 
**dlemma_bool** | **Boolean** |  | [optional] 
**dlemma_labels** | [**[LogicLabels]**](LogicLabels.md) |  | [optional] 
**dlemma_strings** | **[Object]** |  | [optional] 
**dlemma_predicate** | [**DlemmaPredicate**](DlemmaPredicate.md) |  | [optional] 
**dlemma_attributes** | [**[Attr]**](Attr.md) |  | [optional] 
**dlemma_loc** | [**Loc**](Loc.md) |  | [optional] 


