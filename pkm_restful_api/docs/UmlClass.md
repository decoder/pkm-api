# PkmRestfulApi.UmlClass

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**inherits** | **[Object]** |  | [optional] 
**attributes** | [**[UmlAttribute]**](UmlAttribute.md) |  | [optional] 
**operations** | [**[UmlOperation]**](UmlOperation.md) |  | [optional] 


