# PkmRestfulApi.Ltype

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logic_type_info** | [**LogicTypeInfo**](LogicTypeInfo.md) |  | [optional] 
**logic_types** | [**[LvType]**](LvType.md) |  | [optional] 


