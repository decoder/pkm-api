# PkmRestfulApi.TNamed

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**typeinfo** | [**Typeinfo**](Typeinfo.md) |  | [optional] 
**TNamed_attrs** | [**[Attr]**](Attr.md) |  | [optional] 


