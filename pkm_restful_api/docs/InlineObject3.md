# PkmRestfulApi.InlineObject3

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**superuser_name** | **String** | MongoDB \&quot;superuser\&quot; that can create a MongoDB database root | [optional] 
**superuser_auth_db** | **String** | MongoDB authentication database for superuser | [optional] 
**superuser_password** | **String** | superuser password | [optional] 
**pkm_admin_user** | [**PkmUser**](PkmUser.md) |  | [optional] 


