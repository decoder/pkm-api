# PkmRestfulApi.PkmUmlStateMachineTransitions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceID** | **String** |  | 
**targetName** | **String** |  | 
**targetID** | **String** |  | 
**id** | **String** |  | 
**sourceName** | **String** |  | 


