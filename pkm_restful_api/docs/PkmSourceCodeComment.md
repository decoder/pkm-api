# PkmRestfulApi.PkmSourceCodeComment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceFile** | **String** |  | 
**comments** | [**[JavaComment]**](JavaComment.md) |  | 
**fileEncoding** | **String** |  | 
**fileMimeType** | **String** |  | 
**fileFormat** | **String** |  | 
**type** | **String** | type of document for the GUI, typically &#39;Comment&#39; | [optional] 


