# PkmRestfulApi.FilesApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteFile**](FilesApi.md#deleteFile) | **DELETE** /files/{dbName}/{filename} | 
[**getFile**](FilesApi.md#getFile) | **GET** /files/{dbName}/{filename} | 
[**getFiles**](FilesApi.md#getFiles) | **GET** /files/{dbName} | 
[**postFiles**](FilesApi.md#postFiles) | **POST** /files/{dbName} | 
[**putFiles**](FilesApi.md#putFiles) | **PUT** /files/{dbName} | 



## deleteFile

> PkmFile deleteFile(dbName, filename, key)



delete a file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.FilesApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | filename
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteFile(dbName, filename, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| filename | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getFile

> PkmFile getFile(dbName, filename, key, opts)



Get a file from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.FilesApi();
var dbName = "dbName_example"; // String | Database name
var filename = "filename_example"; // String | filename
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
};
apiInstance.getFile(dbName, filename, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **filename** | **String**| filename | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmFile**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getFiles

> [PkmFile] getFiles(dbName, key, opts)



Get files from the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.FilesApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false, // Boolean | toggle abbreviated response (without content)/full response, useful for lowering response footprint
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0, // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
  'type': "type_example", // String | type of file
  'gitWorkingTree': "gitWorkingTree_example", // String | Git working tree
  'gitDirty': true, // Boolean | A dirty flag intended for the PKM Git service to indicate that a PKM File has been modified since the last synchronization with the Git working tree.
  'gitUnmerged': true // Boolean | A flag intended for the user provided by the PKM Git service to indicate that a PKM File is unmerged (merge conflict ?) since the last synchronization with the Git working tree.
};
apiInstance.getFiles(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (without content)/full response, useful for lowering response footprint | [optional] [default to false]
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]
 **type** | **String**| type of file | [optional] 
 **gitWorkingTree** | **String**| Git working tree | [optional] 
 **gitDirty** | **Boolean**| A dirty flag intended for the PKM Git service to indicate that a PKM File has been modified since the last synchronization with the Git working tree. | [optional] 
 **gitUnmerged** | **Boolean**| A flag intended for the user provided by the PKM Git service to indicate that a PKM File is unmerged (merge conflict ?) since the last synchronization with the Git working tree. | [optional] 

### Return type

[**[PkmFile]**](PkmFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postFiles

> String postFiles(dbName, key, body)



Insert some files (which are not yet present) into the database. The related documents (source code AST, comments and annotations, etc. ) which originate from the files are invalidated.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.FilesApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Files
apiInstance.postFiles(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Files | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putFiles

> putFiles(dbName, key, body)



Insert (if not yet present) or update (if already present) some files into the database. The related documents (source code AST, comments and annotations, etc. ) which originate from the files are invalidated.

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.FilesApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmFile()]; // [PkmFile] | Files
apiInstance.putFiles(dbName, key, body).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmFile]**](PkmFile.md)| Files | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

