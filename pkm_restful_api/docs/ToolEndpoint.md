# PkmRestfulApi.ToolEndpoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **String** |  | 
**method** | **String** |  | 
**pathFields** | [**[ToolPathField]**](ToolPathField.md) |  | [optional] 
**queryParameters** | [**[ToolQueryParameter]**](ToolQueryParameter.md) |  | [optional] 
**requestBody** | **Object** |  | [optional] 



## Enum: MethodEnum


* `get` (value: `"get"`)

* `put` (value: `"put"`)

* `post` (value: `"post"`)

* `delete` (value: `"delete"`)




