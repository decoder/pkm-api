# PkmRestfulApi.Daxiomatic

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Daxiomatic_string** | **String** |  | [optional] 
**Daxiomatic_global_annotations** | **[Object]** |  | [optional] 
**Daxiomatic_attributes** | **[Object]** |  | [optional] 
**Daxiomatic_loc** | [**Loc**](Loc.md) |  | [optional] 


