# PkmRestfulApi.PkmCveSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CVE_data_meta** | [**PkmCveCVEDataMeta**](PkmCveCVEDataMeta.md) |  | 
**cna_container** | [**PkmCveCnaContainer**](PkmCveCnaContainer.md) |  | [optional] 
**data_format** | **String** |  | 
**data_type** | **String** |  | 
**data_version** | **String** |  | 


