# PkmRestfulApi.ProjectApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteProject**](ProjectApi.md#deleteProject) | **DELETE** /project/{projectName} | 
[**getProject**](ProjectApi.md#getProject) | **GET** /project/{projectName} | 
[**getProjects**](ProjectApi.md#getProjects) | **GET** /project | 
[**postProject**](ProjectApi.md#postProject) | **POST** /project | 
[**putProject**](ProjectApi.md#putProject) | **PUT** /project | 



## deleteProject

> deleteProject(projectName, key)



delete a project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ProjectApi();
var projectName = "projectName_example"; // String | Project name
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteProject(projectName, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectName** | **String**| Project name | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProject

> PkmProject getProject(projectName, key, opts)



Get a project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ProjectApi();
var projectName = "projectName_example"; // String | Project name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint
};
apiInstance.getProject(projectName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectName** | **String**| Project name | 
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**PkmProject**](PkmProject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProjects

> [PkmProject] getProjects(key, opts)



Get all projects

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ProjectApi();
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'abbrev': false // Boolean | toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint
};
apiInstance.getProjects(key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 
 **abbrev** | **Boolean**| toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint | [optional] [default to false]

### Return type

[**[PkmProject]**](PkmProject.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postProject

> String postProject(key, body)



create a project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ProjectApi();
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.PkmProject(); // PkmProject | 
apiInstance.postProject(key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 
 **body** | [**PkmProject**](PkmProject.md)|  | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putProject

> String putProject(key, body)



Put a project (create or update)

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.ProjectApi();
var key = "key_example"; // String | Access key to the PKM
var body = new PkmRestfulApi.PkmProject(); // PkmProject | 
apiInstance.putProject(key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Access key to the PKM | 
 **body** | [**PkmProject**](PkmProject.md)|  | 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

