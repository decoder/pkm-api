# PkmRestfulApi.AcslppLoc

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pos_start** | [**AcslppPos**](AcslppPos.md) |  | [optional] 
**pos_end** | [**AcslppPos**](AcslppPos.md) |  | [optional] 


