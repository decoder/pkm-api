# PkmRestfulApi.AnnotationsApi

All URIs are relative to *http://pkm-api_pkm_1:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAnnotation**](AnnotationsApi.md#deleteAnnotation) | **DELETE** /annotations/{dbName}/{artefactId} | 
[**deleteAnnotations**](AnnotationsApi.md#deleteAnnotations) | **DELETE** /annotations/{dbName} | 
[**getAnnotation**](AnnotationsApi.md#getAnnotation) | **GET** /annotations/{dbName}/{artefactId} | 
[**getAnnotations**](AnnotationsApi.md#getAnnotations) | **GET** /annotations/{dbName} | 
[**postAnnotations**](AnnotationsApi.md#postAnnotations) | **POST** /annotations/{dbName} | 
[**putAnnotations**](AnnotationsApi.md#putAnnotations) | **PUT** /annotations/{dbName} | 



## deleteAnnotation

> deleteAnnotation(dbName, artefactId, key)



Delete annotation with the given ID in the given project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.AnnotationsApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | the ID of the annotation
var key = "key_example"; // String | Access key to the PKM
apiInstance.deleteAnnotation(dbName, artefactId, key).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| the ID of the annotation | 
 **key** | **String**| Access key to the PKM | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteAnnotations

> deleteAnnotations(dbName, key, opts)



Delete annotations concerning the given dbName

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.AnnotationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'path': "path_example", // String | the url-coded path in the pkm the retrieved annotations are about
  'access': "access_example" // String | json path allowing to retrieve the text to analyze in the retrieved data
};
apiInstance.deleteAnnotations(dbName, key, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **path** | **String**| the url-coded path in the pkm the retrieved annotations are about | [optional] 
 **access** | **String**| json path allowing to retrieve the text to analyze in the retrieved data | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAnnotation

> PkmAnnotations getAnnotation(dbName, artefactId, key)



Get annotation with the given ID in the given project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.AnnotationsApi();
var dbName = "dbName_example"; // String | Database name
var artefactId = "artefactId_example"; // String | the ID of the annotation
var key = "key_example"; // String | Access key to the PKM
apiInstance.getAnnotation(dbName, artefactId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **artefactId** | **String**| the ID of the annotation | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**PkmAnnotations**](PkmAnnotations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAnnotations

> [PkmAnnotations] getAnnotations(dbName, key, opts)



Get annotations in the given project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.AnnotationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var opts = {
  'path': "path_example", // String | the url-coded path in the pkm the retrieved annotations are about
  'access': "access_example", // String | json path allowing to retrieve the text to analyze in the retrieved data
  'skip': 0, // Number | number of elements to skip, useful for lowering response footprint
  'limit': 0 // Number | maximum number of elements in the response (zero means no limit), useful for lowering response footprint
};
apiInstance.getAnnotations(dbName, key, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **path** | **String**| the url-coded path in the pkm the retrieved annotations are about | [optional] 
 **access** | **String**| json path allowing to retrieve the text to analyze in the retrieved data | [optional] 
 **skip** | **Number**| number of elements to skip, useful for lowering response footprint | [optional] [default to 0]
 **limit** | **Number**| maximum number of elements in the response (zero means no limit), useful for lowering response footprint | [optional] [default to 0]

### Return type

[**[PkmAnnotations]**](PkmAnnotations.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## postAnnotations

> [String] postAnnotations(dbName, key, body)



Insert annotations in the given project

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.AnnotationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmAnnotations()]; // [PkmAnnotations] | Annotations
apiInstance.postAnnotations(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmAnnotations]**](PkmAnnotations.md)| Annotations | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## putAnnotations

> [String] putAnnotations(dbName, key, body)



Insert (if not yet present) or update (if already present) some annotations into the database

### Example

```javascript
var PkmRestfulApi = require('pkm_restful_api');

var apiInstance = new PkmRestfulApi.AnnotationsApi();
var dbName = "dbName_example"; // String | Database name
var key = "key_example"; // String | Access key to the PKM
var body = [new PkmRestfulApi.PkmAnnotations()]; // [PkmAnnotations] | Annotations
apiInstance.putAnnotations(dbName, key, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| Database name | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**[PkmAnnotations]**](PkmAnnotations.md)| Annotations | 

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

