# PkmRestfulApi.GitJobParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dbName** | **String** | database name | [optional] 
**options** | **Object** |  | [optional] 


