# PkmRestfulApi.GFunDecl

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spec** | [**Spec**](Spec.md) |  | [optional] 
**varinfo** | [**Varinfo**](Varinfo.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


