# PkmRestfulApi.DefinitionData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**canPassInRegisters** | **Boolean** |  | [optional] 
**copyAssign** | [**CopyAssign**](CopyAssign.md) |  | 
**copyCtor** | [**CopyCtor**](CopyCtor.md) |  | 
**defaultCtor** | [**DefaultCtor**](DefaultCtor.md) |  | 
**dtor** | [**Dtor**](Dtor.md) |  | 
**isAggregate** | **Boolean** |  | [optional] 
**isLiteral** | **Boolean** |  | [optional] 
**isPOD** | **Boolean** |  | [optional] 
**isStandardLayout** | **Boolean** |  | [optional] 
**isTrivial** | **Boolean** |  | [optional] 
**isTriviallyCopyable** | **Boolean** |  | [optional] 
**moveAssign** | [**MoveAssign**](MoveAssign.md) |  | 
**moveCtor** | [**MoveCtor**](MoveCtor.md) |  | 
**hasVariantMembers** | **Boolean** |  | [optional] 
**canConstDefaultInit** | **Boolean** |  | [optional] 
**hasConstexprNonCopyMoveConstructor** | **Boolean** |  | [optional] 
**isEmpty** | **Boolean** |  | [optional] 


