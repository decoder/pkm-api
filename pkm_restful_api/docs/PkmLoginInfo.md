# PkmRestfulApi.PkmLoginInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_name** | **String** | The user&#39;s name | [optional] 
**user_password** | **String** | The user&#39;s password | [optional] 


