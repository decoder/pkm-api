# PkmRestfulApi.PkmSourceCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceFile** | **String** | related source code file | 
**globals** | **[{String: Object}]** |  | [optional] 
**globinit** | [**PkmCSourceCodeGlobinit**](PkmCSourceCodeGlobinit.md) |  | [optional] 
**globinitcalled** | **Boolean** |  | [optional] 
**type** | **String** | type of document for the GUI, typically &#39;Code&#39; | [optional] 
**manifest** | [**[ManifestItem]**](ManifestItem.md) | Index of C++ artefacts in the document to simplify the search for C++ artefacts | 
**inner** | [**[Node]**](Node.md) | List of child C++ artefacts | [optional] 


