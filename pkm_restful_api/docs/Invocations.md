# PkmRestfulApi.Invocations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invocationID** | **String** |  | 
**tool** | **String** |  | 
**invocationConfiguration** | **Object** |  | 
**user** | **String** |  | 
**timestampRequest** | **String** |  | 
**timestampStart** | **String** |  | [optional] 
**timestampCompleted** | **String** |  | [optional] 
**invocationStatus** | **String** |  | 
**invocationResults** | [**[PkmInvocationInvocationResults]**](PkmInvocationInvocationResults.md) |  | [optional] 



## Enum: InvocationStatusEnum


* `PENDING` (value: `"PENDING"`)

* `RUNNING` (value: `"RUNNING"`)

* `COMPLETED` (value: `"COMPLETED"`)

* `FAILED` (value: `"FAILED"`)




