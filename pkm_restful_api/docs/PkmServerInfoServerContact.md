# PkmRestfulApi.PkmServerInfoServerContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The identifying name of the contact person/organization | [optional] 
**url** | **String** | The URL pointing to the contact information | [optional] 
**email** | **String** | The email address of the contact person/organization | [optional] 


