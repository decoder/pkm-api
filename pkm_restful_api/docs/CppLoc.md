# PkmRestfulApi.CppLoc

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pos_start** | [**CppPos**](CppPos.md) |  | [optional] 
**pos_end** | [**CppPos**](CppPos.md) |  | [optional] 


