# PkmRestfulApi.Plet

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logic_info** | [**LogicInfo**](LogicInfo.md) |  | [optional] 
**predicate** | [**Predicate**](Predicate.md) |  | [optional] 


