# PkmRestfulApi.TermOffset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TNoOffset** | **Object** |  | [optional] 
**TField** | [**TField**](TField.md) |  | [optional] 
**TModel** | **Object** | TBD | [optional] 
**TIndex** | **Object** | TBD | [optional] 


