# PkmRestfulApi.UmlModule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**classes** | [**[UmlClass]**](UmlClass.md) |  | 
**associations** | [**[UmlAssociation]**](UmlAssociation.md) |  | [optional] 


