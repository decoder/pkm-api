# PkmRestfulApi.LogicVarKind

## Enum


* `LVGlobal` (value: `"LVGlobal"`)

* `LVC` (value: `"LVC"`)

* `LVFormal` (value: `"LVFormal"`)

* `LVQuant` (value: `"LVQuant"`)

* `LVLocal` (value: `"LVLocal"`)


