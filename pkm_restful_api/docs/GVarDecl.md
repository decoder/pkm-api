# PkmRestfulApi.GVarDecl

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**varinfo** | [**Varinfo**](Varinfo.md) |  | [optional] 
**loc** | [**Loc**](Loc.md) |  | [optional] 


