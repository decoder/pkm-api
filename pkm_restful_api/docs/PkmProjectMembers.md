# PkmRestfulApi.PkmProjectMembers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | member name | 
**owner** | **Boolean** | a flag indicating ownership of the project | [optional] 
**roles** | **[String]** |  | [optional] 



## Enum: [RolesEnum]


* `Owner` (value: `"Owner"`)

* `Developer` (value: `"Developer"`)

* `Reviewer` (value: `"Reviewer"`)

* `Maintainer` (value: `"Maintainer"`)




