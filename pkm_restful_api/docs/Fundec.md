# PkmRestfulApi.Fundec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**svar** | [**Svar**](Svar.md) |  | [optional] 
**sformals** | **[Object]** |  | [optional] 
**slocals** | **[Object]** |  | [optional] 
**smaxid** | **Number** |  | [optional] 
**sbody** | [**Sbody**](Sbody.md) |  | [optional] 
**sallstmts** | **[Object]** |  | [optional] 
**sspec** | [**Sspec**](Sspec.md) |  | [optional] 


