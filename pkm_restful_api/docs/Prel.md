# PkmRestfulApi.Prel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | [**Relation**](Relation.md) |  | [optional] 
**term** | [**Term**](Term.md) |  | [optional] 


