# PkmRestfulApi.InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**git_commands** | **[[String]]** | Git commands | 
**options** | [**GitRunDbNameOptions**](GitRunDbNameOptions.md) |  | [optional] 


