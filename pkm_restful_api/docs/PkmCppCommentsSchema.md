# PkmRestfulApi.PkmCppCommentsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourceFile** | **String** |  | [optional] 
**comments** | [**[PkmCppCommentsComments]**](PkmCppCommentsComments.md) |  | [optional] 


