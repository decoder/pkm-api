/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmLog'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/PkmLog'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.LogApi = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmLog);
  }
}(this, function(ApiClient, PkmLog) {
  'use strict';

  /**
   * Log service.
   * @module api/LogApi
   * @version 1.0.0
   */

  /**
   * Constructs a new LogApi. 
   * @alias module:api/LogApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Delete a Log document from the database
     * @param {String} dbName Database name
     * @param {String} artefactId the ID of the Log document
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.deleteLogWithHttpInfo = function(dbName, artefactId, key) {
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling deleteLog");
      }
      // verify the required parameter 'artefactId' is set
      if (artefactId === undefined || artefactId === null) {
        throw new Error("Missing the required parameter 'artefactId' when calling deleteLog");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling deleteLog");
      }

      var pathParams = {
        'dbName': dbName,
        'artefactId': artefactId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/log/{dbName}/{artefactId}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Delete a Log document from the database
     * @param {String} dbName Database name
     * @param {String} artefactId the ID of the Log document
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.deleteLog = function(dbName, artefactId, key) {
      return this.deleteLogWithHttpInfo(dbName, artefactId, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Delete all Log documents from the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.deleteLogsWithHttpInfo = function(dbName, key) {
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling deleteLogs");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling deleteLogs");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/log/{dbName}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Delete all Log documents from the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.deleteLogs = function(dbName, key) {
      return this.deleteLogsWithHttpInfo(dbName, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get a Log document from the database
     * @param {String} dbName Database name
     * @param {String} artefactId the ID of the Log document
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/PkmLog} and HTTP response
     */
    this.getLogWithHttpInfo = function(dbName, artefactId, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getLog");
      }
      // verify the required parameter 'artefactId' is set
      if (artefactId === undefined || artefactId === null) {
        throw new Error("Missing the required parameter 'artefactId' when calling getLog");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getLog");
      }

      var pathParams = {
        'dbName': dbName,
        'artefactId': artefactId
      };
      var queryParams = {
        'abbrev': opts['abbrev'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = PkmLog;
      return this.apiClient.callApi(
        '/log/{dbName}/{artefactId}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get a Log document from the database
     * @param {String} dbName Database name
     * @param {String} artefactId the ID of the Log document
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/PkmLog}
     */
    this.getLog = function(dbName, artefactId, key, opts) {
      return this.getLogWithHttpInfo(dbName, artefactId, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get all Log documents from the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint (default to false)
     * @param {Number} opts.skip number of elements to skip, useful for lowering response footprint (default to 0)
     * @param {Number} opts.limit maximum number of elements in the response (zero means no limit), useful for lowering response footprint (default to 0)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<module:model/PkmLog>} and HTTP response
     */
    this.getLogsWithHttpInfo = function(dbName, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getLogs");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getLogs");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'abbrev': opts['abbrev'],
        'skip': opts['skip'],
        'limit': opts['limit'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [PkmLog];
      return this.apiClient.callApi(
        '/log/{dbName}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get all Log documents from the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint (default to false)
     * @param {Number} opts.skip number of elements to skip, useful for lowering response footprint (default to 0)
     * @param {Number} opts.limit maximum number of elements in the response (zero means no limit), useful for lowering response footprint (default to 0)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/PkmLog>}
     */
    this.getLogs = function(dbName, key, opts) {
      return this.getLogsWithHttpInfo(dbName, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Post one or more Log documents into the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmLog>} body Log documents
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<String>} and HTTP response
     */
    this.postLogsWithHttpInfo = function(dbName, key, body) {
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling postLogs");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling postLogs");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling postLogs");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ['String'];
      return this.apiClient.callApi(
        '/log/{dbName}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Post one or more Log documents into the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmLog>} body Log documents
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<String>}
     */
    this.postLogs = function(dbName, key, body) {
      return this.postLogsWithHttpInfo(dbName, key, body)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Insert (if not yet present) or update (if already present) one or more Log documents into the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmLog>} body Log documents
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<String>} and HTTP response
     */
    this.putLogsWithHttpInfo = function(dbName, key, body) {
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling putLogs");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling putLogs");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling putLogs");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ['String'];
      return this.apiClient.callApi(
        '/log/{dbName}', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Insert (if not yet present) or update (if already present) one or more Log documents into the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmLog>} body Log documents
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<String>}
     */
    this.putLogs = function(dbName, key, body) {
      return this.putLogsWithHttpInfo(dbName, key, body)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
