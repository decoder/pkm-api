/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/GitJob', 'model/InlineObject', 'model/InlineObject1', 'model/InlineResponse200', 'model/PkmFile', 'model/PkmGitWorkingTree'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/GitJob'), require('../model/InlineObject'), require('../model/InlineObject1'), require('../model/InlineResponse200'), require('../model/PkmFile'), require('../model/PkmGitWorkingTree'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.GitApi = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.GitJob, root.PkmRestfulApi.InlineObject, root.PkmRestfulApi.InlineObject1, root.PkmRestfulApi.InlineResponse200, root.PkmRestfulApi.PkmFile, root.PkmRestfulApi.PkmGitWorkingTree);
  }
}(this, function(ApiClient, GitJob, InlineObject, InlineObject1, InlineResponse200, PkmFile, PkmGitWorkingTree) {
  'use strict';

  /**
   * Git service.
   * @module api/GitApi
   * @version 1.0.0
   */

  /**
   * Constructs a new GitApi. 
   * @alias module:api/GitApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * delete a file in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} filename filename
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.deleteGitFileWithHttpInfo = function(dbName, gitWorkingTree, filename, key) {
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling deleteGitFile");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling deleteGitFile");
      }
      // verify the required parameter 'filename' is set
      if (filename === undefined || filename === null) {
        throw new Error("Missing the required parameter 'filename' when calling deleteGitFile");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling deleteGitFile");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree,
        'filename': filename
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/git/files/{dbName}/{gitWorkingTree}/{filename}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * delete a file in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} filename filename
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.deleteGitFile = function(dbName, gitWorkingTree, filename, key) {
      return this.deleteGitFileWithHttpInfo(dbName, gitWorkingTree, filename, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * delete files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.deleteGitFilesWithHttpInfo = function(dbName, gitWorkingTree, key) {
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling deleteGitFiles");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling deleteGitFiles");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling deleteGitFiles");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/git/files/{dbName}/{gitWorkingTree}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * delete files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.deleteGitFiles = function(dbName, gitWorkingTree, key) {
      return this.deleteGitFilesWithHttpInfo(dbName, gitWorkingTree, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * delete a Git Working Tree document from the database
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.dontDeletePkmFiles a flag to control deletion of files in the PKM together with the Git working trees. (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.deleteGitWorkingTreeWithHttpInfo = function(dbName, gitWorkingTree, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling deleteGitWorkingTree");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling deleteGitWorkingTree");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling deleteGitWorkingTree");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
        'dontDeletePkmFiles': opts['dontDeletePkmFiles'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/git/working_trees/{dbName}/{gitWorkingTree}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * delete a Git Working Tree document from the database
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.dontDeletePkmFiles a flag to control deletion of files in the PKM together with the Git working trees. (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.deleteGitWorkingTree = function(dbName, gitWorkingTree, key, opts) {
      return this.deleteGitWorkingTreeWithHttpInfo(dbName, gitWorkingTree, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Delete all Git Working Tree documents present in the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.dontDeletePkmFiles a flag to control deletion of files in the PKM together with the Git working trees. (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.deleteGitWorkingTreesWithHttpInfo = function(dbName, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling deleteGitWorkingTrees");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling deleteGitWorkingTrees");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'dontDeletePkmFiles': opts['dontDeletePkmFiles'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/git/working_trees/{dbName}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Delete all Git Working Tree documents present in the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.dontDeletePkmFiles a flag to control deletion of files in the PKM together with the Git working trees. (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.deleteGitWorkingTrees = function(dbName, key, opts) {
      return this.deleteGitWorkingTreesWithHttpInfo(dbName, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/InlineResponse200} and HTTP response
     */
    this.getGitConfigWithHttpInfo = function(dbName, gitWorkingTree, key) {
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getGitConfig");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling getGitConfig");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getGitConfig");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = InlineResponse200;
      return this.apiClient.callApi(
        '/git/config/{dbName}/{gitWorkingTree}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/InlineResponse200}
     */
    this.getGitConfig = function(dbName, gitWorkingTree, key) {
      return this.getGitConfigWithHttpInfo(dbName, gitWorkingTree, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * get a file in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} filename filename
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without content)/full response, useful for lowering response footprint (default to false)
     * @param {String} opts.encoding text encoding (default to 'utf8')
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/PkmFile} and HTTP response
     */
    this.getGitFileWithHttpInfo = function(dbName, gitWorkingTree, filename, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getGitFile");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling getGitFile");
      }
      // verify the required parameter 'filename' is set
      if (filename === undefined || filename === null) {
        throw new Error("Missing the required parameter 'filename' when calling getGitFile");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getGitFile");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree,
        'filename': filename
      };
      var queryParams = {
        'abbrev': opts['abbrev'],
        'encoding': opts['encoding'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = PkmFile;
      return this.apiClient.callApi(
        '/git/files/{dbName}/{gitWorkingTree}/{filename}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * get a file in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} filename filename
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without content)/full response, useful for lowering response footprint (default to false)
     * @param {String} opts.encoding text encoding (default to 'utf8')
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/PkmFile}
     */
    this.getGitFile = function(dbName, gitWorkingTree, filename, key, opts) {
      return this.getGitFileWithHttpInfo(dbName, gitWorkingTree, filename, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * get files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without content)/full response, useful for lowering response footprint (default to false)
     * @param {String} opts.encoding text encoding (default to 'utf8')
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<module:model/PkmFile>} and HTTP response
     */
    this.getGitFilesWithHttpInfo = function(dbName, gitWorkingTree, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getGitFiles");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling getGitFiles");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getGitFiles");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
        'abbrev': opts['abbrev'],
        'encoding': opts['encoding'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [PkmFile];
      return this.apiClient.callApi(
        '/git/files/{dbName}/{gitWorkingTree}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * get files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.abbrev toggle abbreviated response (without content)/full response, useful for lowering response footprint (default to false)
     * @param {String} opts.encoding text encoding (default to 'utf8')
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/PkmFile>}
     */
    this.getGitFiles = function(dbName, gitWorkingTree, key, opts) {
      return this.getGitFilesWithHttpInfo(dbName, gitWorkingTree, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get Git Job. Getting a finished or failed job, unpublish it (it is no longer available))
     * @param {Number} jobId Job identifier
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/GitJob} and HTTP response
     */
    this.getGitJobWithHttpInfo = function(jobId, key) {
      var postBody = null;
      // verify the required parameter 'jobId' is set
      if (jobId === undefined || jobId === null) {
        throw new Error("Missing the required parameter 'jobId' when calling getGitJob");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getGitJob");
      }

      var pathParams = {
        'jobId': jobId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = GitJob;
      return this.apiClient.callApi(
        '/git/jobs/{jobId}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get Git Job. Getting a finished or failed job, unpublish it (it is no longer available))
     * @param {Number} jobId Job identifier
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/GitJob}
     */
    this.getGitJob = function(jobId, key) {
      return this.getGitJobWithHttpInfo(jobId, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get Git Working Tree documents by Git working tree from the database
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/PkmGitWorkingTree} and HTTP response
     */
    this.getGitWorkingTreeWithHttpInfo = function(dbName, gitWorkingTree, key) {
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getGitWorkingTree");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling getGitWorkingTree");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getGitWorkingTree");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = PkmGitWorkingTree;
      return this.apiClient.callApi(
        '/git/working_trees/{dbName}/{gitWorkingTree}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get Git Working Tree documents by Git working tree from the database
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/PkmGitWorkingTree}
     */
    this.getGitWorkingTree = function(dbName, gitWorkingTree, key) {
      return this.getGitWorkingTreeWithHttpInfo(dbName, gitWorkingTree, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get all Git Working Tree documents from the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Number} opts.skip number of elements to skip, useful for lowering response footprint (default to 0)
     * @param {Number} opts.limit maximum number of elements in the response (zero means no limit), useful for lowering response footprint (default to 0)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link Array.<module:model/PkmGitWorkingTree>} and HTTP response
     */
    this.getGitWorkingTreesWithHttpInfo = function(dbName, key, opts) {
      opts = opts || {};
      var postBody = null;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling getGitWorkingTrees");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getGitWorkingTrees");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'skip': opts['skip'],
        'limit': opts['limit'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [PkmGitWorkingTree];
      return this.apiClient.callApi(
        '/git/working_trees/{dbName}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get all Git Working Tree documents from the database
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {Object} opts Optional parameters
     * @param {Number} opts.skip number of elements to skip, useful for lowering response footprint (default to 0)
     * @param {Number} opts.limit maximum number of elements in the response (zero means no limit), useful for lowering response footprint (default to 0)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/PkmGitWorkingTree>}
     */
    this.getGitWorkingTrees = function(dbName, key, opts) {
      return this.getGitWorkingTreesWithHttpInfo(dbName, key, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * post some files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmFile>} body Files
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link String} and HTTP response
     */
    this.postGitFilesWithHttpInfo = function(dbName, gitWorkingTree, key, body) {
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling postGitFiles");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling postGitFiles");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling postGitFiles");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling postGitFiles");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = 'String';
      return this.apiClient.callApi(
        '/git/files/{dbName}/{gitWorkingTree}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * post some files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmFile>} body Files
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link String}
     */
    this.postGitFiles = function(dbName, gitWorkingTree, key, body) {
      return this.postGitFilesWithHttpInfo(dbName, gitWorkingTree, key, body)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Run Git commands
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of Git job (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/GitJob} and HTTP response
     */
    this.postGitRunWithHttpInfo = function(dbName, key, body, opts) {
      opts = opts || {};
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling postGitRun");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling postGitRun");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling postGitRun");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'asynchronous': opts['asynchronous'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = GitJob;
      return this.apiClient.callApi(
        '/git/run/{dbName}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Run Git commands
     * @param {String} dbName Database name
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of Git job (default to false)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/GitJob}
     */
    this.postGitRun = function(dbName, key, body, opts) {
      return this.postGitRunWithHttpInfo(dbName, key, body, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Update the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject1} body 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.putGitConfigWithHttpInfo = function(dbName, gitWorkingTree, key, body) {
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling putGitConfig");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling putGitConfig");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling putGitConfig");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling putGitConfig");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/git/config/{dbName}/{gitWorkingTree}', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Update the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject1} body 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.putGitConfig = function(dbName, gitWorkingTree, key, body) {
      return this.putGitConfigWithHttpInfo(dbName, gitWorkingTree, key, body)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * put some files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmFile>} body Files
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing HTTP response
     */
    this.putGitFilesWithHttpInfo = function(dbName, gitWorkingTree, key, body) {
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling putGitFiles");
      }
      // verify the required parameter 'gitWorkingTree' is set
      if (gitWorkingTree === undefined || gitWorkingTree === null) {
        throw new Error("Missing the required parameter 'gitWorkingTree' when calling putGitFiles");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling putGitFiles");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling putGitFiles");
      }

      var pathParams = {
        'dbName': dbName,
        'gitWorkingTree': gitWorkingTree
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = null;
      return this.apiClient.callApi(
        '/git/files/{dbName}/{gitWorkingTree}', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * put some files in the Git working tree associated to the database.
     * @param {String} dbName Database name
     * @param {String} gitWorkingTree Git working tree
     * @param {String} key Access key to the PKM
     * @param {Array.<module:model/PkmFile>} body Files
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}
     */
    this.putGitFiles = function(dbName, gitWorkingTree, key, body) {
      return this.putGitFilesWithHttpInfo(dbName, gitWorkingTree, key, body)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
