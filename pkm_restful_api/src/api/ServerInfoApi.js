/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmServerInfo'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/PkmServerInfo'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.ServerInfoApi = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmServerInfo);
  }
}(this, function(ApiClient, PkmServerInfo) {
  'use strict';

  /**
   * ServerInfo service.
   * @module api/ServerInfoApi
   * @version 1.0.0
   */

  /**
   * Constructs a new ServerInfoApi. 
   * @alias module:api/ServerInfoApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Returns information about the PKM instance
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/PkmServerInfo} and HTTP response
     */
    this.getServerInfoWithHttpInfo = function() {
      var postBody = null;

      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = PkmServerInfo;
      return this.apiClient.callApi(
        '/server_info', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Returns information about the PKM instance
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/PkmServerInfo}
     */
    this.getServerInfo = function() {
      return this.getServerInfoWithHttpInfo()
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
