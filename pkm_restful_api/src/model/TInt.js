/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Attr'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Attr'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.TInt = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.Attr);
  }
}(this, function(ApiClient, Attr) {
  'use strict';



  /**
   * The TInt model module.
   * @module model/TInt
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>TInt</code>.
   * @alias module:model/TInt
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>TInt</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TInt} obj Optional instance to populate.
   * @return {module:model/TInt} The populated <code>TInt</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('TInt_kind')) {
        obj['TInt_kind'] = ApiClient.convertToType(data['TInt_kind'], 'String');
      }
      if (data.hasOwnProperty('TInt_attributes')) {
        obj['TInt_attributes'] = ApiClient.convertToType(data['TInt_attributes'], [Attr]);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/TInt.TIntKindEnum} TInt_kind
   */
  exports.prototype['TInt_kind'] = undefined;
  /**
   * @member {Array.<module:model/Attr>} TInt_attributes
   */
  exports.prototype['TInt_attributes'] = undefined;


  /**
   * Allowed values for the <code>TInt_kind</code> property.
   * @enum {String}
   * @readonly
   */
  exports.TIntKindEnum = {
    /**
     * value: "IBool"
     * @const
     */
    "IBool": "IBool",
    /**
     * value: "IChar"
     * @const
     */
    "IChar": "IChar",
    /**
     * value: "ISChar"
     * @const
     */
    "ISChar": "ISChar",
    /**
     * value: "IUChar"
     * @const
     */
    "IUChar": "IUChar",
    /**
     * value: "IInt"
     * @const
     */
    "IInt": "IInt",
    /**
     * value: "IUInt"
     * @const
     */
    "IUInt": "IUInt",
    /**
     * value: "IShort"
     * @const
     */
    "IShort": "IShort",
    /**
     * value: "IUShort"
     * @const
     */
    "IUShort": "IUShort",
    /**
     * value: "ILong"
     * @const
     */
    "ILong": "ILong",
    /**
     * value: "IULong"
     * @const
     */
    "IULong": "IULong",
    /**
     * value: "ILongLong"
     * @const
     */
    "ILongLong": "ILongLong",
    /**
     * value: "IULongLong"
     * @const
     */
    "IULongLong": "IULongLong"  };


  return exports;
}));


