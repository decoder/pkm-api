/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmCppAnnotationsAnnotations'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PkmCppAnnotationsAnnotations'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmCppAnnotations = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmCppAnnotationsAnnotations);
  }
}(this, function(ApiClient, PkmCppAnnotationsAnnotations) {
  'use strict';



  /**
   * The PkmCppAnnotations model module.
   * @module model/PkmCppAnnotations
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmCppAnnotations</code>.
   * @alias module:model/PkmCppAnnotations
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>PkmCppAnnotations</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmCppAnnotations} obj Optional instance to populate.
   * @return {module:model/PkmCppAnnotations} The populated <code>PkmCppAnnotations</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('sourceFile')) {
        obj['sourceFile'] = ApiClient.convertToType(data['sourceFile'], 'String');
      }
      if (data.hasOwnProperty('annotations')) {
        obj['annotations'] = ApiClient.convertToType(data['annotations'], [PkmCppAnnotationsAnnotations]);
      }
    }
    return obj;
  }

  /**
   * @member {String} sourceFile
   */
  exports.prototype['sourceFile'] = undefined;
  /**
   * @member {Array.<module:model/PkmCppAnnotationsAnnotations>} annotations
   */
  exports.prototype['annotations'] = undefined;



  return exports;
}));


