/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/JavaLoc'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./JavaLoc'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.JavaComment = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.JavaLoc);
  }
}(this, function(ApiClient, JavaLoc) {
  'use strict';



  /**
   * The JavaComment model module.
   * @module model/JavaComment
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>JavaComment</code>.
   * @alias module:model/JavaComment
   * @class
   * @param loc {module:model/JavaLoc} 
   * @param commentInEnvironment {String} 
   * @param comments {Array.<String>} 
   */
  var exports = function(loc, commentInEnvironment, comments) {
    var _this = this;

    _this['loc'] = loc;
    _this['commentInEnvironment'] = commentInEnvironment;
    _this['comments'] = comments;
  };

  /**
   * Constructs a <code>JavaComment</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/JavaComment} obj Optional instance to populate.
   * @return {module:model/JavaComment} The populated <code>JavaComment</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('loc')) {
        obj['loc'] = JavaLoc.constructFromObject(data['loc']);
      }
      if (data.hasOwnProperty('commentInEnvironment')) {
        obj['commentInEnvironment'] = ApiClient.convertToType(data['commentInEnvironment'], 'String');
      }
      if (data.hasOwnProperty('comments')) {
        obj['comments'] = ApiClient.convertToType(data['comments'], ['String']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/JavaLoc} loc
   */
  exports.prototype['loc'] = undefined;
  /**
   * @member {String} commentInEnvironment
   */
  exports.prototype['commentInEnvironment'] = undefined;
  /**
   * @member {Array.<String>} comments
   */
  exports.prototype['comments'] = undefined;



  return exports;
}));


