/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/GlobalKind', 'model/Loc1'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./GlobalKind'), require('./Loc1'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmCCommentsComments = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.GlobalKind, root.PkmRestfulApi.Loc1);
  }
}(this, function(ApiClient, GlobalKind, Loc1) {
  'use strict';



  /**
   * The PkmCCommentsComments model module.
   * @module model/PkmCCommentsComments
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmCCommentsComments</code>.
   * @alias module:model/PkmCCommentsComments
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>PkmCCommentsComments</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmCCommentsComments} obj Optional instance to populate.
   * @return {module:model/PkmCCommentsComments} The populated <code>PkmCCommentsComments</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('global_kind')) {
        obj['global_kind'] = GlobalKind.constructFromObject(data['global_kind']);
      }
      if (data.hasOwnProperty('loc')) {
        obj['loc'] = Loc1.constructFromObject(data['loc']);
      }
      if (data.hasOwnProperty('comments')) {
        obj['comments'] = ApiClient.convertToType(data['comments'], ['String']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/GlobalKind} global_kind
   */
  exports.prototype['global_kind'] = undefined;
  /**
   * @member {module:model/Loc1} loc
   */
  exports.prototype['loc'] = undefined;
  /**
   * @member {Array.<String>} comments
   */
  exports.prototype['comments'] = undefined;



  return exports;
}));


