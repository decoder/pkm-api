/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmCveCnaContainerAffectedVersions'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PkmCveCnaContainerAffectedVersions'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmCveCnaContainerAffectedProducts = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmCveCnaContainerAffectedVersions);
  }
}(this, function(ApiClient, PkmCveCnaContainerAffectedVersions) {
  'use strict';



  /**
   * The PkmCveCnaContainerAffectedProducts model module.
   * @module model/PkmCveCnaContainerAffectedProducts
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmCveCnaContainerAffectedProducts</code>.
   * @alias module:model/PkmCveCnaContainerAffectedProducts
   * @class
   * @param versions {Array.<module:model/PkmCveCnaContainerAffectedVersions>} 
   */
  var exports = function(versions) {
    var _this = this;

    _this['versions'] = versions;
  };

  /**
   * Constructs a <code>PkmCveCnaContainerAffectedProducts</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmCveCnaContainerAffectedProducts} obj Optional instance to populate.
   * @return {module:model/PkmCveCnaContainerAffectedProducts} The populated <code>PkmCveCnaContainerAffectedProducts</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('versions')) {
        obj['versions'] = ApiClient.convertToType(data['versions'], [PkmCveCnaContainerAffectedVersions]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/PkmCveCnaContainerAffectedVersions>} versions
   */
  exports.prototype['versions'] = undefined;



  return exports;
}));


