/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmCveCVEDataMeta', 'model/PkmCveCnaContainer'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PkmCveCVEDataMeta'), require('./PkmCveCnaContainer'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmCveSchema = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmCveCVEDataMeta, root.PkmRestfulApi.PkmCveCnaContainer);
  }
}(this, function(ApiClient, PkmCveCVEDataMeta, PkmCveCnaContainer) {
  'use strict';



  /**
   * The PkmCveSchema model module.
   * @module model/PkmCveSchema
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmCveSchema</code>.
   * @alias module:model/PkmCveSchema
   * @class
   * @param CVE_data_meta {module:model/PkmCveCVEDataMeta} 
   * @param data_format {String} 
   * @param data_type {String} 
   * @param data_version {String} 
   */
  var exports = function(CVE_data_meta, data_format, data_type, data_version) {
    var _this = this;

    _this['CVE_data_meta'] = CVE_data_meta;
    _this['data_format'] = data_format;
    _this['data_type'] = data_type;
    _this['data_version'] = data_version;
  };

  /**
   * Constructs a <code>PkmCveSchema</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmCveSchema} obj Optional instance to populate.
   * @return {module:model/PkmCveSchema} The populated <code>PkmCveSchema</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('CVE_data_meta')) {
        obj['CVE_data_meta'] = PkmCveCVEDataMeta.constructFromObject(data['CVE_data_meta']);
      }
      if (data.hasOwnProperty('cna-container')) {
        obj['cna-container'] = PkmCveCnaContainer.constructFromObject(data['cna-container']);
      }
      if (data.hasOwnProperty('data_format')) {
        obj['data_format'] = ApiClient.convertToType(data['data_format'], 'String');
      }
      if (data.hasOwnProperty('data_type')) {
        obj['data_type'] = ApiClient.convertToType(data['data_type'], 'String');
      }
      if (data.hasOwnProperty('data_version')) {
        obj['data_version'] = ApiClient.convertToType(data['data_version'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {module:model/PkmCveCVEDataMeta} CVE_data_meta
   */
  exports.prototype['CVE_data_meta'] = undefined;
  /**
   * @member {module:model/PkmCveCnaContainer} cna-container
   */
  exports.prototype['cna-container'] = undefined;
  /**
   * @member {String} data_format
   */
  exports.prototype['data_format'] = undefined;
  /**
   * @member {String} data_type
   */
  exports.prototype['data_type'] = undefined;
  /**
   * @member {String} data_version
   */
  exports.prototype['data_version'] = undefined;



  return exports;
}));


