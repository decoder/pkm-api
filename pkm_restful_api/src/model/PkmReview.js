/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmReviewReviewArtifacts', 'model/PkmReviewReviewComments'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PkmReviewReviewArtifacts'), require('./PkmReviewReviewComments'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmReview = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmReviewReviewArtifacts, root.PkmRestfulApi.PkmReviewReviewComments);
  }
}(this, function(ApiClient, PkmReviewReviewArtifacts, PkmReviewReviewComments) {
  'use strict';



  /**
   * The PkmReview model module.
   * @module model/PkmReview
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmReview</code>.
   * PKM review
   * @alias module:model/PkmReview
   * @class
   * @param reviewTitle {String} 
   * @param reviewStatus {module:model/PkmReview.ReviewStatusEnum} 
   * @param reviewOpenDate {String} 
   * @param reviewAuthor {String} 
   */
  var exports = function(reviewTitle, reviewStatus, reviewOpenDate, reviewAuthor) {
    var _this = this;

    _this['reviewTitle'] = reviewTitle;
    _this['reviewStatus'] = reviewStatus;
    _this['reviewOpenDate'] = reviewOpenDate;
    _this['reviewAuthor'] = reviewAuthor;
  };

  /**
   * Constructs a <code>PkmReview</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmReview} obj Optional instance to populate.
   * @return {module:model/PkmReview} The populated <code>PkmReview</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('reviewID')) {
        obj['reviewID'] = ApiClient.convertToType(data['reviewID'], 'String');
      }
      if (data.hasOwnProperty('reviewTitle')) {
        obj['reviewTitle'] = ApiClient.convertToType(data['reviewTitle'], 'String');
      }
      if (data.hasOwnProperty('reviewStatus')) {
        obj['reviewStatus'] = ApiClient.convertToType(data['reviewStatus'], 'String');
      }
      if (data.hasOwnProperty('reviewOpenDate')) {
        obj['reviewOpenDate'] = ApiClient.convertToType(data['reviewOpenDate'], 'String');
      }
      if (data.hasOwnProperty('reviewCompletedDate')) {
        obj['reviewCompletedDate'] = ApiClient.convertToType(data['reviewCompletedDate'], 'String');
      }
      if (data.hasOwnProperty('reviewAuthor')) {
        obj['reviewAuthor'] = ApiClient.convertToType(data['reviewAuthor'], 'String');
      }
      if (data.hasOwnProperty('reviewComments')) {
        obj['reviewComments'] = ApiClient.convertToType(data['reviewComments'], [PkmReviewReviewComments]);
      }
      if (data.hasOwnProperty('reviewArtifacts')) {
        obj['reviewArtifacts'] = ApiClient.convertToType(data['reviewArtifacts'], [PkmReviewReviewArtifacts]);
      }
    }
    return obj;
  }

  /**
   * @member {String} reviewID
   */
  exports.prototype['reviewID'] = undefined;
  /**
   * @member {String} reviewTitle
   */
  exports.prototype['reviewTitle'] = undefined;
  /**
   * @member {module:model/PkmReview.ReviewStatusEnum} reviewStatus
   */
  exports.prototype['reviewStatus'] = undefined;
  /**
   * @member {String} reviewOpenDate
   */
  exports.prototype['reviewOpenDate'] = undefined;
  /**
   * @member {String} reviewCompletedDate
   */
  exports.prototype['reviewCompletedDate'] = undefined;
  /**
   * @member {String} reviewAuthor
   */
  exports.prototype['reviewAuthor'] = undefined;
  /**
   * @member {Array.<module:model/PkmReviewReviewComments>} reviewComments
   */
  exports.prototype['reviewComments'] = undefined;
  /**
   * @member {Array.<module:model/PkmReviewReviewArtifacts>} reviewArtifacts
   */
  exports.prototype['reviewArtifacts'] = undefined;


  /**
   * Allowed values for the <code>reviewStatus</code> property.
   * @enum {String}
   * @readonly
   */
  exports.ReviewStatusEnum = {
    /**
     * value: "OPEN"
     * @const
     */
    "OPEN": "OPEN",
    /**
     * value: "CLOSED"
     * @const
     */
    "CLOSED": "CLOSED"  };


  return exports;
}));


