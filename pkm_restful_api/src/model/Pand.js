/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Predicate'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Predicate'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.Pand = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.Predicate);
  }
}(this, function(ApiClient, Predicate) {
  'use strict';



  /**
   * The Pand model module.
   * @module model/Pand
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>Pand</code>.
   * @alias module:model/Pand
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>Pand</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Pand} obj Optional instance to populate.
   * @return {module:model/Pand} The populated <code>Pand</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('predicate')) {
        obj['predicate'] = Predicate.constructFromObject(data['predicate']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/Predicate} predicate
   */
  exports.prototype['predicate'] = undefined;



  return exports;
}));


