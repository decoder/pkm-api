/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LogicVar'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LogicVar'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.LVarInfo = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.LogicVar);
  }
}(this, function(ApiClient, LogicVar) {
  'use strict';



  /**
   * The LVarInfo model module.
   * @module model/LVarInfo
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>LVarInfo</code>.
   * TODO
   * @alias module:model/LVarInfo
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>LVarInfo</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/LVarInfo} obj Optional instance to populate.
   * @return {module:model/LVarInfo} The populated <code>LVarInfo</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('logic_var')) {
        obj['logic_var'] = LogicVar.constructFromObject(data['logic_var']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/LogicVar} logic_var
   */
  exports.prototype['logic_var'] = undefined;



  return exports;
}));


