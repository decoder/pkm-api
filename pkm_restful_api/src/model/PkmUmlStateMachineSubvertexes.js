/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmUmlStateMachineSubvertexes = factory(root.PkmRestfulApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';



  /**
   * The PkmUmlStateMachineSubvertexes model module.
   * @module model/PkmUmlStateMachineSubvertexes
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmUmlStateMachineSubvertexes</code>.
   * @alias module:model/PkmUmlStateMachineSubvertexes
   * @class
   * @param isSourceOfTransition {Boolean} 
   * @param previousSubvertexes {Array.<String>} 
   * @param isTargetOfTransition {Boolean} 
   * @param name {String} 
   * @param nextSubvertexes {Array.<String>} 
   * @param id {String} 
   * @param type {String} 
   */
  var exports = function(isSourceOfTransition, previousSubvertexes, isTargetOfTransition, name, nextSubvertexes, id, type) {
    var _this = this;

    _this['isSourceOfTransition'] = isSourceOfTransition;
    _this['previousSubvertexes'] = previousSubvertexes;
    _this['isTargetOfTransition'] = isTargetOfTransition;
    _this['name'] = name;
    _this['nextSubvertexes'] = nextSubvertexes;
    _this['id'] = id;
    _this['type'] = type;
  };

  /**
   * Constructs a <code>PkmUmlStateMachineSubvertexes</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmUmlStateMachineSubvertexes} obj Optional instance to populate.
   * @return {module:model/PkmUmlStateMachineSubvertexes} The populated <code>PkmUmlStateMachineSubvertexes</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('isSourceOfTransition')) {
        obj['isSourceOfTransition'] = ApiClient.convertToType(data['isSourceOfTransition'], 'Boolean');
      }
      if (data.hasOwnProperty('previousSubvertexes')) {
        obj['previousSubvertexes'] = ApiClient.convertToType(data['previousSubvertexes'], ['String']);
      }
      if (data.hasOwnProperty('isTargetOfTransition')) {
        obj['isTargetOfTransition'] = ApiClient.convertToType(data['isTargetOfTransition'], 'Boolean');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('nextSubvertexes')) {
        obj['nextSubvertexes'] = ApiClient.convertToType(data['nextSubvertexes'], ['String']);
      }
      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'String');
      }
      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {Boolean} isSourceOfTransition
   */
  exports.prototype['isSourceOfTransition'] = undefined;
  /**
   * @member {Array.<String>} previousSubvertexes
   */
  exports.prototype['previousSubvertexes'] = undefined;
  /**
   * @member {Boolean} isTargetOfTransition
   */
  exports.prototype['isTargetOfTransition'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {Array.<String>} nextSubvertexes
   */
  exports.prototype['nextSubvertexes'] = undefined;
  /**
   * @member {String} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} type
   */
  exports.prototype['type'] = undefined;



  return exports;
}));


