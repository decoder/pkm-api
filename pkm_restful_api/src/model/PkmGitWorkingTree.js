/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PkmGitWorkingTreeLinked'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PkmGitWorkingTreeLinked'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmGitWorkingTree = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.PkmGitWorkingTreeLinked);
  }
}(this, function(ApiClient, PkmGitWorkingTreeLinked) {
  'use strict';



  /**
   * The PkmGitWorkingTree model module.
   * @module model/PkmGitWorkingTree
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmGitWorkingTree</code>.
   * Data model for a Git Working Tree in PKM
   * @alias module:model/PkmGitWorkingTree
   * @class
   * @param git_branch {String} Tracked Git branch
   * @param git_commit_id {String} SHA1 ID of current commit
   */
  var exports = function(git_branch, git_commit_id) {
    var _this = this;

    _this['git_branch'] = git_branch;
    _this['git_commit_id'] = git_commit_id;
  };

  /**
   * Constructs a <code>PkmGitWorkingTree</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmGitWorkingTree} obj Optional instance to populate.
   * @return {module:model/PkmGitWorkingTree} The populated <code>PkmGitWorkingTree</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('directory')) {
        obj['directory'] = ApiClient.convertToType(data['directory'], 'String');
      }
      if (data.hasOwnProperty('git_directory')) {
        obj['git_directory'] = ApiClient.convertToType(data['git_directory'], 'String');
      }
      if (data.hasOwnProperty('git_branch')) {
        obj['git_branch'] = ApiClient.convertToType(data['git_branch'], 'String');
      }
      if (data.hasOwnProperty('git_commit_id')) {
        obj['git_commit_id'] = ApiClient.convertToType(data['git_commit_id'], 'String');
      }
      if (data.hasOwnProperty('git_config')) {
        obj['git_config'] = ApiClient.convertToType(data['git_config'], Object);
      }
      if (data.hasOwnProperty('linked')) {
        obj['linked'] = ApiClient.convertToType(data['linked'], [PkmGitWorkingTreeLinked]);
      }
    }
    return obj;
  }

  /**
   * Directory of the (main) Git working tree
   * @member {String} directory
   */
  exports.prototype['directory'] = undefined;
  /**
   * Git directory
   * @member {String} git_directory
   */
  exports.prototype['git_directory'] = undefined;
  /**
   * Tracked Git branch
   * @member {String} git_branch
   */
  exports.prototype['git_branch'] = undefined;
  /**
   * SHA1 ID of current commit
   * @member {String} git_commit_id
   */
  exports.prototype['git_commit_id'] = undefined;
  /**
   * Git configuration of the Git directory (usually .git/config) associated to a Git working tree
   * @member {Object} git_config
   */
  exports.prototype['git_config'] = undefined;
  /**
   * Linked Git working trees created with 'git worktree add'
   * @member {Array.<module:model/PkmGitWorkingTreeLinked>} linked
   */
  exports.prototype['linked'] = undefined;



  return exports;
}));


