/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Attr', 'model/Enuminfo'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Attr'), require('./Enuminfo'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.TEnum = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.Attr, root.PkmRestfulApi.Enuminfo);
  }
}(this, function(ApiClient, Attr, Enuminfo) {
  'use strict';



  /**
   * The TEnum model module.
   * @module model/TEnum
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>TEnum</code>.
   * @alias module:model/TEnum
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>TEnum</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TEnum} obj Optional instance to populate.
   * @return {module:model/TEnum} The populated <code>TEnum</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('enuminfo')) {
        obj['enuminfo'] = Enuminfo.constructFromObject(data['enuminfo']);
      }
      if (data.hasOwnProperty('TEnum_attrs')) {
        obj['TEnum_attrs'] = ApiClient.convertToType(data['TEnum_attrs'], [Attr]);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/Enuminfo} enuminfo
   */
  exports.prototype['enuminfo'] = undefined;
  /**
   * @member {Array.<module:model/Attr>} TEnum_attrs
   */
  exports.prototype['TEnum_attrs'] = undefined;



  return exports;
}));


