/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmInvocationInvocationResults = factory(root.PkmRestfulApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';



  /**
   * The PkmInvocationInvocationResults model module.
   * @module model/PkmInvocationInvocationResults
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmInvocationInvocationResults</code>.
   * @alias module:model/PkmInvocationInvocationResults
   * @class
   * @param path {String} 
   */
  var exports = function(path) {
    var _this = this;

    _this['path'] = path;
  };

  /**
   * Constructs a <code>PkmInvocationInvocationResults</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmInvocationInvocationResults} obj Optional instance to populate.
   * @return {module:model/PkmInvocationInvocationResults} The populated <code>PkmInvocationInvocationResults</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('path')) {
        obj['path'] = ApiClient.convertToType(data['path'], 'String');
      }
      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} path
   */
  exports.prototype['path'] = undefined;
  /**
   * @member {String} type
   */
  exports.prototype['type'] = undefined;



  return exports;
}));


