/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmCveCnaContainerAffectedVersions = factory(root.PkmRestfulApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';



  /**
   * The PkmCveCnaContainerAffectedVersions model module.
   * @module model/PkmCveCnaContainerAffectedVersions
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmCveCnaContainerAffectedVersions</code>.
   * @alias module:model/PkmCveCnaContainerAffectedVersions
   * @class
   * @param version_value {String} 
   */
  var exports = function(version_value) {
    var _this = this;

    _this['version_value'] = version_value;
  };

  /**
   * Constructs a <code>PkmCveCnaContainerAffectedVersions</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmCveCnaContainerAffectedVersions} obj Optional instance to populate.
   * @return {module:model/PkmCveCnaContainerAffectedVersions} The populated <code>PkmCveCnaContainerAffectedVersions</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('version_value')) {
        obj['version_value'] = ApiClient.convertToType(data['version_value'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {String} version_value
   */
  exports.prototype['version_value'] = undefined;



  return exports;
}));


