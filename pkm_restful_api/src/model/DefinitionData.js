/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/CopyAssign', 'model/CopyCtor', 'model/DefaultCtor', 'model/Dtor', 'model/MoveAssign', 'model/MoveCtor'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./CopyAssign'), require('./CopyCtor'), require('./DefaultCtor'), require('./Dtor'), require('./MoveAssign'), require('./MoveCtor'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.DefinitionData = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.CopyAssign, root.PkmRestfulApi.CopyCtor, root.PkmRestfulApi.DefaultCtor, root.PkmRestfulApi.Dtor, root.PkmRestfulApi.MoveAssign, root.PkmRestfulApi.MoveCtor);
  }
}(this, function(ApiClient, CopyAssign, CopyCtor, DefaultCtor, Dtor, MoveAssign, MoveCtor) {
  'use strict';



  /**
   * The DefinitionData model module.
   * @module model/DefinitionData
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>DefinitionData</code>.
   * @alias module:model/DefinitionData
   * @class
   * @param copyAssign {module:model/CopyAssign} 
   * @param copyCtor {module:model/CopyCtor} 
   * @param defaultCtor {module:model/DefaultCtor} 
   * @param dtor {module:model/Dtor} 
   * @param moveAssign {module:model/MoveAssign} 
   * @param moveCtor {module:model/MoveCtor} 
   */
  var exports = function(copyAssign, copyCtor, defaultCtor, dtor, moveAssign, moveCtor) {
    var _this = this;

    _this['copyAssign'] = copyAssign;
    _this['copyCtor'] = copyCtor;
    _this['defaultCtor'] = defaultCtor;
    _this['dtor'] = dtor;
    _this['moveAssign'] = moveAssign;
    _this['moveCtor'] = moveCtor;
  };

  /**
   * Constructs a <code>DefinitionData</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DefinitionData} obj Optional instance to populate.
   * @return {module:model/DefinitionData} The populated <code>DefinitionData</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('canPassInRegisters')) {
        obj['canPassInRegisters'] = ApiClient.convertToType(data['canPassInRegisters'], 'Boolean');
      }
      if (data.hasOwnProperty('copyAssign')) {
        obj['copyAssign'] = CopyAssign.constructFromObject(data['copyAssign']);
      }
      if (data.hasOwnProperty('copyCtor')) {
        obj['copyCtor'] = CopyCtor.constructFromObject(data['copyCtor']);
      }
      if (data.hasOwnProperty('defaultCtor')) {
        obj['defaultCtor'] = DefaultCtor.constructFromObject(data['defaultCtor']);
      }
      if (data.hasOwnProperty('dtor')) {
        obj['dtor'] = Dtor.constructFromObject(data['dtor']);
      }
      if (data.hasOwnProperty('isAggregate')) {
        obj['isAggregate'] = ApiClient.convertToType(data['isAggregate'], 'Boolean');
      }
      if (data.hasOwnProperty('isLiteral')) {
        obj['isLiteral'] = ApiClient.convertToType(data['isLiteral'], 'Boolean');
      }
      if (data.hasOwnProperty('isPOD')) {
        obj['isPOD'] = ApiClient.convertToType(data['isPOD'], 'Boolean');
      }
      if (data.hasOwnProperty('isStandardLayout')) {
        obj['isStandardLayout'] = ApiClient.convertToType(data['isStandardLayout'], 'Boolean');
      }
      if (data.hasOwnProperty('isTrivial')) {
        obj['isTrivial'] = ApiClient.convertToType(data['isTrivial'], 'Boolean');
      }
      if (data.hasOwnProperty('isTriviallyCopyable')) {
        obj['isTriviallyCopyable'] = ApiClient.convertToType(data['isTriviallyCopyable'], 'Boolean');
      }
      if (data.hasOwnProperty('moveAssign')) {
        obj['moveAssign'] = MoveAssign.constructFromObject(data['moveAssign']);
      }
      if (data.hasOwnProperty('moveCtor')) {
        obj['moveCtor'] = MoveCtor.constructFromObject(data['moveCtor']);
      }
      if (data.hasOwnProperty('hasVariantMembers')) {
        obj['hasVariantMembers'] = ApiClient.convertToType(data['hasVariantMembers'], 'Boolean');
      }
      if (data.hasOwnProperty('canConstDefaultInit')) {
        obj['canConstDefaultInit'] = ApiClient.convertToType(data['canConstDefaultInit'], 'Boolean');
      }
      if (data.hasOwnProperty('hasConstexprNonCopyMoveConstructor')) {
        obj['hasConstexprNonCopyMoveConstructor'] = ApiClient.convertToType(data['hasConstexprNonCopyMoveConstructor'], 'Boolean');
      }
      if (data.hasOwnProperty('isEmpty')) {
        obj['isEmpty'] = ApiClient.convertToType(data['isEmpty'], 'Boolean');
      }
    }
    return obj;
  }

  /**
   * @member {Boolean} canPassInRegisters
   */
  exports.prototype['canPassInRegisters'] = undefined;
  /**
   * @member {module:model/CopyAssign} copyAssign
   */
  exports.prototype['copyAssign'] = undefined;
  /**
   * @member {module:model/CopyCtor} copyCtor
   */
  exports.prototype['copyCtor'] = undefined;
  /**
   * @member {module:model/DefaultCtor} defaultCtor
   */
  exports.prototype['defaultCtor'] = undefined;
  /**
   * @member {module:model/Dtor} dtor
   */
  exports.prototype['dtor'] = undefined;
  /**
   * @member {Boolean} isAggregate
   */
  exports.prototype['isAggregate'] = undefined;
  /**
   * @member {Boolean} isLiteral
   */
  exports.prototype['isLiteral'] = undefined;
  /**
   * @member {Boolean} isPOD
   */
  exports.prototype['isPOD'] = undefined;
  /**
   * @member {Boolean} isStandardLayout
   */
  exports.prototype['isStandardLayout'] = undefined;
  /**
   * @member {Boolean} isTrivial
   */
  exports.prototype['isTrivial'] = undefined;
  /**
   * @member {Boolean} isTriviallyCopyable
   */
  exports.prototype['isTriviallyCopyable'] = undefined;
  /**
   * @member {module:model/MoveAssign} moveAssign
   */
  exports.prototype['moveAssign'] = undefined;
  /**
   * @member {module:model/MoveCtor} moveCtor
   */
  exports.prototype['moveCtor'] = undefined;
  /**
   * @member {Boolean} hasVariantMembers
   */
  exports.prototype['hasVariantMembers'] = undefined;
  /**
   * @member {Boolean} canConstDefaultInit
   */
  exports.prototype['canConstDefaultInit'] = undefined;
  /**
   * @member {Boolean} hasConstexprNonCopyMoveConstructor
   */
  exports.prototype['hasConstexprNonCopyMoveConstructor'] = undefined;
  /**
   * @member {Boolean} isEmpty
   */
  exports.prototype['isEmpty'] = undefined;



  return exports;
}));


