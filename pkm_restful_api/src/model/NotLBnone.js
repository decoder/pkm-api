/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/LBpred', 'model/LBread', 'model/LBterm'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./LBpred'), require('./LBread'), require('./LBterm'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.NotLBnone = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.LBpred, root.PkmRestfulApi.LBread, root.PkmRestfulApi.LBterm);
  }
}(this, function(ApiClient, LBpred, LBread, LBterm) {
  'use strict';



  /**
   * The NotLBnone model module.
   * @module model/NotLBnone
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>NotLBnone</code>.
   * @alias module:model/NotLBnone
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>NotLBnone</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/NotLBnone} obj Optional instance to populate.
   * @return {module:model/NotLBnone} The populated <code>NotLBnone</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('LBreads')) {
        obj['LBreads'] = ApiClient.convertToType(data['LBreads'], [LBread]);
      }
      if (data.hasOwnProperty('LBterm')) {
        obj['LBterm'] = LBterm.constructFromObject(data['LBterm']);
      }
      if (data.hasOwnProperty('LBpred')) {
        obj['LBpred'] = LBpred.constructFromObject(data['LBpred']);
      }
      if (data.hasOwnProperty('LBinductive')) {
        obj['LBinductive'] = ApiClient.convertToType(data['LBinductive'], [Object]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/LBread>} LBreads
   */
  exports.prototype['LBreads'] = undefined;
  /**
   * @member {module:model/LBterm} LBterm
   */
  exports.prototype['LBterm'] = undefined;
  /**
   * @member {module:model/LBpred} LBpred
   */
  exports.prototype['LBpred'] = undefined;
  /**
   * @member {Array.<Object>} LBinductive
   */
  exports.prototype['LBinductive'] = undefined;



  return exports;
}));


