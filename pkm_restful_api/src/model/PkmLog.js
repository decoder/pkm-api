/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Errors', 'model/Messages', 'model/Warnings'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Errors'), require('./Messages'), require('./Warnings'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.PkmLog = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.Errors, root.PkmRestfulApi.Messages, root.PkmRestfulApi.Warnings);
  }
}(this, function(ApiClient, Errors, Messages, Warnings) {
  'use strict';



  /**
   * The PkmLog model module.
   * @module model/PkmLog
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>PkmLog</code>.
   * Log JSON schema
   * @alias module:model/PkmLog
   * @class
   * @param tool {String} tool name/tag
   * @param nature_of_report {String} e.g \"Proof report\", \"Modeling report\", \"Testing report\", \"GUI report\", \"NER report\", \"Summarization report\", etc.
   * @param start_running_time {String} start running time
   * @param end_running_time {String} end running time
   * @param status {Boolean} flag of failure of the process: true=not failed
   */
  var exports = function(tool, nature_of_report, start_running_time, end_running_time, status) {
    var _this = this;

    _this['tool'] = tool;
    _this['nature of report'] = nature_of_report;
    _this['start running time'] = start_running_time;
    _this['end running time'] = end_running_time;
    _this['status'] = status;
  };

  /**
   * Constructs a <code>PkmLog</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PkmLog} obj Optional instance to populate.
   * @return {module:model/PkmLog} The populated <code>PkmLog</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'String');
      }
      if (data.hasOwnProperty('tool')) {
        obj['tool'] = ApiClient.convertToType(data['tool'], 'String');
      }
      if (data.hasOwnProperty('nature of report')) {
        obj['nature of report'] = ApiClient.convertToType(data['nature of report'], 'String');
      }
      if (data.hasOwnProperty('start running time')) {
        obj['start running time'] = ApiClient.convertToType(data['start running time'], 'String');
      }
      if (data.hasOwnProperty('end running time')) {
        obj['end running time'] = ApiClient.convertToType(data['end running time'], 'String');
      }
      if (data.hasOwnProperty('messages')) {
        obj['messages'] = Messages.constructFromObject(data['messages']);
      }
      if (data.hasOwnProperty('warnings')) {
        obj['warnings'] = Warnings.constructFromObject(data['warnings']);
      }
      if (data.hasOwnProperty('errors')) {
        obj['errors'] = Errors.constructFromObject(data['errors']);
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'Boolean');
      }
      if (data.hasOwnProperty('details')) {
        obj['details'] = ApiClient.convertToType(data['details'], Object);
      }
    }
    return obj;
  }

  /**
   * ID of Log (automatically generated when missing)
   * @member {String} id
   */
  exports.prototype['id'] = undefined;
  /**
   * tool name/tag
   * @member {String} tool
   */
  exports.prototype['tool'] = undefined;
  /**
   * e.g \"Proof report\", \"Modeling report\", \"Testing report\", \"GUI report\", \"NER report\", \"Summarization report\", etc.
   * @member {String} nature of report
   */
  exports.prototype['nature of report'] = undefined;
  /**
   * start running time
   * @member {String} start running time
   */
  exports.prototype['start running time'] = undefined;
  /**
   * end running time
   * @member {String} end running time
   */
  exports.prototype['end running time'] = undefined;
  /**
   * @member {module:model/Messages} messages
   */
  exports.prototype['messages'] = undefined;
  /**
   * @member {module:model/Warnings} warnings
   */
  exports.prototype['warnings'] = undefined;
  /**
   * @member {module:model/Errors} errors
   */
  exports.prototype['errors'] = undefined;
  /**
   * flag of failure of the process: true=not failed
   * @member {Boolean} status
   */
  exports.prototype['status'] = undefined;
  /**
   * e.g. parameters of the tool, analysis reports, etc.
   * @member {Object} details
   */
  exports.prototype['details'] = undefined;



  return exports;
}));


