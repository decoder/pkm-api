/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Varinfo'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Varinfo'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.Svar = factory(root.PkmRestfulApi.ApiClient, root.PkmRestfulApi.Varinfo);
  }
}(this, function(ApiClient, Varinfo) {
  'use strict';



  /**
   * The Svar model module.
   * @module model/Svar
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>Svar</code>.
   * @alias module:model/Svar
   * @class
   */
  var exports = function() {
    var _this = this;

  };

  /**
   * Constructs a <code>Svar</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Svar} obj Optional instance to populate.
   * @return {module:model/Svar} The populated <code>Svar</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('varinfo')) {
        obj['varinfo'] = Varinfo.constructFromObject(data['varinfo']);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/Varinfo} varinfo
   */
  exports.prototype['varinfo'] = undefined;



  return exports;
}));


