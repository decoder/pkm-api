/**
 * PkmRestfulApi
 * RESTful API of PKM (Persistent Knowledge Monitor)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.PkmRestfulApi) {
      root.PkmRestfulApi = {};
    }
    root.PkmRestfulApi.BuiltinLabel = factory(root.PkmRestfulApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';


  /**
   * Enum class BuiltinLabel.
   * @enum {}
   * @readonly
   */
  var exports = {
    /**
     * value: "Here"
     * @const
     */
    "Here": "Here",
    /**
     * value: "Old"
     * @const
     */
    "Old": "Old",
    /**
     * value: "Pre"
     * @const
     */
    "Pre": "Pre",
    /**
     * value: "Post"
     * @const
     */
    "Post": "Post",
    /**
     * value: "LoopEntry"
     * @const
     */
    "LoopEntry": "LoopEntry",
    /**
     * value: "LoopCurrent"
     * @const
     */
    "LoopCurrent": "LoopCurrent",
    /**
     * value: "Init"
     * @const
     */
    "Init": "Init"  };

  /**
   * Returns a <code>BuiltinLabel</code> enum value from a Javascript object name.
   * @param {Object} data The plain JavaScript object containing the name of the enum value.
   * @return {module:model/BuiltinLabel} The enum <code>BuiltinLabel</code> value.
   */
  exports.constructFromObject = function(object) {
    return object;
  }

  return exports;
}));


