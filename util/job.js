/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

const Logger = require('./logger');

/** A job
 */
class Job extends Logger
{
	static next_id = 0;
	static jobs = new Map();
	
	/** Find job by identifier
	 * 
	 * @param {integer} job_id - Job identifier
	 * @return {(Job|undefined)} a job or undefined if not found
	 */
	static find(job_id)
	{
		return Job.jobs.get(job_id);
	}
	
	/** Publish the job
	 * 
	 * @param {number} job_id - Job identifier
	 */
	publish()
	{
		Job.jobs.set(this.id, this);
	}
	
	/** Unpublish the job
	 * 
	 * @param {number} job_id - Job identifier
	 */
	unpublish()
	{
		Job.jobs.delete(this.id);
	}
	
	/** Job options
	 * 
	 * @typedef {Object} JobOptions
	 * @properties {boolean} debug - enable/disable debug messages
	 */
	
	/** constructor
	 * 
	 * @param {string} service_name - service name
	 * @param {Object} parameters - parameters
	 * @param {JobOptions} [options] - job options
	 */
	constructor(service_name, parameters, options)
	{
		super();
		this.id = Job.next_id++;
		this.service_name = service_name;
		this.parameters = parameters;
		this.options = (options !== undefined) ? options : {};
		this.state = 'pending';
		this.start_date = new Date().toUTCString();
		
		this.publish();
	}
	
	/** Run job.
	 *  The job state changes from 'pending' to 'running'.
	 *  The job state will change from 'running' to either 'finished' or 'failed' once promise is respectively resolved or rejected
	 * 
	 * @param {Promise} promise - a promise
	 * @return {Promise) a promise
	 */
	run(promise)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			
			setTimeout(function()
			{
				this.state = 'running';
				if(debug)
				{
					console.log('job #' + this.id + ' running');
				}
				
				promise.then(() =>
				{
					if(debug)
					{
						console.log('job #' + this.id + ' finished');
					}
					this.end_date = new Date().toUTCString();
					this.state = 'finished';
					resolve(this);
				}).catch((err) =>
				{
					const { deep_copy } = require('./deep_copy');
					
					if(typeof err === 'string')
					{
						this.err = { message : err };
					}
					else if(typeof err === 'number')
					{
						this.err = { code : err };
					}
					else if(Array.isArray(err))
					{
						this.err = { inner : deep_copy(err) };
					}
					else
					{
						this.err = deep_copy(err);
					}
					
					this.end_date = new Date().toUTCString();
					this.state = 'failed';
					
					if(debug)
					{
						console.log('job #' + this.id + ' failed');
					}
					
					reject(this);
				});
			}.bind(this));
		});
	}
};

module.exports = Job;
