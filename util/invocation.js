/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

/** An invocation */
class Invocation
{
	/** constructor
	 * 
	 * @param {Object} invocation_obj - invocation object
	 */
	constructor(invocation_obj = {})
	{
		Object.assign(this, invocation_obj);
	}
	
	/** Push an invocation result
	 * 
	 * @param {string} path - path to the PKM resource (with URI encoded components)
	 * @param {string} type - type of the resource
	 */
	pushResult(path, type)
	{
		if(this.invocationResults === undefined) this.invocationResults = [];
		let result =
		{
			path : path
		};
		if(type !== undefined) result.type = type;
		this.invocationResults.push(result);
	}
	
	/** Push source code files as invocation results
	 * 
	 * @param {string} dbName - database name
	 * @param {Array.<File>} source_code_files - source code files
	 */
	pushRawSourceCodes(dbName, source_code_files)
	{
		source_code_files.forEach((source_code_file) =>
		{
			this.pushResult('/rawsourcecode/' + encodeURIComponent(dbName) + '/' + encodeURIComponent(source_code_file.rel_path));
		});
	}
	
	/** Push documentation files as invocation results
	 * 
	 * @param {string} dbName - database name
	 * @param {Array.<File>} doc_files - documentation files
	 */
	pushRawDocs(dbName, doc_files)
	{
		doc_files.forEach((doc_file) =>
		{
			this.pushResult('/rawdoc/' + encodeURIComponent(dbName) + '/' + encodeURIComponent(doc_file.rel_path));
		});
	}
	
	/** Push source code artefacts as invocation results
	 * 
	 * @param {string} dbName - database name
	 * @param {string} lang - language (c, cpp, or java)
	 * @param {string} type - type (sourcecode, comments, annotations)
	 * @param {Array.<Object>} source_code_documents - source code documents (either source code AST, comments, or annotations)
	 */
	pushSourceCodes(dbName, lang, type, source_code_documents)
	{
		source_code_documents.forEach((source_code_document) =>
		{
			this.pushResult('/code/' + encodeURIComponent(lang) + '/' + encodeURIComponent(type) + '/' + encodeURIComponent(dbName) + '/' + encodeURIComponent(source_code_document.sourceFile), type);
		});
	}
	
	/** Push ASFM documents
	 * 
	 * @param {string} dbName - database name
	 * @param {Array.<Object>} doc_documents - ASFM documents
	 */
	pushDocs(dbName, doc_documents)
	{
		doc_documents.forEach((doc_document) =>
		{
			this.pushResult('/doc/asfm/artefacts/' + encodeURIComponent(dbName) + '?doc=' + encodeURIComponent(doc_document.name));
		});
	}
	
	/** Push ASFM documents
	 * 
	 * @param {string} dbName - database name
	 * @param {Array.<string>} artefactIds - Log artefact IDs
	 */
	pushLogs(dbName, artefactIds)
	{
		artefactIds.forEach((artefactId) =>
		{
			this.pushResult('/log/' + encodeURIComponent(dbName) + '/' + encodeURIComponent(artefactId), 'log');
		});
	}
	
	/** Set completion time stamp (format is YYYYMMDD_hhmmss, e.g. 20211102_172235 for November 2nd, 2021, at 17'22"35)
	 * 
	 * @param {string} dbName - database name
	 * @param {Date} date - date of completion
	 */
	setTimestampCompleted(date)
	{
		this.timestampCompleted = date.getFullYear().toString().padStart(4, '0') +
		                          (date.getMonth() + 1).toString().padStart(2, '0') +
		                          date.getDate().toString().padStart(2, '0') +
		                          '_' +
		                          date.getHours().toString().padStart(2, '0') +
		                          date.getMinutes().toString().padStart(2, '0') +
		                          date.getSeconds().toString().padStart(2, '0');
	}
}

module.exports = Invocation;
