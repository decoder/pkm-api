/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Deep copy of a variable
	* 
	* @param {*} input - a variable
	* @return {*} a copy of variable
	*/
function deep_copy(input)
{
	if((typeof input !== 'object') || (input === null) || (input === undefined)) return input;
	if(Array.isArray(input))
	{
		let output = [];
		for(var i = 0; i < input.length; ++i)
		{
			output.push(deep_copy(input[i]));
		}
		return output;
	}
	else if(input instanceof Error)
	{
		let output =
		{
			name : input.name,
			message : input.message
		};
		return output;
	}
	else if(input instanceof String)
	{
		return input;
	}
	else
	{
		let output = {};
		Object.keys(input).forEach((key) =>
		{
			output[key] = deep_copy(input[key]);
		});
		return output;
	}
}

exports.deep_copy = deep_copy;
