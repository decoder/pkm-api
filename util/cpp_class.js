/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

class CppClass
{
	constructor(document)
	{
		this.scan(document);
		Object.assign(this, document);
	}
	
	scan(document)
	{
		if(document.hasOwnProperty('kind'))
		{
			switch(document.kind)
			{
				case 'CXXRecordDecl':
					if(document.hasOwnProperty('completeDefinition'))
					{
						this.completeDefinition = document.completeDefinition;
					}
					
					if(document.hasOwnProperty('inner') && Array.isArray(document.inner))
					{
						let fields = [];
						let methods = [];
						document.inner.forEach((inner_element) =>
						{
							if(inner_element.hasOwnProperty('kind') && (!inner_element.hasOwnProperty('isImplicit') || !inner_element.isImplicit))
							{
								switch(inner_element.kind)
								{
									case 'FieldDecl':
										if(inner_element.hasOwnProperty('id') && inner_element.hasOwnProperty('name'))
										{
											fields.push({ id : inner_element.id });
										}
										break;
									case 'CXXConstructorDecl':
									case 'CXXConversionDecl':
									case 'CXXDestructorDecl':
									case 'CXXMethodDecl':
									case 'FunctionDecl':
									case 'FunctionTemplateDecl':
										if(inner_element.hasOwnProperty('id'))
										{
											methods.push({ id : inner_element.id });
										}
										break;
								}
							}
						});
						if(fields.length) this.fields = fields;
						if(methods.length) this.methods = methods;
					}
					break;
				case 'ClassTemplateDecl':
					if(document.hasOwnProperty('inner') && Array.isArray(document.inner))
					{
						let record_decl = document.inner.find((inner_element) => inner_element.hasOwnProperty('kind') && (inner_element.kind === 'CXXRecordDecl'));
						if(record_decl !== undefined)
						{
							this.scan(record_decl);
						}
					}
					break;
			}
		}
	}
}

module.exports = CppClass;
