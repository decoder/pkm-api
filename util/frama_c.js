/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const child_process = require('child_process');
const path = require('path');
const asyncPool = require('tiny-async-pool');
const { deep_copy } = require('./deep_copy');
const File = require('./file');
const fs = require('fs');
const oboe = require('oboe');
const Logger = require('./logger');

/** A class to fix 'pos_path' or 'uri' attributes in a document */
class FramaCDocumentFixer
{
	/** Frama-C document fixer options
	 * 
	 * @typedef {Object} FramaCDocumentFixerOptions
	 * @property {string} [frama_c_share_path] - Frama-C share directory (default: none)
	 * @property {boolean} [debug] - enable/disable debugging messages (default: false)
	 * @property {Logger} [logger] - a logger (default: none)
	 */
	
	/** constructor
	 * 
	 * @param {string} root_directory_path - Root directory path
	 * @param {FramaCDocumentFixerOptions} [options] - options
	 */
	constructor(root_directory_path, options)
	{
		this.root_directory_path = root_directory_path;
		this.options = options;
		this.logger = this.options.hasOwnProperty('logger') ? this.options.logger : new Logger();
		this.unfixed_file_paths = new Set();
		this.system_files_map = new Map();
	}
	
	/** Fix path
	 * 
	 * @param {string} abs_path absolute - path (or URI) to fix
	 * @param {string} attribute - attribute to fix (either 'pos_path' or 'uri')
	 * @return {string} a fixed absolute path
	 */ 
	fix_path(abs_path, attribute)
	{
		const root_directory_prefix = ((attribute === 'uri') ? 'file://' : '') + this.root_directory_path;
		if(abs_path.startsWith(root_directory_prefix))
		{
			return ((attribute === 'uri') ? 'file://' : '') + abs_path.slice(root_directory_prefix.length + 1);
		}
		else if(this.options.frama_c_share_path !== undefined)
		{
			const frama_c_share_prefix = ((attribute === 'uri') ? 'file://' : '') + this.options.frama_c_share_path;
			if(abs_path.startsWith(frama_c_share_prefix))
			{
				const fixed_path = ((attribute === 'uri') ? 'file://' : '') + 'frama-c' + abs_path.slice(frama_c_share_prefix.length);
				if(attribute !== 'uri')
				{
					this.system_files_map.set(abs_path, fixed_path);
				}
				return fixed_path;
			}
		}
		
		if(attribute !== 'uri')
		{
			if(!this.unfixed_file_paths.has(abs_path))
			{
				this.unfixed_file_paths.add(abs_path)
				this.warn('File path \'' + abs_path + '\' can\'t be fixed because it is neither in root directory nor in Frama-C share directory');
			}
		}
		
		return abs_path;
	}
	
	/** Fix array
	 * 
	 * @param {Array.<Object>} document - a document
	 * @param {string} attribute - attribute to fix (either 'pos_path' or 'uri')
	 * @return {Array.<Object>} a clone of document
	 */ 
	fix_array(document, attribute)
	{
		var new_document = [];
		for(var i = 0; i < document.length; ++i)
		{
			new_document.push(this.fix(document[i], attribute));
		}
		return new_document;
	}

	/** Fix object. Every pos_path attributes are fixed in the returned document.
	 * 
	 * @param {Object} document - a document
	 * @param {string} attribute - attribute to fix (either 'pos_path' or 'uri')
	 * @return {Object} a fixed document
	 */ 
	fix_object(document, attribute)
	{
		var new_document = {};
		Object.keys(document).forEach((key) =>
		{
			new_document[key] = ((key === attribute) && (typeof document[key] === 'string')) ? this.fix_path(document[key], attribute) : this.fix(document[key], attribute);
		});
		return new_document;
	}

	/** Fix a document
	 * 
	 * @param {*} document - a document
	 * @param {string} attribute - attribute to fix (either 'pos_path' or 'uri')
	 * @return {*} a fixed document
	 */
	fix(document, attribute)
	{
		if(!(typeof document === 'object') || (document === null)) return document;
		return Array.isArray(document) ? this.fix_array(document, attribute) : this.fix_object(document, attribute);
	}
	
	/** Log a message
	 * 
	 * @param {string} msg - a message
	 */
	log(msg)
	{
		this.logger.log(msg);
	}
	
	/** Log a warning message
	 * 
	 * @param {string} msg - a warning message
	 */
	warn(msg)
	{
		this.logger.warn(msg);
	}
	
	/** Log an error message
	 * 
	 * @param {string} msg - an error message
	 */
	error(msg)
	{
		this.logger.error(msg);
	}
}

/** Frama-C mode
 * 
 * @typedef {Object} FramaCMode
 * @property {boolean} parser - enable/disable parsing
 * @property {boolean} eva - enable/disable EVA
 * @property {boolean} wp - enable/disable wp
 * 
 */

/** Frama-C EVA options
 * 
 * @typedef {Object} FramaCEVAOptions
 * @property {string} main - main function for EVA
 */

/** Frama-C WP options
 * 
 * @typedef {Object} FramaCWPOptions
 * @property {Array<string>} wp_fct - functions for WP
 */

/**
 * Frama-C parser options
 * 
 * @typedef {Object} FramaCOptions
 * @property {boolean} debug - enable/disable debugging messages (default: false)
 * @property {Logger} logger - a logger (default: none)
 * @property {FramaCEVAOptions} eva - EVA options
 * @property {FramaCWPOptions} wp - WP options
 * @property {boolean} includes_system_files - enable/disable inclusion of system files in the result
 */

/** Frama-C parser result
 * 
 * @typedef {Object} FramaCParserResult
 * @property {Array.<Object>} source_code_documents - source code (AST) documents
 * @property {Array.<Object>} annotations_documents - annotations documents
 * @property {Array.<Object>} comments_documents - comments documents
 */

/** Frama-C EVA result
 * 
 * @typedef {Object} FramaCEVAResult
 * @properties {Object} report - an EVA report (SARIF format)
 */

/** Frama-C WP result
 * 
 * @typedef {Object} FramaCWPResult
 * @properties {Object} report - a WP report (SARIF format)
 */

/**
 * Frama-C parser result
 * 
 * @typedef {Object} FramaCResult
 * @property {FramaCParserResult} parser - parser result
 * @property {FramaCEVAResult} eva - EVA result
 * @property {FramaCWPResult} wp - WP result
 */

/** run frama-c with json plugin and fix pos_path fields
 * 
 * @param {FileSystem} file_system - a file system
 * @param {Array.<CompileCommand>} compile_commands - compile commands
 * @param {FramaCMode} mode - mode
 * @param {FramaCOptions} [options] - Frama-C runner options
 * @return {Promise<FramaCResult>} a promise
 */
function frama_c(file_system, compile_commands, mode, options = {})
{
	return new Promise((resolve, reject) =>
	{
		let compile_commands_for_file_saving = compile_commands
			.filter((compile_command) => compile_command.is_for_c_compiler())
			.map((compile_command) => compile_command.for_file_saving(file_system.root_directory));
		
		const debug = (options.debug !== undefined) && options.debug;
		const logger = (options.logger !== undefined) ? options.logger : new Logger();
		const includes_system_files = !!options.includes_system_files;
		
		file_system.writeFile(new File('compile_commands.json', JSON.stringify(compile_commands_for_file_saving), 'utf8', 'application/json')).then(() =>
		{
			// Get Frama-C share path
			new Promise((resolve, reject) =>
			{
				logger.log('Running \'frama-c\' \'-print-share-path\'');
				child_process.execFile('frama-c', [ '-print-share-path' ], (exec_file_err, stdout, stderr) =>
				{
					if(exec_file_err)
					{
						logger.error(JSON.stringify(deep_copy(exec_file_err)));
						reject(exec_file_err);
					}
					else
					{
						const frama_c_share_path = stdout;
						logger.log('Frama-C share path is \'' + frama_c_share_path + '\'');
						resolve(frama_c_share_path);
					}
				});
			}).then((frama_c_share_path) =>
			{
				const working_directory = file_system.root_directory;
				
				// Prepare Frama-C arguments
				
				// starting with kernel
				let args = [ '-json-compilation-database', path.join(working_directory, 'compile_commands.json'), '-keep-comments' ];
				args = args.concat(compile_commands_for_file_saving.map((compile_command_for_file_saving) => compile_command_for_file_saving.file));
				
				if(options.args !== undefined)
				{
					args = args.concat(options.args);
				}
				
				// and breaking down the mode into steps
				let steps = [];
				if(mode.parser) steps.push('json');
				if(mode.eva) steps.push('eva');
				if(mode.wp) steps.push('wp');
					//				if(mode.eva || mode.wp) steps.push('mdr-gen');
				
				for(let step_num = 0; step_num < steps.length; ++step_num)
				{
					if(step_num != 0)
					{
						args = args.concat([ '-then' ]);
					}
					const step = steps[step_num];
					switch(step)
					{
						case 'json':
							args = args.concat([ '-json', '-json-out', 'result.json' ]);
							break;
						case 'eva':
							args = args.concat([ '-eva' ]);
							if((options.eva !== undefined) && (options.eva.main !== undefined))
							{
								args = args.concat(['-main', options.eva.main ]);
							}
							break;
						case 'wp':
							args = args.concat([ '-wp' ]);
							if((options.eva !== undefined) && (options.eva.main !== undefined))
							{
								args = args.concat(['-wp-fct', options.wp_fct.join(',') ]);
							}
							break;
						case 'mdr-gen':
							args = args.concat([ ]);
//							args = args.concat([ '-mdr-gen', 'sarif', '-mdr-out', 'report.json' ]);
							break;
					}
				}
				
				const exec_file_options =
				{
					cwd : working_directory,
					maxBuffer : 512 * 1024 * 1024 // 512 MiB
				};
				
				// Call frama-c
				logger.log('From Directory \'' + working_directory + '\', running \'frama-c\' ' + args.map(arg => '\'' + arg + '\'').join(' '));
				child_process.execFile('frama-c', args, exec_file_options, (exec_file_err, stdout, stderr) =>
				{
					if(exec_file_err)
					{
						logger.error(JSON.stringify(deep_copy(exec_file_err)));
						
						if(stdout)
						{
							logger.log(stdout);
						}
						if(stderr)
						{
							logger.warn(stderr);
						}
						reject(exec_file_err);
					}
					else
					{
						let frama_c_result = {};
						
						if(stdout)
						{
							logger.log(stdout);
						}
						if(stderr)
						{
							logger.warn(stderr);
						}
						
						const frama_c_document_fixer = new FramaCDocumentFixer(file_system.root_directory, { frama_c_share_path: frama_c_share_path, debug: debug, logger: logger });
						
						let promises = [];
						
						if(steps.includes('json'))
						{
							frama_c_result.parser = {};
							
							promises.push(new Promise((resolve, reject) =>
							{
								// Fix paths in output JSON files
								var fix_file_promises = [];
								[ 'result.json', 'annot_result.json', 'comments_result.json' ].forEach((result_filename) =>
								{
									fix_file_promises.push(new Promise((resolve, reject) =>
									{
										const result_abs_path = path.join(working_directory, result_filename);
										
										file_system.stat(result_abs_path).then((stats) =>
										{
											if(!stats.size)
											{
												// file is empty
												logger.warn('\'' + result_abs_path + '\' is empty');
												resolve();
											}
											else
											{
												oboe(fs.createReadStream(result_abs_path)).done((document) =>
												{
													const new_document = frama_c_document_fixer.fix(document, 'pos_path');
													
													const system_files = new Set(frama_c_document_fixer.system_files_map.values());
													
													switch(result_filename)
													{
														case 'result.json':
															const code = new_document;
															
															function get_filename(global)
															{
																if(global.GType) return global.GType.loc.pos_start.pos_path;
																if(global.GCompTag) return global.GCompTag.loc.pos_start.pos_path;
																if(global.GCompTagDecl) return global.GCompTagDecl.loc.pos_start.pos_path;
																if(global.GEnumTag) return global.GEnumTag.loc.pos_start.pos_path;
																if(global.GEnumTagDecl) return global.GEnumTagDecl.loc.pos_start.pos_path;
																if(global.GVarDecl) return global.GVarDecl.loc.pos_start.pos_path;
																if(global.GFunDecl) return global.GFunDecl.loc.pos_start.pos_path;
																if(global.GVar) return global.GVar.loc.pos_start.pos_path;
																if(global.GFun) return global.GFun.loc.pos_start.pos_path;
																if(global.GAsm) return global.GAsm.loc.pos_start.pos_path;
																if(global.GPragma) return global.GPragma.loc.pos_start.pos_path;
																if(global.GText) return global.GText.loc.pos_start.pos_path;
																if(global.GAnnot) return global.GAnnot.loc.pos_start.pos_path;
																return null;
															}
															
															let code_map = new Map();
															code.globals.forEach((global) =>
															{
																let filename = get_filename(global);
																if(filename && (includes_system_files || !system_files.has(filename)))
																{
																	if(code_map.has(filename))
																	{
																		let tmp = code_map.get(filename); 
																		tmp.globals.push(global);
																		code_map.set(filename, tmp);
																	}
																	else
																	{
																		let tmp =
																		{
																			sourceFile : filename,
																			globals : [ global ],
																			globinit : { option : "None" },
																			globinitcalled : false
																		};
																		code_map.set(filename, tmp);
																	}
																}
															});
															
															frama_c_result.parser.source_code_documents = Array.from(code_map.values());
															break;
														case 'annot_result.json':
															const annotations = new_document.annotations;
															
															let annotations_map = new Map();
															annotations.forEach((annotation) =>
															{
																let filename = annotation.loc.pos_start.pos_path;
																if(filename && (includes_system_files || !system_files.has(filename)))
																{
																	if(annotations_map.has(filename))
																	{
																		let tmp = annotations_map.get(filename); 
																		tmp.annotations.push(annotation);
																		annotations_map.set(filename, tmp);
																	}
																	else
																	{
																		let tmp =
																		{
																			sourceFile : filename,
																			annotations : [ annotation ]
																		};
																		annotations_map.set(filename, tmp);
																	}
																}
															});
															
															frama_c_result.parser.annotations_documents = Array.from(annotations_map.values());
															break;
														case 'comments_result.json':
															const comments = new_document.comments;
															
															let comments_map = new Map();
															comments.forEach((comment) =>
															{
																let filename = comment.loc.pos_start.pos_path;
																if(filename && (includes_system_files || !system_files.has(filename)))
																{
																	if(comments_map.has(filename))
																	{
																		let tmp = comments_map.get(filename); 
																		tmp.comments.push(comment);
																		comments_map.set(filename, tmp);
																	}
																	else
																	{
																		let tmp =
																		{
																			sourceFile : filename,
																			comments : [ comment ]
																		};
																		comments_map.set(filename, tmp);
																	}
																}
															});
															
															frama_c_result.parser.comments_documents = Array.from(comments_map.values());
															break;
													}
													
													resolve();
												}).fail((oboe_err) =>
												{
													logger.error('In File \'' + result_filename + '\', ' + JSON.stringify(deep_copy(oboe_err)));
													reject(oboe_err);
												});
											}
										}).catch((stat_err) =>
										{
											logger.error('Error with File \'' + result_filename + '\', ' + JSON.stringify(deep_copy(stat_err)));
											reject(stat_err);
										});
									}));
								});
								
								Promise.all(fix_file_promises).then(() =>
								{
									resolve();
								}).catch((err) =>
								{
									reject(err);
								});
							}));
						}
						
						// if(steps.includes('mdr-gen'))
						// {
						// 	promises.push(new Promise((resolve, reject) =>
						// 	{
						// 		const report_abs_path = path.join(working_directory, 'report.json');
						// 		oboe(fs.createReadStream(report_abs_path)).done((document) =>
						// 		{
						// 			const new_document = frama_c_document_fixer.fix(document, 'uri');
						// 			frama_c_result.report = new_document;
									
						// 			resolve();
						// 		}).fail((oboe_err) =>
						// 		{
						// 			logger.errors('In File \'report.json\', ' + JSON.stringify(deep_copy(oboe_err)));
						// 			reject(oboe_err);
						// 		});
						// 	}));
						// }
						
						Promise.all(promises).then(() =>
						{
							if(includes_system_files)
							{
								let read_system_file_promises = [];
								
								frama_c_document_fixer.system_files_map.forEach((rel_path, abs_host_file_path) =>
								{
									read_system_file_promises.push(new Promise((resolve, reject) =>
									{
										fs.readFile(abs_host_file_path, { encoding : 'utf8', flag : 'r' }, (read_file_err, host_file_content) =>
										{
											if(read_file_err)
											{
												const msg = 'Can\'t read File \'' + abs_host_file_path + '\', ' + JSON.stringify(deep_copy(read_file_err));
												reject(new Error(msg));
											}
											else
											{
												if(frama_c_result.system_files === undefined) frama_c_result.system_files = [];
												const system_file = new File(
													rel_path,          // rel_path
													host_file_content, // content
												);
												frama_c_result.system_files.push(system_file);
												
												resolve();
											}
										});
									}));
								});
								
								Promise.all(read_system_file_promises).then(() =>
								{
									resolve(frama_c_result);
								}).catch((err) =>
								{
									reject(err);
								});
							}
							else
							{
								resolve(frama_c_result);
							}
						}).catch((err) =>
						{
							reject(err);
						});
					}
				});
			}).catch((err) =>
			{
				reject(err);
			});
		}).catch((err) =>
		{
			reject(err);
		});
	});
}

exports.frama_c = frama_c;
