/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Convert a command line to an array of string arguments
 * 
 * @param {string} command - a command line string
 * @return {Array.<string>} an array of string arguments
 */
function command2arguments(command)
{
	let arg;
	let args = [];
	let state = 0;
	for(var i = 0; i < command.length; ++i)
	{
		const c = command[i];
		
		switch(state)
		{
			case 0:
				if(c == '\\')
				{
					state = 10;
					break;
				}
				if((c == ' ') || (c == '\t'))
				{
					if(arg === undefined)
					{
						break;
					}
					
					args.push(arg);
					arg = undefined;
					break;
				}
				if(c == '\'')
				{
					state = 1;
					break;
				}
				if(c == '"')
				{
					state = 2;
					break;
				}
				
				arg = (arg === undefined) ? c : (arg + c);
				break;
			case 1: // single quote
				if(c == '\\')
				{
					state = 11;
					break;
				}
				else if(c == '\'')
				{
					state = 0;
					break;
				}
				else
				{
					arg = (arg === undefined) ? c : (arg + c);
				}
				break;
			
			case 2:
				if(c == '\\')
				{
					state = 12;
					break;
				}
				else if(c == '"')
				{
					state = 0;
					break;
				}
				else
				{
					arg = (arg === undefined) ? c : (arg + c);
				}
				break;
				
			case 10: // escape
				arg = (arg === undefined) ? c : (arg + c);
				state = 1;
				break;
				
			case 11: // escape in quoted string
				if(c == '\'')
				{
					arg = (arg === undefined) ? '\\' : (arg + '\\');
					state = 0;
					break;
				}
				else
				{
					arg = (arg === undefined) ? c : (arg + c);
					state = 1;
				}
				break;
				
			case 12: // escape in doublequoted string
				arg = (arg === undefined) ? c : (arg + c);
				state = 2;
				break;
		}
	}

	if(arg !== undefined)
	{
		args.push(arg);
	}
	
	return args;
}

exports.command2arguments = command2arguments;
