/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const fs = require('fs');
const docx = require('docx');
const xml2js = require('xml2js');
const unzipper = require('unzipper');
const stream = require('stream');
const { deep_copy } = require('./deep_copy');
const Logger = require('./logger');

/** asfm_to_doc options
 * 
 * @typedef {Object} AsfmToDocOptions
 * @property {boolean} debug - enable/disable debugging messages (default: false)
 * @property {Logger} logger - a logger (default: none)
 * @property {string} config_xml_path - path to config.xml (doc_to_asfm configuration file)
 * @property {string} asfm_dotx_path - path to asfm.dotx (MS Word template file)
 */

/** Convert an ASFM object to a Buffer with .docx format
 * 
 * @param {Object} asfm_obj - an ASFM object
 * @param {AsfmToDocOptions} [options] - options
 * @return {Promise.<Buffer>}
 */
function asfm_to_doc(asfm_obj, options = {})
{
	return new Promise((resolve, reject) =>
	{
		const debug = (options.debug !== undefined) && options.debug;
		const logger = (options.logger !== undefined) ? options.logger : new Logger();
		
		// read config.xml
		new Promise((resolve, reject) =>
		{
			const config_xml_path = (options.config_xml_path !== undefined) ? options.config_xml_path : (__dirname + '/../doc/config.xml');
			
			if(debug)
			{
				logger.log('Using \'' + config_xml_path + '\' configuration file');
			}
			
			const config_xml_content = fs.readFileSync(config_xml_path, 'utf8');
			
			const find_element = (obj, key) =>
			{
				if(typeof obj === 'object')
				{
					if(Array.isArray(obj))
					{
						for(let i = 0; i < obj.length; ++i)
						{
							const found = find_element(obj[i], key);
							if(found !== undefined) return found;
						}
					}
					else
					{
						const obj_keys = Object.keys(obj);
						for(let i = 0; i < obj_keys.length; ++i)
						{
							const obj_key = obj_keys[i];
							if(obj_key === key) return obj[obj_key];
							
							const found = find_element(obj[obj_key], key);
							if(found !== undefined) return found;
						}
					}
				}
				
				return undefined;
			}

			xml2js.parseString(config_xml_content, (err, config) =>
			{
				let styles = {};
				
				if(config.doc_to_json !== undefined)
				{
					[ 'application', 'unit', 'class', 'types', 'fields', 'macros', 'methods' ].forEach((key) =>
					{
						if(config.doc_to_json[key] !== undefined)
						{
							const style = find_element(config.doc_to_json[key], 'styleSection');
							styles[key] = style;
						}
					});
				}
				
				resolve(styles);
			});
		}).then((styles) =>
		{
			// get word/styles.xml from dotx template (asfm.dotx)
			new Promise((resolve, reject) =>
			{
				let word_styles_xml_content = '';
				
				const write = (chunk, encoding, callback) =>
				{
					if(Buffer.isBuffer(chunk))
					{
						word_styles_xml_content += chunk.toString('utf8');
					}
					else if(typeof chunk === 'string')
					{
						word_styles_xml_content += chunk;
					}
					callback();
				}
				
				const final = () =>
				{
					if(word_styles_xml_content.length)
					{
						resolve(word_styles_xml_content);
					}
					else
					{
						const msg = 'no word/styles.xml found';
						logger.error(msg);
						reject(new Error(msg));
					}
				}
				
				const asfm_dotx_path = (options.asfm_dotx_path !== undefined) ? options.asfm_dotx_path : (__dirname + '/../doc/asfm.dotx');
				
				if(debug)
				{
					logger.log('Using \'' + asfm_dotx_path + '\' MS Word template file');
				}
				
				const rs = fs.createReadStream(asfm_dotx_path);
				rs.on('error', (err) =>
				{
					const msg = 'Error with File \'' + asfm_dotx_path + '\', ' + JSON.stringify(deep_copy(err));
					logger.error(msg);
					reject(new Error(err));
				});
				
				rs.pipe(unzipper.Parse())
					.on('entry', (entry) =>
					{
						const filename = entry.path;
						const type = entry.type;
						if((type === 'File') && (filename === 'word/styles.xml'))
						{
							entry.pipe(new stream.Writable({ write : write, final : final}));
						}
						else
						{
							entry.autodrain().on('error', (err) =>
							{
								reject(err);
							});
						}
					}).on('error', (err) =>
					{
						logger.error(JSON.stringify(deep_copy(err)));
						reject(err);
					});
			}).then((word_styles_xml_content) =>
			{
				// build a docx document
				const doc = new docx.Document(
				{
					title: asfm_obj.name,
					externalStyles: word_styles_xml_content
				});
			
				let section =
				{
					children: []
				};
				
				const paragraph = new docx.Paragraph({ text: asfm_obj.name, style: styles.application });
				section.children.push(paragraph);
				
				if(Array.isArray(asfm_obj.units))
				{
					asfm_obj.units.forEach((unit) =>
					{
						const paragraph = new docx.Paragraph({ text: unit.name, style: styles.unit });
						section.children.push(paragraph);
						
						if(Array.isArray(unit.classes))
						{
							unit.classes.forEach((cls) =>
							{
								const class_name_paragraph = new docx.Paragraph({ text: cls.name, style: styles.class });
								section.children.push(class_name_paragraph);
								
								if(cls.hasOwnProperty('doc'))
								{
									cls.doc.split('\n').forEach((text) =>
									{
										const class_doc_paragraph = new docx.Paragraph({ text: text });
										section.children.push(class_doc_paragraph);
									});
								}
								
								[ 'types', 'fields', 'macros', 'methods' ].forEach((key) =>
								{
									const members = cls[key];
									if(Array.isArray(members) && (styles[key] !== undefined))
									{
										members.forEach((member) =>
										{
											const member_name_paragraph = new docx.Paragraph({ text: member.name, style: styles[key] });
											section.children.push(member_name_paragraph);
											if(member.hasOwnProperty('doc'))
											{
												member.doc.split('\n').forEach((text) =>
												{
													const member_doc_paragraph = new docx.Paragraph({ text: text });
													section.children.push(member_doc_paragraph);
												});
											}
										});
									}
								});
							});
						}
					});
				}
			
				doc.addSection(section);
				
				// Note: toBuffer is bugged probably because of jszip not exporting a true node.js Buffer instance
				docx.Packer.toBase64String(doc).then((base64_str) =>
				{
					// serve the docx document in a buffer
					resolve(Buffer.from(base64_str, 'base64'));
				}).catch((err) =>
				{
					logger.error(JSON.stringify(deep_copy(err)));
					reject(err);
				});
			}).catch((err) =>
			{
				reject(err);
			});
		}).catch((err) =>
		{
			reject(err);
		});
	});
}

exports.asfm_to_doc = asfm_to_doc;
