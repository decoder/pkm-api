/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Read a password
 * 
 * @param {string} [prompt] - a prompt
 */
function read_password(prompt)
{
	return new Promise((resolve, reject) =>
	{
		let password = ''
		
		if(process.stdin.setRawMode)
		{
			// stdin is a TTY
			process.stdout.write(prompt ? prompt : 'password:');
			process.stdin.setRawMode(true);
			process.stdin.setEncoding('utf-8');
			process.stdin.resume();
			
			const process_data_fn = (c) =>
			{
				if(((c >= ' ') && (c <= '~')) || (c >= '\u0080')) // printable UTF-8 characters
				{
					password += c;
				}
				else if((c == '\r') ||     // carriage return
								(c == '\n') ||     // line feed
								(c == '\u0004'))   // ctrl-d
				{
					process.stdout.write('\r\n'); // go to the beginning of next line
					process.stdin.removeListener('data', process_data_fn);
					process.stdin.removeListener('error', process_error_fn);
					process.stdin.setRawMode(false);
					process.stdin.pause();
					resolve(password); // call resolve callback
				}
				else if((c.charCodeAt(0) === 127) && (password.length != 0)) // backspace
				{
					password = password.slice(0, password.length - 1); // remove last character
				}
				else if(c == '\u0003')      // ctrl-c
				{
					process.stdin.removeListener('data', process_data_fn);
					process.stdin.removeListener('error', process_error_fn);
					process.stdin.setRawMode(false);
					process.stdin.pause();
					reject('password required');
				}
			};
			const process_error_fn = (error) =>
			{
				process.stdin.removeListener('data', process_data_fn);
				process.stdin.removeListener('error', process_error_fn);
				reject(error); // call reject callback
			};
			
			process.stdin.on(
				'data',
				process_data_fn
			).on(
				'error',
				process_error_fn
			);
		}
		else
		{
			// stdin is a pipe
			process.stdin.setEncoding('utf-8');
			process.stdin.resume();
			
			const process_data_fn = (chunk) =>
			{
				let linefeed_index = chunk.indexOf('\n');
				let got_password = (linefeed_index >= 0);
				password += (linefeed_index >= 0) ? chunk.substr(0, linefeed_index) : chunk;
				if(linefeed_index >= 0)
				{
					process.stdin.removeListener('data', process_data_fn)
					process.stdin.removeListener('error', process_error_fn)
					process.stdin.pause();
					if((linefeed_index + 1) < chunk.length)
					{
						var remainder = chunk.substr(linefeed_index + 1);
						process.stdin.unshift(remainder);
					}
					resolve(password);
				}
			};
			
			const process_error_fn = (error) =>
			{
				process.stdin.removeListener('data', process_data_fn)
				process.stdin.removeListener('error', process_error_fn)
				process.stdin.pause();
				reject(error);
			};
			
			process.stdin.on(
				'data',
				process_data_fn
			).on(
				'error',
				process_error_fn
			);
		}
	});
}

exports.read_password = read_password;
