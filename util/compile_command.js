/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const path = require('path');
const { command2arguments } = require('./command2arguments');

class CompileCommand
{
	/** constructor
	 * 
	 * @param {string} directory - build directory, i.e. working directory relative to the virtual root directory of a project
	 * @param {string} file - path relative to the virtual root directory of a project
	 * @param {string} command - command line; preprocessor include directives are relative to "directory" property
	 * @param {Array.<string>} _arguments - command line argument; preprocessor include directives are relative to "directory" property
	 * @param {string} output - output
	 */
	constructor(directory, file, command, _arguments, output)
	{
		this.directory = directory;
		this.file = file;
// 		if(command !== undefined) this.command = command;
		if(_arguments !== undefined)
		{
			this.arguments = _arguments;
		}
		else if(command !== undefined)
		{
			this.arguments = command2arguments(command);
		}
		if(output !== undefined) this.output = output;
	}
	
	/** Test whether file is for a C compiler
	 * 
	 * @return {boolean} the test result
	 */
	is_for_c_compiler()
	{
		return this.file.endsWith('.c');
	}
	
	/** Test whether file is for a C++ compiler
	 * 
	 * @return {boolean} the test result
	 */
	is_for_c_plus_plus_compiler()
	{
		return this.file.endsWith('.cc') ||
		       this.file.endsWith('.cp') ||
		       this.file.endsWith('.cxx') ||
		       this.file.endsWith('.cpp') ||
		       this.file.endsWith('.CPP') ||
		       this.file.endsWith('.c++') ||
		       this.file.endsWith('.C');
	}
	
	/** Convert compile command for file saving
	 * 
	 * @param {string} root_directory - root directory
	 * @return {CompileCommand} a command command ready for saving in a file
	 */
	for_file_saving(root_directory)
	{
		return CompileCommand.from(this, root_directory);
	}
	
	/** Build a CompileCommand instance from an object that looks like a compile command
	 * 
	 * @param {Object} obj - an object
	 * @param {string} [root_directory] - root directory used to replace @ROOT@ in command and arguments
	 * @return {CompileCommand} an instance of CompileCommand
	 */
	static from(obj, root_directory)
	{
		return new CompileCommand(
			obj.directory,
			obj.file,
			((obj.command !== undefined) && (root_directory !== undefined) && (root_directory !== '@ROOT@')) ? obj.command.replace(/@ROOT@/g, root_directory) : obj.command,
			((obj.arguments !== undefined) && (root_directory !== undefined) && (root_directory !== '@ROOT@')) ? obj.arguments.map(argument => argument.replace(/@ROOT@/g, root_directory)) : obj.arguments,
			obj.output
		);
	}
};

module.exports = CompileCommand;
