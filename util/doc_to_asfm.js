/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const child_process = require('child_process');
const path = require('path');
const { deep_copy } = require('./deep_copy');
const Logger = require('./logger');

/** 
 * ASFM Document Id tagger
 */
class AsfmDocumentIdTagger
{
	/** constructor */
	constructor()
	{
		this.next_id = 0;
	}
	
	/**
	 * Recursively scan, copy and tag an ASFM document
	 * 
	 * @param {Object} document - an ASFM document
	 * @param {boolean} tag - a flag to enable/disable Id tagging
	 * @param {Number} [parent_id] - Id of parent
	 */
	scan(document, tag, parent_id)
	{
		if((typeof document !== 'object') || (document === null) || (document === undefined)) return document;
		
		if(Array.isArray(document))
		{
			let new_array = [];
			for(let i = 0; i < document.length; ++i)
			{
				const new_element = this.scan(document[i], true, parent_id);
				if(new_element !== undefined)
				{
					new_array.push(new_element);
				}
			}
			
			return new_array;
		}
		else
		{
			let new_document = {};
			
			if(tag)
			{
				new_document.id = this.next_id++;
			}
			
			if(parent_id !== undefined)
			{
				new_document.parent = parent_id;
			}
			
			Object.keys(document).forEach((key) =>
			{
				new_document[key] = this.scan(document[key], true, new_document.id);
			});
			return new_document;
		}
	}
	
	/**
	 * Copy and tag an ASFM document
	 * 
	 * @param {Object} document - an ASFM document
	 */
	tag(document)
	{
		return this.scan(document, false);
	}
}

/**
 * doc_to_asfm options
 * 
 * @typedef {Object} DocToAsfmOptions
 * @property {boolean} debug - enable/disable debugging messages (default: false)
 * @property {Logger} logger - a logger (default: none)
 */

/** Run doc_to_asfm
 * 
 * @param {FileSystem} file_system - a file system
 * @param {string} file_path - a file path
 * @param {DocToAsfmOptions} [options] - options
 * 
 * @return {Promise.<Object>} a promise
 */
function doc_to_asfm(file_system, file_path, options = {})
{
	return new Promise((resolve, reject) =>
	{
		const debug = (options.debug !== undefined) && options.debug;
		const logger = (options.logger !== undefined) ? options.logger : new Logger();
		const working_directory = file_system.root_directory;
		const input_abs_path = path.join(working_directory, file_path);
		const output_abs_path = path.join(working_directory, file_path + '.doc_to_asm.json');

		new Promise((resolve, reject) =>
		{
			const exec_path = 'doc_to_asfm';
			
			const args =
			[
				'-c',
				path.resolve(__dirname + '/../doc/config.xml'),
				input_abs_path,
				'-o',
				output_abs_path
			];
			
			const exec_file_options =
			{
				cwd : working_directory
			};
			
			logger.log('From Directory \'' + working_directory + '\', running \'' + exec_path + '\' ' + args.map(arg => '\'' + arg + '\'').join(' '));
			child_process.execFile(exec_path, args, (exec_file_err, stdout, stderr) =>
			{
				if(exec_file_err)
				{
					logger.error(JSON.stringify(deep_copy(exec_file_err)));
					if(stdout)
					{
						logger.log(stdout);
					}
					if(stderr)
					{
						logger.warn(stderr);
					}
					reject(exec_file_err);
				}
				else
				{
					if(stdout)
					{
						logger.log(stdout);
					}
					if(stderr)
					{
						logger.warn(stderr);
					}
					resolve();
				}
			});
		}).then(() =>
		{
			file_system.readFile(output_abs_path).then((output_file) =>
			{
				file_system.unlink(output_abs_path).then(() =>
				{
					let document;
					try
					{
						document = JSON.parse(output_file.content);
					}
					catch(err)
					{
						logger.error('In File \'' + output_abs_path + '\', ' + JSON.stringify(deep_copy(err)));
						reject(err);
						return;
					}
					
					let asfm_id_tagger = new AsfmDocumentIdTagger();
					resolve(asfm_id_tagger.tag(document));
				}).catch((unlink_err) =>
				{
					logger.error('Error unlinking File \'' + output_abs_path + '\', ' + JSON.stringify(deep_copy(unlink_err)));
					reject(unlink_err);
				});
			}).catch((read_file_err) =>
			{
				logger.error('Error reading File \'' + output_abs_path + '\', ' + JSON.stringify(deep_copy(read_file_err)));
				reject(read_file_err);
			});
		}).catch((err) =>
		{
			reject(err);
		});
	});
}

exports.doc_to_asfm = doc_to_asfm;
