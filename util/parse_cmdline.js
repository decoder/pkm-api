/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** Parse command line
 * 
 * @param {string[]} option_names - valid option names
 * @return {Promise} a promise
 */
function parse_cmdline(option_names)
{
	return new Promise((resolve, reject) =>
	{
		var cmdline =
		{
			options : {},
			args : []
		};
		var end_of_options = false;
		for(i = 2; i < process.argv.length; ++i)
		{
			var arg = process.argv[i];
			
			if(end_of_options)
			{
				cmdline.args.push(arg);
			}
			else if(arg == '--')
			{
				end_of_options = true;
			}
			else if(arg.substring(0, 2) != '--')
			{
				end_of_options = true;
				cmdline.args.push(arg);
			}
			else
			{
				var option = arg.substring(2);
				var option_name;
				var option_value;
				var is_flag = false;
				var delimiter_pos = option.indexOf('=');
				if(delimiter_pos < 0)
				{
					is_flag = true;
					delimiter_pos = option.indexOf('-');
					if(delimiter_pos < 0)
					{
						option_name = option;
						option_value = true;
					}
					else
					{
						if(option.substring(0, delimiter_pos) == 'enable')
						{
							option_name = option.substring(delimiter_pos + 1);
							option_value = true;
						}
						else if(option.substring(0, delimiter_pos) == 'disable')
						{
							option_name = option.substring(delimiter_pos + 1);
							option_value = false;
						}
						else
						{
							option_name = option;
							option_value = true;
						}
					}
				}
				else
				{
					option_name = option.substring(0, delimiter_pos);
					option_value = option.substring(delimiter_pos + 1);
				}
				
				var unknown_option = true;
				for(var j = 0; j < option_names.length; ++j)
				{
					const option_is_array = option_names[j].endsWith('*');
					const option_is_flag = option_names[j].endsWith('!');
					const match = ((option_is_array || option_is_flag) ? option_names[j].slice(0, option_names[j].length - 1) : option_names[j]) == option_name;
					
					if(match)
					{
						if(option_is_array)
						{
							if(cmdline.options[option_name] === undefined)
							{
								// first assignment
								cmdline.options[option_name] = new Array(option_value);
							}
							else
							{
								cmdline.options[option_name].push(option_value);
							}
						}
						else if(option_is_flag)
						{
							if((option_value == 'true') || (option_value === true))
							{
								cmdline.options[option_name] = true;
							}
							else if((option_value == 'false') || (option_value === false))
							{
								cmdline.options[option_name] = false;
							}
							else if(option_value == '1')
							{
								cmdline.options[option_name] = 1;
							}
							else if(option_value == '0')
							{
								cmdline.options[option_name] = 0;
							}
						}
						else
						{
							cmdline.options[option_name] = option_value;
						}
						unknown_option = false;
					}
				}
				if(unknown_option)
				{
					var err = "Unknown option '" + option_name + "'";
					reject(err);
					return;
				}
			}
		}
		
		resolve(cmdline);
	});
}

exports.parse_cmdline = parse_cmdline;
