/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

/** A logger that not only prints log messages on the console but also keeps messages so that they can be later inspected
 */
class Logger
{
	/** constructor
	 */
	constructor()
	{
		this.messages = [];
		this.warnings = [];
		this.errors = [];
	}
	
	/** Log a message
	 * 
	 * @param {string} msg - a message
	 */
	log(msg)
	{
		msg.toString().split('\n').forEach((msg) =>
		{
			this.messages.push(msg);
			console.log(msg);
		});
	}
	
	/** Log a warning message
	 * 
	 * @param {string} msg - a message
	 */
	warn(msg)
	{
		msg.toString().split('\n').forEach((msg) =>
		{
			const text = 'WARNING! ' + msg;
			this.warnings.push(text);
			console.warn(text);
		});
	}
	
	/** Log an error message
	 * 
	 * @param {string} msg - a message
	 */
	error(msg)
	{
		msg.toString().split('\n').forEach((msg) =>
		{
			const text = 'ERROR! ' + msg;
			this.errors.push(text);
			console.error(text);
		});
	}
}

module.exports = Logger;
