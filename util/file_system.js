/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const File = require('./file');
const { deep_copy } = require('./deep_copy');

/**
 * File system options
 * 
 * @typedef {Object} FileSystemOptions
 * @property {string} encoding - default file encoding ('utf8')
 * @property {boolean} debug - enable/disable debugging messages
 * @property {Logger} logger - a logger (default: none)
 * @property {number} write_mode - default write mode
 * @property {string} tmp_dir - temporary directory (default: return value of calling 'os.tmpdir()')
 */

/**
 * Read file options
 * 
 * @typedef {Object} ReadFileOptions
 * @property {string} encoding - file encoding (e.g. 'utf8')
 * @property {string} mime_type - file MIME type
 */

/**
 * Write file options
 * 
 * @typedef {Object} WriteFileOptions
 * @property {number} write_mode - write mode
 */

class FileSystem
{
	static msg_prefix = '[FileSystem] ';
	
	/** constructor
	 * 
	 * synopsis:
	 *   new FileSystem();
	 *   new FileSystem('/path/to/directory');
	 *   new FileSystem({ encoding : 'utf8' });
	 *   new FileSystem('/path/to/directory', { encoding : 'utf8' });
	 * 
	 * @param {string} [root_directory] - the root directory on the host file system
	 * @param {FileSystemOptions} [options] - options
	 * 
	 */
	constructor(root_directory, options)
	{
		if(options === undefined)
		{
			if(typeof root_directory === 'object')
			{
				options = root_directory;
				root_directory = undefined;
			}
		}
		
		if(root_directory !== undefined)
		{
			const path = require('path');
			this.root_directory = path.resolve(root_directory);
		}
		this.options = (options !== undefined) ? options
		                                       : {
		                                           encoding : 'utf8',
		                                           write_mode : (fs.constants.S_IRUSR | fs.constants.S_IWUSR |  // read-write for user
		                                                         fs.constants.S_IRGRP | fs.constants.S_IWGRP | // read-write for group
		                                                         fs.constants.S_IROTH | fs.constants.S_IWOTH) // read-write for others (Note: umask will narrow access rights)
		                                         };
	}
	
	/** Get the absolute path (on the host file system) from a file relative path
	 * 
	 * @param {string} rel_path - a file relative path
	 * @return {string} the absolute path of the file on the host file system
	 */
	get_abs_path(rel_path)
	{
		const path = require('path');
		return path.normalize(path.join(this.root_directory, rel_path));
	}
	
	/** Make an absolute path
	 * 
	 * @param {string} _path - a path either relative to root directory or absolute
	 * @return {string} an absolute path
	 */
	make_abs_path(_path)
	{
		const path = require('path');
		
		if(path.isAbsolute(_path)) return path.normalize(_path);
		
		return path.join(this.root_directory, path.normalize(_path));
	}
	
	/** Make a relative path
	 * 
	 * @param {string} _path - an absolute path
	 * @return {string} a path relative to root directory
	 */
	make_rel_path(abs_path)
	{
		const path = require('path');
		
		if(!path.isAbsolute(abs_path))
		{
			throw new Error(abs_path + ' is not an absolute path');
		}
		
		return path.relative(this.root_directory, abs_path);
	}
	
	/** Check that absolute file path is not referencing something outside of root directory
	 * 
	 * @param {string} abs_host_file_path - an absolute file path on the host
	 * @return {boolean} 
	 */
	check_abs_file_path(abs_host_file_path)
	{
		const path = require('path');
		const parsed_host_file_path = path.parse(abs_host_file_path);
		
		// check that parent directory is not outside root directory:
		// 1. check that parent directory is on the same root as root directory (e.g. c:\ vs d:\)
		// 2. check that relative path from root directory to parent directory do not start with '..'
		const parsed_root_directory = path.parse(this.root_directory);
		const relative = path.relative(this.root_directory, path.join(parsed_host_file_path.root, parsed_host_file_path.dir));
		return ((parsed_host_file_path.root == parsed_root_directory.root) && !relative.startsWith('..'))
	}
	
	/** Check that absolute directory path is not referencing something outside of root directory
	 * 
	 * @param {string} abs_host_dir_path - an absolute directory path on the host
	 * @return {boolean} 
	 */
	check_abs_directory_path(abs_host_dir_path)
	{
		const path = require('path');
		const parsed_host_file_path = path.parse(abs_host_dir_path);
		
		// check that directory is not outside root directory:
		// 1. check that directory is on the same root as root directory (e.g. c:\ vs d:\)
		// 2. check that directory path from root directory to the directory do not start with '..'
		const parsed_root_directory = path.parse(this.root_directory);
		const relative = path.relative(this.root_directory, abs_host_dir_path);
		return ((parsed_host_file_path.root == parsed_root_directory.root) && !relative.startsWith('..'))
	}
	
	/** read file
	 * 
	 * @param {string} host_file_path - a file path on the host (either relative to root directory or absolute)
	 * @param {ReadFileOptions} options - reading file options
	 * @return {Promise<File>} a promise
	 */
	readFile(host_file_path, options)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			const os = require('os');
			const fs = require('fs');
			const path = require('path');
			
			const abs_host_file_path = this.make_abs_path(host_file_path);
			if(!this.check_abs_file_path(abs_host_file_path))
			{
				const msg = '\'' + abs_host_file_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			
			fs.stat(abs_host_file_path, (stat_err, stats) =>
			{
				if(stat_err)
				{
					const msg = 'Error with File \'' + abs_host_file_path + '\':' + JSON.stringify(deep_copy(stat_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else if(!stats.isFile() && !stats.isSymbolicLink())
				{
					const msg = abs_host_file_path + ' is neither a regular file nor a symbolic link';
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					const read_file_options =
					{
						encoding : ((options === undefined) || (options.encoding === undefined)) ? this.options.encoding : options.encoding,
						flag : 'r' // read
					};
					
// 					if(debug)
// 					{
// 						this.log('Reading File \'' + abs_host_file_path + '\' (' + (read_file_options.encoding ? (read_file_options.encoding + ' encoding'): 'raw') + ')');
// 					}
					fs.readFile(abs_host_file_path, read_file_options, (read_file_err, host_file_content) =>
					{
						if(read_file_err)
						{
							const msg = 'Can\'t read File \'' + abs_host_file_path + '\', ' + JSON.stringify(deep_copy(read_file_err));
							this.error(msg);
							reject(new Error(msg));
						}
						else
						{
							if(debug) this.log('Read File \'' + abs_host_file_path + '\'')
							
							var file_rel_path = path.relative(this.root_directory, abs_host_file_path);
							if(os.platform() == 'win32') file_rel_path = file_rel_path.replace(/\\/g, '/');

							// create the intermediate representation of the file
							const file = new File(
								file_rel_path,                                              // rel_path
								host_file_content,                                          // content
								read_file_options.encoding,                                 // encoding
								(options && options.type) ? options.type : null,            // type
								(options && options.mime_type) ? options.mime_type : null
							);
								
							// resolve the promise: the file
							resolve(file);
						}
					});
				}
			});
		});
	}

	/** write file
	 * 
	 * @param {File} file - a file
	 * @param {WriteFileOptions} [options] - writing file options
	 * @return {Promise} a promise
	 */
	writeFile(file, options)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			const fs = require('fs');
			const path = require('path');
			const abs_host_file_path = file.get_abs_path(this.root_directory);
			if(!this.check_abs_file_path(abs_host_file_path))
			{
				const msg = '\'' + abs_host_file_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			
			if(file.content === undefined)
			{
				let err_msg = 'Can\'t write File \'' + abs_host_file_path + '\' because it has no content defined';
				this.error(''+ err_msg);
				reject(new Error(err_msg));
				return;
			}
			
			const parsed_abs_host_file_path = path.parse(abs_host_file_path);
			const abs_host_parent_directory_path = path.join(parsed_abs_host_file_path.root, parsed_abs_host_file_path.dir);
			
			if(debug)
			{
// 				this.log('Creating Directory \'' + abs_host_parent_directory_path + '\'');
			}
			fs.mkdir(abs_host_parent_directory_path, { recursive : true }, (mkdir_err) =>
			{
				if(mkdir_err)
				{
					const msg = 'Error with Directory \'' + abs_host_parent_directory_path + '\': ' + JSON.stringify(deep_copy(mkdir_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					if(debug)
					{
						this.log('Created Directory \'' + abs_host_parent_directory_path + '\'');
					}
					 
					const write_file_options =
					{
						encoding : (file.encoding === undefined) ? this.options.encoding : file.encoding,
						mode : (((options === undefined) || (options.write_mode === undefined)) ? this.options.write_mode
						                                                                        : options.write_mode), 
						flag : 'w' // write, truncate
					};
					
// 					if(debug)
// 					{
// 						this.log('Writing File \'' + abs_host_file_path + '\' (' + (Buffer.isBuffer(file.content) ? 'raw' : (write_file_options.encoding + ' encoding')) + ')');
// 					}
					fs.writeFile(abs_host_file_path, file.content, write_file_options, (write_file_err) =>
					{
						if(write_file_err)
						{
							const msg = 'Can\'t write File \'' + abs_host_file_path + '\': ' + JSON.stringify(deep_copy(write_file_err));
							this.error(msg);
							reject(new Error(msg));
						}
						else
						{
							if(debug)
							{
								this.log('Wrote File \'' + abs_host_file_path + '\'');
							}
							
							resolve();
						}
					});
				}
			});
		});
	}
	
	/** Stat
	 * 
	 * @param {string} host_file_path - a file path on the host (either relative to root directory or absolute)
	 * @param {Object} options - stat options
	 * @return {Promise} a promise
	 */
	stat(host_file_path, options)
	{
		return new Promise((resolve, reject) =>
		{
			const path = require('path');
			const fs = require('fs');
			
			const abs_host_file_path = this.make_abs_path(host_file_path);
			if(!this.check_abs_file_path(abs_host_file_path))
			{
				const msg = '\'' + abs_host_file_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			fs.stat(abs_host_file_path, options, (stat_err, stats) =>
			{
				if(stat_err)
				{
					const msg = 'Error with File \'' + abs_host_file_path + '\': ' + JSON.stringify(deep_copy(stat_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					resolve(stats);
				}
			});
		});
	}
	
	/** Access
	 * 
	 * @param {string} host_file_path - a file path on the host (either relative to root directory or absolute)
	 * @param {Object} options - access options
	 * @return {Promise} a promise
	 */
	access(host_file_path, options)
	{
		return new Promise((resolve, reject) =>
		{
			const path = require('path');
			const fs = require('fs');
			
			const abs_host_file_path = this.make_abs_path(host_file_path);
			if(!this.check_abs_file_path(abs_host_file_path))
			{
				const msg = '\'' + abs_host_file_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			fs.access(abs_host_file_path, ((options !== undefined) && (options.mode !== undefined)) ? options.mode : fs.constants.F_OK, (access_err) =>
			{
				if(access_err)
				{
					const msg = 'Error with File \'' + abs_host_file_path + '\': ' + JSON.stringify(deep_copy(access_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					resolve();
				}
			});
		});
	}
	
	/** Unlink
	 * 
	 * @param {string} host_file_path - a file path on the host (either relative to root directory or absolute)
	 * @return {Promise} a promise
	 */
	unlink(host_file_path)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			const path = require('path');
			const fs = require('fs');
			
			const abs_host_file_path = this.make_abs_path(host_file_path);
			if(!this.check_abs_file_path(abs_host_file_path))
			{
				const msg = '\'' + abs_host_file_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
// 			if(debug)
// 			{
// 				this.log('Deleting \'' + abs_host_file_path + '\'');
// 			}
			fs.unlink(abs_host_file_path, (unlink_err) =>
			{
				if(unlink_err)
				{
					const msg = 'Error unlinking File \'' + abs_host_file_path + '\': ' + JSON.stringify(deep_copy(unlink_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					if(debug)
					{
						this.log('Deleted \'' + abs_host_file_path + '\'');
					}
					resolve();
				}
			});
		});
	}
	
	/** Make directory recursively
	 * 
	 * @param {string} host_dir_path - a directory path on the host (either relative to root directory or absolute)
	 * @return {Promise} a promise
	 */
	mkdir_p(host_dir_path)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			const path = require('path');
			const fs = require('fs');
			
			const abs_host_dir_path = this.make_abs_path(host_dir_path);
			if(!this.check_abs_directory_path(abs_host_dir_path))
			{
				const msg = '\'' + abs_host_dir_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			
// 			if(debug) this.log('Creating Directory \'' + abs_host_dir_path + '\'');
											 
			fs.mkdir(abs_host_dir_path, { recursive : true }, (mkdir_err) =>
			{
				if(mkdir_err)
				{
					const msg = 'Error creating Directory \'' + abs_host_dir_path + '\': ' + JSON.stringify(deep_copy(mkdir_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					if(debug) this.log('Created Directory \'' + abs_host_dir_path + '\'');
					resolve();
				}
			});
		});
	}
	
	/** Remove directory recursively
	 * 
	 * @param {string} host_dir_path - a directory path on the host (either relative to root directory or absolute)
	 * @return {Promise} a promise
	 */
	rmdir_r(host_dir_path)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			const path = require('path');
			const fs = require('fs');
			
			const abs_host_dir_path = this.make_abs_path(host_dir_path);
			if(!this.check_abs_directory_path(abs_host_dir_path))
			{
				const msg = '\'' + abs_host_dir_path + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			
			// recursively delete directory on the host file system
// 			if(debug) this.log('Deleting Directory \'' + abs_host_dir_path + '\'');
			fs.rmdir(abs_host_dir_path, { maxRetries : 5, recursive : true }, (rmdir_err) =>
			{
				if(rmdir_err)
				{
					const msg = 'Error removing Directory \'' + abs_host_dir_path + '\': ' + JSON.stringify(deep_copy(rmdir_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					if(debug) this.log('Deleted Directory \'' + abs_host_dir_path + '\'');
					resolve();
				}
			});
		});
	}
	
	/** Make root directory
	 * 
	 * @return {Promise} a promise
	 */
	make_root_directory()
	{
		return new Promise((resolve, reject) =>
		{
			const fs = require('fs');
			
			const debug = this.options.debug;
			
			if(this.root_directory !== undefined)
			{
				if(!this.is_temporary)
				{
					// create a directory on the host file system
// 					if(debug) this.log('Creating root directory');
					fs.mkdir(this.root_directory, { recursive : true }, (mkdir_err) =>
					{
						if(mkdir_err)
						{
							const msg = 'Error creating Directory \'' + this.root_directory + '\': ' + JSON.stringify(deep_copy(mkdir_err));
							this.error(msg);
							reject(new Error(msg));
						}
						else
						{
							this.is_temporary = false;
							if(debug) this.log('Created root Directory \'' + this.root_directory + '\'');
							resolve();
						}
					});
				}
				else
				{
					const msg = 'Can\'t create root directory because it has been already created as temporary';
					this.error(msg);
					reject(new Error(msg));
				}
			}
			else
			{
				const msg = 'Root directory is undefined';
				this.error(msg);
				reject(new Error(msg));
			}
		});
	}
	
	/** Remove root directory
	 * 
	 * @return {Promise} a promise
	 */
	remove_root_directory()
	{
		return new Promise((resolve, reject) =>
		{
			const fs = require('fs');
			
			const debug = this.options.debug;
			
			if(this.is_temporary)
			{
				const msg = 'Can\'t remove root directory because file system has a temporary root directory';
				this.error(msg);
				reject(new Error(msg));
			}
			
			if(this.root_directory === undefined)
			{
				const msg = 'Can\'t remove root directory because it is undefined';
				this.error(msg);
				reject(new Error(msg));
			}
			
			// recursively delete root directory on the host file system
// 			if(debug) this.log('Deleting root Directory \'' + this.root_directory + '\'');
			fs.rmdir(this.root_directory, { maxRetries : 5, recursive : true }, (rmdir_err) =>
			{
				if(rmdir_err)
				{
					const msg = 'Error removing Directory \'' + this.root_directory + '\': ' + JSON.stringify(deep_copy(rmdir_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					if(debug) this.log('Deleted root Directory \'' + this.root_directory + '\'');
					this.is_temporary = false;
					this.root_directory = undefined;
					resolve();
				}
			});
		});
	}
	
	/** Make temporary root directory
	 * 
	 * @return {Promise} a promise
	 */
	make_temporary_root_directory()
	{
		return new Promise((resolve, reject) =>
		{
			const fs = require('fs');
			const path = require('path');
			const os = require('os');
			
			const debug = this.options.debug;
			
			if(this.root_directory !== undefined)
			{
				const msg = 'Can\'t create a temporary root directory because file system has a already a permanent root directory';
				this.error(msg);
				reject(new Error(msg));
			}
			
			// create a temporary directory on the host file system
// 			if(debug) this.log('Creating temporary root directory');
			const tmp_dir = this.options.tmp_dir || os.tmpdir();
			fs.mkdtemp(path.join(tmp_dir, 'pkm-'), (mkdtemp_err, temporary_root_directory) =>
			{
				if(mkdtemp_err)
				{
					const msg = 'Error creating temporary directory: ' + JSON.stringify(deep_copy(mkdtemp_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					this.root_directory = temporary_root_directory;
					this.is_temporary = true;
					if(debug) this.log('Created temporary root Directory \'' + this.root_directory + '\'');
					
					resolve();
				}
			});
		});
	}
	
	/** Remove temporary root directory
	 * 
	 * @return {Promise} a promise
	 */
	remove_temporary_root_directory()
	{
		return new Promise((resolve, reject) =>
		{
			if(!this.is_temporary)
			{
				const msg = 'can\'t remove Directory \'' + this.root_directory + '\' because it is not a temporary root directory';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			
			const fs = require('fs');
			
			const debug = this.options.debug;
			// recursively delete root directory on the host file system
// 			if(debug) this.log('Deleting temporary root Directory \'' + this.root_directory + '\'');
			fs.rmdir(this.root_directory, { maxRetries : 5, recursive : true }, (rmdir_err) =>
			{
				if(rmdir_err)
				{
					const msg = 'Error removing Directory \'' + this.root_directory + '\': ' + JSON.stringify(deep_copy(rmdir_err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					if(debug) this.log('Deleted temporary root Directory \'' + this.root_directory + '\'');
					this.is_temporary = false;
					this.root_directory = undefined;
					resolve();
				}
			});
		});
	}
	
	/** Make absolute path relative to root directory
	 * 
	 * @param {string} abs_path absolute - path to fix
	 * @return {string} a fixed path
	 */ 
	fix_path(abs_path)
	{
		if(abs_path.startsWith(this.root_directory))
		{
			return abs_path.slice(this.root_directory.length + 1);
		}
		
		return abs_path;
	}
	
	/** Find options
	 * 
	 * @typedef {Object} FindOptions
	 * @property {RegExp} filename_regexp - filename regular expression
	 * @property {RegExp} dirname_regexp - directory name regular expression
	 * @property {Array<string>} exclude_dirpaths - directory paths to exclude
	 * @property {('file'|'directory')} type - type
	 */
	
	/** Find according to a path regular expression
	 * 
	 * @param {string} starting_point - starting point (directory name)
	 * @param {FindOptions} [options] - find options
	 * @return {Promise.Array<string>} a promise
	 */
	find(starting_point, options)
	{
		return new Promise((resolve, reject) =>
		{
			const debug = this.options.debug;
			const os = require('os');
			const fs = require('fs');
			const path = require('path');
			
			const abs_starting_point = this.make_abs_path(starting_point);
			if(!this.check_abs_directory_path(abs_starting_point))
			{
				const msg = '\'' + abs_starting_point + '\' is referencing something outside of root directory \'' + this.root_directory + '\'';
				this.error(msg);
				reject(new Error(msg));
				return;
			}
			
			fs.readdir(abs_starting_point, { withFileTypes : true }, (err, dirents) =>
			{
				if(err)
				{
					const msg = 'Error reading Directory \'' + abs_starting_point + '\': ' + JSON.stringify(deep_copy(err));
					this.error(msg);
					reject(new Error(msg));
				}
				else
				{
					let paths = [];
					let subdir_promises = [];
					
					dirents.forEach((dirent) =>
					{
						const name = dirent.name.toString();
						if(dirent.isFile())
						{
							if((options === undefined) ||
							   (((options.type === undefined) ||
							   (options.type == 'file')) &&
							   ((options.filename_regexp === undefined) ||
							   options.filename_regexp.test(name))))
							{
								paths.push(path.join(abs_starting_point, name));
							}
						}
						else if(dirent.isDirectory())
						{
              const dirpath = path.join(abs_starting_point, name);
							if((options === undefined) ||
							   (options.exclude_dirpaths === undefined) ||
							   !options.exclude_dirpaths.includes(dirpath))
							{
								if((options === undefined) ||
								   (((options.dirname_regexp === undefined) || options.dirname_regexp.test(name)) &&
								   ((options.type === undefined) || (options.type == 'directory'))))
								{
									paths.push(path.join(abs_starting_point, name));
								}

								subdir_promises.push(this.find(path.join(abs_starting_point, name), options));
							}
						}
					});
					
					Promise.all(subdir_promises).then((subdir_paths) =>
					{
						subdir_paths.forEach((_paths) => _paths.forEach((path) => paths.push(path)));
						resolve(paths);
					}).catch((err) =>
					{
						reject(err);
					});
				}
			});
		});
	}
	
	log(msg)
	{
		if(this.options.logger !== undefined)
		{
			this.options.logger.log(FileSystem.msg_prefix + msg);
		}
		else
		{
			console.log(FileSystem.msg_prefix + msg);
		}
	}

	warn(msg)
	{
		if(this.options.logger !== undefined)
		{
			this.options.logger.warn(FileSystem.msg_prefix + msg);
		}
		else
		{
			console.warn(FileSystem.msg_prefix + msg);
		}
	}

	error(msg)
	{
		if(this.options.logger !== undefined)
		{
			this.options.logger.error(FileSystem.msg_prefix + msg);
		}
		else
		{
			console.error(FileSystem.msg_prefix + msg);
		}
	}
};

module.exports = FileSystem;
