/*
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const child_process = require('child_process');
const path = require('path');
const asyncPool = require('tiny-async-pool');
const { deep_copy } = require('./deep_copy');
const File = require('./file');
const Logger = require('./logger');

/**
 * Excavator options
 * 
 * @typedef {Object} ExcavatorOptions
 * @property {boolean} debug - enable/disable debugging messages (default: false)
 * @property {Logger} logger - a logger (default: none)
 * @property {Array.<string>} sources - the list of compilation units to process; if not specified or empty, it means all compilation units
 * @property {Array.<string>} suppress_types - the list of types to suppress while analysis, e.g. types to be considered as compiler builtins
 * @property {Array.<string>} suppress_functions - the list of functions to suppress while analysis, e.g. functions to be considered as compiler builtins
 * @property {string} output_dir - the output directory of the analysis (default: binary + '_out'); the result of the analysis is a reversed engineered C source codes
 */

/**
 * Excavator result
 * 
 * @typedef {Object} ExcavatorResult
 * @property {Array.<string>} c_source_code_filenames - list of generated C source codes filenames
 */

/** Run Excavator
 * 
 * @param {FileSystem} file_system - a file system
 * @param {string} binary - executable binary filename to process
 * @param {ExcavatorOptions} [options] - options
 * 
 * @return {Promise.<ExcavatorResult>} a promise
 */
function excavator(file_system, binary, options = {})
{
	return new Promise((resolve, reject) =>
	{
		const debug = (options.debug !== undefined) && options.debug;
		const logger = (options.logger !== undefined) ? options.logger : new Logger();
		const working_directory = file_system.root_directory;
		const binary_abs_path = path.join(working_directory, binary);
		let output_dir = ((options.output_dir !== undefined) && options.output_dir && (options.output_dir !== binary)) ? options.output_dir : (binary + '_out');
		
		// create Excavator JSON configuration file
		new Promise((resolve, reject) =>
		{
			let excavator_config =
			{
				binary : binary,
				verbose: (debug ? 2 : 1)
			};
			if(options.sources !== undefined) excavator_config.sources = options.sources;
			if(options.suppress_types !== undefined) excavator_config.suppress_types = options.suppress_types;
			if(options.suppress_functions !== undefined) excavator_config.suppress_functions = options.suppress_functions;
			excavator_config.output_dir = output_dir;
			
			let config_file = new File('excavator_config.json', JSON.stringify(excavator_config), 'utf8', 'application/json');
			file_system.writeFile(config_file).then(() =>
			{
				resolve();
			}).catch((write_excavator_config_file_err) =>
			{
				const msg = 'Error writing File \'' + config_file.rel_path + '\', ' + JSON.stringify(deep_copy(write_excavator_config_file_err));
				logger.error(msg);
				reject(new Error(msg));
			});
		}).then(() => new Promise((resolve, reject) =>
		{
			// run Excavator
			const exec_path = 'unisim-excavator-0.2.0';
			
			const args =
			[
				'excavator_config.json'
			];
			
			const exec_file_options =
			{
				cwd : working_directory
			};
			
			logger.log('From Directory \'' + working_directory + '\', running \'' + exec_path + '\' ' + args.map(arg => '\'' + arg + '\'').join(' '));
			child_process.execFile(exec_path, args, exec_file_options, (exec_file_err, stdout, stderr) =>
			{
				if(exec_file_err)
				{
					logger.error(JSON.stringify(deep_copy(exec_file_err)));
					
					if(stdout)
					{
						logger.log(stdout);
					}
					if(stderr)
					{
						logger.warn(stderr);
					}
					reject(exec_file_err);
				}
				else
				{
					file_system.find(output_dir, { type : 'file', filename_regexp : new RegExp('\.c$') }).then((c_source_code_filenames) =>
					{
						let excavator_result =
						{
							c_source_code_filenames : c_source_code_filenames
						};
						if(stdout)
						{
							logger.log(stdout);
						}
						if(stderr)
						{
							logger.warn(stderr);
						}
						resolve(excavator_result);
					}).catch((err) =>
					{
						reject(err);
					});
				}
			});
		})).then((excavator_result) =>
		{
			resolve(excavator_result);
		}).catch((err) =>
		{
			reject(err);
		});
	});
}

exports.excavator = excavator;
