#!/bin/bash

echo "PKM start script."

# make sure pkm-api:pkm-api owns everything in /pkm-api/git-root (possibly a docker volume)
chown -R pkm-api:pkm-api /pkm-api/git-root

VOLUME_CONFIG=/pkm-api/volume_config
[ -f "${VOLUME_CONFIG}/pkm_config.json" ] && \
    cp -f "${VOLUME_CONFIG}/pkm_config.json" /pkm-api
[ -f "${VOLUME_CONFIG}/secret" ] && \
    cp -f "${VOLUME_CONFIG}/secret" /pkm-api
  
# initialize PKM creating PKM users database and PKM administrator admin@users
SUPERUSER=$(jq -e .superuser /run/secrets/credentials.json)
if [ $? -ne 0 ] || [ "${SUPERUSER}" = 'null' ]; then
	SUPERUSER='admin'
else
	SUPERUSER=$(echo -n "${SUPERUSER}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
fi
SUPERUSER_AUTH_DB=$(jq -e .superuser_auth_db /run/secrets/credentials.json)
if [ $? -ne 0 ] || [ "${SUPERUSER_AUTH_DB}" = 'null' ]; then
	SUPERUSER_AUTH_DB='admin'
else
	SUPERUSER_AUTH_DB=$(echo -n "${SUPERUSER_AUTH_DB}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
fi
SUPERUSER_PASSWORD=$(jq -e .superuser_password /run/secrets/credentials.json)
if [ $? -ne 0 ] || [ "${SUPERUSER_PASSWORD}" = 'null' ]; then
	SUPERUSER_PASSWORD='admin'
else
	SUPERUSER_PASSWORD=$(echo -n "${SUPERUSER_PASSWORD}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
fi
PKM_ADMIN=$(jq -e .pkm_admin /run/secrets/credentials.json)
if [ $? -ne 0 ] || [ "${SUPERUSER}" = 'null' ]; then
	PKM_ADMIN='admin'
else
	PKM_ADMIN=$(echo -n "${PKM_ADMIN}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
fi
PKM_ADMIN_PASSWORD=$(jq -e .pkm_admin_password /run/secrets/credentials.json)
if [ $? -ne 0 ] || [ "${PKM_ADMIN_PASSWORD}" = 'null' ]; then
	PKM_ADMIN_PASSWORD='admin'
else
	PKM_ADMIN_PASSWORD=$(echo -n "${PKM_ADMIN_PASSWORD}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
fi

while true; do
	if echo -e "${SUPERUSER_PASSWORD}\n${PKM_ADMIN_PASSWORD}" | su -c "/pkm-api/bin/initialize_PKM --superuser=\"${SUPERUSER}\" --superuser-auth-db=\"${SUPERUSER_AUTH_DB}\" --admin=${PKM_ADMIN}" pkm-api; then
		echo "PKM initialized"
		break
	fi
	echo "PKM initialization failed, retrying later..."
	sleep 2
done

export NODE_PATH="/pkm-api:$NODE_PATH"


runuser -u pkm-api -- /pkm-api/server/start_server.sh
