#!/bin/bash

# make sure pkm-api:pkm-api owns everything in /pkm-api/mongodb/data and /pkm-api/mongodb/log (possibly docker volumes)
chown -R pkm-api:pkm-api /pkm-api/mongodb/data
chown -R pkm-api:pkm-api /pkm-api/mongodb/log

if [ ! -e '/pkm-api/mongodb/data/initialized' ]; then
	echo "service not yet initialized"
	
	# start mongodb server (authentication security is disabled by default on fresh installation)
	echo "starting mongod"
	mongod --dbpath /pkm-api/mongodb/data --logpath /pkm-api/mongodb/log/mongod.log --logappend --noauth --config /etc/mongod.conf --bind_ip 0.0.0.0 &
	MONGOD_PID=$!

	# create admin@admin
	SUPERUSER=$(jq -e .superuser /run/secrets/credentials.json)
	if [ $? -ne 0 ] || [ "${SUPERUSER}" = 'null' ]; then
		SUPERUSER='admin'
	else
		SUPERUSER=$(echo -n "${SUPERUSER}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
	fi
	SUPERUSER_AUTH_DB=$(jq -e .superuser_auth_db /run/secrets/credentials.json)
	if [ $? -ne 0 ] || [ "${SUPERUSER_AUTH_DB}" = 'null' ]; then
		SUPERUSER_AUTH_DB='admin'
	else
		SUPERUSER_AUTH_DB=$(echo -n "${SUPERUSER_AUTH_DB}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
	fi
	SUPERUSER_PASSWORD=$(jq -e .superuser_password /run/secrets/credentials.json)
	if [ $? -ne 0 ] || [ "${SUPERUSER_PASSWORD}" = 'null' ]; then
		SUPERUSER_PASSWORD='admin'
	else
		SUPERUSER_PASSWORD=$(echo -n "${SUPERUSER_PASSWORD}" | sed -n 's/^"\([^"]*\)"$/\1/p' | sed -e 's/"/\\"/g')
	fi
	
	echo "creating ${SUPERUSER}@${SUPERUSER_AUTH_DB}"
	TRY=5
	while true; do
		sleep 2
		mongo << EOF
use ${SUPERUSER_AUTH_DB}
db.createUser(
 {
   user : "${SUPERUSER}",
   pwd : "${SUPERUSER_PASSWORD}",
   roles :
   [
     { role : "root", db : "admin" }
   ]
 }
)
EOF
		if [ $? -eq 0 ]; then
			echo "${SUPERUSER}@${SUPERUSER_AUTH_DB} has been created"
			# stop mongodb
			echo "stopping mongod"
			kill $MONGOD_PID
			wait
		
			echo "service has been successfully initialized"
			touch /pkm-api/mongodb/data/initialized
			chown -R pkm-api:pkm-api /pkm-api/mongodb
			break
		else
			echo "mongod is not accessible or has not yet started"
			if [ $TRY -ne 0 ]; then
				echo "retrying later..."
				sleep 2
				TRY=$(($TRY-1))
			else
				echo "creating superuser failed"
				echo "stoping mongod"
				kill $MONGOD_PID
				wait
				echo "service initialization failed"
				exit 1
			fi
		fi
	done
else
	echo "service has already been initialized"
fi

# start mongodb server listening any clients 
echo "starting mongod"
runuser -u pkm-api -- mongod --dbpath /pkm-api/mongodb/data --logpath /pkm-api/mongodb/log/mongod.log --logappend --auth --config /etc/mongod.conf --bind_ip 0.0.0.0
