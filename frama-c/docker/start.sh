#!/bin/bash

echo "Frama-C for PKM start script."

VOLUME_CONFIG=/pkm-api/volume_config
[ -f "${VOLUME_CONFIG}/frama-c/server_config.json" ] && \
    cp -f "${VOLUME_CONFIG}/frama-c/server_config.json" /pkm-api/frama-c && \
    cp -f "${VOLUME_CONFIG}/frama-c/server_config.json" /pkm-api/frama-c/server
  
runuser -u frama-c -- /pkm-api/frama-c/server/start_server.sh
