openapi: 3.0.0
info:
  contact:
    email: decoder@decoder-project.eu
    name: DECODER project
    url: https://www.decoder-project.eu
  description: API of Frama-C for PKM
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  title: FramaCRestfulApiForPkm
  version: 1.0.0
servers:
- url: http://pkm-api_frama-c_1:8081
paths:
  /frama_c/{dbName}:
    post:
      description: Runs Frama-C static program analyzer
      operationId: frama_c
      parameters:
      - description: The project name from where to get source code/header files,
          the compile commands, and put the parsing results (Source code ASTs, Comments,
          and Annotations) and/or the analysis results
        explode: false
        in: path
        name: dbName
        required: true
        schema:
          type: string
        style: simple
      - description: flag to control asynchronous/synchronous execution of Frama-C
          job
        explode: true
        in: query
        name: asynchronous
        required: false
        schema:
          default: false
          type: boolean
        style: form
      - description: invocation identifier (optional)
        explode: true
        in: query
        name: invocationID
        required: false
        schema:
          type: string
        style: form
      - description: Access key to the PKM
        explode: false
        in: header
        name: key
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        $ref: '#/components/requestBodies/inline_object'
        content:
          application/json:
            schema:
              properties:
                source_file_paths:
                  description: Source Code filenames to process
                  items:
                    type: string
                  type: array
                mode:
                  $ref: '#/components/schemas/_frama_c__dbName__mode'
                options:
                  $ref: '#/components/schemas/_frama_c__dbName__options'
              required:
              - source_file_paths
              type: object
        required: true
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
          description: Created, returns a job
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "400":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Bad request
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "401":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Unauthorized operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "403":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Forbidden operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "404":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Not found, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "500":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Internal Server Error, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
      tags:
      - FramaC
      x-codegen-request-body-name: body
      x-eov-operation-handler: controllers/FramaCController
  /frama_c/jobs/{jobId}:
    get:
      description: Get Frama-C Job. Getting a finished or failed job, unpublish it
        (it is no longer available)
      operationId: getJob
      parameters:
      - description: Job identifier
        explode: false
        in: path
        name: jobId
        required: true
        schema:
          minimum: 0
          type: integer
        style: simple
      - description: Access key to the PKM
        explode: false
        in: header
        name: key
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
          description: Successful operation, returns a job
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "400":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Bad request
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "401":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Unauthorized operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "403":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Forbidden operation, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "404":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Not found, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
        "500":
          content:
            application/json:
              schema:
                description: error message
                type: string
          description: Internal Server Error, returns an error message
          headers:
            Access-Control-Allow-Origin:
              $ref: '#/components/headers/Access-Control-Allow-Origin'
            Access-Control-Allow-Methods:
              $ref: '#/components/headers/Access-Control-Allow-Methods'
            Access-Control-Allow-Headers:
              $ref: '#/components/headers/Access-Control-Allow-Headers'
      tags:
      - FramaC
      x-eov-operation-handler: controllers/FramaCController
components:
  headers:
    Access-Control-Allow-Origin:
      explode: false
      schema:
        default: '*'
        type: string
      style: simple
    Access-Control-Allow-Methods:
      explode: false
      schema:
        default: GET, POST, PUT, DELETE, OPTIONS
        type: string
      style: simple
    Access-Control-Allow-Headers:
      explode: false
      schema:
        default: Origin, Content-Type, Accept, key
        type: string
      style: simple
  parameters:
    keyParam:
      description: Access key to the PKM
      explode: false
      in: header
      name: key
      required: true
      schema:
        type: string
      style: simple
    InvocationIdParam:
      description: invocation identifier (optional)
      explode: true
      in: query
      name: invocationID
      required: false
      schema:
        type: string
      style: form
  requestBodies:
    inline_object:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/inline_object'
      required: true
  schemas:
    Job:
      description: a job
      example:
        end_date: end_date
        err: '{}'
        service_name: service_name
        warnings: warnings
        id: 0
        state: pending
        parameters:
          dbName: dbName
        logs: logs
        start_date: start_date
      properties:
        id:
          description: job identifier
          minimum: 0
          type: integer
        service_name:
          description: name of the service running the job
          type: string
        parameters:
          $ref: '#/components/schemas/Job_parameters'
        state:
          description: job state
          enum:
          - pending
          - running
          - failed
          - finished
          type: string
        start_date:
          description: date (GMT) when job started
          type: string
        end_date:
          description: date (GMT) when job ended
          type: string
        logs:
          description: log messages
          type: string
        warnings:
          description: warning messages
          type: string
        err:
          description: error
          type: object
      type: object
    _frama_c__dbName__mode:
      description: The operating modes
      properties:
        parser:
          description: Enable/disable parser (implicitely enabled when neither EVA
            nor WP are enabled)
          type: boolean
        eva:
          description: Enable/disable EVA (Evolved Value Analysis) plug-in
          type: boolean
        wp:
          description: Enable/disable WP (Weakest Precondition calculus) plug-in
          type: boolean
      type: object
    _frama_c__dbName__options_eva:
      description: EVA options
      properties:
        main:
          description: main function for EVA
          type: string
      type: object
    _frama_c__dbName__options_wp:
      description: WP options
      properties:
        wp_fct:
          description: functions for WP
          items:
            description: function name
            type: string
          type: array
      type: object
    _frama_c__dbName__options:
      description: options
      properties:
        args:
          description: Frama-C command line options
          items:
            type: string
          type: array
        eva:
          $ref: '#/components/schemas/_frama_c__dbName__options_eva'
        wp:
          $ref: '#/components/schemas/_frama_c__dbName__options_wp'
        includes_system_files:
          description: Enable/Disable inclusion of system files (i.e. system headers)
            in the result
          type: boolean
      type: object
    inline_object:
      properties:
        source_file_paths:
          description: Source Code filenames to process
          items:
            type: string
          type: array
        mode:
          $ref: '#/components/schemas/_frama_c__dbName__mode'
        options:
          $ref: '#/components/schemas/_frama_c__dbName__options'
      required:
      - source_file_paths
      type: object
    Job_parameters:
      description: parameters of the job
      example:
        dbName: dbName
      properties:
        dbName:
          description: database name
          type: string
      type: object
