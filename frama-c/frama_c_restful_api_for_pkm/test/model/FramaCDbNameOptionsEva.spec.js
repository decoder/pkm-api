/**
 * FramaCRestfulApiForPkm
 * API of Frama-C for PKM
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.FramaCRestfulApiForPkm);
  }
}(this, function(expect, FramaCRestfulApiForPkm) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new FramaCRestfulApiForPkm.FramaCDbNameOptionsEva();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('FramaCDbNameOptionsEva', function() {
    it('should create an instance of FramaCDbNameOptionsEva', function() {
      // uncomment below and update the code to test FramaCDbNameOptionsEva
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptionsEva();
      //expect(instance).to.be.a(FramaCRestfulApiForPkm.FramaCDbNameOptionsEva);
    });

    it('should have the property main (base name: "main")', function() {
      // uncomment below and update the code to test the property main
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptionsEva();
      //expect(instance).to.be();
    });

  });

}));
