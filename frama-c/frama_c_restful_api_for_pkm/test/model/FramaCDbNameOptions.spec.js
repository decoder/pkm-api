/**
 * FramaCRestfulApiForPkm
 * API of Frama-C for PKM
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.FramaCRestfulApiForPkm);
  }
}(this, function(expect, FramaCRestfulApiForPkm) {
  'use strict';

  var instance;

  beforeEach(function() {
    // create a new instance
    //instance = new FramaCRestfulApiForPkm.FramaCDbNameOptions();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('FramaCDbNameOptions', function() {
    it('should create an instance of FramaCDbNameOptions', function() {
      // uncomment below and update the code to test FramaCDbNameOptions
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptions();
      //expect(instance).to.be.a(FramaCRestfulApiForPkm.FramaCDbNameOptions);
    });

    it('should have the property args (base name: "args")', function() {
      // uncomment below and update the code to test the property args
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptions();
      //expect(instance).to.be();
    });

    it('should have the property eva (base name: "eva")', function() {
      // uncomment below and update the code to test the property eva
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptions();
      //expect(instance).to.be();
    });

    it('should have the property wp (base name: "wp")', function() {
      // uncomment below and update the code to test the property wp
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptions();
      //expect(instance).to.be();
    });

    it('should have the property includes_system_files (base name: "includes_system_files")', function() {
      // uncomment below and update the code to test the property includes_system_files
      //var instance = new FramaCRestfulApiForPkm.FramaCDbNameOptions();
      //expect(instance).to.be();
    });

  });

}));
