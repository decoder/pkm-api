/**
 * FramaCRestfulApiForPkm
 * API of Frama-C for PKM
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/InlineObject', 'model/Job'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/InlineObject'), require('../model/Job'));
  } else {
    // Browser globals (root is window)
    if (!root.FramaCRestfulApiForPkm) {
      root.FramaCRestfulApiForPkm = {};
    }
    root.FramaCRestfulApiForPkm.FramaCApi = factory(root.FramaCRestfulApiForPkm.ApiClient, root.FramaCRestfulApiForPkm.InlineObject, root.FramaCRestfulApiForPkm.Job);
  }
}(this, function(ApiClient, InlineObject, Job) {
  'use strict';

  /**
   * FramaC service.
   * @module api/FramaCApi
   * @version 1.0.0
   */

  /**
   * Constructs a new FramaCApi. 
   * @alias module:api/FramaCApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Runs Frama-C static program analyzer
     * @param {String} dbName The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations) and/or the analysis results
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of Frama-C job (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Job} and HTTP response
     */
    this.framaCWithHttpInfo = function(dbName, key, body, opts) {
      opts = opts || {};
      var postBody = body;
      // verify the required parameter 'dbName' is set
      if (dbName === undefined || dbName === null) {
        throw new Error("Missing the required parameter 'dbName' when calling framaC");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling framaC");
      }
      // verify the required parameter 'body' is set
      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling framaC");
      }

      var pathParams = {
        'dbName': dbName
      };
      var queryParams = {
        'asynchronous': opts['asynchronous'],
        'invocationID': opts['invocationID'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Job;
      return this.apiClient.callApi(
        '/frama_c/{dbName}', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Runs Frama-C static program analyzer
     * @param {String} dbName The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations) and/or the analysis results
     * @param {String} key Access key to the PKM
     * @param {module:model/InlineObject} body 
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.asynchronous flag to control asynchronous/synchronous execution of Frama-C job (default to false)
     * @param {String} opts.invocationID invocation identifier (optional)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Job}
     */
    this.framaC = function(dbName, key, body, opts) {
      return this.framaCWithHttpInfo(dbName, key, body, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get Frama-C Job. Getting a finished or failed job, unpublish it (it is no longer available)
     * @param {Number} jobId Job identifier
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Job} and HTTP response
     */
    this.getJobWithHttpInfo = function(jobId, key) {
      var postBody = null;
      // verify the required parameter 'jobId' is set
      if (jobId === undefined || jobId === null) {
        throw new Error("Missing the required parameter 'jobId' when calling getJob");
      }
      // verify the required parameter 'key' is set
      if (key === undefined || key === null) {
        throw new Error("Missing the required parameter 'key' when calling getJob");
      }

      var pathParams = {
        'jobId': jobId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'key': key
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Job;
      return this.apiClient.callApi(
        '/frama_c/jobs/{jobId}', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null
      );
    }

    /**
     * Get Frama-C Job. Getting a finished or failed job, unpublish it (it is no longer available)
     * @param {Number} jobId Job identifier
     * @param {String} key Access key to the PKM
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Job}
     */
    this.getJob = function(jobId, key) {
      return this.getJobWithHttpInfo(jobId, key)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
