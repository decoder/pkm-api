# FramaCRestfulApiForPkm.FramaCApi

All URIs are relative to *http://pkm-api_frama-c_1:8081*

Method | HTTP request | Description
------------- | ------------- | -------------
[**framaC**](FramaCApi.md#framaC) | **POST** /frama_c/{dbName} | 
[**getJob**](FramaCApi.md#getJob) | **GET** /frama_c/jobs/{jobId} | 



## framaC

> Job framaC(dbName, key, body, opts)



Runs Frama-C static program analyzer

### Example

```javascript
var FramaCRestfulApiForPkm = require('frama_c_restful_api_for_pkm');

var apiInstance = new FramaCRestfulApiForPkm.FramaCApi();
var dbName = "dbName_example"; // String | The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations) and/or the analysis results
var key = "key_example"; // String | Access key to the PKM
var body = new FramaCRestfulApiForPkm.InlineObject(); // InlineObject | 
var opts = {
  'asynchronous': false, // Boolean | flag to control asynchronous/synchronous execution of Frama-C job
  'invocationID': "invocationID_example" // String | invocation identifier (optional)
};
apiInstance.framaC(dbName, key, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| The project name from where to get source code/header files, the compile commands, and put the parsing results (Source code ASTs, Comments, and Annotations) and/or the analysis results | 
 **key** | **String**| Access key to the PKM | 
 **body** | [**InlineObject**](InlineObject.md)|  | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of Frama-C job | [optional] [default to false]
 **invocationID** | **String**| invocation identifier (optional) | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getJob

> Job getJob(jobId, key)



Get Frama-C Job. Getting a finished or failed job, unpublish it (it is no longer available)

### Example

```javascript
var FramaCRestfulApiForPkm = require('frama_c_restful_api_for_pkm');

var apiInstance = new FramaCRestfulApiForPkm.FramaCApi();
var jobId = 56; // Number | Job identifier
var key = "key_example"; // String | Access key to the PKM
apiInstance.getJob(jobId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| Job identifier | 
 **key** | **String**| Access key to the PKM | 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

