# FramaCRestfulApiForPkm.InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_file_paths** | **[String]** | Source Code filenames to process | 
**mode** | [**FramaCDbNameMode**](FramaCDbNameMode.md) |  | [optional] 
**options** | [**FramaCDbNameOptions**](FramaCDbNameOptions.md) |  | [optional] 


