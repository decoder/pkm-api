# FramaCRestfulApiForPkm.FramaCDbNameOptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**args** | **[String]** | Frama-C command line options | [optional] 
**eva** | [**FramaCDbNameOptionsEva**](FramaCDbNameOptionsEva.md) |  | [optional] 
**wp** | [**FramaCDbNameOptionsWp**](FramaCDbNameOptionsWp.md) |  | [optional] 
**includes_system_files** | **Boolean** | Enable/Disable inclusion of system files (i.e. system headers) in the result | [optional] 


