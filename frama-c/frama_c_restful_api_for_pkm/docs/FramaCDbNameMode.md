# FramaCRestfulApiForPkm.FramaCDbNameMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parser** | **Boolean** | Enable/disable parser (implicitely enabled when neither EVA nor WP are enabled) | [optional] 
**eva** | **Boolean** | Enable/disable EVA (Evolved Value Analysis) plug-in | [optional] 
**wp** | **Boolean** | Enable/disable WP (Weakest Precondition calculus) plug-in | [optional] 


