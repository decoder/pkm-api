	return new Promise((resolve, reject) =>
	{
		try
		{
			const mode = (body.mode !== undefined) ? body.mode : { parser : true };
			const source_file_paths = body.source_file_paths;
			const options = body.options;
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const tmp_dir = (global.server_config !== undefined) && global.server_config.tmp_dir;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('frama-c', Object.assign({ dbName : dbName, key : key }, body), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const code_api = new PkmRestfulApi.CodeApi();
					const compile_command_api = new PkmRestfulApi.CompileCommandApi();
					const File = require('../../../util/file.js');
					const FileSystem = require('../../../util/file_system');
					const CompileCommand = require('../../../util/compile_command');
					const frama_c = require('../../../util/frama_c').frama_c;
					
					// create a temporary file system
					const file_system = new FileSystem({ debug : debug, logger : job, ...((tmp_dir !== undefined) ? { tmp_dir : tmp_dir } : {}) });
					file_system.make_temporary_root_directory().then(() =>
					{
						// a temporary file system was created
						new Promise((resolve, reject) =>
						{
							// get the list of all source files in the database (without the content)
							code_api.getRawSourceCodes(dbName, key, { abbrev : true }).then((abbrev_source_files) =>
							{
								let write_source_files_promises = [];
								
								// for each source file of database
								abbrev_source_files.forEach((abbrev_source_file) =>
								{
									write_source_files_promises.push(new Promise((resolve, reject) =>
									{
									// get the source file including its content
										code_api.getRawSourceCode(dbName, abbrev_source_file.rel_path, key).then((source_code_file) =>
										{
											if(source_code_file.format != 'text')
											{
												const err_msg = 'File \'' + abbrev_source_file.rel_path + '\' is not a text file';
												console.warn(err_msg);
												reject(err_msg);
												return;
											}
											
											if(source_code_file.content === undefined)
											{
												const err_msg = 'File \'' + abbrev_source_file.rel_path + '\' has no content defined';
												console.warn(err_msg);
												reject(err_msg);
												return;
											}
											
											// replace @ROOT@ by effective root directory
											source_code_file.content = source_code_file.content.replace(/@ROOT@/g, file_system.root_directory);
											
											// write the content of the source file on the host file system
											file_system.writeFile(File.from(source_code_file)).then(() =>
											{
												// the content of the source file was written on the host file system
												resolve();
											}).catch((write_file_err) =>
											{
												// an error occurred while writting the content of the source file on the host file system
												reject(write_file_err);
											});
										}).catch((get_source_file_err) =>
										{
											// an error occurred while getting the source file including its content
											reject(get_source_file_err);
										});
									}));
								});
								
								Promise.all(write_source_files_promises).then(() =>
								{
									// all source files were written on the host file system
									resolve();
								}).catch((write_source_files_err) =>
								{
									// an error occurred while writting all source files on the host file system
									reject(write_source_files_err);
								});
							}).catch((get_abbrev_source_files_err) =>
							{
								// an error occurred while getting the list of all source files in the database (without the content)
								reject(get_abbrev_source_files_err);
							});
						}).then(() => new Promise((resolve, reject) =>
						{
							// all source files were written on the host file system
							
							// get the compile commands
							let get_compile_command_promises = [];
								
							source_file_paths.forEach((source_file_path) =>
							{
								get_compile_command_promises.push(new Promise((resolve, reject) =>
								{
									// get compile command if available
									compile_command_api.getCompileCommand(dbName, source_file_path, key).then((compile_commands) =>
									{
										if(compile_commands.length > 0)
										{
											resolve(CompileCommand.from(compile_commands[0], file_system.root_directory));
										}
									}).catch((err) =>
									{
										if(err.status == 404)
										{
											job.warn('compile command for \'' + source_file_path + '\' not found');
											const path = require('path');
											const compile_command = new CompileCommand(path.join(file_system.root_directory, path.dirname(source_file_path)), source_file_path, '/usr/bin/cc -I.');
											job.warn('defaulting to ' + JSON.stringify(compile_command, null, 2) + ' as compile command');
											resolve(compile_command);
										}
										else
										{
											job.warn('can\'t get compile command for \'' + source_file_path + '\':' + err);
											reject(err);
										}
									});
								}));
							});
								
							Promise.all(get_compile_command_promises).then((compile_commands) =>
							{
								resolve(compile_commands);
							}).catch((err) =>
							{
								reject(err);
							});
						})).then((compile_commands) => new Promise((resolve, reject) =>
						{
							let frama_c_options = Object.assign(
							{
								debug : debug,
								logger : job
							}, options);
							
							// run Frama-Clang
							frama_c(file_system, compile_commands, mode, frama_c_options).then((frama_c_result) =>
							{
								const c_source_code_documents = (frama_c_result.parser !== undefined) && frama_c_result.parser.source_code_documents;
								const c_annotations_documents = (frama_c_result.parser !== undefined) && frama_c_result.parser.annotations_documents;
								const c_comments_documents = (frama_c_result.parser !== undefined) && frama_c_result.parser.comments_documents;
								const system_files = frama_c_result.system_files;
								const report = frama_c_result.report;
								
								if(debug && c_source_code_documents)
								{
									job.log(c_source_code_documents.length + ' C source code documents generated');
								}
								
								if(debug && c_annotations_documents)
								{
									job.log(c_annotations_documents.length + ' C annotations documents generated');
								}
								
								if(debug && c_comments_documents)
								{
									job.log(c_comments_documents.length + ' C comments documents generated');
								}
								
								if(debug && system_files)
								{
									job.log(system_files.length + ' system files generated');
								}
								
								if(debug && report)
								{
									job.log('One report generated');
								}
								
								if(report)
								{
									job.report = report;
								}
								
								((!c_source_code_documents) ? Promise.resolve() : new Promise((resolve, reject) =>
								{
									let put_c_source_codes_promises = [];
									c_source_code_documents.forEach((c_source_code_document) =>
									{
										if(debug)
										{
											job.log('putting a C source code document for \'' + c_source_code_document.sourceFile + '\'');
										}
										put_c_source_codes_promises.push(new Promise((resolve, reject) =>
										{
											code_api.putCSourceCodes(dbName, key, [ c_source_code_document ]).then(() =>
											{
												if(invocation !== undefined) invocation.pushSourceCodes(dbName, 'c', 'sourcecode', [ c_source_code_document ]);
												
												resolve();
											}).catch((put_c_source_codes_err) =>
											{
												const msg = 'can\'t put C source code documents for \'' + c_source_code_document.sourceFile + '\'' + (put_c_source_codes_err.status ? (': PKM server returned error status ' + put_c_source_codes_err.status) : '');
												job.error(msg);
												reject(new Error(msg));
											});
										}));
									});
									
									Promise.all(put_c_source_codes_promises).then(() =>
									{
										resolve();
									}).catch((err) =>
									{
										reject(err);
									});
								})).then(() =>
								{
									return (!c_annotations_documents) ? Promise.resolve() : new Promise((resolve, reject) =>
									{
										let put_c_annotations_promises = [];
										c_annotations_documents.forEach((c_annotations_document) =>
										{
											if(debug)
											{
												job.log('putting a C annotations document for \'' + c_annotations_document.sourceFile + '\'');
											}
											put_c_annotations_promises.push(new Promise((resolve, reject) =>
											{
												code_api.putCAnnotations(dbName, key, [ c_annotations_document ]).then(() =>
												{
													if(invocation !== undefined) invocation.pushSourceCodes(dbName, 'c', 'annotations', [ c_annotations_document ]);
												
													resolve();
												}).catch((put_c_annotations_err) =>
												{
													const msg = 'can\'t put C annotations documents for \'' + c_annotations_document.sourceFile + '\'' + (put_c_annotations_err.status ? (': PKM server returned error status ' + put_c_annotations_err.status) : '');
													job.error(msg);
													reject(new Error(msg));
												});
											}));
										});
										
										Promise.all(put_c_annotations_promises).then(() =>
										{
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									});
								}).then(() =>
								{
									return (!c_comments_documents) ? Promise.resolve() : new Promise((resolve, reject) =>
									{
										let put_c_comments_promises = [];
										c_comments_documents.forEach((c_comments_document) =>
										{
											if(debug)
											{
												job.log('putting a C comments document for \'' + c_comments_document.sourceFile + '\'');
											}
											put_c_comments_promises.push(new Promise((resolve, reject) =>
											{
												code_api.putCComments(dbName, key, [ c_comments_document ]).then(() =>
												{
													if(invocation !== undefined) invocation.pushSourceCodes(dbName, 'c', 'comments', [ c_comments_document ]);
												
													resolve();
												}).catch((put_c_comments_err) =>
												{
													const msg = 'can\'t put C comments documents for \'' + c_comments_document.sourceFile + '\'' + (put_c_comments_err.status ? (': PKM server returned error status ' + put_c_comments_err.status) : '');
													job.error(msg);
													reject(new Error(msg));
												});
											}));
										});
										
										Promise.all(put_c_comments_promises).then(() =>
										{
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									});
								}).then(() =>
								{
									return (!system_files) ? Promise.resolve() : new Promise((resolve, reject) =>
									{
										let put_system_file_promises = [];
										
										system_files.forEach((system_file) =>
										{
											if(debug)
											{
												job.log('putting File \'' + system_file.rel_path + '\'');
											}
											put_system_file_promises.push(new Promise((resolve, reject) =>
											{
												code_api.putRawSourceCodes(dbName, key, [ system_file ]).then(() =>
												{
													if(invocation !== undefined) invocation.pushRawSourceCodes(dbName, [ system_file ]);
												
													resolve();
												}).catch((put_raw_source_codes_err) =>
												{
													const msg = 'can\'t put File \'' + system_file.rel_path + '\'' + (put_raw_source_codes_err.status ? (': PKM server returned error status ' + put_raw_source_codes_err.status) : '');
													job.error(msg);
													reject(new Error(msg));
												});
											}));
										});
										
										Promise.all(put_system_file_promises).then(() =>
										{
											resolve();
										}).catch((err) =>
										{
											reject(err);
										});
									});
								}).then(() =>
								{
									// abstract representation of the source code was updated in the database
									resolve()
								}).catch((err) =>
								{
									// an error occurred while updating the database
									reject(err);
								});
							}).catch((err) =>
							{
								// an error occurred while running Frama-C
								reject(err);
							});
						})).catch((err) =>
						{
							// delay the promise rejection until temporary file system has been deleted
							error = err;
							status = false;
						}).finally(() =>
						{
							// delete previously created temporary file system
							(do_not_remove_temporary_root_directory ? Promise.resolve() : file_system.remove_temporary_root_directory()).catch((err) =>
							{
								// an error occurred while deleting the temporary file system
								const { deep_copy } = require('../../../util/deep_copy');
								job.error(JSON.stringify(deep_copy(err)));
								
								if(status)
								{
									error = err;
									status = false;
								}
							}).finally(() =>
							{
								resolve();
							});
						});
					}).catch((err) =>
					{
						// an error occurred while creating the temporary file system
						reject(err);
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the invocation and logs in the database have been updated
					error = err;
					status = false;
				}).finally(() => 
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let nature_of_report = [];
						if(mode.parser) nature_of_report.push('Parser report');
						if(mode.eva) nature_of_report.push('Testing report');
						if(mode.wp) nature_of_report.push('Proof report');
						
						let log_document =
						{
							tool: 'Frama-C',
							'nature of report' : nature_of_report.join(', '),
							'start running time' : job.start_date,
							'end running time': end_date.toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors: job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(job.report !== undefined)
						{
							log_document.details.report = job.report;
						}
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});
