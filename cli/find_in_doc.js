#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: find_in_doc <options>");
	console.log("");
	console.log("Find in documentation of a database matching document name, unit name, and class name and invariant, field, method, type or macro name.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                    display this help and exit");
	console.log("\t--version                 output version information and exit");
	console.log("\t--user=<name>             MongoDB user name");
	console.log("\t--db=<database>           MongoDB database name");
	console.log("\t--host=<host:port>        MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>       MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("\t--debug                   enable debugging messages (default: " + (PKM.global_config.debug ? "enabled" : "disabled") + ")");
	console.log("\t--doc=<name regex>        regular expression for doc name");
	console.log("\t--unit=<name regex>       regular expression for unit name");
	console.log("\t--class=<name regex>      regular expression for class name");
	console.log("\t--invariant=<name regex>  regular expression for invariant name");
	console.log("\t--field=<name regex>      regular expression for field name");
	console.log("\t--method=<name regex>     regular expression for method name");
	console.log("\t--type=<name regex>       regular expression for type name");
	console.log("\t--macro=<name regex>      regular expression for macro name");
	console.log("\t--constant=<name regex>   regular expression for constant name");
	console.log("\t--member=<name regex>     regular expression for member name (either invariant, field, method, type, macro, or constant name)");
	console.log("");
	console.log("Examples:");
	console.log("");
	console.log("find_in_doc --user=garfield --db=mydb --class=vector --field=");
	console.log("");
	console.log("find_in_doc --user=garfield --db=mydb --class=vector --method=add_vector");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'debug!', 'doc', 'unit', 'class', 'invariant', 'field', 'method', 'method', 'type', 'macro', 'constant', 'member']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	const doc_name = cmdline.options['doc'];
	const unit_name = cmdline.options['unit'];
	const class_name = cmdline.options['class'];
	const invariant_name = cmdline.options['invariant'];
	const field_name = cmdline.options['field'];
	const method_name = cmdline.options['method'];
	const type_name = cmdline.options['type'];
	const macro_name = cmdline.options['macro'];
	const constant_name = cmdline.options['constant'];
	const member_name = cmdline.options['member'];
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(cmdline.args.length) { console.log("No arguments after options are expected"); help(); process.exit(1); }
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	pkm_promise.then((pkm) =>
	{
		let query = {};
		if(doc_name !== undefined) query.doc = doc_name;
		if(unit_name !== undefined) query.unit = unit_name;
		if(class_name !== undefined) query.class = class_name;
		if(invariant_name !== undefined) query.invariant = invariant_name;
		if(field_name !== undefined) query.field = field_name;
		if(method_name !== undefined) query.method = method_name;
		if(type_name !== undefined) query.type = type_name;
		if(macro_name !== undefined) query.macro = macro_name;
		if(constant_name !== undefined) query.constant = constant_name;
		if(member_name !== undefined) query.member = member_name;
		
		pkm.find_in_doc(dbName, query).then((result) =>
		{
			console.log(JSON.stringify(result, null, 2));
			pkm.close();
			process.exit(0);
		}).catch(function(err)
		{
			pkm.close();
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
