#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: get_tools <options> [<tools>]");
	console.log("");
	console.log("Get Tool documents from a database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                  display this help and exit");
	console.log("\t--version               output version information and exit");
	console.log("\t--user=<name>           MongoDB user name");
	console.log("\t--db=<database>         MongoDB database name");
	console.log("\t--host=<host:port>      MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>     MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("\t--debug                 enable debugging messages (default: " + (PKM.global_config.debug ? "enabled" : "disabled") + ")");
	console.log("\t--root=<directory>      root directory (default: value of environment variable DB_ROOT)");
	console.log("\t--encoding=<encoding>   encoding (default: utf8)");
	console.log("");
	console.log("Examples:");
	console.log("");
	console.log("get_tools --user=garfield --db=mydb tool_1");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'root', 'encoding', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	const root_directory = cmdline.options['root'] || process.env.DB_ROOT;
	const encoding = cmdline.options['encoding'] || 'utf8';
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!root_directory) { console.log("No root directory specified"); help(); process.exit(1); }
	if(!encoding) { console.log("No encoding specified"); help(); process.exit(1); }
	const toolIDs = cmdline.args;
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	pkm_promise.then((pkm) =>
	{
		const File = require('../util/file');
		const FileSystem = require('../util/file_system');
		const file_system = new FileSystem(root_directory, { encoding : encoding, debug : pkm.config.debug });
		
		let get_tools_promises = (toolIDs.length != 0)
		                                    ? toolIDs.map((toolID) => pkm.get_tools(dbName, { toolID : toolID }))
		                                    : [ pkm.get_tools(dbName) ];
		
		Promise.all(get_tools_promises).then((tool_documents) =>
		{
			let write_file_promises = [];
			
			tool_documents.forEach((tool_documents) =>
			{
				tool_documents.forEach((tool_document) =>
				{
					let tool_document_content = JSON.stringify(tool_document, null, 2);

					const tool_file = new File(
						'tool_' + tool_document.toolID + '.json',   // rel_path
						tool_document_content, // content
						'utf8',                   // encoding
						'Tool',                 // type
						'application/json'        // MIME type
					);
					
					write_file_promises.push(file_system.writeFile(tool_file));
				});
			});
			
			Promise.all(write_file_promises).then(() =>
			{
				pkm.close();
				process.exit(0);
			}).catch((err) =>
			{
				pkm.close();
				console.error(err);
				process.exit(1);
			});
		}).catch((err) =>
		{
			pkm.close();
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
