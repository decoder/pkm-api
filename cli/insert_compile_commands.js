#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: insert_compile_commands <options> <compile_commands_filenames>");
	console.log("");
	console.log("Insert compile commands from a file which format complies with JSON Compilation Database Format Specification into a database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                  display this help and exit");
	console.log("\t--version               output version information and exit");
	console.log("\t--user=<name>           MongoDB user name");
	console.log("\t--db=<database>         MongoDB database name");
	console.log("\t--host=<host:port>      MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>     MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("\t--debug                 enable debugging messages (default: " + (PKM.global_config.debug ? "enabled" : "disabled") + ")");
	console.log("\t--root=<directory>      root directory (default: value of environment variable DB_ROOT)");
	console.log("\t--encoding=<encoding>   encoding (default: utf8)");
	console.log("");
	console.log("Examples:");
	console.log("");
	console.log("insert_compile_commands --user=garfield --db=mydb compile_commands.json");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'root', 'encoding', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	const root_directory = cmdline.options['root'] || process.env.DB_ROOT;
	const encoding = cmdline.options['encoding'] || 'utf8';
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!root_directory) { console.log("No root directory specified"); help(); process.exit(1); }
	if(!encoding) { console.log("No encoding specified"); help(); process.exit(1); }
	if(!cmdline.args.length) { console.log("No source files provided"); help(); process.exit(1); }
	const host_compile_commands_file_paths = cmdline.args;
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	pkm_promise.then((pkm) =>
	{
		const FileSystem = require('../util/file_system');
		const file_system = new FileSystem(root_directory, { encoding : encoding, debug : pkm.config.debug });

		var insert_compile_commands_promises = [];
		host_compile_commands_file_paths.forEach((host_compile_commands_file_path) =>
		{
			const path = require('path');
			insert_compile_commands_promises.push(file_system.readFile(path.resolve(host_compile_commands_file_path)).then((compile_commands_file) =>
			{
				return new Promise((resolve, reject) =>
				{
					let compile_commands;
					
					try
					{
						compile_commands = JSON.parse(compile_commands_file.content);
					}
					catch(err)
					{
						reject('In \'' + host_compile_commands_file_path + '\'', err);
						return;
					}
					
					if(Array.isArray(compile_commands))
					{
						const path = require('path');
						
						if(compile_commands.find((entry) => (entry.directory !== undefined) && path.isAbsolute(entry.directory) && !file_system.check_path(entry.directory)) !== undefined)
						{
							reject('In \'' + host_compile_commands_file_path + '\', some directory paths are either not absolute or outside of root directory');
							return;
						}
						
						if(compile_commands.find((entry) => (entry.file !== undefined) && ((path.isAbsolute(entry.file) && !file_system.check_path(entry.file)) || (!path.isAbsolute(entry.file) && file_system.check_path(path.join(entry.directory), entry.file)))) !== undefined)
						{
							reject('In \'' + host_compile_commands_file_path + '\', some file paths are outside of root directory');
							return;
						}

						compile_commands.forEach((entry) =>
						{
							if(entry.directory !== undefined)
							{
								entry.directory = file_system.fix_path(entry.directory);
							}
							if(entry.file !== undefined)
							{
								if(path.isAbsolute(entry.file))
								{
									entry.file = file_system.fix_path(entry.file);
								}
								else
								{
									entry.file = file_system.fix_path(path.join(entry.directory), entry.file);
								}
							}
						});
						
						pkm.insert_compile_commands(dbName, compile_commands).then(() =>
						{
							resolve();
						}).catch((err) =>
						{
							reject(err);
						});
					}
					else
					{
						reject('File \'' + host_compile_commands_file_path + '\' does not look like a JSON Compilation Database');
					}
				});
			}));
		});
		
		Promise.all(insert_compile_commands_promises).then(() =>
		{
			pkm.close();
			process.exit(0);
		}).catch(function(err)
		{
			pkm.close();
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
