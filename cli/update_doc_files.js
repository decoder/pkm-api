#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: update_doc_files <options> [file_names]");
	console.log("");
	console.log("Update Documentation files into a database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                  display this help and exit");
	console.log("\t--version               output version information and exit");
	console.log("\t--user=<name>           MongoDB user name");
	console.log("\t--db=<database>         MongoDB database name");
	console.log("\t--host=<host:port>      MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>     MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("\t--debug                 enable debugging messages (default: " + (PKM.global_config.debug ? "enabled" : "disabled") + ")");
	console.log("\t--root=<directory>      root directory (default: value of environment variable DB_ROOT)");
	console.log("\t--format=<format>       either 'binary' or 'text' (default: 'text')");
	console.log("\t--encoding=<encoding>   text encoding (default: utf8)");
	console.log("");
	console.log("Examples:");
	console.log("");
	console.log("update_doc_files --user=garfield --db=mydb --format=binary doc.docx");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'root', 'format', 'encoding', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	const root_directory = cmdline.options['root'] || process.env.DB_ROOT;
	const format = cmdline.options['format'] || 'text';
	const encoding = cmdline.options['encoding'] || 'utf8';
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!root_directory) { console.log("No root directory specified"); help(); process.exit(1); }
	if(!encoding) { console.log("No encoding specified"); help(); process.exit(1); }
	if((format != 'binary') && (format != 'text')) { console.log("Format must be either 'binary' or 'text'"); help(); process.exit(1); }
	if(!cmdline.args.length) { console.log("No source files provided"); help(); process.exit(1); }
	const host_doc_file_paths = cmdline.args;
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	pkm_promise.then((pkm) =>
	{
		const FileSystem = require('../util/file_system');
		const file_system = new FileSystem(root_directory, { encoding : ((format === 'text') ? encoding : null), debug : pkm.config.debug });

		var doc_files = [];
		var read_file_promises = [];
		host_doc_file_paths.forEach((host_doc_file_path) =>
		{
			const path = require('path');
			read_file_promises.push(file_system.readFile(path.resolve(host_doc_file_path)).then((doc_file) =>
			{
				return new Promise((resolve, reject) =>
				{
					doc_files.push(doc_file);
					resolve();
				});
			}));
		});
		
		Promise.all(read_file_promises).then(() =>
		{
			pkm.update_doc_files(dbName, doc_files).then(function()
			{
				pkm.close();
				process.exit(0);
			}).catch(function(err)
			{
				pkm.close();
				console.error(err);
				process.exit(1);
			});
		}).catch((err) =>
		{
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
