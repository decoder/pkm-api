#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: run_frama_clang <options> <source_file_names>");
	console.log("");
	console.log("Run Frama-Clang analysis tool on C++ source code files from a database and update C++ source code (AST),");
	console.log("C++ annotations and C++ comments in that database. Logs are updated in the database.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                        display this help and exit");
	console.log("\t--version                     output version information and exit");
	console.log("\t--user=<name>                 MongoDB user name");
	console.log("\t--db=<database>               MongoDB database name");
	console.log("\t--host=<host:port>            MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>           MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("Examples:");
	console.log("");
	console.log("run_frama_clang --user=garfield --db=mydb hello.cpp");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!cmdline.args.length) { console.log("No source files provided"); help(); process.exit(1); }
	const source_file_paths = cmdline.args;
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	let exit_status = 0;

	pkm_promise.then((pkm) =>
	{
		// connection with PKM was established
		
		// promise chain
		new Promise((resolve, reject) =>
		{
			// create a logger
			const Logger = require('../util/logger');
			const logger = new Logger();
			
			// create a temporary file system
			const FileSystem = require('../util/file_system');
			const file_system = new FileSystem({ debug : pkm.config.debug, logger : logger, ...((pkm.tmp_dir !== undefined) ? { tmp_dir : pkm.tmp_dir } : {}) });
			
			const start_date = new Date().toGMTString();
			
			let error;
			
			// run the tool in a remote environment other than the current directory
			new Promise((resolve, reject) =>
			{
				file_system.make_temporary_root_directory().then(() =>
				{
					// a temporary file system was created
					let error;
					
					new Promise((resolve, reject) =>
					{
						// get the list of all source files in the database (without the content)
						pkm.get_source_files(dbName, {}, { projection: { filecontent : 0 } }).then((abbrev_source_files) =>
						{
							let write_source_files_promises = [];
							
							// for each source file of database
							abbrev_source_files.forEach((abbrev_source_file) =>
							{
								write_source_files_promises.push(new Promise((resolve, reject) =>
								{
								// get the source file including its content
									pkm.get_source_files(dbName, { filename : abbrev_source_file.rel_path }).then((source_code_files) =>
									{
										// Note: there's actually only one source file even if we get an array
										const source_code_file = source_code_files[0];
										
										if(typeof source_code_file.content !== 'string')
										{
											const err_msg = 'File \'' + abbrev_source_file.rel_path + '\' is not a text file';
											console.warn(err_msg);
											reject(err_msg);
											return;
										}
										
										// replace @ROOT@ by effective root directory
										source_code_file.content = source_code_file.content.replace(/@ROOT@/g, file_system.root_directory);
										
										// write the content of the source file on the host file system
										file_system.writeFile(source_code_file).then(() =>
										{
											// the content of the source file was written on the host file system
											resolve();
										}).catch((write_file_err) =>
										{
											// an error occurred while writting the content of the source file on the host file system
											reject(write_file_err);
										});
									}).catch((get_source_file_err) =>
									{
										// an error occurred while getting the source file including its content
										reject(get_source_file_err);
									});
								}));
							});
							
							Promise.all(write_source_files_promises).then(() =>
							{
								// all source files were written on the host file system
								resolve();
							}).catch((write_source_files_err) =>
							{
								// an error occurred while writting all source files on the host file system
								reject(write_source_files_err);
							});
						}).catch((get_abbrev_source_files_err) =>
						{
							// an error occurred while getting the list of all source files in the database (without the content)
							reject(get_abbrev_source_files_err);
						});
					}).then(() => new Promise((resolve, reject) =>
					{
						// all source files were written on the host file system
						
						// get the compile commands
						const CompileCommand = require('../util/compile_command');
						
						let get_compile_command_promises = [];
						
						// for each source code files
						source_file_paths.forEach((source_file_path) =>
						{
							get_compile_command_promises.push(new Promise((resolve, reject) =>
							{
								// get compile command if available
								pkm.get_compile_commands(dbName, { file : source_file_path }).then((compile_commands) =>
								{
									if(compile_commands.length > 0)
									{
										resolve(CompileCommand.from(compile_commands[0], file_system.root_directory));
									}
								}).catch((err) =>
								{
									const error = require('../core/error');
									if(err instanceof error.PKM_NotFound)
									{
										console.warn('compile command for \'' + source_file_path + '\' not found');
										const path = require('path');
										const compile_command = new CompileCommand(path.join(file_system.root_directory, path.dirname(source_file_path)), source_file_path, '/usr/bin/c++ -I.');
										resolve(compile_command);
									}
									else
									{
										console.warn('can\'t get compile command for \'' + source_file_path + '\':' + err);
										reject(err);
									}
								});
							}));
						});
						
						Promise.all(get_compile_command_promises).then((compile_commands) =>
						{
							resolve(compile_commands);
						}).catch((err) =>
						{
							reject(err);
						});
					})).then((compile_commands) => new Promise((resolve, reject) =>
					{
						const frama_clang = require('../util/frama_clang').frama_clang;
							
						let frama_clang_options =
						{
							debug : debug,
							logger : logger
						};
							
						// run Frama-Clang
						frama_clang(file_system, compile_commands, frama_clang_options).then((frama_clang_result) =>
						{
							const cpp_source_code_documents = frama_clang_result.source_code_documents;
							const cpp_annotations_documents = frama_clang_result.annotations_documents;
							const cpp_comments_documents = frama_clang_result.comments_documents;
							const system_files = frama_clang_result.system_files;
							
							if(debug && cpp_source_code_documents)
							{
								console.log(cpp_source_code_documents.length + ' C++ source code documents generated');
							}
								
							if(debug && cpp_annotations_documents)
							{
								console.log(cpp_annotations_documents.length + ' C++ annotations documents generated');
							}
							
							if(debug && cpp_comments_documents)
							{
								console.log(cpp_comments_documents.length + ' C++ comments documents generated');
							}

							if(debug && system_files)
							{
								console.log(system_files.length + ' system files generated');
							}
							
							// update database with the abstract representation of the source code
							((!cpp_source_code_documents) ? Promise.resolve() : pkm.update_cpp_source_code(dbName, cpp_source_code_documents )).then(() =>
							 (!cpp_annotations_documents) ? Promise.resolve() : pkm.update_cpp_annotations(dbName, cpp_annotations_documents)).then(() =>
							 (!cpp_comments_documents) ? Promise.resolve() : pkm.update_cpp_comments(dbName, cpp_comments_documents)).then(() =>
							 (!system_files) ? Promise.resolve() : pkm.update_source_files(dbName, system_files)).then(() =>
							{
								// abstract representation of the source code was updated in the database
								resolve();
							}).catch((err) =>
							{
								// an error occurred while updating the database
								reject(err);
							});
						}).catch((err) =>
						{
							// an error occurred while running Frama-Clang
							reject(err);
						});
					})).catch((err) =>
					{
						// delay the promise rejection until temporary file system has been deleted
						error = err;
						exit_status = 1;
					}).finally(() =>
					{
						// delete previously created temporary file system
						file_system.remove_temporary_root_directory().catch((err) =>
						{
							// an error occurred while deleting the temporary file system
							if(exit_status)
							{
								console.error(err);
							}
							else
							{
								error = err;
								exit_status = 1;
							}
						}).finally(() =>
						{
							if(exit_status)
							{
								reject(error);
							}
							else
							{
								resolve();
							}
						});
					});
				}).catch((err) =>
				{
					// an error occurred while creating the temporary file system
					exit_status = 1;
					reject(err);
				});
			}).catch((err) =>
			{
				// delay the promise rejection until the logs in the database have been updated
				error = err;
			}).finally(()=>
			{
				let log_document =
				{
					tool: 'Frama-Clang',
					'nature of report' : 'Parser report',
					'start running time' : start_date,
					'end running time': new Date().toGMTString(),
					messages: logger.messages,
					warnings: logger.warnings,
					status: (exit_status === 0)
				};
				
				pkm.insert_logs(dbName, [ log_document ]).then(() =>
				{
					if(exit_status)
					{
						reject(error);
					}
					else
					{
						resolve();
					}
				}).catch((err) =>
				{
					reject(err);
				});
			});
		}).catch((err) =>
		{
			// an error occurred in the promise chain
			console.error(err);
		}).finally(() =>
		{
			pkm.close();
			process.exit(exit_status);
		});
	}).catch((err) =>
	{
		// an error occurred while login
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
