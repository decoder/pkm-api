"use strict";

function help()
{
	console.log("Usage: md_to_json <input file> <output file>");
	console.log("\tinput files must be .md");
}

if(process.argv.length < 4)
{
	console.log("Insufficient arguments found");
	help();
	process.exit(1);
}

const path = require('path');
const assert = require ('assert');
const fs = require ('fs');
const md2json = require('md-2-json');

var val = process.argv[2];
var out_val = process.argv[3];

if(val.endsWith('.md'))
{
	let content = fs.readFileSync(val, 'utf8');
	let res = md2json.parse (content);
	fs.writeFileSync(out_val,JSON.stringify(res));
}
else
{
	console.log('Wrong file suffix, shall be .md')
}
