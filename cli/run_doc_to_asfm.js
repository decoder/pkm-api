#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: run_doc_to_asfm <options> <file_name>");
	console.log("");
	console.log("Run doc_to_asfm conversion tool on a .docx file from a database and update documentation.");
	console.log("Logs are updated in the database.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                        display this help and exit");
	console.log("\t--version                     output version information and exit");
	console.log("\t--user=<name>                 MongoDB user name");
	console.log("\t--db=<database>               MongoDB database name");
	console.log("\t--host=<host:port>            MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>           MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("Examples:");
	console.log("");
	console.log("run_doc_to_asfm --user=garfield --db=mydb vector2.docx");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'debug!' ]).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!cmdline.args.length) { console.log("No .docx file provided"); help(); process.exit(1); }
	if(cmdline.args.length > 1) { console.log("No more than one file is expected"); help(); process.exit(1); }
	const file_path = cmdline.args[0];
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	let exit_status = 0;
	
	pkm_promise.then((pkm) =>
	{
		// connection with PKM was established

		// promise chain
		new Promise((resolve, reject) =>
		{
			// create a logger
			const Logger = require('../util/logger');
			const logger = new Logger();
			
			// create a temporary file system
			const FileSystem = require('../util/file_system');
			const file_system = new FileSystem({ debug : pkm.config.debug, logger : logger, ...((pkm.tmp_dir !== undefined) ? { tmp_dir : pkm.tmp_dir } : {}) });
			
			const start_date = new Date().toGMTString();
			
			let error;
			
			// run the tool in a remote environment other than the current directory
			new Promise((resolve, reject) =>
			{
				file_system.make_temporary_root_directory().then(() =>
				{
					// a temporary file system was created
					let error;
					
					new Promise((resolve, reject) =>
					{
						// get the doc file
						pkm.get_doc_files(dbName, { filename : file_path }).then((doc_files) =>
						{
							// Note: there's actually only one file even if we get an array
							const doc_file = doc_files[0];
									
							// write the content of the doc file on the host file system
							file_system.writeFile(doc_file).then(() =>
							{
								// the content of the doc file was written on the host file system
								resolve();
							}).catch((write_file_err) =>
							{
								// an error occurred while writting the content of the doc file on the host file system
								reject(write_file_err);
							});
						}).catch((get_doc_files_err) =>
						{
							// an error occurred while getting the doc file
							reject(get_doc_files_err);
						});
					}).then(() => new Promise((resolve, reject) =>
					{
						const doc_to_asfm = require('../util/doc_to_asfm').doc_to_asfm;
						
						// doc file was written on the host file system
						let doc_to_asfm_options =
						{
							debug : debug,
							logger : logger
						};
						
						doc_to_asfm(file_system, file_path, doc_to_asfm_options).then((doc_document) =>
						{
							doc_document.sourceFile = file_path;
							
							pkm.update_docs(dbName, [ doc_document ]).then(() =>
							{
								// database has been updated
								resolve();
							}).catch((err) =>
							{
								// an error occurred while updating the database
								reject(err);
							});
						}).catch((err) =>
						{
							// an error occurred while running doc_to_asfm
							reject(err);
						});
					})).catch((err) =>
					{
						// delay the promise rejection until temporary file system has been deleted
						error = err;
						exit_status = 1;
					}).finally(() =>
					{
						// delete previously created temporary file system
						file_system.remove_temporary_root_directory().catch((err) =>
						{
							// an error occurred while deleting the temporary file system
							if(exit_status)
							{
								console.error(err);
							}
							else
							{
								error = err;
								exit_status = 1;
							}
						}).finally(() =>
						{
							if(exit_status)
							{
								reject(error);
							}
							else
							{
								resolve();
							}
						});
					});
				}).catch((err) =>
				{
					// an error occurred while creating the temporary file system
					exit_status = 1;
					reject(err);
				});
			}).catch((err) =>
			{
				// delay the promise rejection until the logs in the database have been updated
				error = err;
			}).finally(()=>
			{
				let log_document =
				{
					tool: 'doc_to_asfm',
					'nature of report' : 'Docx to ASFM conversion',
					'start running time' : start_date,
					'end running time': new Date().toGMTString(),
					messages: logger.messages,
					warnings: logger.warnings,
					status: (exit_status === 0)
				};
				
				pkm.insert_logs(dbName, [ log_document ]).then(() =>
				{
					if(exit_status)
					{
						reject(error);
					}
					else
					{
						resolve();
					}
				}).catch((err) =>
				{
					reject(err);
				});
			});
		}).catch((err) =>
		{
			// an error occurred in the promise chain
			console.error(err);
		}).finally(() =>
		{
			pkm.close();
			process.exit(exit_status);
		});
	}).catch((err) =>
	{
		// connection to the PKM failed
		console.error(err);
		exit_status = 1;
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
