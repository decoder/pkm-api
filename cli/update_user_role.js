#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: update_user_role <options> <command> <user>");
	console.log("");
	console.log("Create a user in PKM management database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                     display this help and exit");
	console.log("\t--version                  output version information and exit");
	console.log("\t--admin=<name>             PKM administrator");
	console.log("\t--role=<role>              Role");
	console.log("\t--db=<database>            MongoDB database name");
	console.log("\t--host=<host:port>         MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>        MongoDB PKM management database name (default: '" + PKM.global_config.pkm_db  + "')");
	console.log("");
	console.log("Examples:");
	console.log("");
	console.log("update_user_role --admin=admin --db=mydb --role=Developer grant garfield");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'admin', 'db', 'host', 'pkm-db', 'role', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const admin = cmdline.options['admin'];
	const role = cmdline.options['role'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	const cmd = (cmdline.args.length > 0) ? cmdline.args[0] : null;
	const user_name = (cmdline.args.length > 1) ? cmdline.args[1] : null;
	if(!admin) { console.log("No admin specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!role) { console.log("No role specified"); help(); process.exit(1); }
	if(!cmd) { console.log("No command specified"); help(); process.exit(1); }
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if((cmd != 'grant') && (cmd != 'revoke')) { console.log("Unknown command '" + cmd + "'"); help(); process.exit(1); }
	if(cmdline.args.length > 2) { console.log("No more than two arguments are expected after options"); help(); process.exit(1); }
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(admin + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((admin_password) => PKM.login(admin, admin_password, config));
	pkm_promise.then((pkm) =>
	{
		pkm.get_user(user_name).then((user) =>
		{
			switch(cmd)
			{
				case 'grant':
					user.grant(dbName, role);
					break;
				case 'revoke':
					user.revoke(dbName, role);
					break;
			}
			
			pkm.update_user(user).then(() =>
			{
				pkm.close();
				process.exit(0);
			}).catch((err) =>
			{
			pkm.close();
			console.error(err);
			process.exit(1);
			});
		}).catch((err) =>
		{
			pkm.close();
			console.error(err);
			process.exit(1);
		});
// 		pkm.update_user_role(dbName, role, cmd, user_name).then(function()
// 		{
// 			pkm.close();
// 			process.exit(0);
// 		}).catch(function(err)
// 		{
// 			pkm.close();
// 			console.error(err);
// 			process.exit(1);
// 		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
