#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: run_asfm_to_doc <options> <doc name>");
	console.log("");
	console.log("Run asfm_to_doc conversion tool on an ASFM documentation from a database and update documentation files.");
	console.log("Logs are updated in the database.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                        display this help and exit");
	console.log("\t--version                     output version information and exit");
	console.log("\t--user=<name>                 MongoDB user name");
	console.log("\t--db=<database>               MongoDB database name");
	console.log("\t--host=<host:port>            MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>           MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("Examples:");
	console.log("");
	console.log("run_asfm_to_doc --user=garfield --db=mydb 'The Graphic library'");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'debug!' ]).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.log("No database specified"); help(); process.exit(1); }
	if(!cmdline.args.length) { console.log("No .docx file provided"); help(); process.exit(1); }
	if(cmdline.args.length > 1) { console.log("No more than one doc name is expected"); help(); process.exit(1); }
	const doc_name = cmdline.args[0];
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	let exit_status = 0;
	
	pkm_promise.then((pkm) =>
	{
		// connection with PKM was established

		// promise chain
		new Promise((resolve, reject) =>
		{
			// create a logger
			const Logger = require('../util/logger');
			const logger = new Logger();
			
			const start_date = new Date().toGMTString();
			
			let error;
			
			// run the tool
			new Promise((resolve, reject) =>
			{
				const asfm_to_doc = require('../util/asfm_to_doc').asfm_to_doc;
						
				let asfm_to_doc_options =
				{
					debug : debug,
					logger : logger
				};
				
				if(debug)
				{
					logger.log('getting \'' + doc_name + '\' document');
				}
				
				pkm.get_docs(dbName, { name : doc_name }).then((documents) =>
				{
					// Note: there's actually only one document even if we get an array
					const asfm_obj = documents[0];
					
					asfm_to_doc(asfm_obj, asfm_to_doc_options).then((buffer) =>
					{
						const File = require('../util/file.js');
						
						const file_path = (asfm_obj.sourceFile !== undefined) ? asfm_obj.sourceFile : (asfm_obj.name + '.docx')
						const doc_file = new File(
							file_path,  // rel_path
							buffer,     // content
							null,       // encoding
							'',         // type (let PKM guess the type)
							'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // MIME type
						);
					
						if(debug)
						{
							logger.log('updating \'' + doc_file.rel_path + '\' file');
						}
						
						pkm.update_doc_files(dbName, [ doc_file ]).then(() =>
						{
							// database has been updated
							resolve();
						}).catch((err) =>
						{
							// an error occurred while updating the database
							reject(err);
						});
					}).catch((err) =>
					{
						// an error occurred while running asfm_to_doc
						reject(err);
					});
				}).catch((err) =>
				{
					// an error occurred while getting ASFM object
					reject(err);
				});
			}).catch((err) =>
			{
				// delay the promise rejection until the logs in the database have been updated
				exit_status = 1;
				error = err;
			}).finally(()=>
			{
				let log_document =
				{
					tool: 'doc_to_asfm',
					'nature of report' : 'ASFM to Docx conversion',
					'start running time' : start_date,
					'end running time': new Date().toGMTString(),
					messages: logger.messages,
					warnings: logger.warnings,
					status: (exit_status === 0)
				};
				
				pkm.insert_logs(dbName, [ log_document ]).then(() =>
				{
					if(exit_status)
					{
						reject(error);
					}
					else
					{
						resolve();
					}
				}).catch((err) =>
				{
					reject(err);
				});
			});
		}).catch((err) =>
		{
			// an error occurred in the promise chain
			console.error(err);
		}).finally(() =>
		{
			pkm.close();
			process.exit(exit_status);
		});
	}).catch((err) =>
	{
		// connection to the PKM failed
		console.error(err);
		exit_status = 1;
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
