#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: finalize_PKM <options>");
	console.log("");
	console.log("Finalize PKM: Drop all PKM projects and the PKM management database, then drop all users in PKM management database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("");
	console.log("Options:");
	console.log("\t--help                         display this help and exit");
	console.log("\t--version                      output version information and exit");
	console.log("\t--superuser=<name>             MongoDB \"superuser\" that can drop roles and users in MongoDB PKM management database");
	console.log("\t--superuser-auth-db<database>  MongoDB authentication database for superuser (default: 'admin')");
	console.log("\t--host=<host:port>             MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>            MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("Examples:");
	console.log("");
	console.log("finalize_PKM --superuser=admin --superuser-auth-db=admin --admin=admin --pkm-db=users");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'superuser', 'host', 'superuser-auth-db', 'admin', 'pkm-db', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const superuser_name = cmdline.options['superuser'];
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const superuser_auth_db = cmdline.options['superuser-auth-db'] || 'admin';
	const pkm_db = cmdline.options['pkm-db'];
	if(!superuser_name) { console.log("No superuser specified"); help(); process.exit(1); }
	if(cmdline.args.length) { console.log("No arguments after options are expected"); help(); process.exit(1); }
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	password_reader.read_password(superuser_name + '@' + superuser_auth_db + '\'s password:').then((superuser_password) =>
	{
		PKM.finalize(superuser_name, superuser_auth_db, superuser_password, config).then(() =>
		{
			process.exit(0);
		}).catch((err) =>
		{
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
