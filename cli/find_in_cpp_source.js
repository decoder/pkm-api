#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: find_in_cpp_source <options>");
	console.log("");
	console.log("Find artefacts in C++ source code of a database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("Options:");
	console.log("\t--help                  display this help and exit");
	console.log("\t--version               output version information and exit");
	console.log("\t--user=<name>           MongoDB user name");
	console.log("\t--db=<database>         MongoDB database name");
	console.log("\t--host=<host:port>      MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>     MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("\t--debug                 enable debugging messages (default: " + (PKM.global_config.debug ? "enabled" : "disabled") + ")");
	console.log("\t--kind=<kind|/regex/>   kind of C++ artefacts as either an exact value or a /regular expression/ (e.g. '/^class$|^struct$/')");
	console.log("\t--id=<id>               numeric identifier of C++ artefact (e.g. 1234)");
	console.log("\t--path=<path|/regex/>   abstract path of C++ artefacts as either an exact value or a /regular expression/ (e.g. '/^std::log[fl]?$/')");
	console.log("\t--name=<name|/regex/>   name of C++ artefacts as either an exact value or a /regular expression/ (e.g. '/^log[fl]?$/')");
	console.log("");
	console.log("Examples:");
	console.log("");
	console.log("find_in_cpp_source --user=garfield --db=mydb --kind=function --path=std::max");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'user', 'db', 'host', 'pkm-db', 'debug!', "kind", "id", "path" ]).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const user_name = cmdline.options['user'];
	const dbName = cmdline.options['db'];
	const pkm_db = cmdline.options['pkm-db'];
	const kind = cmdline.options['kind'];
	const id_str = cmdline.options['id'];
	const path = cmdline.options['path'];
	if(!user_name) { console.log("No user specified"); help(); process.exit(1); }
	if(!dbName) { console.error("No database specified"); help(); process.exit(1); }
	let id;
	if(id_str !== undefined)
	{
		id = parseInt(id_str, 10);
		if(isNaN(id) || (id < 0))
		{
			console.error("Id must be an integer >= 0");
			help();
			process.exit(1);
		}
	}
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	const pkm_promise = password_reader.read_password(user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s password:').then((user_password) => PKM.login(user_name, user_password, config));
	pkm_promise.then((pkm) =>
	{
		let query = {};
		if(id !== undefined) query.id = id;
		if(kind !== undefined) query.kind = kind;
		if(path !== undefined) query.path = path;
		pkm.find_in_cpp_source(dbName, query).then(function(results)
		{
			console.log(JSON.stringify(results, null, 2));
			pkm.close();
			process.exit(0);
		}).catch(function(err)
		{
			pkm.close();
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
