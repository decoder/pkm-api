#!/usr/bin/env bash
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/*
 * This file is part of PKM (Persistent Knowledge Monitor).
 * Copyright (c) 2020 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives,
 *                    OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.
 * 
 * PKM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation.
 * 
 * PKM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with PKM.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const PKM = require('../core/pkm');
const password_reader = require('../util/password_reader');
const parse_cmdline = require('../util/parse_cmdline.js').parse_cmdline;

function help()
{
	console.log("Usage: initialize_PKM <options>");
	console.log("");
	console.log("Initialize PKM: Create the PKM management database, then create a PKM administrator in PKM management database.");
	console.log("");
	console.log("See 'pkm_config.json' for default settings.");
	console.log("");
	console.log("");
	console.log("Options:");
	console.log("\t--help                          display this help and exit");
	console.log("\t--version                       output version information and exit");
	console.log("\t--superuser=<name>              MongoDB \"superuser\" that can create a MongoDB database user with root privileges");
	console.log("\t--superuser-auth-db=<database>  MongoDB authentication database for superuser (default: 'admin')");
	console.log("\t--admin=<name>                  MongoDB PKM administrator that will be created");
	console.log("\t--host=<host:port>              MongoDB host (default: '" + PKM.global_config.db_host + "')");
	console.log("\t--pkm-db=<database>             MongoDB PKM management database (default: '" + PKM.global_config.pkm_db + "')");
	console.log("\t--first-name=<first name>       PKM administrator's first name");
	console.log("\t--last-name=<last name>         PKM administrator's last name");
	console.log("\t--email=<email address>         PKM administrator's email address");
	console.log("\t--phone=<phone number>          PKM administrator's phone number");
	console.log("Examples:");
	console.log("");
	console.log("initialize_PKM --superuser=admin --superuser-auth-db=admin --admin=admin --pkm-db=users");
	console.log("");
	console.log("Report bugs to <" + PKM.info.contact.email + ">");
	console.log("");
	console.log(PKM.copyright_notice());
}

parse_cmdline(['help', 'version', 'superuser', 'host', 'superuser-auth-db', 'admin', 'pkm-db', 'first-name', 'last-name', 'email', 'phone', 'debug!']).then((cmdline) =>
{
	if(cmdline.options['help']) { help(); process.exit(0); }
	if(cmdline.options['version']) { console.log(PKM.info.version); process.exit(0); }
	const superuser_name = cmdline.options['superuser'];
	const host = cmdline.options['host'];
	const debug = cmdline.options['debug'];
	const pkm_admin_user_name = cmdline.options['admin'];
	const superuser_auth_db = cmdline.options['superuser-auth-db'] || 'admin';
	const pkm_db = cmdline.options['pkm-db'];
	const pkm_admin_user_first_name = cmdline.options['first-name'];
	const pkm_admin_user_last_name = cmdline.options['last-name'];
	const pkm_admin_user_email = cmdline.options['email'];
	const pkm_admin_user_phone = cmdline.options['phone'];
	if(!superuser_name) { console.log("No superuser specified"); help(); process.exit(1); }
	if(!pkm_admin_user_name) { console.log("No PKM administrator specified"); help(); process.exit(1); }
	if(cmdline.args.length) { console.log("No arguments after options are expected"); help(); process.exit(1); }
	
	const config = {
		db_host : host,
		pkm_db : pkm_db,
		debug : debug
	};
	
	password_reader.read_password(superuser_name + '@' + superuser_auth_db + '\'s password:').then((superuser_password) =>
	{
		password_reader.read_password(pkm_admin_user_name + '@' + (pkm_db ? pkm_db : PKM.global_config.pkm_db) + '\'s new password:').then((pkm_admin_user_password) =>
		{
			const User = require('../core/user');
			const pkm_admin_user = new User(
				pkm_admin_user_name,        // user's name
				pkm_admin_user_password,    // user's password
				pkm_admin_user_first_name,  // first name
				pkm_admin_user_last_name,   // last name
				pkm_admin_user_email,       // email
				pkm_admin_user_phone,       // phone
			);
			PKM.initialize(superuser_name, superuser_auth_db, superuser_password, pkm_admin_user, config).then(() =>
			{
				process.exit(0);
			}).catch((err) =>
			{
				console.error(err);
				process.exit(1);
			});
		}).catch((err) =>
		{
			console.error(err);
			process.exit(1);
		});
	}).catch((err) =>
	{
		console.error(err);
		process.exit(1);
	});
}).catch((err) =>
{
	// error on the command line
	console.error(err);
	help();
	process.exit(1);
});
