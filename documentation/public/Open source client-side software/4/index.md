# 4 Users management

PKM has a user management which leverages on MongoDB user management.

When using the REST API, the schema of a PKM user is the following:

	{
	  "$schema": "http://json-schema.org/draft-07/schema#",
	  "$id": "https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/api/pkm-user-schema.json",
	  "title": "PKM User JSON schema",
	  "description": "Data model for a user in PKM",
	  "type": "object",
	  "properties": {
	    "name": {
	      "type": "string",
	      "description": "user's name"
	    },
	    "password": {
	      "type": "string",
	      "description": "user's password"
	    },
	    "first_name": {
	      "type": "string",
	      "description": "first name"
	    },
	    "last_name": {
	      "type": "string",
	      "description": "last name"
	    },
	    "email": {
	      "type": "string",
	      "description": "email address"
	    },
	    "phone": {
	      "type": "string",
	      "description": "phone number"
	    },
	    "roles": {
	      "type": "array",
	      "items": {
	        "type": "object",
	        "properties": {
	          "db": {
	            "type": "string",
	            "description": "database name"
	          },
	          "role": {
	            "type": "string",
	            "description": "role name",
	            "enum": [
	              "Owner",
	              "Developer",
	              "Reviewer",
	              "Maintainer"
	            ]
	          }
	        },
	        "required": [
	          "db",
	          "role"
	        ]
	      }
	    },
	    "git_user_credentials": {
	      "type": "array",
	      "items": {
	        "$ref": "pkm-git-user-credential-schema.json"
	      }
	    }
	  },
	  "required": [
	    "name"
	  ]
	}

The schema of a Git credential within a PKM user is the following:

	{
	  "$schema": "http://json-schema.org/draft-07/schema#",
	  "$id": "https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/api/pkm-git-user-credential-schema.json",
	  "title": "PKM Git User's credential JSON schema",
	  "description": "Data model for Git User's credential in PKM",
	  "type": "object",
	  "properties": {
	    "git_remote_url": {
	      "type": "string",
	      "description": "Git remote URL"
	    },
	    "git_user_name": {
	      "type": "string",
	      "description": "Git user's name"
	    },
	    "git_password": {
	      "type": "string",
	      "description": "Git password (HTTPS password/token or SSH passphrase to unlock SSH private key)"
	    },
	    "git_ssh_private_key": {
	      "type": "string",
	      "description": "Git SSH private key"
	    }
	  },
	  "required": [
	    "git_remote_url"
	  ]
	}

A PKM user has an set of roles in property `roles`, which entries are pair of role name and database (i.e. project) name.
A PKM user with a role in a project is a member.
A PKM administrator is a PKM user with role `'root'` in MongoDB database `'admin'`.
Only PKM administrators can manage users and create or delete projects.

The PKM provides with the following operations for managing users:

**Create a new PKM user** (REST API 🔐, see UserApi.postUser in SDK):

	POST /user
	{
		"name": "garfield",
		"password": "12345678"
	}

This operation creates a new PKM user with given user informations.

**Create a new PKM user** (CLI):

	$ create_user --admin=admin garfield

Create a new PKM user with given user informations.

**Create a new PKM user or update an existing PKM user** (REST API 🔐, see UserApi.putUser in SDK):

	PUT /user
	{
		"name": "garfield",
		"email": "garfield@catcorp.com"
	}

This operation creates a new PKM user or updates an existing PKM user with given user informations.

**Delete a PKM user** (REST API 🔐, see UserApi.deleteUser in SDK)

	DELETE /user/{userName}

This operation deletes a PKM user with a given user's name

There are some more operations related to users in the legacy command line interface (CLI):

* update_user_role
* update_user_password
* create_user
* drop_user
* drop_all_users

The writer do not recommend the use of these legacy commands.
