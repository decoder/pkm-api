# A Appendix

## A.1 REST API documentations <a id="1"></a>

The following REST API documentations (generated with OpenAPI generator) are available online:

* PKM: [https://decoder.ow2.io/pkm-api/public/pkm_restful_api/index.html](https://decoder.ow2.io/pkm-api/public/pkm_restful_api/index.html)
* Frama-C for PKM: [https://decoder.ow2.io/pkm-api/public/frama_c_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/frama_c_restful_api_for_pkm/index.html)
* Frama-Clang for PKM: [https://decoder.ow2.io/pkm-api/public/frama_clang_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/frama_clang_restful_api_for_pkm/index.html)
* ASFM for PKM: [https://decoder.ow2.io/pkm-api/public/asfm_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/asfm_restful_api_for_pkm/index.html)
* UNISIM Excavator for PKM: [https://decoder.ow2.io/pkm-api/public/unisim_excavator_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/unisim_excavator_restful_api_for_pkm/index.html)

## A.2 Command Line Interface (CLI) <a id="2"></a>

**create_all_collections**

	Usage: create_all_collections <options>
	
	Create all predefined collections in a database.
	The predefined collections are: Annotations, RawSourcecode, RawUML, sourcecodeC, sourcecodeCPP, sourcecodeJava, 
	annotationsACSL, annotationsACSLPP, annotationsJML, commentsC, commentsCPP, commentsjava, RawDocumentation, 
	Documentation, GraphicalDocumentation, UMLClasses, UMLStateMachines, TESTARStateModels, TESTARTestResults, Logs, 
	TraceabilityMatrix, Project, CompileCommands, CVEList, RawBinaries, GitWorkingTrees, Tools, Invocations, 
	TESTARSettings, MethodologyStatus, Reviews.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	create_all_collections --admin=admin --db=mydb
	

**create_all_roles**

	Usage: create_all_roles <options>
	
	Create all predefined user roles in a database.
	The predefined roles are: Owner, Developer, Reviewer, Maintainer.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	create_all_roles --admin=admin --db=mydb
	

**create_collection**

	Usage: create_collection <options> <collection>
	
	Create a collection in a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	create_collection --admin=admin --db=mydb Sourcecode
	

**create_db**

	Usage: create_db <options>
	
	Create a database with predefined collections and roles.
	The predefined collections are: Annotations, RawSourcecode, RawUML, sourcecodeC, sourcecodeCPP, sourcecodeJava, 
	annotationsACSL, annotationsACSLPP, annotationsJML, commentsC, commentsCPP, commentsjava, RawDocumentation, 
	Documentation, GraphicalDocumentation, UMLClasses, UMLStateMachines, TESTARStateModels, TESTARTestResults, Logs, 
	TraceabilityMatrix, Project, CompileCommands, CVEList, RawBinaries, GitWorkingTrees, Tools, Invocations, 
	TESTARSettings, MethodologyStatus, Reviews.
	
	The predefined roles are: Owner, Developer, Reviewer, Maintainer.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	create_db --admin=admin --db=mydb
	

**create_project**

	Usage: create_project <options>
	
	Create an empty PKM project stored in a database with the same name.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--owner=<user>          PKM user that will own the project (default: same as admin)
		--project=<project>     PKM project name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	create_project --admin=admin --project=myproject --owner=garfield
	

**create_role**

	Usage: create_role <options> [actions_list]
	
	Create a role in a database.
	See 'https://docs.mongodb.com/v3.4/reference/privilege-actions/' for available actions.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--role=<role>           Role name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	create_role --admin=bob --role=Developer --db=mydb update
	

**create_user**

	Usage: create_user <options> <user>
	
	Create a user in PKM management database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                     display this help and exit
		--version                  output version information and exit
		--admin=<name>             PKM administrator
		--host=<host:port>         MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>        MongoDB PKM management database name (default: 'pkm')
		--first-name=<first name>  User's first name
		--last-name=<last name>    User's last name
		--email=<email address>    User's email address
		--phone=<phone number>     User's phone number
	
	Examples:
	
	create_user --admin=admin garfield
	

**delete_compile_commands**

	Usage: delete_compile_commands <options> [<filenames>]
	
	Delete the compile commands associated to some files in a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	delete_compile_commands --user=garfield --db=mydb example/vector2.c
	

**delete_doc_files**

	Usage: delete_doc_files <options> [<file_names>]
	
	Delete Documentation files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_doc_files --user=garfield --db=mydb doc.docx
	

**delete_docs**

	Usage: delete_docs <options> [<names>]
	
	Delete Documentation documents.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_docs --user=garfield --db=mydb vector
	

**delete_executable_binary_files**

	Usage: delete_executable_binary_files <options> [<file_names>]
	
	Delete Executable Binary files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_executable_binary_files --user=garfield --db=mydb executable-binary.elf
	

**delete_project**

	Usage: delete_project <options>
	
	Delete a PKM project stored in a database with the same name.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--project=<project>     PKM project name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_project --admin=admin --project=myproject
	

**delete_source_files**

	Usage: delete_source_files <options> [<file_names>]
	
	Delete source code files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_source_files --user=garfield --db=mydb vector2.c
	

**delete_uml_class_diagrams**

	Usage: delete_uml_class_diagrams <options> [<names>]
	
	Delete UML class diagrams.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_uml_class_diagrams --user=garfield --db=mydb vector
	

**delete_uml_files**

	Usage: delete_uml_files <options> [<file_names>]
	
	Delete UML files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_uml_files --user=garfield --db=mydb vector2.uml
	

**delete_uml_state_machines**

	Usage: delete_uml_state_machines <options> [<names>]
	
	Delete UML state machines.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	delete_uml_state_machines --user=garfield --db=mydb vector
	

**drop_all_collections**

	Usage: drop_all_collections <options>
	
	Drop all predefined collections in a database.
	The predefined collections are: Annotations, RawSourcecode, RawUML, sourcecodeC, sourcecodeCPP, sourcecodeJava, 
	annotationsACSL, annotationsACSLPP, annotationsJML, commentsC, commentsCPP, commentsjava, RawDocumentation, 
	Documentation, GraphicalDocumentation, UMLClasses, UMLStateMachines, TESTARStateModels, TESTARTestResults, Logs, 
	TraceabilityMatrix, Project, CompileCommands, CVEList, RawBinaries, GitWorkingTrees, Tools, Invocations, 
	TESTARSettings, MethodologyStatus, Reviews.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	drop_all_collections --admin=admin --db=mydb
	

**drop_all_roles**

	Usage: drop_all_roles <options>
	
	Drop all roles in a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	drop_all_roles --admin=admin --db=mydb
	

**drop_all_users**

	Usage: drop_all_users <options>
	
	Drop all users from a database.
	
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	drop_all_users --admin=admin
	

**drop_collection**

	Usage: drop_collection <options> <collection>
	
	Drop a collection from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	drop_collection --admin=admin --db=mydb Sourcecode
	

**drop_db**

	Usage: drop_db <options>
	
	Drop a database (all collections and roles).
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	drop_db --admin=admin --db=mydb
	

**drop_role**

	Usage: drop_role <options> [actions_list]
	
	Drop a role from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--admin=<name>          PKM administrator
		--role=<role>           Role name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	drop_role --admin=bob --role=Developer --db=mydb update
	

**drop_user**

	Usage: drop_user <options> <user>
	
	Drop a user from PKM management database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                     display this help and exit
		--version                  output version information and exit
		--admin=<name>             PKM administrator
		--host=<host:port>         MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>        MongoDB PKM management database name (default: 'pkm')
	
	Examples:
	
	drop_user --admin=admin garfield
	

**finalize_PKM**

	Usage: finalize_PKM <options>
	
	Finalize PKM: Drop all PKM projects and the PKM management database, then drop all users in PKM management database.
	
	See 'pkm_config.json' for default settings.
	
	
	Options:
		--help                         display this help and exit
		--version                      output version information and exit
		--superuser=<name>             MongoDB "superuser" that can drop roles and users in MongoDB PKM management 
	database
		--superuser-auth-db<database>  MongoDB authentication database for superuser (default: 'admin')
		--host=<host:port>             MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>            MongoDB PKM management database (default: 'pkm')
	Examples:
	
	finalize_PKM --superuser=admin --superuser-auth-db=admin --admin=admin --pkm-db=users
	

**find_in_cpp_class**

	Usage: find_in_cpp_class <options> <class name> field [field name]
	or: find_in_cpp_class <options> <class name> method [method name]
	
	Find fields or methods from a C++ class in a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	find_in_cpp_class --user=garfield --db=mydb vector method add
	

**find_in_cpp_source**

	Usage: find_in_cpp_source <options>
	
	Find artefacts in C++ source code of a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--kind=<kind|/regex/>   kind of C++ artefacts as either an exact value or a /regular expression/ (e.g. 
	'/^class$|^struct$/')
		--id=<id>               numeric identifier of C++ artefact (e.g. 1234)
		--path=<path|/regex/>   abstract path of C++ artefacts as either an exact value or a /regular expression/ (e.g. 
	'/^std::log[fl]?$/')
		--name=<name|/regex/>   name of C++ artefacts as either an exact value or a /regular expression/ (e.g. 
	'/^log[fl]?$/')
	
	Examples:
	
	find_in_cpp_source --user=garfield --db=mydb --kind=function --path=std::max
	

**find_in_c_source**

	Usage: find_in_c_source <options> function [function_name]
	or: find_in_c_source <options> variable [variable_name]
	or: find_in_c_source <options> type [comp/enum_name]
	or: find_in_c_source <options> file [file_name]
	or: find_in_c_source <options> dir [dir_name]
	
	Find in source code of a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	find_in_c_source --user=garfield --db=mydb function add_vector
	

**find_in_doc**

	Usage: find_in_doc <options>
	
	Find in documentation of a database matching document name, unit name, and class name and invariant, field, method, 
	type or macro name.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                    display this help and exit
		--version                 output version information and exit
		--user=<name>             MongoDB user name
		--db=<database>           MongoDB database name
		--host=<host:port>        MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>       MongoDB PKM management database (default: 'pkm')
		--debug                   enable debugging messages (default: disabled)
		--doc=<name regex>        regular expression for doc name
		--unit=<name regex>       regular expression for unit name
		--class=<name regex>      regular expression for class name
		--invariant=<name regex>  regular expression for invariant name
		--field=<name regex>      regular expression for field name
		--method=<name regex>     regular expression for method name
		--type=<name regex>       regular expression for type name
		--macro=<name regex>      regular expression for macro name
		--constant=<name regex>   regular expression for constant name
		--member=<name regex>     regular expression for member name (either invariant, field, method, type, macro, or 
	constant name)
	
	Examples:
	
	find_in_doc --user=garfield --db=mydb --class=vector --field=
	
	find_in_doc --user=garfield --db=mydb --class=vector --method=add_vector
	

**find_in_java_class**

	Usage: find_in_java_class <options> <class name> field [field_name]
	or: find_in_java_class <options> <class name> method [method_name]
	
	Find fields or methods from a Java class in a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	find_in_java_class --user=garfield --db=mydb vector method
	

**find_in_java_source**

	Usage: find_in_java_source <options> kind
	where kind must be 'class'
	returns the list of Java classes in the source code.
	
	Find in Java source code of a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	find_in_java_source --user=garfield --db=mydb class
	

**find_in_uml_class_diagrams**

	Usage: find_in_uml_class_diagrams <options> <className> operations|attributes
	or: find_in_uml_class_diagrams <options> <className> <operationName> parameters|returnType
	or: find_in_uml_class_diagrams <options> <className> <attributeName> attributeType
	or: find_in_uml_class_diagrams <options> <className> attributes <type>
	
	Find in UML class diagrams of a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	find_in_uml_class_diagrams --user=garfield --db=mydb vector attributes
	
	find_in_uml_class_diagrams --user=garfield --db=mydb vector operations
	
	find_in_uml_class_diagrams --user=garfield --db=mydb vector add_vector parameters
	
	find_in_uml_class_diagrams --user=garfield --db=mydb vector add_vector returnType
	
	find_in_uml_class_diagrams --user=garfield --db=mydb vector attributes int
	

**get_c_annotations**

	Usage: get_c_annotations <options> [<source_file_names>]
	
	Get C annotations from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--merge=<flag>          enable/disable merging according to file (default: true)
	
	Examples:
	
	get_c_annotations --user=garfield --db=mydb vector2.c
	

**get_c_comments**

	Usage: get_c_comments <options> [<source_file_names>]
	
	Get C comments from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--merge=<flag>          enable/disable merging according to file (default: true)
	
	Examples:
	
	get_c_comments --user=garfield --db=mydb --host=localhost:27071 vector2.c
	

**get_compile_commands**

	Usage: get_compile_commands <options> [<filenames>]
	
	Get compile commands associated to some files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_compile_commands --user=garfield --db=mydb example/vector2.c
	

**get_cpp_annotations**

	Usage: get_cpp_annotations <options> [<source_file_names>]
	
	Get C++ annotations from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--merge=<flag>          enable/disable merging according to file (default: true)
	
	Examples:
	
	get_cpp_annotations --user=garfield --db=mydb hello.cpp
	

**get_cpp_comments**

	Usage: get_cpp_comments <options> [<source_file_names>]
	
	Get C++ comments from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--merge=<flag>          enable/disable merging according to file (default: true)
	
	Examples:
	
	get_cpp_comments --user=garfield --db=mydb --host=localhost:27071 hello.cpp
	

**get_cpp_source_codes**

	Usage: get_cpp_source_codes <options> [<source_file_names>]
	
	Get C++ source codes (JSON AST) from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--merge=<flag>          enable/disable merging according to file (default: true)
	
	Examples:
	
	get_cpp_source_codes --user=garfield --db=mydb hello.cpp
	

**get_c_source_codes**

	Usage: get_c_source_codes <options> [<source_file_names>]
	
	Get C source codes (JSON AST) from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--merge=<flag>          enable/disable merging according to file (default: true)
	
	Examples:
	
	get_c_source_codes --user=garfield --db=mydb vector2.c
	

**get_doc_files**

	Usage: get_doc_files <options> [<doc_file_names>]
	
	Get Documentation files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_doc_files --user=garfield --db=mydb doc.docx
	

**get_docs**

	Usage: get_docs <options> [<names>]
	
	Get Documentation documents from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_docs --user=garfield --db=mydb vector2
	

**get_executable_binary_files**

	Usage: get_executable_binary_files <options> [<executable_binary_file_names>]
	
	Get Executable Binary files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_executable_binary_files --user=garfield --db=mydb executable-binary.elf
	

**get_files**

	Usage: get_files <options> [<file_names>]
	
	Get files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--directory=<directory> path relative to root directory of directory to get
	
	Examples:
	
	get_files --user=garfield --db=mydb --directory=repository
	

**get_invocations**

	Usage: get_invocations <options> [<invocations>]
	
	Get Invocation documents from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_invocations --user=garfield --db=mydb invocation_1
	

**get_logs**

	Usage: get_logs <options> [<logs>]
	
	Get log documents from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
		--abbrev                enabled abbreviated output (default: false)
	
	Examples:
	
	get_logs --user=garfield --db=mydb log_1
	

**get_project**

	Usage: get_project <options>
	
	Get the informations of a PKM project.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--project=<project>     PKM project name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	get_project --user=garfield --project=myproject
	

**get_projects**

	Usage: get_projects <options>
	
	Get the informations of all PKM projects.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	get_project --user=garfield --project=myproject
	

**get_source_files**

	Usage: get_source_files <options> [<source_file_names>]
	
	Get source code files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_source_files --user=garfield --db=mydb vector2.c
	

**get_tools**

	Usage: get_tools <options> [<tools>]
	
	Get Tool documents from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_tools --user=garfield --db=mydb tool_1
	

**get_uml_class_diagrams**

	Usage: get_uml_class_diagrams <options> [<names>]
	
	Get UML class diagrams from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_uml_class_diagrams --user=garfield --db=mydb vector2
	

**get_uml_files**

	Usage: get_uml_files <options> [<uml_file_names>]
	
	Get UML files from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_uml_files --user=garfield --db=mydb vector2.uml
	

**get_uml_state_machines**

	Usage: get_uml_state_machines <options> [<names>]
	
	Get UML state machines from a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	get_uml_state_machines --user=garfield --db=mydb vector2
	

**get_user_projects**

	Usage: get_user_projects <options>
	
	Get the informations of PKM user's projects.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	get_user_projects --user=garfield
	

**get_users**

	Usage: get_users <options>
	
	Get all PKM users.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	get_users --user=admin
	

**initialize_PKM**

	Usage: initialize_PKM <options>
	
	Initialize PKM: Create the PKM management database, then create a PKM administrator in PKM management database.
	
	See 'pkm_config.json' for default settings.
	
	
	Options:
		--help                          display this help and exit
		--version                       output version information and exit
		--superuser=<name>              MongoDB "superuser" that can create a MongoDB database user with root privileges
		--superuser-auth-db=<database>  MongoDB authentication database for superuser (default: 'admin')
		--admin=<name>                  MongoDB PKM administrator that will be created
		--host=<host:port>              MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>             MongoDB PKM management database (default: 'pkm')
		--first-name=<first name>       PKM administrator's first name
		--last-name=<last name>         PKM administrator's last name
		--email=<email address>         PKM administrator's email address
		--phone=<phone number>          PKM administrator's phone number
	Examples:
	
	initialize_PKM --superuser=admin --superuser-auth-db=admin --admin=admin --pkm-db=users
	

**insert_compile_commands**

	Usage: insert_compile_commands <options> <compile_commands_filenames>
	
	Insert compile commands from a file which format complies with JSON Compilation Database Format Specification into a 
	database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_compile_commands --user=garfield --db=mydb compile_commands.json
	

**insert_doc_files**

	Usage: insert_doc_files <options> <doc_file_names>
	
	Insert Documentation files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--format=<format>       either 'binary' or 'text' (default: 'text')
		--encoding=<encoding>   text encoding (default: utf8)
	
	Examples:
	
	insert_doc_files --user=garfield --db=mydb --format=binary doc.docx
	

**insert_executable_binary_files**

	Usage: insert_executable_binary_files <options> <executable_binary_file_names>
	
	Insert Executable Binary files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_executable_binary_files --user=garfield --db=mydb executable-binary.elf
	

**insert_files**

	Usage: insert_files <options> [file_names]
	
	Update files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--directory=<directory> path relative to root directory of directory to insert into the database
	
	Examples:
	
	insert_files --user=garfield --db=mydb --directory=repository
	

**insert_source_files**

	Usage: insert_source_files <options> <source_file_names>
	
	Insert source code files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_source_files --user=garfield --db=mydb vector2.c
	

**insert_testar_state_model_files**

	Usage: insert_testar_state_model_files <options> <file_names>
	
	Insert TESTAR state models into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_testar_state_model_files.bash --user=garfield --db=mydb data.json
	

**insert_testar_test_results_files**

	Usage: insert_testar_test_result_files <options> <file_names>
	
	Insert TESTAR test results into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_testar_test_result_files.bash --user=garfield --db=mydb data.json
	

**insert_uml_class_diagram_files**

	Usage: insert_uml_class_diagram_files <options> <file_names>
	
	Insert UML class diagrams into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_uml_class_diagram_files --user=garfield --db=mydb vector2.json
	

**insert_uml_files**

	Usage: insert_uml_files <options> <uml_file_names>
	
	Insert UML files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_uml_files --user=garfield --db=mydb vector2.uml
	

**insert_uml_state_machine_files**

	Usage: insert_uml_state_machine_files <options> <file_names>
	
	Insert UML state machines into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	insert_uml_state_machine_files --user=garfield --db=mydb vector2.json
	

**run_asfm_to_doc**

	Usage: run_asfm_to_doc <options> <doc name>
	
	Run asfm_to_doc conversion tool on an ASFM documentation from a database and update documentation files.
	Logs are updated in the database.
	
	Options:
		--help                        display this help and exit
		--version                     output version information and exit
		--user=<name>                 MongoDB user name
		--db=<database>               MongoDB database name
		--host=<host:port>            MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>           MongoDB PKM management database (default: 'pkm')
	Examples:
	
	run_asfm_to_doc --user=garfield --db=mydb 'The Graphic library'
	

**run_doc_to_asfm**

	Usage: run_doc_to_asfm <options> <file_name>
	
	Run doc_to_asfm conversion tool on a .docx file from a database and update documentation.
	Logs are updated in the database.
	
	Options:
		--help                        display this help and exit
		--version                     output version information and exit
		--user=<name>                 MongoDB user name
		--db=<database>               MongoDB database name
		--host=<host:port>            MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>           MongoDB PKM management database (default: 'pkm')
	Examples:
	
	run_doc_to_asfm --user=garfield --db=mydb vector2.docx
	

**run_frama_c**

	Usage: run_frama_c <options> <source_file_names>
	
	Run Frama-C analysis tool on C source code files from a database and, if parser is enabled, update C source code (AST),
	C annotations and C comments in that database. Logs are updated in the database.
	
	Options:
		--help                        display this help and exit
		--version                     output version information and exit
		--user=<name>                 MongoDB user name
		--db=<database>               MongoDB database name
		--host=<host:port>            MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>           MongoDB PKM management database (default: 'pkm')
		--parser                      Enable Frama-C parser (default: implicitely enabled when neither EVA nor WP are 
	enabled)
		--eva                         Enable Frama-C EVA
		--wp                          Enable Frama-C WP
		--main=<main function>        Main function for EVA (default: 'main')
		--wp-fct=<function>           Function for WP (this option can occur several times to provide several functions)
	Examples:
	
	run_frama_c --user=garfield --db=mydb vector2.c
	

**run_frama_clang**

	Usage: run_frama_clang <options> <source_file_names>
	
	Run Frama-Clang analysis tool on C++ source code files from a database and update C++ source code (AST),
	C++ annotations and C++ comments in that database. Logs are updated in the database.
	
	Options:
		--help                        display this help and exit
		--version                     output version information and exit
		--user=<name>                 MongoDB user name
		--db=<database>               MongoDB database name
		--host=<host:port>            MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>           MongoDB PKM management database (default: 'pkm')
	Examples:
	
	run_frama_clang --user=garfield --db=mydb hello.cpp
	

**update_compile_commands**

	Usage: update_compile_commands <options> <compile_commands_filenames>
	
	Update compile commands from a file which format complies with JSON Compilation Database Format Specification into a 
	database
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	update_compile_commands --user=garfield --db=mydb compile_commands.json
	

**update_doc_files**

	Usage: update_doc_files <options> [file_names]
	
	Update Documentation files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--format=<format>       either 'binary' or 'text' (default: 'text')
		--encoding=<encoding>   text encoding (default: utf8)
	
	Examples:
	
	update_doc_files --user=garfield --db=mydb --format=binary doc.docx
	

**update_executable_binary_files**

	Usage: update_executable_binary_files <options> [file_names]
	
	Update Executable Binary files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	update_executable_binary_files --user=garfield --db=mydb executable-binary.elf
	

**update_files**

	Usage: update_files <options> [file_names]
	
	Update files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--directory=<directory> path relative to root directory of directory to update into the database
	
	Examples:
	
	update_files --user=garfield --db=mydb --directory=repository
	

**update_source_files**

	Usage: update_source_files <options> [file_names]
	
	Update source code files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	update_source_files --user=garfield --db=mydb vector2.c
	

**update_uml_class_diagram_files**

	Usage: update_uml_class_diagram_files <options> <file_names>
	
	Update UML class diagrams into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	update_uml_class_diagram_files --user=garfield --db=mydb vector2.json
	

**update_uml_files**

	Usage: update_uml_files <options> [file_names]
	
	Update UML files into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	update_uml_files --user=garfield --db=mydb vector2.uml
	

**update_uml_state_machine_files**

	Usage: update_uml_state_machine_files <options> <file_names>
	
	Update UML state machines into a database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--user=<name>           MongoDB user name
		--db=<database>         MongoDB database name
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
		--root=<directory>      root directory (default: value of environment variable DB_ROOT)
		--encoding=<encoding>   encoding (default: utf8)
	
	Examples:
	
	update_uml_state_machine_files --user=garfield --db=mydb vector2.json
	

**update_user_password**

	Usage: update_user_password <options> [user]
	
	Update user's password.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                  display this help and exit
		--version               output version information and exit
		--host=<host:port>      MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>     MongoDB PKM management database (default: 'pkm')
		--debug                 enable debugging messages (default: disabled)
	
	Examples:
	
	update_user_password garfield
	

**update_user_role**

	Usage: update_user_role <options> <command> <user>
	
	Create a user in PKM management database.
	
	See 'pkm_config.json' for default settings.
	
	Options:
		--help                     display this help and exit
		--version                  output version information and exit
		--admin=<name>             PKM administrator
		--role=<role>              Role
		--db=<database>            MongoDB database name
		--host=<host:port>         MongoDB host (default: 'pkm-api_mongodb_1:27017')
		--pkm-db=<database>        MongoDB PKM management database name (default: 'pkm')
	
	Examples:
	
	update_user_role --admin=admin --db=mydb --role=Developer grant garfield
	

