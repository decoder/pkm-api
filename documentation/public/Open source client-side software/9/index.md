# 9 Querying artefacts

This chapter present all the queries that the PKM supports.

In the PKM REST API, this corresponds to the HTTP GET requests.
These operations can return one the following HTTP status codes:

* 200: The operation is successful.
* 400: Bad request. The request has invalid request parameters in the URL.
* 401: The operation (labeled with a 🔐 symbol in this document) requires prior authentication (see Section 3).
* 403: Forbidden operation. The logged in user does not have sufficient rights to perform this operation.
* 404: Not found. The requested resource does not exist.
* 500: Internal server error. An error of this type is most likely due to a bug in the PKM server.

To lower the response footprint of GET requests, the PKM implements a windowing mechanism.
Indeed, the GET requests that return a bare list have support for a 1D window while those returning a 2D matrix have support for a 2D window.
There are some query parameters in the GET request (after the question mark in request URL) to configure that windowing mechanism.

The 1D window query parameters are:

* `skip`: starting index (i.e. number of elements to skip)
* `limit`: window length (i.e. maximum number of elements in the response)

The 2D window query parameters are:

* `fromRow`: starting row number (i.e. number of rows to skip)
* `fromColumn`: starting column number (i.e. number of columns to skip)
* `toRow`: last row number
* `toColumn`: last column number

Note that only the traceability matrix currently uses a 2D window.

## 9.1 Files <a id="1"></a>

**Download a file** (REST API 🔐, see IOApi.download in SDK):

	GET /io/{dbName}/{filename}?key=…

This operation returns in the response body the requested file content.
Unlike other PKM operations, the client shall pass the PKM access key in the query part of URL using the parameter named `key`.
This operation is convenient for directly downloading a file from the PKM through HTTP(S) with the right content type (MIME type), so that a web browser, which has support for the content type, can seamlessly display it.

**Get all files** (REST API 🔐, see FilesApi.getFiles in SDK):

	GET /files/{dbName}?abbrev=…

This operation returns in the response body a list of all files in the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only file metadata and suppresses the file content from the response.

**Get a file** (REST API 🔐, see FilesApi.getFile in SDK):

	GET /files/{dbName}/{filename}?abbrev=…

This operation returns in the response body a list containing only the file with the given name `{filename}` of the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only file metadata and suppresses the file content from the response.

**Get files** (CLI):

	$ get_files --user=garfield --db=OpenCV --root=$(pwd) 'OpenCV/modules/core/src/matrix_sparse.cpp'

**Get files of a directory** (CLI):

	$ get_files --user=garfield --db=OpenCV --root=$(pwd) --directory=repository
	
### 9.1.1 Source codes <a id="1.1"></a>

**Get source code files** (REST API 🔐, see CodeApi.getRawSourceCodes in SDK):

	GET /code/rawsourcecode/{dbName}?abbrev=…

This operation returns in the response body a list of all source code files in the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only source code file metadata and suppresses the source code file content from the response.

**Get a source code file** (REST API 🔐, see CodeApi.getRawSourceCode in SDK):

	GET /code/rawsourcecode/{dbName}/{filename}?abbrev=…

This operation returns in the response body a list containing only the source code file with the given name `{filename}` of the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only source code file metadata and suppresses the source code file content from the response.

**Get source code files** (CLI):

	$ get_source_files --user=garfield --db=myproject main.c

### 9.1.2 UML <a id="1.2"></a>

**Get UML2 files** (REST API 🔐, see UMLApi.getRawUMLs in SDK):

	GET /uml/rawuml/{dbName}?abbrev=…

This operation returns in the response body a list of all UML files in the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only UML file metadata and suppresses the UML file content from the response.

**Get a UML2 file** (REST API 🔐, see UMLApi.getRawUML in SDK):

	GET /uml/rawuml/{dbName}/{filename}?abbrev=…

This operation returns in the response body a list containing only the UML file with the given name `{filename}` of the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only UML file metadata and suppresses the UML file content from the response.

**Get UML2 files** (CLI):

	$ get_uml_files --user=garfield --db=myproject model.uml

### 9.1.3 Documentation <a id="1.3"></a>

**Get documentation files** (REST API 🔐, see DocApi.getRawDocs in SDK):

	GET /doc/rawdoc/{dbName}?abbrev=…

This operation returns in the response body a list of all documentation files in the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only documentation file metadata and suppresses the documentation file content from the response.

**Get a documentation file** (REST API 🔐, see DocApi.getRawDoc in SDK):

	GET /doc/rawdoc/{dbName}/{filename}?abbrev=…

This operation returns in the response body a list containing only the documentation file with the given name `{filename}` of the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only documentation file metadata and suppresses the documentation file content from the response.

**Get documentation files** (CLI):

	$ get_doc_files --user=garfield --db=myproject doc.docx

### 9.1.4 Executable binaries <a id="1.4"></a>

**Get executable binary files** (REST API 🔐, see ExecutableBinaryApi.getExecutableBinaries in SDK):

	GET /bin/executable/{dbName}?abbrev=…

This operation returns in the response body a list of all executable binary files in the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only executable binary file metadata and suppresses the executable binary file content from the response.

**Get an executable binary file** (REST API 🔐, see ExecutableBinaryApi.getExecutableBinary in SDK):

	GET /bin/executable/{dbName}/{filename}?abbrev=…

This operation returns in the response body a list containing only the executable binary file with the given name `{filename}` of the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only executable binary file metadata and suppresses the executable binary file content from the response.

**Get executable binary files** (CLI):

	$ get_executable_binary_files --user=garfield --db=linux --root=$(pwd) vmlinux

## 9.2 Source code <a id="2"></a>

### 9.2.1 Abstract Syntax Trees (ASTs) <a id="2.1"></a>

**Get C source code ASTs** (REST API 🔐, see CodeApi.getCSourceCodes in SDK):

	GET /code/c/sourcecode/{dbName}

This operation returns in the response body a list of all C source code ASTs in the project with the given name `{dbName}`.

**Get C source code ASTs related to a source code file** (REST API 🔐, see CodeApi.getCSourceCodesBySourceCodeFilename in SDK):

	GET /code/c/sourcecode/{dbName}/{filename}

This operation returns in the response body a list of C source code ASTs related to the C source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get C source code ASTs** (CLI):

	$ get_c_source_codes --user=garfield --db=myproject --root=$(pwd)

**Get C source code ASTs related to source code files** (CLI):

	get_c_source_codes --user=garfield --db=myproject --root=$(pwd) main.c

**Get C functions** (REST API 🔐, see CodeApi.getCFunctions in SDK):

	GET /code/c/functions/{dbName}

This operation returns in the response body a list of the global functions in the C source codes of the project with the given name `{dbName}`.

**Get C variables** (REST API 🔐, see CodeApi.getCVariables in SDK):

	GET /code/c/variables/{dbName}

This operation returns in the response body a list of the global variables in the C source codes of the project with the given name `{dbName}`.

**Get C types** (REST API 🔐, see CodeApi.getCTypes in SDK):

	GET /code/c/types/{dbName}

This operation returns in the response body a list of the global types in the C source codes of the project with the given name `{dbName}`.

**Get a C function** (REST API 🔐, see CodeApi.getCFunctionsByName in SDK):

	GET /code/c/functions/{dbName}/{funcname}

This operation returns in the response body a list of the global functions with given name `{funcname}` in the C source codes of the project with the given name `{dbName}`.

**Get a C variable** (REST API 🔐, see CodeApi.getCVariablesByName in SDK):

	GET /code/c/variables/{dbName}/{varname}

This operation returns in the response body a list of the global variables with given name `{varname}` in the C source codes of the project with the given name `{dbName}`.

**Get a C type** (REST API 🔐, see CodeApi.getCTypesByName in SDK):

	GET /code/c/types/{dbName}/{typename}

This operation returns in the response body a list of the global types with given name `{typename}` in the C source codes of the project with the given name `{dbName}`.

**Get a C function, variable, or type** (CLI):

	$ find_in_c_source --user=garfield --db=myproject function main

**Get C++ source code ASTs** (REST API 🔐, see CodeApi.getCPPSourceCodes in SDK):

	GET /code/cpp/sourcecode/{dbName}

This operation returns in the response body a list of all C++ source code ASTs in the project with the given name `{dbName}`.

**Get C++ source code ASTs related to a source code file** (REST API 🔐, see CodeApi.getCPPSourceCodesBySourceCodeFilename):

	GET /code/cpp/sourcecode/{dbName}/{filename}

This operation returns in the response body a list of C++ source code ASTs related to the C++ source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get C++ source code ASTs** (CLI):

	$ get_cpp_source_codes --user=garfield --db=OpenCV --root=$(pwd)

**Get C++ source code ASTs related to source code files** (CLI):

	$ get_cpp_source_codes --user=garfield --db=OpenCV --root=$(pwd) 'OpenCV/modules/core/src/matrix_sparse.cpp'

**Get C++ artefacts from source code ASTs** (REST API 🔐, see CodeApi.getCPPSourceCodeArtefacts in SDK):

	GET /code/cpp/artefacts/sourcecode/{dbName}?id=…&kind=…&path=…&name=…

This operation returns in the response body a list of C++ source code artefacts (`function`, `variable`, `typedef`, `struct`, `union`, `class`, `field` or `method`) matching the query and in the project with the given name `{dbName}`.

The query has the following parameters:

* `id`: positive integer which is a numerical unique identifier of the C++ source code artefact
* `kind`: either `function`, `variable`, `typedef`, `struct`, `union`, `class`, `field`,`method`, or `member`
* `path`: simplified (without template arguments) fully qualified name (e.g. "`std::max`") of artefact
* `name`: name of artefact (e.g. "`max`")

Query parameters `kind`, `path`, and `name` accept either a regular expressions (between two `'/'`) or a bare string.
Kind `member` stands for either Kinds `field` or `method`.
The matching mechanism ignores omitted parameters or parameters with an empty value (any values match).

**Get C++ artefacts from source code ASTs** (CLI):

	$ find_in_cpp_source --user=garfield --db=OpenCV --kind=function --path=cv::SparseMat::convertTo

**Get C++ class fields** (REST API 🔐, see CodeApi.getCPPClassFields in SDK):

	GET /code/cpp/class/fields/{dbName}/{className}

This operation returns a list of fields in C++ class with given name `{className}` of the project with the given name `{dbName}`.

**Get C++ class methods** (REST API 🔐, see CodeApi.getCPPClassMethods in SDK):

	GET /code/cpp/class/methods/{dbName}/{className}

This operation returns a list of methods in C++ class with given name `{className}` of the project with the given name `{dbName}`.

**Get C++ class fields or methods** (CLI):

	$ find_in_cpp_class --user=garfield --db=mydb cv::SparseMat method

**Get Java source code ASTs** (REST API 🔐, see CodeApi.getJavaSourceCodes in SDK):

	GET /code/java/sourcecode/{dbName}

This operation returns in the response body a list of all Java source code ASTs in the project with the given name `{dbName}`.

**Get Java source code ASTs related to a source code file** (REST API 🔐, see CodeApi.getJavaSourceCodesBySourceCodeFilename in SDK):

	GET /code/java/sourcecode/{dbName}/{filename}

This operation returns in the response body a list of Java source code ASTs related to the Java source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get Java classes** (REST API 🔐, see CodeApi.getJavaClasses in SDK):

	GET /code/java/classes/{dbName}

This operation returns in the response body the list of Java classes in the project with the given name `{dbName}`.

**Get Java classes** (CLI):

	$ find_in_java_source --user=garfield --db=myproject class

**Get Java class fields** (REST API 🔐, see CodeApi.getJavaClassFields in SDK):

	GET /code/java/class/fields/{dbName}/{className}

This operation returns a list of fields in Java class with given name `{className}` of the project with the given name `{dbName}`.

**Get Java class methods** (REST API 🔐, see CodeApi.getJavaClassMethods in SDK):

	GET /code/java/class/methods/{dbName}/{className}

This operation returns a list of methods in Java class with given name `{className}` of the project with the given name `{dbName}`.

**Get Java class fields or methods** (CLI):

	$ find_in_java_class --user=garfield --db=myproject vector method

**Get source code ASTs for any language** (REST API 🔐, see CodeApi.getSourceCodes in SDK):

	GET /code/sourcecode/{dbName}

This operation returns in the response body a list of all source code ASTs in the project with the given name `{dbName}`.

**Get source code ASTs for any language related to a source code file** (REST API 🔐, see CodeApi.getSourceCodesBySourceCodeFilename in SDK):

	GET /code/sourcecode/{dbName}/{filename}

This operation returns in the response body a list of source code ASTs related to the source code file with the given name `{filename}` of the project with the given name `{dbName}`.

### 9.2.2 Comments <a id="2.2"></a>

**Get C comments** (REST API 🔐, see CodeApi.getCComments in SDK):

	GET /code/c/comments/{dbName}

This operation returns in the response body a list of all C source code comments in the project with the given name `{dbName}`.

**Get C comments related to a source code file** (REST API 🔐, see CodeApi.getCCommentsBySourceCodeFilename in SDK):

	GET /code/c/comments/{dbName}/{filename}

This operation returns in the response body a list of C source code comments related to the C source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get C comments** (CLI):

	$ get_c_comments --user=garfield --db=myproject

**Get C comments related to source code files** (CLI):

	$ get_c_comments --user=garfield --db=myproject main.c

**Get C++ comments** (REST API 🔐, see CodeApi.getCPPComments in SDK):

	GET /code/cpp/comments/{dbName}

This operation returns in the response body a list of all C++ source code comments in the project with the given name `{dbName}`.

**Get C++ comments related to a source code file** (REST API 🔐, see CodeApi.getCPPCommentsBySourceCodeFilename in SDK):

	GET /code/cpp/comments/{dbName}/{filename}

This operation returns in the response body a list of C++ source code comments related to the C++ source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get C++ comments** (CLI):

	$ get_cpp_comments --user=garfield --db=myproject --root=$(pwd)

**Get C++ comments related to source code files** (CLI):

	$ get_cpp_comments --user=garfield --db=OpenCV --root=$(pwd) 'OpenCV/modules/core/src/matrix_sparse.cpp'
	
**Get a C++ comment** (REST API 🔐, see CodeApi.getCPPCommentArtefacts in SDK):

	GET /code/cpp/artefacts/comments/{dbName}?id=…

This operation returns in the response body a list containing only the C++ comment matching the query and in the project with the given name `{dbName}`.
Query parameter `id` accepts positive integer, which is a numerical unique identifier of the C++ source code comment.
The matching mechanism ignores omitted parameters or parameters with an empty value (any values match).

**Get Java comments** (REST API 🔐, see CodeApi.getJavaComments in SDK):

	GET /code/java/comments/{dbName}

This operation returns in the response body a list of all Java source code comments in the project with the given name `{dbName}`.

**Get Java comments related to a source code file** (REST API 🔐, see CodeApi.getJavaCommentsBySourceCodeFilename in SDK):

	GET /code/java/comments/{dbName}/{filename}

This operation returns in the response body a list of Java source code comments related to the Java source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get source code comments for any language** (REST API 🔐, see CodeApi.getSourceCodeComments in SDK):

	GET /code/comments/{dbName}

This operation returns in the response body a list of all source code comments in the project with the given name `{dbName}`.

**Get source code comments for any language related to a source code file** (REST API 🔐, see CodeApi.getSourceCodeCommentsBySourceCodeFilename in SDK):

	GET /code/comments/{dbName}/{filename}

This operation returns in the response body a list of source code comments related to the source code file with the given name `{filename}` of the project with the given name `{dbName}`.

### 9.2.2 Annotations <a id="2.3"></a>

**Get C annotations** (REST API 🔐, see CodeApi.getCAnnotations in SDK):

	GET /code/c/annotations/{dbName}

This operation returns in the response body a list of all C source code annotations in the project with the given name `{dbName}`.

**Get C annotations related to a source code file** (REST API 🔐, see CodeApi.getCAnnotationsBySourceCodeFilename in SDK):

	GET /code/c/annotations/{dbName}/{filename}

This operation returns in the response body a list of C source code annotations related to the C source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get C annotations** (CLI):

	$ get_c_annotations --user=garfield --db=myproject --root=$(pwd)

**Get C annotations related to source code files** (CLI):

	$ get_c_annotations --user=garfield --db=myproject --root=$(pwd) main.c

**Get C++ annotations** (REST API 🔐, see CodeApi.getCPPAnnotations in SDK):

	GET /code/cpp/annotations/{dbName}

This operation returns in the response body a list of all C++ source code annotations in the project with the given name `{dbName}`.

**Get C++ annotations related to a source code file** (REST API 🔐, see CodeApi.getCPPAnnotationsBySourceCodeFilename in SDK):

	GET /code/cpp/annotations/{dbName}/{filename}

This operation returns in the response body a list of C++ source code annotations related to the C++ source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get C++ annotations** (CLI):

	$ get_cpp_annotations --user=garfield --db=OpenCV --root=$(pwd)

**Get C++ annotations related to source code files** (CLI):

	$ get_cpp_annotations --user=garfield --db=OpenCV --root=$(pwd) 'OpenCV/modules/core/src/matrix_sparse.cpp'

**Get a C++ annotation** (REST API 🔐, see CodeApi.getCPPAnnotationArtefacts in SDK):

	GET /code/cpp/artefacts/annotations/{dbName}?id=xxxx

This operation returns in the response body a list containing only the C++ annotation matching the query and in the project with the given name `{dbName}`.
Query parameter `id` accepts positive integer, which is a numerical unique identifier of the C++ source code annotation.
The matching mechanism ignores omitted parameters or parameters with an empty value (any values match).

**Get Java annotations** (REST API 🔐, see CodeApi.getJavaAnnotations in SDK):

	GET /code/java/annotations/{dbName}

This operation returns in the response body a list of all Java source code annotations in the project with the given name `{dbName}`.

**Get Java annotations related to a source code file** (REST API 🔐, see CodeApi.getJavaAnnotationsBySourceCodeFilename in SDK):

	GET /code/java/annotations/{dbName}/{filename}

This operation returns in the response body a list of Java source code annotations related to the Java source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get source code annotations for any language** (REST API 🔐, see CodeApi.getSourceCodeAnnotations in SDK):

	GET /code/annotations/{dbName}

This operation returns in the response body a list of all source code annotations in the project with the given name `{dbName}`.

**Get source code annotations for any language related to a source code file** (REST API 🔐, see CodeApi.getSourceCodeAnnotationsBySourceCodeFilename in SDK):

	GET /code/annotations/{dbName}/{filename}

This operation returns in the response body a list of source code annotations related to the source code file with the given name `{filename}` of the project with the given name `{dbName}`.

## 9.3 UML classes & state machines <a id="3"></a>

**Get UML2 class diagrams** (REST API 🔐, see UMLApi.getUMLClassDiagrams in SDK):

	GET /uml/uml_class/{dbName}

This operation returns in the response body a list of all UML class diagrams in the project with the given name `{dbName}`.

**Get a UML2 class diagram** (REST API 🔐, see UMLApi.getUMLClassDiagram in SDK):

	GET /uml/uml_class/{dbName}/{diagramName}

This operation returns in the response body a list containing only the UML class diagram with the given name `{diagramName}` of the project with the given name `{dbName}`.

**Get UML2 class diagrams** (CLI):

	$ get_uml_class_diagrams --user=garfield --db=myproject --root=$(pwd)

**Get some UML2 class diagrams** (CLI):

	$ get_uml_class_diagrams --user=garfield --db=myproject --root=$(pwd) 'MyClassDiagram'

**Get a UML2 class** (REST API 🔐, see UMLApi.getUMLClass in SDK):

	GET /uml/uml_class/class/{dbName}/{className}

This operation returns in the response body a list containing only the UML class with the given name `{className}` of the project with the given name `{dbName}`.

**Get a UML2 class attributes and operations** (REST API 🔐, see UMLApi.getUMLClass in SDK):

	$ find_in_uml_class_diagrams --user=garfield --db=myproject 'MyClass' operations

**Get UML2 state machines** (REST API 🔐, see UMLApi.getUMLStateMachines in SDK):

	GET /uml/uml_state_machine/{dbName}

This operation returns in the response body a list of all UML state machines in the project with the given name `{dbName}`.

**Get a UML2 state machine** (REST API 🔐, see UMLApi.getUMLStateMachine in SDK):

	GET /uml/uml_state_machine/{dbName}/{stateMachineName}

This operation returns in the response body a list containing only the UML state machines with the given name `{stateMachineName}` of the project with the given name `{dbName}`.

**Get UML2 state machines** (CLI):

	$ get_uml_state_machines --user=garfield --db=myproject --root=$(pwd)

**Get UML2 state machines** (CLI):

	$ get_uml_state_machines --user=garfield --db=myproject --root=$(pwd) 'MyStateMachine'

## 9.4 Abstract Semi-Formal Model (ASFM) and Graphical documentation (in GSL) <a id="4"></a>

**Get documentation as ASFM** (REST API 🔐, see DocApi.getDocs in SDK):

	GET /doc/asfm/docs/{dbName}?doc=…&filename=…

This operation returns in the response body a list of ASFMs in the project with the given name `{dbName}`.
This operation returns the ASFMs matching the query and in the project with the given name `{dbName}`.
When the query part is missing, this returns all ASFMs.

The query has the following parameters:

* `doc`: name of the ASFM
* `filename`: related documentation filename (e.g. .docx file)

**Get documentation as ASFM** (CLI):

	$ get_docs --user=garfield --db=myproject --root=$(pwd) "User's Manual"

**Get artefact in documentation as ASFM** (REST API 🔐, see DocApi.getDocArtefacts in SDK):

	GET /doc/asfm/artefacts/{dbName}?id=…&doc=…&filename=…&unit=…&class=…&invariant=…&field=…&method=…&type=…&macro=…&constant=…&member=…

This operation returns in the response body a list of ASFM artefacts (`unit`, `class`, `invariant`, `field`, `method`, `type`, `macro` or `constant`) matching the query and in the project with the given name `{dbName}`.

The query has the following parameters:

* `id`: positive integer which is a numerical unique identifier of the ASFM artefact
* `doc`: name of the ASFM
* `filename`: related documentation filename (e.g. .docx file)
* `unit`: unit name
* `class`: class name
* `invariant`: invariant name
* `field`: field name
* `method`: method name
* `type`: type name
* `macro`: macro name
* `constant`: constant name
* `member`: any of invariant, field, method, type, macro or constant name

Query parameters `doc`, `filename`, `unit`, `class`, `invariant`, `field`, `method`, `type`, `macro`, `constant`, and `member` accept either a regular expressions (between two `'/'`) or a bare string.
The matching mechanism ignores omitted parameters or parameters with an empty value (any values match).

The type of artefacts, which this operation returns, depends on the innermost level in the ASFM hierarchy that has matches.
The levels from the top to the bottom in the hierarchy are:

1. document level (`doc` or `filename` query parameters): the matching algorithm checks whether `(doc matches OR filename matches)`
2. unit level (`unit` query parameter): the matching algorithm checks whether `(unit matches AND id matches)`
3. class level (`class` query parameter): the matching algorithm checks whether `(class matches AND id matches)`
4. class member level (`method`, `field`, `type`, `invariant`, `macro`, `constant` or `member` query parameters): the matching algorithm checks whether `((method matches OR field matches OR type matches OR invariant matches OR macro matches OR constant macthes OR member matches) AND id matches)`

**Get artefact in documentation as ASFM** (CLI):

	$ find_in_doc --user=garfield --db=myproject --class=MyClass --method=MyMethod

**Get the graphical documentations of classes** (REST API 🔐, see DocApi.getGraphicalDocs in SDK):

	GET /doc/gsl/docs/{dbName}?class=…&object=…

This operation returns in the response body a list of graphical documentations of classes in the project with the given name `{dbName}`.
This operation returns the class graphical documentations matching the query and in the project with the given name `{dbName}`.
When the query part is missing, this returns all class graphical documentations.

The query has the following parameters:

* `class`: class name
* `object`: object name

## 9.5 Compile commands <a id="5"></a>

**Get compile commands** (REST API 🔐, see CompileCommandApi.getCompileCommands in SDK):

	GET /compile_command/{dbName}

This operation returns in the response body a list of all compile commands in the project with the given name `{dbName}`.

**Get compile commands** (CLI):

	$ get_compile_commands --user=garfield --db=OpenCV --root=$(pwd)

**Get a compile command related to a source code file** (REST API 🔐, see CompileCommandApi.getCompileCommand)

	GET /compile_command/{dbName}/{filename}

This operation returns in the response body a list containing only the compile command related to the source code file with the given name `{filename}` of the project with the given name `{dbName}`.

**Get a compile command related to a source code file** (CLI)

	$ get_compile_commands --user=garfield --db=OpenCV --root=$(pwd) 'OpenCV/modules/core/src/matrix_sparse.cpp'

## 9.6 Common Vulnerabilities and Exposures (CVE) <a id="6"></a>

**Get CVE entries** (REST API 🔐, see CVEApi.getCVEs in SDK):

	GET /cve/{dbName}?id=…&state=…&assigner=…

This operation returns in the response body a list of CVE entries matching the query and in the project with the given name `{dbName}`.

The query has the following parameters:

* `id`: an identifier
* `state`: the state of CVE entry
* `assigner`: the assigner for the CVE entry

Query parameters `id`, `state`, and `member` accept a bare string.
The matching mechanism ignores omitted parameters or parameters with an empty value (any values match).

## 9.7 Annotations <a id="7"></a>

**Get annotations** (REST API 🔐, see AnnotationsApi.getAnnotations in SDK):

	GET /annotations/{dbName}?path=…&access=…

This operation returns in the response body the list of annotations, for the given `{path}` and the given `{access}` when specified, in the project with the given name `{dbName}`.

**Get annotations related to a path** (REST API 🔐, see AnnotationsApi.getAnnotation in SDK):

	GET /annotations/{dbName}/{artefactId}

This operation returns in the response body an annotation the given artefact identifier `{artefactId}` of the project with the given name `{dbName}`.

## 9.8 Traceability Matrix <a id="8"></a>

**Get Traceability Matrix cells** (REST API 🔐, see TraceabilityMatrixApi.getTraceabilityMatrix in SDK):

	GET /traceability_matrix/{dbName}

This operation returns in the response body the whole traceability 2D matrix as a list of traceability matrix cells of the project with the given name `{dbName}`.

**Get a Traceability Matrix cell** (REST API 🔐, see TraceabilityMatrixApi.getTraceabilityMatrixCell in SDK):

	GET /traceability_matrix/{dbName}/{artefactId}

This operation returns in the response body the traceability matrix cell with the given identifier `{artefactId}` of the project with the given name `{dbName}`.

## 9.9 Logs and reports <a id="9"></a>

**Get logs** (REST API 🔐, see LogApi.getLogs in SDK):

	GET /log/{dbName}?abbrev=…

This operation returns in the response body a list of all logs in the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only log metadata and suppresses the messages, warnings, errors, and details of log from the response.

**Get a log** (REST API 🔐, see LogApi.getLog in SDK):

	GET /log/{dbName}/{artefactId}?abbrev=…

This operation returns in the response body the log with the given identifier `{artefactId}` of the project with the given name `{dbName}`.
When the `abbrev` query parameter is set to true, this operation returns only log metadata and suppresses the messages, warnings, errors, and details of log from the response.

## 9.10 TESTAR <a id="10"></a>

**Get all TESTAR settings** (REST API 🔐, see TESTARApi.getTESTARSettings in SDK):

	GET /testar/settings/{dbName}

This operation returns in the response body the TESTAR settings in the project with the given name `{dbName}`.

**Get all TESTAR test results** (REST API 🔐, see TESTARApi.getAllTESTARTestResults in SDK):

	GET /testar/test_results/{dbName}

This operation returns in the response body a list of all TESTAR test results in the project with the given name `{dbName}`.

**Get TESTAR test results** (REST API 🔐, see TESTARApi.getTESTARTestResults in SDK):

	GET /testar/test_results/{dbName}/{artefactId}

This operation returns in the response body the TESTAR test results with the given identifier `{artefactId}` of the project with the given name `{dbName}`.

**Get all TESTAR state models** (REST API 🔐, see TESTARApi.getAllTESTARStateModel in SDK):

	GET /testar/state_model/{dbName}

This operation returns in the response body a list of all TESTAR state models in the project with the given name `{dbName}`.

**Get a TESTAR state model** (REST API 🔐, see TESTARApi.getTESTARStateModel in SDK):

	GET /testar/state_model/{dbName}/{artefactId}

This operation returns in the response body the TESTAR state model with the given identifier `{artefactId}` of the project with the given name `{dbName}`.

## 9.11 Reviews <a id="11"></a>

**Get reviews** (REST API 🔐, see ReviewsApi.getReviews in SDK):

	GET /reviews/{dbName}?reviewID=…&reviewAuthor=…&reviewStatus=…

This operation returns in the response body a list of reviews in the project with the given name `{dbName}`.
This operation returns the reviews matching the query and in the project with the given name `{dbName}`.
When the query part is missing, this returns all reviews.

The query has the following parameters:

* `reviewID`: identifier of the review
* `reviewAuthor`: author of the review
* `reviewStatus`: status of the review (either `OPEN` or `CLOSED`)

## 9.12 Projects <a id="12"></a>

**Get projects** (REST API 🔐, see ProjectApi.getProjects in SDK):

	GET /project?abbrev=…

This operation returns in the response body the list of all projects.
When the `abbrev` query parameter is set to true, this operation returns only project name and members.

**Get projects** (CLI):

	$ get_projects --user=garfield

**Get a project** (REST API 🔐, see ProjectApi.getProject in SDK):

	GET /project/{projectName}?abbrev=…

This operation returns in the response body the project with the given name `{projectName}`.
When the `abbrev` query parameter is set to true, this operation returns only project name and members.

**Get a project** (CLI):

	$ get_project --user=garfield --project=myproject

**Get user's projects** (REST API 🔐, see UserApi.getUserProjects in SDK):

	GET /user/{userName}/project?abbrev=…

This operation returns in the response body the list of all projects when the user with given name `{userName}` is a member (i.e. he has a role in the project).
When the `abbrev` query parameter is set to true, this operation returns only project name and members.

**Get user's projects** (CLI):

	$ get_user_projects --user=garfield

**Get user's project** (REST API 🔐, see UserApi.getUserProject in SDK):

	GET /user/{userName}/project/{projectName}?abbrev=…

This operation returns in the response body the project with given `{projectName}` where the user with given name `{userName}` is a member (i.e. he has a role in the project).
When the `abbrev` query parameter is set to true, this operation returns only project name and members.

## 9.13 Users <a id="13"></a>

**Get current user** (REST API 🔐, see UserApi.getCurrentUser in SDK):

	GET /user/current

This operation returns in the response body the user associated to the session (i.e. idenfified with the PKM access key).

**Get a user** (REST API 🔐, see UserApi.getUser in SDK):

	GET /user/{userName}

This operation returns in the response body the user with the given name `{userName}`.

## 9.14 Process Engine <a id="14"></a>

**Get tool specifications** (REST API 🔐, see ToolsApi.getTools in SDK):

	GET /tools/{dbName}

This operation returns in the response body a list of all tool specifications in the project with the given name `{dbName}`.

**Get tool specifications** (CLI):

	$ get_tools --user=garfield --db=myproject

**Get a tool specification** (REST API 🔐, see ToolsApi.getTool in SDK):

	GET /tools/{dbName}/{toolID}

This operation returns in the response body the tool specification with the given identifier `{toolID}` of the project with the given name `{dbName}`.

**Get a tool specification** (CLI):

	$ get_tools --user=garfield --db=myproject tool_1

**Get tool invocations** (REST API 🔐, see InvocationsApi.getInvocations in SDK):

	GET /invocations/{dbName}

This operation returns in the response body a list of all tool invocations in the project with the given name `{dbName}`.

**Get tool invocations** (CLI):

	$ get_invocations --user=garfield --db=myproject

**Get a tool invocation** (REST API 🔐, see InvocationsApi.getInvocation in SDK):

	GET /invocations/{dbName}/{invocationID}

This operation returns in the response body the tool invocation with the given identifier `{invocationID}` of the project with the given name `{dbName}`.

**Get a tool invocation** (CLI):

	$ get_invocations --user=garfield --db=myproject invocation_1

**Get the Methodology status** (REST API 🔐, see MethodologyApi.getMethodologyStatus in SDK):

	GET /methodology/status/{dbName}?id=…&name=…&phaseNumber=…

This operation returns in the response body the list of Methodology phases in the project with the given name `{dbName}`.

The query part of the request has the following parameters to select the Methodology phases:

* `id`: identifier of the Methodology phase
* `name`: name of the Methodology phase
* `phaseNumber`: number of the Methodology phase

When the query part is missing, this returns all the Methodology phases.

