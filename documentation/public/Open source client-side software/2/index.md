# 2 Server information

When using the REST API, the PKM server can provide general information about the PKM server and the PKM REST API, such as description, version, licenses, contact, copyright, …
This is useful for identifying the version of the running PKM server and matching the API version with the one that clients use.

**Get the PKM server information** (REST API, see ServerInfoApi.login in SDK):

	GET /server_info

This operation returns, in the response body, information about the PKM server and the PKM REST API.

The information returned has the following form:

	{
		"server": {
			"title": "PKM",
			"description": "Persistent Knowledge Monitor",
			"contact": {
				"name": "DECODER project",
				"url": "https://www.decoder-project.eu",
				"email": "decoder@decoder-project.eu"
			},
			"version": "1.0.0",
			"license": {
				"name": "GNU AGPL v3.0",
				"url": "https://www.gnu.org/licenses/agpl-3.0.en.html"
			},
			"copyright": {
				"year": "2020",
				"owners": [
					"Capgemini Group",
					"Commissariat à l'énergie atomique et aux énergies alternatives",
					"OW2",
					"Sysgo AG",
					"Technikon",
					"Tree Technology",
					"Universitat Politècnica de València"
				]
			}
		},
		"api": {
			"contact": {
				"email": "decoder@decoder-project.eu",
				"name": "DECODER project",
				"url": "https://www.decoder-project.eu"
			},
			"description": "RESTful API of PKM (Persistent Knowledge Monitor)",
			"license": {
				"name": "Apache 2.0",
				"url": "http://www.apache.org/licenses/LICENSE-2.0.html"
			},
			"title": "PkmRestfulApi",
			"version": "1.0.0"
		}
	}
