# 6 Conclusion

This document has described how to install the PKM.
It has also described the implementation of the PKM server that is the software architecture, the built-in support for Git, and the parsers.

There are some lessons that the writer has learned while developing the PKM server.
The MongoDB database server, despite some technical limitations, is easy to use, and its speed is quite good even with a huge amount of complex programming artefacts.
Node.js is a very good piece of software, with a huge choice of packages, which execution speed is also very good considering that Javascript is an interpreted language.
The OpenAPI specification standard is very convenient to document RESTful APIs when everything is JSON and to generate automatically the SDK for the clients.
However, in our experiments, due to the supporting tools (Swagger codegen and OpenAPI generator) limitations and bugs, it has proved to be inconvenient for anything else than JSON (e.g. direct file upload/download, plain text), or with complex JSON schemas (some of them are disabled in the PKM REST API, but still enforced before reaching the database).

The PKM server have also its own limitations that deserve attention in future developments.
The PKM files, and more generally the artefacts in the PKM, have no revision numbers so that it is not easy to track progressive enhancement of the knowledge in the PKM.
The PKM users management, especially concerning the user's roles, is tightly geared to the MongoDB user's management, which is database oriented, but it is not necessarily convenient for a SaaS approach.
Direct upload (as attachment in a POST HTTP request) toward the database is unsupported due to the limitations of OpenAPI supporting tools.
