# A Appendix

## A.1 Javascript SDK documentation <a id="1"></a>

The Javascript SDK documentation (generated with jsdoc) is available at: [https://decoder.ow2.io/pkm-api/public/sdk/index.html](https://decoder.ow2.io/pkm-api/public/sdk/index.html)

## A.2 OpenAPI generator <a id="2"></a>

[OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator) allows generation of API client libraries (SDK generation), server stubs, documentation and configuration automatically given an OpenAPI Specification.
Directory [openapi_gen](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/openapi_gen) contains some generation scripts which leverages on this tool to fully generate server (not only server stubs), SDK for the clients written in Javascript and REST API documentation.
These scripts take a single argument which is a configuration file:

* `generate_server.sh`: generate the REST server and the documentation of the SDKs for the clients
* `generate_client.sh`: generate the SDK for a Javascript client
* `generate.sh`: generate both the REST server and the SDK for a Javascript client

The configuration file contains configuration parameter value assignments (`PARAM=value`).

The configuration parameters are the following:

* `TOOL_NAME`: name of the tool (mandatory)
* `OPENAPI_GENERATOR_VERSION`: version of OpenAPI Generator (default: `5.3.0`)
* `SERVER_REQUEST_BODY_LIMIT`: maximum size of request body (e.g. `'100MB'` or `"(javascript code snippet)"`, default: `'100MB'`)
* `SERVER_COMPRESSION_LEVEL`: gzip compression level of server responses (1=fast to 9=best, default: 5)
* `SERVER_API_SPEC`: OpenAPI specification file of the server (defaut: `'api/openapi.yaml'`)
* `SERVER_API_SCHEMAS`: External JSON schema files referenced by the OpenAPI specification file -- e.g. `(schema1.json schema2.json schema3.json)`, default: `()`
* `SERVER_REQUIRES`: Javascript code snippet intended for Javascript requires statements (e.g. `const PKM = require('pkm')`, default: none)
* `SERVER_CONFIG`: Run-time configuration file of the server (e.g. `'server_config.json'`, default: none)
* `SERVER_DIR`: relative path to the directory where to generate the full server source code (default: `'server'`)
* `CLIENT_SDK_DIR`: relative path to the directory where to generate the SDK for the client written in Javascript (default: `'client'`)

The paths are all relative to directory containing the configuration file.
The actual implementation of each API function shall be in sub-directory `services`.

Figure 7 below shows the OpenAPI generation flow:<a id="figure7"></a>

![Figure 7: OpenAPI generation flow](./openapi_gen.png)

The inputs (with the directory structure) are in green on the left side of the figure.
The inputs are the OpenAPI specification file (`openapi.yaml`), the implementation of POST/PUT/GET/DELETE functions for the resources ("API functions"), and the configuration file for the generation flow (`openapi_gen.conf`).
On the middle of the figure, the processing steps are in blue, and consist in running the OpenAPI generator tool, then fixing the generated server replacing stubs with the actual API functions and adding some functionalities such as HTTPS, gzip, and download supports.
The results of the generation flow are in orange on the right side of the figure.
The results are the SDK for javascript clients, the REST API documentation (written in HTML), and the full server source code.

The services that currently use OpenAPI Generator are the followings:

* PKM
* Frama-C for PKM
* Frama-Clang for PKM
* ASFM for PKM
* UNISIM Excavator for PKM

## A.3 Integration & non-regression testing <a id="3"></a>

There is a set of benchmarks for the DECODER project.
These benchmarks are tests mostly intended for integration testing and non-regression testing of the DECODER Project tool-chain.
There are not only basic tests but also more complex tests with software from the real world and DECODER test cases, such as OpenCV and Linux driver.
They have been used extensively to ensure that the server is performing its missions correctly and scaling up in realistic scenarios.

These benchmarks are available at [https://gitlab.ow2.org/decoder/integration-tests](https://gitlab.ow2.org/decoder/integration-tests).

Each test has a bash script (`run.sh`) to run the test and a dataset (in directory `data`).

The currently available tests are:

* [`project_mngmt`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/project_mngmt): project management (projects, users, ...)
* [`vector2`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/vector2): a small C project
* [`opencv-4.5.0`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/opencv-4.5.0): a big C++ project
* [`mythaistar`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/mythaistar): a small subset of [`mythaistar`](https://github.com/devonfw/my-thai-star)
* [`vmlinux`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/vmlinux): a Linux kernel
* [`cve`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/cve): a small benchmark for testing PKM API about CVE lists
* [`git`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/git): a test of the PKM built-in support for Git
* [`at91sam9-watchdog`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/at91sam9-watchdog): a testbench for the AT91SAM9 Watchdog timer (WDT) Linux driver
* [`e1000e-ethernet`](https://gitlab.ow2.org/decoder/integration-tests/-/tree/master/tests/e1000e-ethernet): a testbench for the Intel e1000e Ethernet Linux driver

The tests use the popular [cURL](https://curl.se/) program to communicate with the PKM and tools through their REST APIs.
