<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="date" content='03/11/22'>
    <title>D1.5 Open source PKM server-side software</title>
    <link rel="stylesheet" href='../style/style.css' type="text/css">
  </head>
  <body>
    <div id="header">
      <a href="https://www.decoder-project.eu/" id="logo"></a>
      <div id="title">
        <span>
          <a href="../index.html">D1.5 Open source PKM server-side software</a>&nbsp;<a href="../index.html">⇑</a>
        </span>  
      </div>
    </div>
    <div id="content">
<h1 id="git-support">4 Git support</h1>
<h2 id="introduction">4.1 Introduction <a id="1"></a></h2>
<p>The PKM has built-in support for Git. It aims to provide user with a way to synchronize PKM with one or more Git repositories on remote Git servers over HTTP/HTTPS/SSH with as much freedom as possible. It can run a list of any Git commands in a request using the standard git command line interface (CLI). It takes care of authentication with the remote Git server (limited to one server per command at a time) for <code>'clone'</code>, <code>'fetch'</code>, <code>'pull'</code> and <code>'push'</code> commands. The PKM can store, in a kind of “wallet” (in MongoDB user’s custom data, encrypted with AES256), the Git user’s credentials either completely or partially (e.g. without the password). The PKM server manages the on-disk Git working trees (the workspaces) and Git directories (the <code>.git</code> directories) in Directory <code>git-root</code> (configurable in <code>pkm_config.json</code>). The PKM synchronizes the PKM Files in the MongoDB database with those in the Git working trees on the local file system and vice versa. The PKM manages some read-only bits in the PKM files to speed up synchronization (a dirty bit) and help identifying unmerged files (unmerged bit), see Section 3.2.2.</p>
<p>Figure 5 below outlines how the support for Git works:<a id="figure5"></a></p>
<figure>
<img src="./pkm-git.png" alt="Figure 5: Built-in support for Git distributed version-control system" /><figcaption aria-hidden="true">Figure 5: Built-in support for Git distributed version-control system</figcaption>
</figure>
<p>The Git program runs under the supervision of the PKM server. The PKM provides the user’s credentials more or less through a pipe (see Section 4.3, Issue 3). It gets the textual results of Git through a pipe and the versioned files from the local file system. Communication with the remote Git servers is left to the standard Git program.</p>
<h2 id="terminology">4.2 Terminology <a id="2"></a></h2>
<p>This section is about Git-related terminology extensively used in this document.</p>
<p>Figure 6 below synthesizes the terminology of Git:<a id="figure6"></a></p>
<figure>
<img src="./git-terminology.png" alt="Figure 6: Git Terminology" /><figcaption aria-hidden="true">Figure 6: Git Terminology</figcaption>
</figure>
<p>A Git working tree is an on-disk workspace for the developer. Each working tree has an associated Git directory, which is where Git actually stores all the information relative to the repository and the history. In most situations, the Git directory is a subdirectory named <code>'.git'</code> of the working tree. However, Git can also manage Git directories outside of the working tree. Besides, a Git working tree can have some secondary working trees, which the <code>git worktree add</code> command can create. The former working tree designates the main working tree while the latter are the linked working trees. The data model in the PKM presented in Section 3.2.13 for the Git working trees reflects this organization.</p>
<h2 id="safety-security">4.3 Safety &amp; Security <a id="3"></a></h2>
<p>Below are the main issues about safety and security problems that may arise when running Git on a server environment and that we have faced while implementing the support for Git in the PKM.</p>
<p><strong>Issue 1</strong>. The first issue is that a Git command can go through parent directories searching for the Git working tree, possibly escaping from the project root directory on the host server. The solution that we have found is to make the PKM run <code>'git rev-parse –show-toplevel'</code>, which can fail, before running any Git command. When it succeeds, we check that the Git working tree is really bound to the directory <code>git-root/&lt;project-name&gt;</code>, i.e. the directory which contains all Git working trees of a project.</p>
<p><strong>Issue 2</strong>. The second issue is that some Git commands can have system-wide paths passed as command line options or parameters. Concretely, we had to find a solution to avoid path out-of-bound situations, for instance with the <code>'clone'</code> command. In fact, for most commands, Git already checks that the paths actually target files in the Git working tree where the command runs. For other commands like <code>'clone'</code>, we have made the PKM parse such commands and then check the value of each options and arguments that Git interprets as a path before spawning Git. Currently only the following commands are parsed: <code>'clone'</code>, <code>'fetch'</code>, <code>'pull'</code>, <code>'push'</code>, <code>'config'</code> and <code>'worktree'</code>. Additional commands of which we are not aware may require special attention.</p>
<p><strong>Issue 3</strong>. The third issue is that some commands requires authentication to a remote server, like <code>'clone'</code>. Indeed, we have had to ensure that a request to the PKM server is not blocked (resulting in no response from the PKM server) because the Git program is waiting credentials forever. This happens because the authentication of Git (one prompt for the user name and one prompt for the password) is interactive. Redirecting the input of Git to provide it the user’s credentials is tricky because Git reads the credentials from the terminal rather a pipe. The solution that we have found is to make the PKM use the <a href="http://sshpass.sourceforge.net"><code>sshpass</code></a> command to provide Git (or ssh) with the credentials. Indeed, when the PKM detects that a command may need the user’s credential, Git runs under the supervision of sshpass that runs itself under the supervision of the PKM server. Because <code>sshpass</code> cannot provide more than one credential for each Git command, the PKM checks the command options to ensure that the command only targets one remote server. When the remote server is just named, i.e. no URL is provided on the command line (e.g. <code>'origin'</code>), the PKM also looks at each <code>remote</code> in the Git local configuration (aka. <code>.git/config</code>). The PKM then checks if it has credentials available (either in the request body or in the user’s “wallet”) for the remote server before actually spawning Git. For HTTP/HTTPS, the PKM provides the user’s name through the URL by rewriting the URL on the fly. As Git may record the given HTTP/HTTPS URLs in the Git config remotes, the PKM make the HTTP/HTTPS URLs anonymous again in the Git config remotes.</p>
<h2 id="users-credential-wallet">4.4 User’s credential “wallet” <a id="4"></a></h2>
<p>Each user has a kind of “wallet” which stores the user’s credential related to Git, either completely or partially (e.g. without the password). This “wallet” is in property <code>git_user_credentials</code> of PKM user (see Section 3.2.1).</p>
<pre><code>PkmUser
{
  &quot;name&quot;: &quot;name&quot;,
  &quot;password&quot;: null,
  &quot;first_name&quot;: &quot;first_name&quot;,
  &quot;last_name&quot;: &quot;last_name&quot;,
  &quot;email&quot;: &quot;email&quot;,
  &quot;phone&quot;: &quot;phone&quot;,
  &quot;roles&quot; :
  [
    { &quot;db&quot;: &quot;db&quot;, &quot;role&quot;: &quot;role&quot; }
  ],
  &quot;git_user_credentials&quot;:
  [
    {
      &quot;git_remote_url: &quot;string“,
      &quot;git_user_name: &quot;string&quot;,
      &quot;git_password&quot;: &quot;string&quot;,
      &quot;git_ssh_private_key&quot;: &quot;string&quot;
    }
  ]
}</code></pre>
<p>Only user can view and modify their own credentials. It is plain text in the REST API, thus it deserves securing the REST API with HTTPS, which PKM server supports too. It is AES256 encrypted in MongoDB database. The decryption key (32 bytes) is stored permanently in a file named <code>'secret'</code> (configurable in File <code>pkm_config.json</code>) on the server disk (read-only for the PKM system user and forbidden for the group and others) or on same docker volume like the one for File <code>pkm_config.json</code>.</p>
<h2 id="operations">4.5 Operations <a id="5"></a></h2>
<p>The PKM REST API has support for the following operations related to the Git support:</p>
<p><strong>Run a Git command sequence</strong></p>
<pre><code>POST /git/run/{dbName}?asynchronous=…
{
    &quot;git_commands&quot;: [ [ &quot;command&quot;, &quot;arg1&quot;, &quot;arg2&quot;, … ], … ],
    &quot;options&quot;: {
        &quot;dont_delete_pkm_files&quot;: false,
        &quot;dont_delete_git_working_tree_files&quot;: false,
        &quot;git_user_credentials&quot;: […]
    }
}</code></pre>
<p>This operation runs the git command sequence passed in request body in the project with the given name <code>{dbName}</code>. The operation returns in the response body a Git job which client can poll when in asynchronous mode (i.e. <code>asynchronous=true</code>) using the property <code>id</code> of the job as a job identifier. When in asynchronous mode, the job is enqueued in a job queue. The default behavior is to run jobs synchronously.</p>
<p>Note that this operation uses both the user’s credentials from the request body and the ones from the user’s credential “wallet”, which can be complete or partial. It merges the user’s credentials giving priority to the ones in the request body.</p>
<p>An overview of the algorithm for the <code>run</code> operation is the following:</p>
<ol type="1">
<li>Scan the Git working trees on the disk, then make anonymous the HTTP/HTTPS URLs in the Git config remotes</li>
<li>Get the list of all on-disk filenames</li>
<li>Get the Git user’s credentials</li>
<li>Synchronize on-disk files with PKM:
<ul>
<li>Get all dirty files from the database and write them on the file system,</li>
<li>then delete files on the file system supported by the PKM which are not in the database (*)</li>
</ul></li>
<li>Run all the Git commands on the file system</li>
<li>Scan the Git working trees on the disk, then make anonymous the HTTP/HTTPS URLs in the Git config remotes</li>
<li>Update the Git working trees metadata in the PKM</li>
<li>Get the list of all on-disk filenames</li>
<li>Get the list of versioned filenames</li>
<li>Get the list of unmerged filenames</li>
<li>Synchronize PKM with on-disk files:
<ul>
<li>Put all files from the Git directory on the file system into the database,</li>
<li>then delete files in the database which are no longer in the Git working directory (*)</li>
</ul></li>
</ol>
<p>Steps 1 to 4 retrieve the user’s credentials and prepare the disk storage for the execution of the Git commands. These steps are essentially about dumping the dirty files from the PKM on the disk, and possibly deleting the files that are no longer in the PKM. Step 5 is about running the Git commands, whose effect is making some directories and files on the disk to appear or disappear. Steps 6 to 11 update the PKM which is essentially filling the PKM with the on-disk files, and deleting files from the PKM which are no longer on the disk.</p>
<p>The actual implementation of this algorithm has some tuning options (<code>dont_delete_pkm_files</code> and <code>dont_delete_git_working_tree_files</code>), to support a lazy synchronization between the PKM (the database) and the Git working trees, to avoid deleting files on one side which have disappeared on the other side, see (*) in the algorithm.</p>
<p><strong>Poll a Git job</strong></p>
<pre><code>GET /git/job/{jobId}</code></pre>
<p>When a Git job is running asynchronously, this operation allows polling the Git job with the given identifier <code>{jobId}</code> until job completion. The job is dequeued when client polls a completed job (either finished or failed).</p>
<p><strong>Access to Git config file</strong></p>
<pre><code>GET /git/config/{dbName}/{gitWorkingTree}

PUT /git/config/{dbName}/{gitWorkingTree}
{
    &quot;git_config&quot;: &quot;content…&quot;
}</code></pre>
<p>These operations provide Read/Write access to the Git local configuration (aka. <code>.git/config</code>)</p>
<p><strong>Access to files in Git working trees</strong></p>
<pre><code>GET /git/files/{dbName}/{gitWorkingTree}

GET /git/files/{dbName}/{gitWorkingTree}/{filename}

POST or PUT /git/files/{dbName}/{gitWorkingTree}
[
    PKM files…
]

DELETE /git/files/{dbName}/{gitWorkingTree}

DELETE /git/files/{dbName}/{gitWorkingTree}/{filename}</code></pre>
<p>These operations provide Read/Write access to the files in Git working trees. They are the only way to access <code>.gitignore</code> files in the Git working trees.</p>
<p><strong>Show the status of Git working trees, i.e. the Git working trees metadata</strong></p>
<pre><code>GET /git/working_trees/{dbName}

GET /git/working_trees/{dbName}/{gitWorkingTree}</code></pre>
<p>These operations return some metadata about Git working trees. They are convenient for quickly inspecting the status of the Git working trees without running Git commands.</p>
<p><strong>Delete Git working trees</strong></p>
<pre><code>DELETE /git/working_trees/{dbName}?dontDeletePkmFiles=…

DELETE /git/working_trees/{dbName}/{gitWorkingTree}?dontDeletePkmFiles=…</code></pre>
<p>These operations allow deleting Git working trees created with <code>'clone'</code>. These operations can also delete the corresponding files in the PKM. There is no other way to delete Git working trees, and the only way to create a new Git working tree is to run a <code>clone</code> command.</p>
    </div>
    <div id="footer">
      <span><a href="../index.html">D1.5 Open source PKM server-side software</a> - 03/11/22</span>
    </div>
  </body>
</html>
