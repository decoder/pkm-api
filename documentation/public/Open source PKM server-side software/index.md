![DECODER project partners](DECODER_partners.png)

# Table of content

1. [Introduction](1/index.md)
    1. [Purpose](1/index.md#1)
    2. [Licensing](1/index.md#2)
    3. [Choosing a database](1/index.md#3)
    4. [Overall organization of the document](1/index.md#4)
2. [Configuration & Installation](2/index.md)
    1. [Configuration (optional)](2/index.md#1)
    2. [Easy installation (no build required)](2/index.md#2)
    3. [Minimal Docker installation](2/index.md#3)
        1. [Setting up administrators' credentials (optional)](2/index.md#3.1)
        2. [Building the docker images](2/index.md#3.2)
        3. [Starting the docker services](2/index.md#3.3)
3. [PKM server architecture](3/index.md)
    1. [Software layers](3/index.md#1)
    2. [MongoDB database](3/index.md#2)
        1. [Project management](3/index.md#2.1)
        2. [Files](3/index.md#2.2)
        3. [Source Code](3/index.md#2.3)
            1. [Abstract Syntax Trees (ASTs)](3/index.md#2.3.1)
            2. [Comments](3/index.md#2.3.2)
            3. [Annotations](3/index.md#2.3.3)
         4. [UML classes & state machines](3/index.md#2.4)
         5. [Abstract Semi-Formal Model (ASFM) and Graphical documentation (in GSL)](3/index.md#2.5)
         6. [Compile commands](3/index.md#2.6)
         7. [Common Vulnerabilities and Exposures (CVE)](3/index.md#2.7)
         8. [Annotations](3/index.md#2.8)
         9. [Traceability Matrix](3/index.md#2.9)
        10. [Logs and reports](3/index.md#2.10)
        11. [TESTAR](3/index.md#2.11)
        12. [Reviews](3/index.md#2.12)
        13. [Git support](3/index.md#2.13)
        14. [Process Engine](3/index.md#2.14)
    3. [Application Programming Interfaces](3/index.md#3)
        1. [Javascript SDK](3/index.md#3.1)
            1. [Session management](3/index.md#3.1.1)
            2. [Document management](3/index.md#3.1.2)
            3. [Document querying](3/index.md#3.1.3)
            4. [Dependent document invalidation](3/index.md#3.1.4)
            5. [Document validation](3/index.md#3.1.5)
            6. [File system I/O](3/index.md#3.1.6)
            7. [Tools invocation](3/index.md#3.1.7)
        2. [Command Line Interface (CLI)](3/index.md#3.2)
        3. [REST API](3/index.md#3.3)
4. [Git support](4/index.md)
    1. [Introduction](4/index.md#1)
    2. [Terminology](4/index.md#2)
    3. [Safety & Security](4/index.md#3)
    4. [User's credential "wallet"](4/index.md#4)
    5. [Operations](4/index.md#5)
5. [Parsers](5/index.md)
    1. [Source codes](5/index.md#1)
        1. [C](5/index.md#1.1)
        2. [C++](5/index.md#1.2)
        3. [Java](5/index.md#1.3)
    2. [Documentation](5/index.md#2)
        1. [Microsoft Office .docx](5/index.md#2.1)
        2. [Code to ASFM](5/index.md#2.2)
    3. [UML](5/index.md#3)
        1. [Classes](5/index.md#3.1)
        2. [State Machines](5/index.md#3.2)
    4. [Executable binaries](5/index.md#4)
        1. [DWARF debugging information](5/index.md#4.1)
6. [Conclusion](6/index.md)
* [Appendix](appendix/index.md)
    1. [Javascript SDK documentation](appendix/index.md#1)
    2. [OpenAPI generator](appendix/index.md#2)
    3. [Integration & non-regression testing](appendix/index.md#3)

# List of Figures

* [Figure 1: DECODER EU Project Tool-chain control center](2/index.md#figure1)
* [Figure 2: Login screen](2/index.md#figure2)
* [Figure 3: PKM server software architecture](3/index.md#figure3)
* [Figure 4: PKM MongoDB collections](3/index.md#figure4)
* [Figure 5: Built-in support for Git distributed version-control system](4/index.md#figure5)
* [Figure 6: Git Terminology](4/index.md#figure6)
* [Figure 7: OpenAPI generation flow](appendix/index.md#figure7)
