# 5 Parsers

The PKM leverages on external parsing tools to:

* Convert source code files into source code ASTs, and to extract comments and annotations (in comments) from the source code files,
* Convert documentation files into ASFM (Abstract Semi-Formal Model) documents,
* Reverse engineer executable binaries.

## 5.1 Source codes <a id="1"></a>

### 5.1.1 C <a id="1.1"></a>

[*Frama-C*](https://www.frama-c.com/) is the parser for the C language.
*Frama-C* also provides EVA (Abstract Interpretation symbolic execution) and WP (program proof using Hoare Logic) analysis plugins.
The C source code annotations for the C language shall be written in [ACSL](https://frama-c.com/html/acsl.html) (Abstract C specification language).
*Frama-C for PKM* has a command line interface and a REST API over HTTP/HTTPS.

The implementation is organized as the following:

* The source code which actually executes Frama-C is in file [`util/frama_c.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/util/frama_c.js),
* The command line interface implementation is in file [`cli/run_frama_c.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/cli/run_frama_c.js),
* *Frama-C for PKM* has an [OpenAPI](https://www.openapis.org) 3 specification in file [`frama-c/api/frama-c-openapi.yaml`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-c/api/frama-c-openapi.yaml) which enables to automatically generate the SDK for many programming languages (see Appendix A.2),
* The REST API implementation is in directory [`frama-c/services`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-c/services),
* The docker stuff is in directory [`frama-c/docker`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-c/docker).

Appendix A.2 contains detailed explanations about implementation design of the REST server that provides developer with the REST API.

### 5.1.2 C++ <a id="1.2"></a>

*Frama-Clang* is the parser for the C++ language. Frama-Clang leverages on [Clang](https://clang.llvm.org) 13 which supports C++ 98, C++03, C++11, C++14, C++17 almost totally, and C++20 partially (see [clang C++ status](https://clang.llvm.org/cxx_status.html) for more details).
The C++ source code annotations for the C++ language shall be written in ACSL++ (Abstract C++ specification language, an extension of ACSL for C++).
*Frama-Clang for PKM* has a command line interface and a REST API over HTTP/HTTPS.

The implementation is organized as the following:

* The source code which actually executes Clang and process its output is in file [`util/frama_clang.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/util/frama_clang.js),
* The command line interface implementation is in file [`cli/run_frama_clang.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/cli/run_frama_clang.js),
* *Frama-Clang for PKM* has an [OpenAPI](https://www.openapis.org) 3 specification in file [`frama-clang/api/frama-clang-openapi.yaml`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-clang/api/frama-clang-openapi.yaml) which enables to automatically generate the SDK for many programming languages (see Appendix A.2),
* The REST API implementation is in directory [`frama-clang/services`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-clang/services),
* The docker stuff is in directory [`frama-clang/docker`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-clang/docker).

Appendix A.2 contains detailed explanations about implementation design of the REST server that provides developer with the REST API.

### 5.1.3 Java <a id="1.3"></a>

*Java parser* provides parsing of the Java language.
The Java source code annotations for the Java language shall be written in JML.
*Java parser* has a REST API over HTTP/HTTPS.
The implementation of java parser is available at [https://gitlab.ow2.org/decoder/javaparsertool/-/tree/master/javaparser](https://gitlab.ow2.org/decoder/javaparsertool/-/tree/master/javaparser).

## 5.2 Documentation <a id="2"></a>

*ASFM tools* provides everything related to ASFM (Abstract Semi-Formal Model) documentation.

The implementation is organized as the following:

* *ASFM for PKM* has an [OpenAPI](https://www.openapis.org) 3 specification in file [`asfm/api/asfm-openapi.yaml`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/asfm/api/asfm-openapi.yaml) which enables to automatically generate the SDK for many programming languages (see Appendix A.2),
* The REST API implementation is in directory [`asfm/services`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/asfm/services),
* The docker stuff is in directory [`asfm/docker`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/asfm/docker).

Appendix A.2 contains detailed explanations about implementation design of the REST server that provides developer with the REST API.

### 5.2.1 Microsoft Office .docx <a id="2.1"></a>

*doc_to_asfm* converts a Microsoft Office .docx file to an ASFM (Abstract Semi-Formal Model) document.
Note that *asfm_to_doc* converts an ASFM (Abstract Semi-Formal Model) documents back to a Microsoft Office .docx file.

The implementation is organized as the following:

* The source code which actually executes *doc_to_asfm* is in file [`util/doc_to_asfm.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/util/doc_to_asfm.js),
* The source code of *asfm_to_doc* which reverts ASFM back to Microsoft Office .docx is in file [`util/asfm_to_doc.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/util/asfm_to_doc.js),
* The command line interface implementation of *doc_to_asfm* is in file [`cli/run_doc_to_asfm.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/cli/run_doc_to_asfm.js),
* The command line interface implementation of *asfm_to_doc* is in file [`cli/run_asfm_to_doc.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/cli/run_asfm_to_doc.js).

### 5.2.2 Code to ASFM <a id="2.2"></a>

*code_to_asfm* converts source code ASTs to an ASFM (Abstract Semi-Formal Model) document.
It currently only supports C++.

## 5.3 UML <a id="3"></a>

### 5.3.1 Classes <a id="3.1"></a>

ClassModelXMI2JSON extracts UML2 classes from an XMI file and convert them to a JSON document.
The implementation is available at [https://gitlab.ow2.org/decoder/javaparsertool/-/tree/master/classmodelxmi2jsontransformer](https://gitlab.ow2.org/decoder/javaparsertool/-/tree/master/classmodelxmi2jsontransformer).

### 5.3.2 State Machines <a id="3.2"></a>

SMModelXMI2JSON extracts UML2 state machines from an XMI file and convert them to a JSON document.
The implementation is available at [https://gitlab.ow2.org/decoder/javaparsertool/-/tree/master/smmodelxmi2jsontransformer](https://gitlab.ow2.org/decoder/javaparsertool/-/tree/master/smmodelxmi2jsontransformer).

## 5.4 Executable binaries <a id="4"></a>

### 5.4.1 DWARF debugging information <a id="4.1"></a>

Excavator is a reverse engineering tool which parses DWARF debugging information in ELF executable binaries, processes compilation units and generates an equivalent (interfaces and code skeletons only) compilable C source code.

The implementation is organized as the following:

* The source code which actually executes Excavator is in file [`util/excavator.js`](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/util/excavator.js),
* *Excavator for PKM* has an [OpenAPI](https://www.openapis.org) 3 specification in file [`excavator/api/excavator-openapi.yaml`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/excavator/api/excavator-openapi.yaml) which enables to automatically generate the SDK for many programming languages (see Appendix A.2),
* The REST API implementation is in directory [`excavator/services`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/excavator/services),
* The docker stuff is in directory [`excavator/docker`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/excavator/docker).

Appendix A.2 contains detailed explanations about implementation design of the REST server that provides developer with the REST API.
