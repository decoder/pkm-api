# 2 Configuration & Installation

## 2.1 Configuration (optional)<a id="1"></a>

The PKM server has a configuration file, named `pkm_config.json`.

The PKM server comes with a default configuration file available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/pkm_config.json).

The default configuration file, whose content is outlined below, is good for most people:

%deps "${BIN}/json_to_md" "${PKM_API}/pkm_config.json"
%"${BIN}/json_to_md" --verbatim --max-depth 0 "${PKM_API}/pkm_config.json"

The JSON schema of the configuration file is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-config-schema.json).

## 2.2 Easy installation (no build required)<a id="2"></a>

A program that provides a command line interface and a control center for managing the services (download, install, update, start …) of DECODER EU Project Tool-chain, as a local instance without remote access, is available at [https://gitlab.ow2.org/decoder/docker-deployment](https://gitlab.ow2.org/decoder/docker-deployment).
This program is based on [docker](https://www.docker.com/) and [docker-compose.](https://docs.docker.com/compose)

To run the program, do the following at the command prompt:

	$ ./app/bin/decoder-eu

On the first use, you should `install` then `start` the toolchain, see Figure 1 below, which shows the control center user's interface:<a id="figure1"></a>

![Figure 1: DECODER EU Project Tool-chain control center](./control-center.png)

As soon as the graphical user interface has started in your default web browser, you can log in using the preconfigured administrator account whose login is "admin" and the password is "admin", see Figure 2 below:<a id="figure2"></a>

![Figure 2: Login screen](./decoder-login.png)

**Minimum Hardware Requirements**

* 16 GiB RAM
* ~100 GB disk free space
* Internet connection

**Dependencies**

* bash >= 4.3
* docker and docker-compose (for Mac, see [Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install))
* sed
* cURL
* xdg-open (optional)
* zenity (optional)

**Command line interface**

	Usage: decoder-eu [<options>] [<commands>]
	
	Options:
	    --version
	        Outputs version information and exit
	
	    --help
	        Display this help and exit
	
	    --user-pref-dir <directory>
	        Set the directory where the user's preferences are located (default: ~/.config/decoder-eu)
	
	    --user-data-dir <directory>
	        Set the directory where the user's data are located (default: ~/.local/share/decoder-eu)
	
	Commands:
	    start
	        Starts the DECODER EU Project Tool-chain services then opens the GUI in your default browser
	
	    gui
	        Opens the GUI in your default browser
	
	    stop
	        Stops the DECODER EU Project Tool-chain services'
	
	    status
	        Shows the status of the DECODER EU Project Tool-chain services
	
	    shell
	        Starts a shell with a preset environment
	
	    reset
	        Resets all data (but preserve your saved preferences) (default: in ~/.local/share/decoder-eu)
	
	    select-tools
	        Select the tools considered for start, install and update commands
	
	    configure
	        Edit configuration file (default: in ~/.config/decoder-eu/config)
	
	    delete-prefs
	        Delete your saved preferences (default: in ~/.config/decoder-eu)
	
	    install
	        Downloads, install, and preconfigure DECODER EU Project Tool-chain ready to use
	
	    update
	        Update DECODER EU Project Tool-chain (without altering your saved preferences)
	
	    uninstall
	        Uninstall DECODER EU Project Tool-chain (but preserve your saved preferences)

## 2.3 Minimal Docker installation<a id="3"></a>

The installation procedure in this section applies only to PKM and some parsers for which [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api) hosts the source code.
This installation procedure is **not** for all the docker images of the DECODER EU Project Tool-chain.
For a complete installation, refer to Section 2.2.

The built and installed services are the following:

* PKM
* Frama-C for PKM
* Frama-Clang for PKM
* ASFM for PKM
* UNISIM Excavator for PKM

This minimal set of services handles the PKM (document management and querying) and parsing C, C++, .docx, and executable binary files. 

### 2.3.1 Setting up administrators' credentials (optional)<a id="3.1"></a>

MongoDB and PKM require an initialization.
When PKM and MongoDB services start for the first time, the initialization starts automatically.
Once initialized, the MongoDB and PKM services gracefully ignore later attempts to initialize.
The stock MongoDB database is virgin with security disabled, so the initialization of the MongoDB service consists in creating an administrator (a "superuser") in database admin (admin@admin), who can create users and roles in any MongoDB database, then enabling MongoDB security.
The initialization of the PKM service consists in creating the PKM management database (where all the PKM users live and where the list of PKM projects is) and then an initial PKM administrator.
That PKM administrator can later create other users and even other PKM administrators in the PKM management database.
The PKM initialization runs on behalf of the MongoDB "superuser".

The credentials used during initialization are in [`mongodb/credentials.json`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/mongodb/credentials.json):

%${BIN}/json_to_md --verbatim ${PKM_API}/mongodb/credentials.json

The default credentials are:

* MongoDB superuser:
	* user: `admin`
	* authentication database: `admin`
	* password: `admin`
* PKM administrator:
	* user: `admin`
	* password: `admin`

File [`pkm_config.json`](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/pkm_config.json) defines the authentication database for the PKM administrator, i.e. the PKM management database, see property `pkm_db` (default: `pkm`).

Later after the initialization, MongoDB shell allows changing the MongoDB superuser credentials while the PKM application programming interfaces (or MongoDB shell) allows changing the PKM administrator credentials.

### 2.3.2 Building the docker images<a id="3.2"></a>

Do the following to build the PKM and some of the parsers docker images:

	$ docker-compose build

### 2.3.4 Starting the docker services<a id="3.4"></a>

Do the following to start the PKM some of the parsers docker services:

	$ docker-compose up
