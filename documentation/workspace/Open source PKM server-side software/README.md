# Workspace for "Open source PKM server-side software"

This is the workspace for writing markdown, webpages, and .docx report for documenting the "Open source PKM server-side software".
It uses a markdown preprocessor to allow including markdown files and run markdown generators.

Example of use for the markdown preprocessor:

	%include "part1.md"
	%include "part2.md"
	%include "part3.md"

	%deps "doc.json"
	%bin/genmd "doc.json"

## Requirements

* bash >= 4.3
* GNU sed
* pandoc
* OpenAPI Generator
* jsdoc

## Build

	$ cd 'documentation/workspace/Open source PKM server-side software'
	$ make

This builds everything (markdown, webpages and .docx report) in [documentation/Open source PKM server-side software](https://gitlab.ow2.org/decoder/pkm-api/-/tree/master/documentation/public/Open%20source%20PKM%20server-side%20software)
