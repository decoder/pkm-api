# 7 Document management

This chapter describes the operations that the PKM provides to manage documents, i.e. inserting, updating and deleting documents.

In the PKM REST API, these operations correspond to the HTTP POST, PUT, and DELETE requests.
These requests can return one the following HTTP status codes:

* 200: The operation is successful.
* 201: The PKM created a new resource.
* 400: Bad request. The request has invalid request parameters in the URL, or the format of the request body is malformed.
       In the latter case, it means that the JSON validation of the input fails against the JSON schema.
* 401: The operation (labeled with a 🔐 symbol in this document) requires prior authentication (see Section 3).
* 403: Forbidden operation. The logged in user does not have sufficient rights to perform this operation.
* 404: Not found; the requested resource does not exist.
* 500: Internal server error. An error of this type is most likely due to a bug in the PKM server.

## 7.1 Files <a id="1"></a>

The PKM REST API models a file as a JSON object with the following properties:

* `rel_path`:
	* Type: `string`
	* Description: The relative path (relative to a virtual root directory for the project or the Git working tree) of the file (POSIX naming scheme)
* `content`:
	* Type: `string`
	* Description: The content of the file. The original file content shall be encoded as `'base64'` (server accepts RFC 4648 §4/§5, and exports RFC 4648 §4) when `'format'` field value is `'binary'`.
* `format`:
	* Type: `string`
	* Description: The format of content, either `'text'` or `'binary'`.
* `encoding`:
	* Type: `string`
	* Description: The text encoding of the original file if the original file is a text according to `'mime_type'` field value.
* `type`:
	* Type: `string`
	* Description: The type of the original file (e.g. Code, Annotation, Comment, or Diagram). When no type is specified, a type may be assigned based on filename extension.
* `mime_type`:
	* Type: `string`
	* Description: The MIME type of the original file (e.g. `'plain/text'`, `'text/markdown'` or `'application/vnd.openxmlformats-officedocument.wordprocessingml.document'`)
* `git_working_tree`:
	* Type: `string`
	* Description: (read-only) The Git working tree path (read-only property) from which the file originates. Only PKM can modify this property.
* `git_dirty`:
	* Type: `boolean`
	* Description: (read-only) A dirty flag intended for the PKM Git service to indicate that a PKM File has been modified since the last synchronization with the Git working tree regardless the file is versioned or not. The flag is set automatically on POST/PUT when the file content has changed. Only PKM can reset this flag.
* `git_unmerged`:
	* Type: `boolean`
	* Description: (read-only) A flag intended for the user provided by the PKM Git service to indicate that a PKM File is unmerged (merge conflict ?) since the last synchronization with the Git working tree. Only PKM can set or reset this flag.

The file name (referred as `filename`, `rel_path`, or `sourceFile`) uniquely identifies the file in a project.
It is a path relative to the virtual root directory of the project or the Git working tree.
PKM File names use the POSIX file naming style with no leading slash character.

The PKM organizes files per collection depending on the file type:

* source code,
* UML,
* documentation,
* and executable binaries.

Subsections 7.1.1, 7.1.2, 7.1.3, and 7.1.4 present the specialized operations for each of these collections.

Below are the general operations where the PKM guesses the collection by applying some rules (filename extension, MIME type, etc.) which are configurable in the server-side PKM configuration file `pkm_config.json`.

**Insert files** (REST API 🔐, see FilesApi.postFiles in SDK):

	POST /files/{dbName}
	[
		{
			"rel_path": "relative/path/to/file",
			"content": "…",
			"format": "text",
			"encoding": "utf8"
		}
	]

This operation inserts the given files from the request body into the project with the given name `{dbName}`.
Note that this operation may fail because the PKM cannot dispatch the files in the right file collection due to missing or incomplete file metadata (filename extension, MIME type).

**Insert files** (CLI):

	$ insert_files --user=garfield --db=OpenCV --root=$(pwd) *.c *.java *.docx

**Insert files from a directory on the host machine** (CLI):

	$ insert_files --user=garfield --db=OpenCV --root=$(pwd) --directory=repository

**Insert or Update files** (REST API 🔐, see FilesApi.putFiles in SDK):

	PUT /files/{dbName}
	[
		{
			"rel_path": "relative/path/to/file",
			"content": "…",
			"format": "text",
			"encoding": "utf8"
		}
	]

This operation inserts the given files from the request body into the project with the given name `{dbName}`, or updates existing files into the project with the given name `{dbName}` with the ones given from the request body.
Note that this operation may fail because the PKM cannot dispatch the files in the right file collection due to missing or incomplete file metadata (filename extension, MIME type).

**Update files** (CLI):

	$ update_files --user=garfield --db=OpenCV --root=$(pwd) *.c *.java *.docx
	
**Update files with the one from a directory on the host machine** (CLI):

	$ update_files --user=garfield --db=OpenCV --root=$(pwd) --directory=repository

**Delete a file** (REST API 🔐, see FilesApi.deleteFile in SDK)

	DELETE /files/{dbName}/{filename}

This operation deletes the file with the given name `{filename}` in the project with the given name `{dbName}`.

### 7.1.1 Source codes <a id="1.1"></a>

**Insert source code files** (REST API 🔐, see CodeApi.postRawSourceCodes in SDK):

	POST /code/rawsourcecode/{dbName}
	[
		{
			"rel_path": "OpenCV/modules/core/src/matrix_sparse.cpp",
			"content": "…",
			"format": "text",
			"encoding": "utf8"
		}
	]

This operation inserts the given files, from the request body, as source code files into the project with the given name `{dbName}`.

**Insert source code files** (CLI):

	$ insert_source_files --user=garfield --db=OpenCV --root=$(pwd) ./OpenCV/modules/core/src/matrix_sparse.cpp

**Insert or Update source code files** (REST API 🔐, see CodeApi.putRawSourceCodes in SDK)

	PUT /code/rawsourcecode/{dbName}
	[
		{
			"rel_path": "OpenCV/modules/core/src/matrix_sparse.cpp",
			"content": "…",
			"format": "text",
			"encoding": "utf8"
		}
	]

This operation inserts the given files, from the request body, as source code files into the project with the given name `{dbName}`, or updates existing source code files into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or Update source code files** (CLI):

	$ update_source_files --user=garfield --db=OpenCV --root=$(pwd) OpenCV/modules/core/src/matrix_sparse.cpp

**Delete all source code files** (REST API 🔐, see CodeApi.deleteRawSourceCodes in SDK):

	DELETE /code/rawsourcecode/{dbName}

This operation deletes all the source code files of the project with the given name `{dbName}`.

**Delete a source code file** (REST API 🔐, see CodeApi.deleteRawSourceCode in SDK):

	DELETE /code/rawsourcecode/{dbName}/{filename}

This operation deletes the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete source code files** (CLI):

	$ delete_source_files --user=garfield --db=OpenCV OpenCV/modules/core/src/matrix_sparse.cpp

### 7.1.2 UML <a id="1.2"></a>

**Insert UML2 XMI files** (REST API 🔐, see UMLApi.postRawUMLs in SDK):

	POST /uml/rawuml/{dbName}
	[
		{
			"rel_path": "model.uml",
			"content": "…",
			"format": "text",
			"encoding": "utf8"
		}
	]

This operation inserts the given files, from the request body, as UML files into the project with the given name `{dbName}`.

**Insert UML2 XMI files** (CLI):

	$ insert_uml_files --user=garfield --db=myproject --root=$(pwd) model.uml

**Insert or Update UML2 XMI files** (REST API 🔐, see UMLApi.putRawUMLs in SDK):

	PUT /uml/rawuml/{dbName}
	[
		{
			"rel_path": "model.uml",
			"content": "…",
			"format": "text",
			"encoding": "utf8"
		}
	]
	
This operation inserts the given files, from the request body, as UML files into the project with the given name `{dbName}`, or updates existing UML files into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or Update UML2 XMI files** (CLI):

	$ update_uml_files --user=garfield --db=myproject --root=$(pwd) model.uml

**Delete all UML2 XMI files** (REST API 🔐, see UMLApi.deleteRawUMLs in SDK):

	DELETE /uml/rawuml/{dbName}

This operation deletes all the UML files of the project with the given name `{dbName}`.

**Delete a UML2 XMI file** (REST API 🔐, see UMLApi.deleteRawUML in SDK):

	DELETE /uml/rawuml/{dbName}/{filename}

This operation deletes the UML file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete UML2 XMI files** (CLI):

	$ delete_uml_files --user=garfield --db=myproject model.uml

### 7.1.3 Documentation <a id="1.3"></a>

**Insert documentation files** (REST API 🔐, see DocApi.postRawDocs in SDK):

	POST /doc/rawdoc/{dbName}
	[
		{
			"rel_path": "doc.docx",
			"content": "…",
			"format": "binary"
		}
	]

This operation inserts the given files, from the request body, as documentation files into the project with the given name `{dbName}`.

**Insert documentation files** (CLI):

	$ insert_doc_files --user=garfield --db=myproject --root=$(pwd) doc.docx

**Insert or Update documentation files** (REST API 🔐, see DocApi.putRawDocs in SDK):

	PUT /doc/rawdoc/{dbName}
	[
		{
			"rel_path": "doc.docx",
			"content": "…",
			"format": "binary"
		}
	]

This operation inserts the given files, from the request body, as documentation files into the project with the given name `{dbName}`, or updates existing documentation files into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or Update documentation files** (CLI):

	$ update_doc_files --user=garfield --db=myproject --root=$(pwd) doc.docx

**Delete all documentation files** (REST API 🔐, see DocApi.deleteRawDocs in SDK):

	DELETE /doc/rawdoc/{dbName}

This operation deletes all the documentation files of the project with the given name `{dbName}`.

**Delete a documentation file** (REST API 🔐, see DocApi.deleteRawDoc in SDK):

	DELETE /doc/rawdoc/{dbName}/{filename}
	
This operation deletes the documentation file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete documentation files** (CLI):

	$ delete_doc_files --user=garfield --db=myproject doc.docx

### 7.1.4 Executable binaries <a id="1.4"></a>

**Insert executable binary files** (REST API 🔐, see ExecutableBinaryApi.postExecutableBinaries in SDK):

	POST /bin/executable/{dbName}
	[
		{
			"rel_path": "vmlinux",
			"content": "…",
			"format": "binary"
		}
	]

This operation inserts the given files, from the request body, as executable binary files into the project with the given name `{dbName}`.

**Insert executable binary files** (CLI):

	$ insert_executable_binaries --user=garfield --db=linux --root=$(pwd) vmlinux

**Insert or Update executable binary files** (REST API 🔐, see ExecutableBinaryApi.putExecutableBinaries in SDK):

	PUT /bin/executable/{dbName}
	[
		{
			"rel_path": "vmlinux",
			"content": "…",
			"format": "binary"
		}
	]

This operation inserts the given files, from the request body, as executable binary files into the project with the given name `{dbName}`, or updates existing executable binary files into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or Update executable binary files** (CLI):

	$ update_executable_binaries --user=garfield --db=linux --root=$(pwd) vmlinux

**Delete all executable binary files** (REST API 🔐, see ExecutableBinaryApi.deleteExecutableBinaries in SDK):

	DELETE /bin/executable/{dbName} -user=garfield --db=linux vmlinux

This operation deletes all the executable binary files of the project with the given name `{dbName}`.

**Delete an executable binary file** (REST API 🔐, see ExecutableBinaryApi.deleteExecutableBinary in SDK):

	DELETE /bin/executable/{dbName}/{filename}

This operation deletes the executable binary file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete executable binary files** (CLI):

	$ delete_executable_binaries -user=garfield --db=linux vmlinux

## 7.2 Source Code <a id="2"></a>

The source code parsers (see Section 8.1) provide support for parsing programming languages to the PKM.
The PKM stores the resulting intermediate representations (Abstract syntax trees, comments, and annotations) as structured documents (JSON format) in dedicated collections.
These structured documents are more suited for implementing queries about elements within the source code (see Section 9.2).
These artefacts are all related to a source file, so the source file name which identifies a source file, also uniquely identifies such an artefact.
The following subsections present the operations of the PKM API, which are intended for parsers for feeding the PKM with these structured documents.

### 7.2.1 Abstract Syntax Trees (ASTs) <a id="2.1"></a>

The schemas for the source code ASTs are available on the pkm-api gitlab repository:

* [for the C language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-c-source-code-schema.json)
* [for the C++ language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-cpp-source-code-schema.json)
* [for the Java language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-java-source-code-schema.json)

The operations for ASTs are the following:

**Insert or update C source code ASTs** (REST API 🔐, see CodeApi.putCSourceCodes in SDK):

	PUT /code/c/sourcecode/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"globals": […],
			"globinit": {…}
		}
	]

This operation inserts the given C source code ASTs from the request body into the project with the given name `{dbName}`, or updates existing C source code ASTs into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all C source code ASTs** (REST API 🔐, see CodeApi.deleteCSourceCodes in SDK):

	DELETE /code/c/sourcecode/{dbName}

This operation deletes all the C source code ASTs of the project with the given name `{dbName}`.

**Delete C source code ASTs related to a C source code file** (REST API 🔐, see CodeApi.deleteCSourceCodesBySourceCodeFilename in SDK):

	DELETE /code/c/sourcecode/{dbName}/{filename}

This operation deletes the C source code ASTs related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Insert or update C++ source code ASTs** (REST API 🔐, see CodeApi.putCPPSourceCodes in SDK):

	PUT /code/cpp/sourcecode/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"manifest": […],
			"inner": […]
		}
	]

This operation inserts the given C++ source code ASTs from the request body into the project with the given name `{dbName}`, or updates existing C++ source code ASTs into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all C++ source code ASTs** (REST API 🔐, see CodeApi.deleteCPPSourceCodes in SDK):

	DELETE /code/cpp/sourcecode/{dbName}

This operation deletes all the C++ source code ASTs of the project with the given name `{dbName}`.

**Delete C++ source code ASTs related to a C++ source code file** (REST API 🔐, see CodeApi.deleteCPPSourceCodesBySourceCodeFilename in SDK):

	DELETE /code/cpp/sourcecode/{dbName}/{filename}

This operation deletes the C++ source code ASTs related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Insert Java source code ASTs** (REST API 🔐, see CodeApi.postJavaSourceCodes in SDK):

	POST /code/java/sourcecode/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"CompilationUnit": {…}
		}
	]

This operation inserts the given Java source code ASTs from the request body into the project with the given name `{dbName}`.

**Insert or update source code ASTs** (REST API 🔐, see CodeApi.putJavaSourceCodes in SDK):

	PUT /code/java/sourcecode/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"CompilationUnit": {…}
		}
	]

This operation inserts the given Java source code ASTs from the request body into the project with the given name `{dbName}`, or updates existing Java source code ASTs into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all Java source code ASTs** (REST API 🔐, see CodeApi.deleteJavaSourceCodes in SDK):

	DELETE /code/java/sourcecode/{dbName}

This operation deletes all the Java source code ASTs of the project with the given name `{dbName}`.

**Delete Java source code ASTs related to a source code file** (REST API 🔐, see CodeApi.deleteJavaSourceCodesBySourceCodeFilename in SDK):

	DELETE /code/java/sourcecode/{dbName}/{filename}

This operation deletes the Java source code ASTs related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete all source code ASTs for any language** (REST API 🔐, see CodeApi.deleteSourceCodes in SDK):

	DELETE /code/sourcecode/{dbName}

This operation deletes all the source code ASTs of the project with the given name `{dbName}`.

**Delete source code ASTs for any language related to a source code file** (REST API 🔐, see CodeApi.deleteSourceCodesBySourceCodeFilename in SDK):

	DELETE /code/sourcecode/{dbName}/{filename}

This operation deletes the source code ASTs related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

### 7.2.2 Comments <a id="2.2"></a>

The schemas for the source code comments are available on the pkm-api gitlab repository:

* [for the C language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-c-comments-schema.json)
* [for the C++ language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-cpp-comments-schema.json)
* [for the Java language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-java-comments-schema.json)

The operations for source code comments are the following:

**Insert or update C source code comments** (REST API 🔐, see CodeApi.putCComments in SDK):

	PUT /code/c/comments/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"comments": […]
		}
	]

This operation inserts the given C comments from the request body into the project with the given name `{dbName}`, or updates existing C comments into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all C source code comments** (REST API 🔐, see CodeApi.deleteCComments in SDK):

	DELETE /code/c/comments/{dbName}

This operation deletes all the C comments of the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Delete C source code comments related to a C source code file** (REST API 🔐, see CodeApi.deleteCCommentsBySourceCodeFilename in SDK):

	DELETE /code/c/comments/{dbName}/{filename}

This operation deletes the C comments related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Insert or update C++ source code comments** (REST API 🔐, see CodeApi.putCPPComments in SDK):

	PUT /code/cpp/comments/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"comments": […]
		}
	]

This operation inserts the given C++ comments from the request body into the project with the given name `{dbName}`, or updates existing C++ comments into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all C++ source code comments** (REST API 🔐, see CodeApi.deleteCPPComments in SDK):

	DELETE /code/cpp/comments/{dbName}

This operation deletes all the C++ comments of the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Delete C++ source code comments related to a C++ source code file** (REST API 🔐, see CodeApi.deleteCPPCommentsBySourceCodeFilename in SDK):

	DELETE /code/cpp/comments/{dbName}/{filename}

This operation deletes the C++ comments related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Insert or update Java source code comments** (REST API 🔐, see CodeApi.putJavaComments in SDK):

	PUT /code/java/comments/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"fileEncoding": "utf-8",
			"fileMimeType": "application/json",
			"fileFormat": "json",
			"comments": […]
		}
	]

This operation inserts the given Java comments from the request body into the project with the given name `{dbName}`, or updates existing Java comments into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all Java source code comments** (REST API 🔐, see CodeApi.deleteJavaComments in SDK):

	DELETE /code/java/comments/{dbName}

This operation deletes all the Java comments of the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Delete Java source code comments related to a Java source code file** (REST API 🔐, see CodeApi.deleteJavaCommentsBySourceCodeFilename in SDK):

	DELETE /code/java/comments/{dbName}/{filename}

This operation deletes the Java comments related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Delete all source code comments for any language** (REST API 🔐, see CodeApi.deleteSourceCodeComments in SDK):

	DELETE /code/comments/{dbName}

This operation deletes all the source code comments of the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

**Delete source code comments for any language related to a source code file** (REST API 🔐, see CodeApi.deleteSourceCodeCommentsBySourceCodeFilename in SDK):

	DELETE /code/comments/{dbName}/{filename}

This operation deletes the source code comments related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.
Note that this operation only affects the intermediate representation of comments in the PKM, not comments in source code files.

### 7.2.3 Annotations <a id="2.3"></a>

The schemas for the source code annotations are available on the pkm-api gitlab repository:

* [for the C language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-acsl-schema.json) ([ACSL](https://frama-c.com/html/acsl.html))
* [for the C++ language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-acslpp-schema.json) ([ACSL++](https://frama-c.com/html/acsl.html))
* [for the Java language](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/jml-schema.json) ([OpenJML](http://www.openjml.org))

The operations for source code annotations are the following:

**Insert or update C source code annotations** (REST API 🔐, see CodeApi.putCAnnotations in SDK):

	PUT /code/c/annotations/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"annotations": […]
		}
	]

This operation inserts the given C annotations from the request body into the project with the given name `{dbName}`, or updates existing C annotations into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all C source code annotations** (REST API 🔐, see CodeApi.deleteCAnnotations in SDK):

	DELETE /code/c/annotations/{dbName}

This operation deletes all the C annotations of the project with the given name `{dbName}`.

**Delete C source code annotations related to a C source code file** (REST API 🔐, see CodeApi.deleteCAnnotationsBySourceCodeFilename in SDK):

	DELETE /code/c/annotations/{dbName}/{filename}

This operation deletes the C annotations related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Insert or update C++ source code annotations** (REST API 🔐, see CodeApi.putCPPAnnotations in SDK):

	PUT /code/cpp/annotations/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"annotations": […]
		}
	]

This operation inserts the given C++ annotations from the request body into the project with the given name `{dbName}`, or updates existing C++ annotations into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all C++ source code annotations** (REST API 🔐, see CodeApi.deleteCPPAnnotations in SDK):

	DELETE /code/cpp/annotations/{dbName}

This operation deletes all the C++ annotations of the project with the given name `{dbName}`.

**Delete C++ source code annotations related to a C++ source code file** (REST API 🔐, see CodeApi.deleteCPPAnnotationsBySourceCodeFilename in SDK):

	DELETE /code/cpp/annotations/{dbName}/{filename}

This operation deletes the C++ annotations related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Insert or update Java source code annotations** (REST API 🔐, see CodeApi.putJavaAnnotations in SDK):

	PUT /code/java/annotations/{dbName}
	[
		{
			"sourceFile": "path/to/source/code/file",
			"fileEncoding": "…",
			"fileFormat": 0,
			"fileMimeType": 0,
			"annotations": […]
		}
	]

This operation inserts the given Java annotations from the request body into the project with the given name `{dbName}`, or updates existing Java annotations into the project with the given name `{dbName}` with the ones given from the request body.


**Delete all Java source code annotations** (REST API 🔐, see CodeApi.deleteJavaAnnotations in SDK):

	DELETE /code/java/annotations/{dbName}

This operation deletes all the Java annotations of the project with the given name `{dbName}`.

**Delete Java source code annotations related to a source code file** (REST API 🔐, see CodeApi.deleteJavaAnnotationsBySourceCodeFilename in SDK):

	DELETE /code/java/annotations/{dbName}/{filename}

This operation deletes the Java annotations related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete all source code annotations for any language** (REST API 🔐, see CodeApi.deleteSourceCodeAnnotations in SDK):

	DELETE /code/annotations/{dbName}

This operation deletes all the source code annotations of the project with the given name `{dbName}`.

**Delete source code annotations for any language related to a source code file** (REST API 🔐, see CodeApi.deleteSourceCodeAnnotationsBySourceCodeFilename in SDK):

	DELETE /code/annotations/{dbName}/{filename}

This operation deletes the source code annotations related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

## 7.3 UML classes & state machines <a id="3"></a>

The schema for the UML are available on the pkm-api gitlab repository:

* [for the UML classes](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-uml-class-diagram-schema.json)
* [for the state machines](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-uml-state-model-schema.json)

The `name` property of UML classes or UML state machines uniquely identifies these artefacts.

The operations for UML are the following:

**Insert UML2 class diagrams** (REST API 🔐, see UMLApi.postUMLClassDiagrams in SDK):

	POST /uml/uml_class/{dbName}
	[
		{
			"type": "…",
			"name": "…",
			"diagram": "…",
			"modules": […]
		}
	]

This operation inserts the given UML class diagrams from the request body into the project with the given name `{dbName}`.

**Insert UML2 class diagrams** (CLI):

	$ insert_uml_class_diagram_files --user=garfield --db=myproject class_diagram.json

**Insert or update UML2 class diagrams** (REST API 🔐, see UMLApi.putUMLClassDiagrams in SDK):

	PUT /uml/uml_class/{dbName}
	[
		{
			"type": "…",
			"name": "…",
			"diagram": "…",
			"modules": […]
		}
	]

This operation inserts the given UML class diagrams from the request body into the project with the given name `{dbName}`, or updates existing UML class diagrams into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or update UML2 class diagrams** (CLI):

	$ update_uml_class_diagram_files --user=garfield --db=myproject class_diagram.json

**Delete all UML2 class diagrams** (REST API 🔐, see UMLApi.deleteUMLClassDiagrams in SDK):

	DELETE /uml/uml_class/{dbName}

This operation deletes all UML class diagrams of the project with the given name `{dbName}`.

**Delete an UML2 class diagram** (CLI):

	$ delete_uml_class_diagrams --user=garfield --db=myproject 'MyClassDiagram'

**Insert UML2 state machines** (REST API 🔐, see UMLApi.postUMLStateMachines in SDK):

	POST /uml/uml_state_machine/{dbName}
	[
		{
			"type": "…",
			"id": "…",
			"diagram": "…",
			"regions": […]
		}
	]
	
This operation inserts the given UML state machines from the request body into the project with the given name `{dbName}`.

**Insert UML2 state machines** (CLI):

	$ insert_uml_state_machine_files --user=garfield --db=myproject state_machine.json
	
**Insert or update UML2 state machines** (REST API 🔐, see UMLApi.putUMLStateMachines in SDK):

	PUT /uml/uml_state_machine/{dbName}
	[
		{
			"type": "…",
			"id": "…",
			"diagram": "…",
			"regions": […]
		}
	]

This operation inserts the given UML state machines from the request body into the project with the given name `{dbName}`, or updates existing UML state machines into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or update UML2 state machines** (CLI):

	$ update_uml_state_machine_files --user=garfield --db=myproject state_machine.json

**Delete all UML2 state machines** (REST API 🔐, see UMLApi.deleteUMLStateMachines in SDK):

	DELETE /uml/uml_state_machine/{dbName}

This operation deletes all UML state machines of the project with the given name `{dbName}`.

**Delete an UML2 state machine** (CLI):

	$ delete_uml_state_machines --user=garfield --db=myproject 'MyStateMachine'

## 7.4 Abstract Semi-Formal Model (ASFM) and Graphical documentation (in GSL) <a id="4"></a>

The schema for ASFM is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-asfm-schema.json).
The schema for the graphical documentation of a class written in GSL is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-gsl-schema.json).

The `name` property of an ASFM uniquely identifies it.
The `class` and `object` properties of a class graphical documentation written in GSL uniquely identify it.

The operations for ASFM and the graphical documentation are the following:

**Insert documentation as ASFM** (REST API 🔐, see DocApi.postDocs in SDK):

	POST /doc/asfm/docs/{dbName}
	[
		{
			"sourceFile": "path/to/documentation/file",
			"name": "…",
			"units": […]
		}
	]

This operation inserts the given ASFMs from the request body into the project with the given name `{dbName}`.

**Insert or update documentation as ASFM** (REST API 🔐, see DocApi.putDocs in SDK):

	PUT /doc/asfm/docs/{dbName}
	[
		{
			"sourceFile": "path/to/documentation/file",
			"name": "…",
			"units": […]
		}
	]

This operation inserts the given ASFMs from the request body into the project with the given name `{dbName}`, or updates existing ASFMs into the project with the given name `{dbName}` with the ones given from the request body.

**Delete some (or all) documentation as ASFM** (REST API 🔐, see DocApi.deleteDocs in SDK):

	DELETE /doc/asfm/docs/{dbName}?doc=…&filename=…

This operation deletes the ASFMs matching the query and in the project with the given name `{dbName}`.
When the query part is missing, this deletes all ASFMs.

The query has the following parameters:

* `doc`: name of the ASFM
* `filename`: related documentation filename (e.g. .docx file)

**Delete documentation as ASFM** (CLI):

	$ delete_docs --user=garfield --db=myproject "User's Manual"

**Insert graphical documentations of classes** (REST API 🔐, see DocApi.postGraphicalDocs in SDK):

	POST /doc/gsl/docs/{dbName}
	[
		{
			"class": "…",
			"object": "…",
			"content": {…}
		}
	]

This operation inserts the given graphical class documentations from the request body into the project with the given name `{dbName}`.

**Insert or update graphical documentations of classes** (REST API 🔐, see DocApi.putGraphicalDocs in SDK):

	PUT /doc/gsl/docs/{dbName}
	[
		{
			"class": "…",
			"object": "…",
			"content": {…}
		}
	]

This operation inserts the given graphical class documentations from the request body into the project with the given name `{dbName}`, or updates existing graphical class documentations into the project with the given name `{dbName}` with the ones given from the request body.

**Delete some (or all) graphical documentations of classes** (REST API 🔐, see DocApi.deleteGraphicalDocs in SDK):

	DELETE /doc/gsl/docs/{dbName}?class=…&object=…

This operation deletes the class graphical documentations matching the query and in the project with the given name `{dbName}`.
When the query part is missing, this deletes all the graphical documentations of classes.

The query has the following parameters:

* `class`: class name
* `object`: object name

## 7.5 Compile commands <a id="5"></a>

The `file` property of a compile command uniquely identifies it.

The schema for the compile commands is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-compile-command-schema.json).

The operations for the compile commands are the following:

**Insert compile commands** (REST API 🔐, see CompileCommandApi.postCompileCommands in SDK):

	POST /compile_command/{dbName}
	[
		{
			"directory": "path/to/directory",
			"file": "path/to/file",
			"command": "…"
		}
	]

This operation inserts the given compile commands from the request body into the project with the given name `{dbName}`.

**Insert compile commands** (CLI):

	$ insert_compile_commands --user=garfield --db=myproject compile_commands.json

**Insert or update compile commands** (REST API 🔐, see CompileCommandApi.putCompileCommands in SDK):

	PUT /compile_command/{dbName}
	[
		{
			"directory": "path/to/directory",
			"file": "path/to/file",
			"command": "…"
		}
	]

This operation inserts the given compile commands from the request body into the project with the given name `{dbName}`, or updates existing compile commands into the project with the given name `{dbName}` with the ones given from the request body.

**Insert or update compile commands** (CLI):

	$ update_compile_commands --user=garfield --db=myproject compile_commands.json

**Delete all compile commands** (REST API 🔐, see CompileCommandApi.deleteCompileCommands in SDK):

	DELETE /compile_command/{dbName}

This operation deletes all compile commands of the project with the given name `{dbName}`.

**Delete compile commands related to a source code file** (REST API 🔐, see CompileCommandApi.deleteCompileCommand in SDK):

	DELETE /compile_command/{dbName}/{filename}

This operation deletes compile commands related to the source code file with the given name `{filename}` in the project with the given name `{dbName}`.

**Delete compile commands related to a source code files** (CLI):

	$ delete_compile_commands --user=garfield --db=myproject main.c

## 7.6 Common Vulnerabilities and Exposures (CVE) <a id="6"></a>

The schema for CVE entries is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-cve-schema.json).

The `CVE_data_meta.ID` property of a CVE entry uniquely identifies it.

The operations for CVE entries are the following:

**Insert CVE entries** (REST API 🔐, see CVEApi.postCVEs in SDK):

	POST /cve/{dbName}
	[
		{
			"CVE_data_meta":
			{
				"ID": "…",
				…
			},
			"data_format": "…",
			"data_type": "…",
			"data_version": "…"
		}
	]

This operation inserts the given CVE entries from the request body into the project with the given name `{dbName}`.

**Insert or update CVE entries** (REST API 🔐, see CVEApi.putCVEs in SDK):

	PUT /cve/{dbName}
	[
		{
			"CVE_data_meta":
			{
				"ID": "…",
				…
			},
			"data_format": "…",
			"data_type": "…",
			"data_version": "…"
		}
	]

This operation inserts the given CVE entries from the request body into the project with the given name `{dbName}`, or updates existing CVE entries into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all CVE entries** (REST API 🔐, see CVEApi.deleteCVEs in SDK):

	DELETE /cve/{dbName}

This operation deletes all CVE entries of the project with the given name `{dbName}`.

**Delete a CVE entry** (REST API 🔐, see CVEApi.deleteCVE in SDK):

	DELETE /cve/{dbName}/{id}

This operation deletes the CVE entry with the given `{id}` in the project with the given name `{dbName}`.

## 7.7 Annotations <a id="7"></a>

The schema for annotations is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-annotations-schema.json).

The `path` and `access` properties of an annotation uniquely identify it.
The `_id` property of an annotation that the PKM automatically assigns at insertion is an artefact identifier, which also uniquely identifies the annotation.

The operations for annotations are the following:

**Insert annotations** (REST API 🔐, see AnnotationsApi.postAnnotations in SDK):

	POST /annotations/{dbName}
	[
		{
			"path": "…",
			"access": "…",
			"ner":
			[
				{…}
			],
			"srl":
			[
				{…}
			]
		}
	]

This operation inserts the given annotations from the request body into the project with the given name `{dbName}`, then returns in the response body the list (same order as from the request body) of assigned artefact identifiers.

**Insert or update annotations** (REST API 🔐, see AnnotationsApi.putAnnotations in SDK):

	PUT /annotations/{dbName}
	[
		{
			"path": "…",
			"access": "…",
			"ner":
			[
				{…}
			],
			"srl":
			[
				{…}
			]
		}
	]

This operation inserts the given annotations from the request body into the project with the given name `{dbName}`, or updates existing annotations into the project with the given name `{dbName}` with the ones given from the request body, then returns in the response body the list (same order as from the request body) of assigned artefact identifiers.

**Delete all annotations** (REST API 🔐, see AnnotationsApi.deleteAnnotations in SDK):

	DELETE /annotations/{dbName}?path=…&access=…

This operation deletes the annotations, for the given `{path}` and the given `{access}` when specified, of the project with the given name `{dbName}`.

**Delete annotations related to a path** (REST API 🔐, see AnnotationsApi.deleteAnnotation in SDK):

	DELETE /annotations/{dbName}/{artefactId}

This operation deletes the annotation with the given artefact identifier `{artefactId}` of the project with the given name `{dbName}`.

## 7.8 Traceability Matrix <a id="8"></a>

The schema for the traceability 2D matrix cell is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-traceability-matrix-schema.json).

The `_id` property of a traceability matrix cell that the PKM automatically assigns at insertion, uniquely identifies the traceability matrix cell.

The operations for the traceability matrix are the following:

**Insert Traceability Matrix cells** (REST API 🔐, see TraceabilityMatrixApi.postTraceabilityMatrix in SDK):

	POST /traceability_matrix/{dbName}
	[
		{
			"src_path": "…",
			"src_access": "…",
			"tgt_path": "…",
			"tgt_access": "…",
			"trace":
			{
				"similarity": 0.9,
				"role": "…"
			}
		}
	]

This operation inserts the given traceability matrix cells from the request body into the project with the given name `{dbName}`.
This operation then returns in the response body the list (same order as from the request body) of assigned artefact identifiers.

**Insert or update Traceability Matrix cells** (REST API 🔐, see TraceabilityMatrixApi.putTraceabilityMatrix in SDK):

	PUT /traceability_matrix/{dbName}
	[
		{
			"src_path": "…",
			"src_access": "…",
			"tgt_path": "…",
			"tgt_access": "…",
			"trace":
			{
				"similarity": 0.9,
				"role": "…"
			}
		}
	]

This operation inserts the given traceability matrix cells from the request body into the project with the given name `{dbName}`, or updates existing traceability matrix cells into the project with the given name `{dbName}` with the ones given from the request body, then returns in the response body the list (same order as from the request body) of assigned artefact identifiers.

**Delete the whole Traceability Matrix** (REST API 🔐, see TraceabilityMatrixApi.deleteTraceabilityMatrix in SDK):

	DELETE /traceability_matrix/{dbName}

This operation deletes all traceability matrix cells of the project with the given name `{dbName}`.

**Delete a cell of the Traceability Matrix** (REST API 🔐, see TraceabilityMatrixApi.deleteTraceabilityMatrixCell in SDK):

	DELETE /traceability_matrix/{dbName}/{artefactId}

This operation deletes the traceability matrix cells identified with the given `{artefactId}` of the project with the given name `{dbName}`.

## 7.9 Logs and reports <a id="9"></a>

The schema for logs is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-log-schema.json).

The `_id` property of a log that the PKM automatically assigns at insertion uniquely identifies the log.

The operations for logs are the following:

**Insert logs** (REST API 🔐, see LogApi.postLogs in SDK):

	POST /log/{dbName}
	[
		{
			"tool": "…",
			"nature of report": "…",
			"start running time": "…",
			"end running time": "…",
			"messages": [ "…", … ],
			"warnings": [ "…", … ],
			"errors": [],
			"status": true,
			"details": {…}
		}
	]

This operation inserts the logs from the request body into the project with the given name `{dbName}` then returns in the response body the list (same order as from the request body) of assigned artefact identifiers.

**Insert or update logs** (REST API 🔐, see LogApi.putLogs in SDK):

	PUT /log/{dbName}
	[
		{
			"id": "…",
			"tool": "…",
			"nature of report": "…",
			"start running time": "…",
			"end running time": "…",
			"messages": [ "…", … ],
			"warnings": [ "…", … ],
			"errors": [],
			"status": true,
			"details": {…}
		}
	]

This operation inserts the given logs from the request body into the project with the given name `{dbName}`, or updates existing logs into the project with the given name `{dbName}` with the ones given from the request body, then returns in the response body the list (same order as from the request body) of assigned artefact identifiers.

**Delete all logs** (REST API 🔐, see LogApi.deleteLogs in SDK):

	DELETE /log/{dbName}

This operation deletes all logs of the project with the given name `{dbName}`.

**Delete a log** (REST API 🔐, see LogApi.deleteLog in SDK):

	DELETE /log/{dbName}/{artefactId}

This operation deletes the log identified with the given `{artefactId}` of the project with the given name `{dbName}`.

## 7.10 TESTAR <a id="10"></a>

The schemas for TESTAR are available on the pkm-api gitlab repository:

* [for TESTAR settings](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/TESTAR_Settings_Schema.json)
* [for TESTAR test results](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/TESTAR_TestResults_Schema.json)
* [for TESTAR state model](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/TESTAR_StateModel_Schema.json)

The TESTAR settings are unique in a project.

The `_id` property of TESTAR test results or TESTAR state model that the PKM automatically assigns at insertion, uniquely identifies these artefacts.

The operations for TESTAR are the following:

**Insert TESTAR settings** (REST API 🔐, see TESTARApi.postTESTARSettings in SDK):

	POST /testar/settings/{dbName}
	{
		…
	}

This operation inserts the given TESTAR settings from the request body into the project with the given name `{dbName}`.

**Insert or update TESTAR settings** (REST API 🔐, see TESTARApi.putTESTARSettings in SDK):

	PUT /testar/settings/{dbName}
	{
		…
	}

This operation inserts the given TESTAR settings from the request body into the project with the given name `{dbName}`, or update the existing TESTAR settings into the project with the given name `{dbName}` with the ones given from the request body.

**Delete TESTAR settings** (REST API 🔐, see TESTARApi.deleteTESTARSettings in SDK):

	DELETE /testar/settings/{dbName}

This operation deletes the TESTAR settings of the project with the given name `{dbName}`.

**Insert TESTAR test results** (REST API 🔐, see TESTARApi.postTESTARTestResults in SDK):

	POST /testar/test_results/{dbName}
	{
		"timestamp": "…",
		"url": "…",
		"sequencesResult": […],
		"htmlsResult": […],
		"logsResult": […],
		"sequencesVerdicts": […],
		"sut": {…},
		"tool": {…},
		"settings": {…}
	}

This operation inserts the given TESTAR test results from the request body into the project with the given name `{dbName}` then returns in the response body the assigned artefact identifier in the form `{ "TESTARTestResults artefactId": artefactId }`.

**Insert TESTAR test results** (CLI):

	$ insert_testar_test_results_files --user=garfield --db=myproject data.json

**Insert a TESTAR state model** (REST API 🔐, see TESTARApi.postTESTARStateModel in SDK):

	POST /testar/state_model/{dbName}
	{
		"timestamp": "…",
		"url": "…",
		"sut": {…},
		"tool": {…},
		"stateModelDataStore": "…",
		"stateModelDataStoreType": "…",
		"stateModelDataStoreServer": "…",
		"stateModelDataStoreDirectory": "…",
		"stateModelDataStoreDB": "…",
		"stateModelDataStoreUser": "…",
		"stateModelDataStorePassword": "…",
		"stateModelIdentifier": "…",
		"stateModelAppName": "…",
		"stateModelAppVersion": "…",
		"stateModelDifference": {…},
		"abstractionId": "…",
		"deterministic": true,
		"unvisitedAbstractActions": 43,
		"numberAbstractStates": 13,
		"numberAbstractActions": 21,
		"numberConcreteStates": 22,
		"numberConcreteActions": ,
		"storeWidgets": true,
		"numberWidgets": 495,
		"numberTestSequences": 5,
		"testSequences": […]
	}

This operation inserts the given TESTAR state model from the request body into the project with the given name `{dbName}` then returns in the response body the assigned artefact identifier in the form `{ "TESTARStateModels artefactId": artefactId }`.

**Insert a TESTAR state model** (CLI):

	$ insert_testar_test_model_files --user=garfield --db=myproject data.json

**Delete TESTAR test results** (REST API 🔐, see TESTARApi.deleteTESTARTestResults in SDK):

	DELETE /testar/test_results/{dbName}/{artefactId}

This operation deletes the TESTAR test results with the given `{artefactId}` of the project with the given name `{dbName}`.

**Delete a TESTAR state model** (REST API 🔐, see TESTARApi.deleteTESTARStateModel in SDK):

	DELETE /testar/state_model/{dbName}/{artefactId}

This operation deletes the TESTAR state model with the given `{artefactId}` of the project with the given name `{dbName}`.

## 7.11 Reviews <a id="11"></a>

The schema for the reviews is available on the [pkm-api gitlab repository](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-review-schema.json).

The `reviewID` property of a review uniquely identifies the review.

The operations for the reviews are the following:

**Insert reviews** (REST API 🔐, see ReviewsApi.postReviews in SDK):

	POST /reviews/{dbName}
	[
		{
			"reviewID": "…",
			"reviewTitle": "…",
			"reviewOpenDate": "…",
			"reviewAuthor": "…",
			"reviewStatus": "OPEN"
		}
	]

This operation inserts the given reviews from the request body into the project with the given name `{dbName}`.

**Insert or Update reviews** (REST API 🔐, see ReviewsApi.putReviews in SDK):

	POST /reviews/{dbName}
	[
		{
			"reviewID": "…",
			"reviewTitle": "…",
			"reviewOpenDate": "…",
			"reviewAuthor": "…",
			"reviewStatus": "OPEN"
		}
	]

This operation inserts the given reviews from the request body into the project with the given name `{dbName}`, or updates existing reviews into the project with the given name `{dbName}` with the ones given from the request body.

**Delete some (or all) reviews** (REST API 🔐, see ReviewsApi.deleteReviews in SDK):

	DELETE /reviews/{dbName}?reviewID=…&reviewAuthor=…&reviewStatus=…

This operation deletes the reviews matching the query and in the project with the given name `{dbName}`.
When the query part is missing, this deletes all reviews.

The query has the following parameters:

* `reviewID`: identifier of the review
* `reviewAuthor`: author of the review
* `reviewStatus`: status of the review (either `OPEN` or `CLOSED`)

## 7.12 Process Engine <a id="12"></a>

The process engine, which orchestrates the execution of tools, has dedicated collections for the tool specifications, the tool invocations, and the methodology status (i.e. each phases of the methodology).
The schemas for these collections are available on the pkm-api gitlab repository:

* [for a tool specification](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/tools.json)
* [for a tool invocation](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/invocations.json)
* [for the methodology phases](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-methodology-status-schema.json)

The `toolID` property of a tool specification uniquely identifies it.
The `invocationID` property of a tool invocation uniquely identifies it.
The `id` property of a Methodology phase uniquely identifies it.

The operations for these collections are the following:

**Insert tool specifications** (REST API 🔐, see ToolsApi.postTools in SDK):

	POST /tools/{dbName}
	[
		{
			"toolID": "…",
			"toolName": "…",
			"server": "…",
			"phases": […],
			"tasks": […],
			"endpoint": {…}
		}
	]

This operation inserts tool specifications from the request body into the project with the given name `{dbName}`.

**Insert or update tool specifications** (REST API 🔐, see ToolsApi.putTools in SDK):

	PUT /tools/{dbName}
	[
		{
			"toolID": "…",
			"toolName": "…",
			"server": "…",
			"phases": […],
			"tasks": […],
			"endpoint": {…}
		}
	]

This operation inserts the given tool specifications from the request body into the project with the given name `{dbName}`, or updates existing tool specifications into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all tool specifications** (REST API 🔐, see ToolsApi.deleteTools in SDK):

	DELETE /tools/{dbName}

This operation deletes all the tool specifications of the project with the given name `{dbName}`.

**Delete a tool specification** (REST API 🔐, see ToolsApi.deleteTool in SDK):

	DELETE /tools/{dbName}/{toolID}

This operation deletes the tool specification with the given `{toolID}` in the project with the given name `{dbName}`.

**Insert tool invocations** (REST API 🔐, see InvocationsApi.postInvocations in SDK):

	POST /invocations/{dbName}
	[
		{
			"invocationID": "…",
			"tool": "…",
			"user": "…",
			"timestampRequest": "…",
			"invocationStatus": "…",
			"invocationConfiguration": {…}
		}
	]

This operation inserts tool invocations from the request body for the given `{path}` into the project with the given name `{dbName}` then returns in the response body the list (same order as from the request body) of assigned invocation IDs.
If an `invocationID` is provided, it shall be a string of 24 hex characters, otherwise it is automatically generated.

**Insert or update tool invocations** (REST API 🔐, see InvocationsApi.putInvocations in SDK):

	PUT /invocations/{dbName}
	[
		{
			"invocationID": "…",
			"tool": "…",
			"user": "…",
			"timestampRequest": "…",
			"invocationStatus": "…",
			"invocationConfiguration": {…}
		}
	]

This operation inserts the given tool invocations from the request body into the project with the given name `{dbName}`, or updates existing tool invocations into the project with the given name `{dbName}` with the ones given from the request body.

**Delete all tool invocations** (REST API 🔐, see InvocationsApi.deleteInvocations in SDK):

	DELETE /invocations/{dbName}

This operation deletes all the tool invocations of the project with the given name `{dbName}`.

**Delete a tool invocation** (REST API 🔐, see InvocationsApi.deleteInvocation in SDK):

	DELETE /invocations/{dbName}/{invocationID}

This operation deletes the tool invocation with the given `{invocationID}` in the project with the given name `{dbName}`.

**Insert Methodology status** (REST API 🔐, see MethodologyApi.postMethodologyStatus in SDK):

	POST /methodology/status/{dbName}
	[
		{
			"id": "hld",
			"name": "HLD",
			"phaseNumber": 1,
			"completed": false,
			"description": "High Level Design",
			"tasks": […]
		},
		…
	]

This operation inserts the given Methodology status from the request body into the project with the given name `{dbName}`.

**Insert or update Methodology status** (REST API 🔐, see MethodologyApi.putMethodologyStatus in SDK):

	PUT /methodology/status/{dbName}
	[
		{
			"id": "hld",
			"name": "HLD",
			"phaseNumber": 1,
			"completed": false,
			"description": "High Level Design",
			"tasks": […]
		},
		…
	]

This operation inserts the given Methodology status from the request body into the project with the given name `{dbName}`, or update the existing Methodology status into the project with the given name `{dbName}` with the ones given from the request body.

**Delete Methodology status** (REST API 🔐, see MethodologyApi.deleteMethodologyStatus in SDK):

	DELETE /methodology/status/{dbName}?id=…&name=…&phaseNumber=…

This operation deletes the list of Methodology phases in the project with the given name `{dbName}`.

The query part of the request has the following parameters to select the Methodology phases:

* `id`: identifier of the Methodology phase
* `name`: name of the Methodology phase
* `phaseNumber`: number of the Methodology phase

When the query part is missing, this deletes all the Methodology phases.
