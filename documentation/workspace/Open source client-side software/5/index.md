# 5 Project management

A development project will store its knowledge into the PKM as a set of collections of documents.
These are therefore stored in a specific DB, such as, for instance, the OpenCV DB.
The collections are identical in all projects as the same kind of knowledge is stored.
Chapters 5, 6 and 7 describe these collections of documents.
Only PKM administrators can create or delete projects.

When using the REST API, the schema of a project is the following:

%deps "${BIN}/json_to_md" "${PKM_API}/api/pkm-project-schema.json"
%"${BIN}/json_to_md" --verbatim "${PKM_API}/api/pkm-project-schema.json"

A PKM project has a name, a set of members, each member having a role in the project, the list of tool specifications, the methodology status, and the TESTAR settings. 

The PKM defines the following roles:

* Owner
* Developer
* Reviewer
* Maintainer

A project owner can assign roles in his project.

The PKM provides the following operations for managing projects:

**Create a project** (REST API 🔐, see ProjectApi.postProject in SDK):

	POST /project
	{
		"name": "OpenCV",
		"members": [ { "name" : "garfield", "roles": "Owner" } ]
	}

Create an empty project with a given name and ownership or membership.

**Create a project** (CLI):

	$ create_project --admin=admin --owner=garfield --project=OpenCV
	
Create an empty project with a given name and ownership.

**Create or Update a project** (REST API 🔐, see ProjectApi.putProject in SDK):
	
	PUT /project
	{
		"name": "OpenCV",
		"members": [
			{ "name" : "garfield", "roles": [ "Owner" ] },
			{ "name" : "odie", "roles": [ "Developer" ] }
		]
	}

Create an empty project or update an existing project with a given name and membership.

**Delete a project** (REST API 🔐, see ProjectApi.deleteProject in SDK):

	DELETE /project/{projectName}

This deletes the project with the given name `{projectName}`.

**Delete a project** (CLI):

	$ delete_project --admin=admin --project=myproject

Delete a project with a given name.

There are some more operations related to projects in the legacy command line interface (CLI):

* create_role
* create_db
* create_collection
* create_all_roles
* create_all_collections
* drop_role
* drop_db
* drop_collection
* drop_all_roles
* drop_all_collections

The writer do not recommend the use of these legacy commands.
