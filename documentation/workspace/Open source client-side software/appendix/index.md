# A Appendix

## A.1 REST API documentations <a id="1"></a>

The following REST API documentations (generated with OpenAPI generator) are available online:

* PKM: [https://decoder.ow2.io/pkm-api/public/pkm_restful_api/index.html](https://decoder.ow2.io/pkm-api/public/pkm_restful_api/index.html)
* Frama-C for PKM: [https://decoder.ow2.io/pkm-api/public/frama_c_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/frama_c_restful_api_for_pkm/index.html)
* Frama-Clang for PKM: [https://decoder.ow2.io/pkm-api/public/frama_clang_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/frama_clang_restful_api_for_pkm/index.html)
* ASFM for PKM: [https://decoder.ow2.io/pkm-api/public/asfm_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/asfm_restful_api_for_pkm/index.html)
* UNISIM Excavator for PKM: [https://decoder.ow2.io/pkm-api/public/unisim_excavator_restful_api_for_pkm/index.html](https://decoder.ow2.io/pkm-api/public/unisim_excavator_restful_api_for_pkm/index.html)

## A.2 Command Line Interface (CLI) <a id="2"></a>

**create_all_collections**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_all_collections.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_all_collections.js"

**create_all_roles**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_all_roles.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_all_roles.js"

**create_collection**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_collection.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_collection.js"

**create_db**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_db.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_db.js"

**create_project**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_project.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_project.js"

**create_role**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_role.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_role.js"

**create_user**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/create_user.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/create_user.js"

**delete_compile_commands**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_compile_commands.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_compile_commands.js"

**delete_doc_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_doc_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_doc_files.js"

**delete_docs**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_docs.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_docs.js"

**delete_executable_binary_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_executable_binary_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_executable_binary_files.js"

**delete_project**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_project.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_project.js"

**delete_source_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_source_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_source_files.js"

**delete_uml_class_diagrams**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_uml_class_diagrams.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_uml_class_diagrams.js"

**delete_uml_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_uml_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_uml_files.js"

**delete_uml_state_machines**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/delete_uml_state_machines.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/delete_uml_state_machines.js"

**drop_all_collections**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_all_collections.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_all_collections.js"

**drop_all_roles**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_all_roles.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_all_roles.js"

**drop_all_users**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_all_users.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_all_users.js"

**drop_collection**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_collection.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_collection.js"

**drop_db**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_db.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_db.js"

**drop_role**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_role.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_role.js"

**drop_user**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/drop_user.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/drop_user.js"

**finalize_PKM**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/finalize_PKM.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/finalize_PKM.js"

**find_in_cpp_class**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_cpp_class.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_cpp_class.js"

**find_in_cpp_source**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_cpp_source.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_cpp_source.js"

**find_in_c_source**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_c_source.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_c_source.js"

**find_in_doc**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_doc.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_doc.js"

**find_in_java_class**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_java_class.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_java_class.js"

**find_in_java_source**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_java_source.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_java_source.js"

**find_in_uml_class_diagrams**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/find_in_uml_class_diagrams.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/find_in_uml_class_diagrams.js"

**get_c_annotations**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_c_annotations.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_c_annotations.js"

**get_c_comments**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_c_comments.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_c_comments.js"

**get_compile_commands**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_compile_commands.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_compile_commands.js"

**get_cpp_annotations**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_cpp_annotations.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_cpp_annotations.js"

**get_cpp_comments**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_cpp_comments.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_cpp_comments.js"

**get_cpp_source_codes**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_cpp_source_codes.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_cpp_source_codes.js"

**get_c_source_codes**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_c_source_codes.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_c_source_codes.js"

**get_doc_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_doc_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_doc_files.js"

**get_docs**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_docs.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_docs.js"

**get_executable_binary_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_executable_binary_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_executable_binary_files.js"

**get_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_files.js"

**get_invocations**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_invocations.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_invocations.js"

**get_logs**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_logs.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_logs.js"

**get_project**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_project.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_project.js"

**get_projects**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_projects.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_projects.js"

**get_source_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_source_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_source_files.js"

**get_tools**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_tools.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_tools.js"

**get_uml_class_diagrams**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_uml_class_diagrams.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_uml_class_diagrams.js"

**get_uml_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_uml_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_uml_files.js"

**get_uml_state_machines**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_uml_state_machines.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_uml_state_machines.js"

**get_user_projects**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_user_projects.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_user_projects.js"

**get_users**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/get_users.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/get_users.js"

**initialize_PKM**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/initialize_PKM.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/initialize_PKM.js"

**insert_compile_commands**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_compile_commands.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_compile_commands.js"

**insert_doc_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_doc_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_doc_files.js"

**insert_executable_binary_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_executable_binary_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_executable_binary_files.js"

**insert_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_files.js"

**insert_source_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_source_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_source_files.js"

**insert_testar_state_model_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_testar_state_model_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_testar_state_model_files.js"

**insert_testar_test_results_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_testar_test_results_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_testar_test_results_files.js"

**insert_uml_class_diagram_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_uml_class_diagram_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_uml_class_diagram_files.js"

**insert_uml_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_uml_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_uml_files.js"

**insert_uml_state_machine_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/insert_uml_state_machine_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/insert_uml_state_machine_files.js"

**run_asfm_to_doc**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/run_asfm_to_doc.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/run_asfm_to_doc.js"

**run_doc_to_asfm**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/run_doc_to_asfm.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/run_doc_to_asfm.js"

**run_frama_c**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/run_frama_c.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/run_frama_c.js"

**run_frama_clang**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/run_frama_clang.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/run_frama_clang.js"

**update_compile_commands**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_compile_commands.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_compile_commands.js"

**update_doc_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_doc_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_doc_files.js"

**update_executable_binary_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_executable_binary_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_executable_binary_files.js"

**update_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_files.js"

**update_source_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_source_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_source_files.js"

**update_uml_class_diagram_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_uml_class_diagram_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_uml_class_diagram_files.js"

**update_uml_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_uml_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_uml_files.js"

**update_uml_state_machine_files**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_uml_state_machine_files.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_uml_state_machine_files.js"

**update_user_password**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_user_password.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_user_password.js"

**update_user_role**

%deps "${BIN}/cli_to_md" "${PKM_API}/cli/update_user_role.js"
%"${BIN}/cli_to_md" "${PKM_API}/cli/update_user_role.js"

