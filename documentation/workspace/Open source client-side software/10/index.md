# 10 Conclusion

This document has described the PKM APIs: a command line interface (CLI) and REST APIs.
While the command line interface has been historically the first PKM API available, the REST APIs made it obsolete because it was unsuitable for a SaaS deployment where the data and computing resources are deported in the cloud.
Indeed, the REST APIs have allowed connecting the DECODER Project tools together over communication networks and creating a GUI front-end as a web application.
Every components of the DECODER Project tool-chain, running on separate docker containers, are now able to exchange thanks to these REST APIs.
Using the OpenAPI specifications made it easier to document REST APIs and write SDKs for PKM and parsers clients.
The REST APIs will permit extending the DECODER Project tool-chain and connecting third party tools to the existing tool-chain.
Hopefully, containerized tool-chain components and the REST APIs should enable a SaaS deployment in the future.
