# 3 Session management

When using the PKM REST API, the PKM provides a session management, which basically uses a token bearer authentication scheme.

The PKM provides the following operations for managing sessions:

**Login** (REST API 🔓, see UserApi.login in SDK):

	POST /user/login
	{
		"user_name": "garfield",
		"user_password": "12345678"
	}

This authenticates a user with provided user's credentials (name and password), opens a session and returns a PKM access key in the response body in the form of `{ "key": "…" }` to the client.
The PKM access key is a token, which identifies a user's session.
To use the other REST API methods, the client shall pass, along with other request parts, the PKM access key in the field named `key` of the HTTP request header.
Note that the PKM server limits the user's session lifetime.
However, the PKM server extends the user's session lifetime each time a client requests any other REST API methods (at the beginning of request servicing).

**Logout** (REST API 🔐, see UserApi.logout in SDK):

	POST /user/logout

This closes the user's session.
It invalidates the PKM access key.

**Test user's session life status/Keep user's session alive** (REST API 🔐, see UserApi.access in SDK):

	POST /user/access

This operation is actually a NOP (no operation).
Indeed, it just allows checking whether a PKM access key identifies an alive user's session, and to extend explicitely the lifetime of user's session.

**Duplicate user's session** (REST API 🔐, see UserApi.dup in SDK):

	POST /user/dup

This operation creates another the user's session, and returns a new PKM access key that identifies the new user's session.
The new user's session is independent from the original user's session which means that when the later ends, the former is still alive, and vice-versa.
