# 1 Introduction

## 1.1 Purpose <a name="1"></a>

The PKM (Persistent Knowledge Monitor) is a living repository for every project, where different representations are kept (including source code, design models, specifications, requirements, etc.) as well as their links in a traceability matrix.
The PKM can be queried and enriched by the actors involved in the project, in order to maintain consistency and keep the most updated and precise information about the project.

This document, which presents the PKM API, is intended for:

* Developers to connect front-ends or third party tools to the PKM and debug the PKM,
* Administrators to do basic administration tasks.

The purpose of this document is not about installing the PKM, maintaining or extending the PKM source code, which is the purpose of sibling document titled "Open source PKM server-side software".

The source code of the PKM is available at [https://gitlab.ow2.org/decoder/pkm-api](https://gitlab.ow2.org/decoder/pkm-api).

## 1.2 Licensing <a name="2"></a>

The PKM is an open source software subject to the licensing terms below:

Copyright © 2020-2021 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives, OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.

The PKM server is licensed under [GNU Affero General Public License version 3](https://gitlab.ow2.org/decoder/pkm-api/-/raw/master/AGPL-3.0.txt).

The SDKs for the clients of PKM and parsers are licensed under [Apache License version 2.0](https://gitlab.ow2.org/decoder/pkm-api/-/raw/master/Apache-2.0.txt).

The parsers and tools are licensed under their respective licenses.

## 1.3 Application Programming Interfaces (APIs) <a name="3"></a>

The PKM and the parsers offers two kinds of APIs:

* A Command Line Interface (CLI): the purpose of this API is to provide administrator and developer with commands for administrative tasks and debugging the PKM and the parsers.
  It has been historically the first API available and the reader should consider it as legacy.
  Not every features are available with this interface.
* A REST API: the purpose of this API is to provide access to the PKM and parsers over HTTP/HTTPS, which is convenient in a Software as a Service (SaaS) approach.
  With this approach, the PKM and the parsers are easily distributable and deployable microservices using [Docker](https://www.docker.com).
  This should be the preferred approach for everything else than debugging.

<a id="figure1"></a>

![Figure 1: Simplified layered architecture of the DECODER project toolchain](decoder-architecture.png)

Figure 1 above shows a simplified layered architecture of the DECODER project toolchain.
It highlights the PKM, the parsers, MongoDB, the Git working trees on the local file system of the server, and the APIs of the PKM and the parsers.
① The command line interface allows to invoke PKM and parsers from the command line (PKM can directly manage execution of some parsers);
② The PKM REST API indirectly provides access to the MongoDB database and the Git working trees on the server that hosts the PKM;
③ The Parsers REST API provides an interface to invoke the parsers.

Figure 2 below outlines the endpoints of the PKM REST API, which provide access to the documents in the PKM:<a id="figure2"></a>

![Figure 2: PKM REST API endpoints](pkm-endpoints.png)

The white boxes show the paths of the endpoints.
The arrows outline the flow for generating new artefacts with parsers.
For a reason of limited space, the figure largely underestimates the generation flow of new artefacts when considering the whole DECODER Project tool-chain.

Note that there are some [OpenAPI](https://www.openapis.org/) specifications of the REST APIs available on the pkm-api gitlab repository for the following:

* [PKM](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/api/pkm-openapi.yaml)
* [Frama-C for PKM](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-c/api/frama-c-openapi.yaml)
* [Frama-Clang for PKM](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/frama-clang/api/frama-clang-openapi.yaml)
* [ASFM for PKM](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/asfm/api/asfm-openapi.yaml)
* [UNISIM Excavator for PKM](https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/excavator/api/excavator-openapi.yaml)

The assets of OpenAPI specifications are twofold for the clients: they are useful (1) to document the REST APIs and (2) to generate automatically the SDKs for the clients using [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator) or [Swagger codegen](https://swagger.io/tools/swagger-codegen), which both have support for many programming languages.

## 1.4 Overall organization of the document <a name="4"></a>

<a id="figure3"></a>

![Figure 3: PKM API overview](pkm-api-overview.png)

Figure 3 shows the logical organization of this document (chapter numbers are highlighted in blue circles), which follows the order of actions to use properly the PKM and the parsers.
Chapter 2 shows how to get the general server information such as the version numbers.
Chapter 3 presents the management of the user's session and particularly the way to authenticate with the PKM.
Chapters 4 and 5 present the administrative tasks such as the users and project management.
Chapter 6 presents how to deal with Git repositories.
Chapter 7 shows how to populate the PKM with artefacts.
Chapter 8 presents how to invoke parser to generate new artefacts.
Chapter 9 shows how to query artefacts of a project.
Chapter 10 concludes this document.
The Appendix details the REST APIs and the command line interface (CLI).
