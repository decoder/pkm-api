# 4 Users management

PKM has a user management which leverages on MongoDB user management.

When using the REST API, the schema of a PKM user is the following:

%deps "${BIN}/json_to_md" "${PKM_API}/api/pkm-user-schema.json"
%"${BIN}/json_to_md" --verbatim "${PKM_API}/api/pkm-user-schema.json"

The schema of a Git credential within a PKM user is the following:

%deps "${BIN}/json_to_md" "${PKM_API}/api/pkm-git-user-credential-schema.json"
%"${BIN}/json_to_md" --verbatim "${PKM_API}/api/pkm-git-user-credential-schema.json"

A PKM user has an set of roles in property `roles`, which entries are pair of role name and database (i.e. project) name.
A PKM user with a role in a project is a member.
A PKM administrator is a PKM user with role `'root'` in MongoDB database `'admin'`.
Only PKM administrators can manage users and create or delete projects.

The PKM provides with the following operations for managing users:

**Create a new PKM user** (REST API 🔐, see UserApi.postUser in SDK):

	POST /user
	{
		"name": "garfield",
		"password": "12345678"
	}

This operation creates a new PKM user with given user informations.

**Create a new PKM user** (CLI):

	$ create_user --admin=admin garfield

Create a new PKM user with given user informations.

**Create a new PKM user or update an existing PKM user** (REST API 🔐, see UserApi.putUser in SDK):

	PUT /user
	{
		"name": "garfield",
		"email": "garfield@catcorp.com"
	}

This operation creates a new PKM user or updates an existing PKM user with given user informations.

**Delete a PKM user** (REST API 🔐, see UserApi.deleteUser in SDK)

	DELETE /user/{userName}

This operation deletes a PKM user with a given user's name

There are some more operations related to users in the legacy command line interface (CLI):

* update_user_role
* update_user_password
* create_user
* drop_user
* drop_all_users

The writer do not recommend the use of these legacy commands.
