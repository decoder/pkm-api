#!/usr/bin/env bash
if ! which help2man &> /dev/null; then
	echo "Please install help2man"
	exit 1
fi

if ! which gzip &> /dev/null; then
	echo "Please install gzip"
	exit 1
fi

API=$(cd $(dirname $0); pwd)
FILES="\
create_all_collections \
create_all_roles \
create_collection \
create_db \
create_project \
create_role \
create_user \
delete_compile_commands \
delete_doc_files \
delete_docs \
delete_executable_binary_files \
delete_project \
delete_source_files \
delete_uml_class_diagrams \
delete_uml_files \
delete_uml_state_machines \
drop_all_collections \
drop_all_roles \
drop_all_users \
drop_collection \
drop_db \
drop_role \
drop_user \
finalize_PKM \
find_in_cpp_class \
find_in_cpp_source \
find_in_c_source \
find_in_doc \
find_in_java_class \
find_in_java_source \
find_in_uml_class_diagrams \
get_c_annotations \
get_c_comments \
get_compile_commands \
get_cpp_annotations \
get_cpp_comments \
get_cpp_source_codes \
get_c_source_codes \
get_doc_files \
get_docs \
get_executable_binary_files \
get_files \
get_invocations \
get_logs \
get_project \
get_projects \
get_source_files \
get_tools \
get_uml_class_diagrams \
get_uml_files \
get_uml_state_machines \
get_users \
get_user_projects \
initialize_PKM \
insert_compile_commands \
insert_doc_files \
insert_executable_binary_files \
insert_files \
insert_source_files \
insert_testar_state_model_files \
insert_testar_test_results_files \
insert_uml_class_diagram_files \
insert_uml_files \
insert_uml_state_machine_files \
run_asfm_to_doc \
run_doc_to_asfm \
run_frama_c \
run_frama_clang \
update_compile_commands \
update_doc_files \
update_executable_binary_files \
update_files \
update_source_files \
update_uml_class_diagram_files \
update_uml_files \
update_uml_state_machine_files \
update_user_password \
update_user_role \
"

mkdir -p "${API}/documentation/man/man1"
rm -f "${API}/documentation/man/man1"/*.1
for FILE in ${FILES}; do
	echo -n "Generating manpage for '${FILE}': "
	if [ -x "${API}/bin/${FILE}" ] && help2man "${API}/bin/${FILE}" | gzip > "${API}/documentation/man/man1/${FILE}.1"; then
		echo "success"
	else
		echo "failure"
		exit 1
	fi
done

echo "Success"
echo "The manpages are in '${API}/documentation/man'"
exit 0
