#!/usr/bin/env bash
API=$(cd $(dirname $0); pwd)
PATH="${API}/node_modules/.bin:${PATH}"

if ! which jsdoc &> /dev/null; then
	echo "Please install jsdoc"
	echo "Tips:"
	echo -e "\tnpm install -g jsdoc"
	exit 1
fi

STATUS=0
DEST_DIR="${API}/documentation/public/sdk"
echo -n "Generating SDK documentation in '${DEST_DIR}': "
rm -rf "${DEST_DIR}"
JSDOC_CONFIG=$(mktemp foo.XXX)
echo '{"templates": {"default": {"includeDate": false}}}' > "${JSDOC_CONFIG}"
if jsdoc -c "${JSDOC_CONFIG}" -d "${DEST_DIR}" core/*.js util/*.js; then
		echo "success"
else
		echo "failure"
		STATUS=1
fi
rm -f "${JSDOC_CONFIG}"

exit ${STATUS}

