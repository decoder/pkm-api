	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					id: artefactId
				};
				pkm.delete_logs(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					const error = require('../../core/error');
					if(error instanceof error.PKM_NotFound)
					{
						// uses _id instead of id for very old databases
						
						// Convert the artefact ID into a MongoDB ID
						const ObjectID = require('mongodb').ObjectID;
						try
						{
							query =
							{
								_id : new ObjectID(artefactId.toString())
							};
						}
						catch(err)
						{
							reject(Service.rejectResponse(new String('Not Found'), 404));
							return;
						}
						pkm.delete_logs(dbName, query).then(() =>
						{
							resolve(Service.successResponse(''));
						}).catch((err) =>
						{
							reject(Service.rejectResponse(new String(err.message), err.code));
						});
					}
					else
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
