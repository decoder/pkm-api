	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					id: artefactId
				};
				let options = {};
				if(abbrev) options.projection = { messages : 0, warnings : 0, errors : 0, details : 0, content : 0 };
				pkm.get_logs(dbName, query, options).then((log_documents) =>
				{
					resolve(Service.successResponse(log_documents[0]));
				}).catch((err) =>
				{
					const error = require('../../core/error');
					if(error instanceof error.PKM_NotFound)
					{
						// uses _id instead of id for very old databases
						
						// Convert the artefact ID into a MongoDB ID
						const ObjectID = require('mongodb').ObjectID;
						try
						{
							query =
							{
								_id : new ObjectID(artefactId.toString())
							};
						}
						catch(err)
						{
							reject(Service.rejectResponse(new String('Not Found'), 404));
							return;
						}
						pkm.get_logs(dbName, query).then((log_documents) =>
						{
							resolve(Service.successResponse(log_documents[0]));
						}).catch((err) =>
						{
							reject(Service.rejectResponse(new String(err.message), err.code));
						});
					}
					else
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
