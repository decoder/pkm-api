	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options =
				{
					sort : { 'end running time': -1 }
				};
				if(abbrev) options.projection = { messages : 0, warnings : 0, errors : 0, details : 0, content : 0 };
				if(skip !== undefined) options.skip = skip;
				if(limit != undefined) options.limit = limit;
				pkm.get_logs(dbName, query, options).then((log_documents) =>
				{
					resolve(Service.successResponse(log_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
