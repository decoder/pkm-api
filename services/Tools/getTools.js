	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options;
				if(phase) query.phases = { $in: [ phase ] };
				if(task) query.tasks = { $in: [ task ] };
				if(skip || limit)
				{
					options = {};
					
					if(skip) options.skip = skip;
					if(limit) options.limit = limit;
				}
				pkm.get_tools(dbName, query, options).then((tool_documents) =>
				{
					resolve(Service.successResponse(tool_documents));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
