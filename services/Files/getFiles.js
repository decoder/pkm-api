	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(type) query.fileType = type;
				if(gitWorkingTree) query.gitWorkingTree = gitWorkingTree;
				if(gitDirty) query.gitDirty = gitDirty;
				if(gitUnmerged) query.gitUnmerged = gitUnmerged;
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_any_files(dbName, query, options).then((files) =>
				{
					let response = files.map((file) => file.export());
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
