	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				pkm.get_uml_files(dbName, query, options).then((uml_files) =>
				{
					const uml_file = uml_files[0];
					resolve(Service.successResponse(uml_file.export()));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
