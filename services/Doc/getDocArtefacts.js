	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query.id = id;
				if(doc !== undefined) query.doc = doc;
				if(filename !== undefined) query.filename = filename;
				if(unit !== undefined) query.unit = unit;
				if(_class !== undefined) query.class = _class;
				if(invariant !== undefined) query.invariant = invariant;
				if(field !== undefined) query.field = field;
				if(method !== undefined) query.method = method;
				if(type !== undefined) query.type = type;
				if(macro !== undefined) query.macro = macro;
				if(constant !== undefined) query.constant = constant;
				if(member !== undefined) query.member = member;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.find_in_doc(dbName, query, options).then((doc_documents) =>
				{
					resolve(Service.successResponse(doc_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
