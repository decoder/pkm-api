	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(id !== undefined) query['CVE_data_meta.ID'] = id;
				if(state !== undefined) query['CVE_data_meta.STATE'] = state;
				if(assigner !== undefined) query['CVE_data_meta.ASSIGNER'] = assigner;
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_cves(dbName, query, options).then((cve_documents) =>
				{
					resolve(Service.successResponse(cve_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
