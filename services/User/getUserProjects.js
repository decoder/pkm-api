	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.get_user(userName).then((user) =>
				{
					let options = {};
					if(abbrev) options.abbrev = true;
					pkm.get_user_projects(user, options).then((projects) =>
					{
						resolve(Service.successResponse(projects));
					}).catch((err) =>
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}).finally(() =>
					{
						pkm.release();
					});
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
