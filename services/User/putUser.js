	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const User = require('../../core/user');
				
				const user_name = body.name;
				if(user_name)
				{
					const user = new User(
						user_name,                // user's name
						body.password,            // user's password
						body.first_name,          // first name
						body.last_name,           // last name
						body.email,               // email
						body.phone,               // phone
						body.roles,               // roles
						body.git_user_credentials // Git user's credentials
					);
					pkm.update_user(user).then(() =>
					{
						resolve(Service.successResponse(''));
					}).catch((err) =>
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}).finally(() =>
					{
						pkm.release();
					});
				}
				else
				{
					const msg = 'expecting a user\'s name in PUT user request body';
					console.warn(msg);
					reject(respondWithCode(400, new String(msg)));
					pkm.release();
				}
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
