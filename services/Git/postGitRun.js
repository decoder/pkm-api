	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const git_commands = body.git_commands || [];
				let options = Object.assign({}, body.options);
				const debug = pkm.config.debug;
				
				const Job = require('../../util/job');
				
				let stripped_body = { ...body };
				delete stripped_body.git_user_credentials;
				let job = new Job('git', Object.assign({ dbName : dbName, key : key }, stripped_body), { debug : debug });
				
				if(asynchronous)
				{
					resolve(Service.successResponse(job, 201));
				}
				
				let promise = job.run(new Promise((resolve, reject) =>
				{
					let error;
					let status = true;
					
					pkm.git(dbName, git_commands, { ...options, logger : job }).catch((err) =>
					{
						// delay the promise rejection until the logs in the database have been updated
						error = err;
						status = false;
					}).finally(() =>
					{
						// Update the Logs in the database
						let log_document =
						{
							tool: 'Git',
							'nature of report' : 'Git',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors: job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						pkm.insert_logs(dbName, [ log_document ]).then(() =>
						{
							if(debug)
							{
								job.log('Put a Log document');
							}
							
							if(status)
							{
								resolve();
							}
							else
							{
								reject(error);
							}
						}).catch((post_logs_err) =>
						{
							const { deep_copy } = require('../../util/deep_copy');
							const msg = 'can\'t insert Log documents: ' + JSON.stringify(deep_copy(post_logs_err));
							job.error(msg);
							reject(new Error(msg));
						}).finally(() =>
						{
							pkm.release();
						});
					});
				}));
				
				if(!asynchronous)
				{
					promise.then((job) =>
					{
						resolve(Service.successResponse(job, 201));
					}).catch((job) =>
					{
						reject(Service.rejectResponse(job, 500));
					}).finally(() =>
					{
						job.unpublish();
					});
				}
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
