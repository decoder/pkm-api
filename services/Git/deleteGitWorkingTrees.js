	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(dontDeletePkmFiles) options.dont_delete_pkm_files = true;
				pkm.delete_git_working_trees(dbName, query, options).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
