	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					directory : gitWorkingTree
				};
				pkm.get_git_working_trees(dbName, query).then((git_working_tree_documents) =>
				{
					const git_working_tree_document = git_working_tree_documents[0];
					resolve(Service.successResponse(git_working_tree_document));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
