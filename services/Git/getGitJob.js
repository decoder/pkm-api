	return new Promise((resolve, reject) =>
	{
		try
		{
			const Job = require('../../util/job');
			
			const job = Job.find(jobId);
			if(job !== undefined)
			{
				if(job.parameters.key == key)
				{
					if((job.state == 'failed') || (job.state == 'finished'))
					{
						job.unpublish();
					}
					resolve(Service.successResponse(job, 200));
				}
				else
				{
					reject(Service.rejectResponse(new String('Forbidden'), 403));
				}
			}
			else
			{
				resolve(Service.rejectResponse(new String('Not Found'), 404));
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});
