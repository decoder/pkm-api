	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			const server_info =
			{
				server: PKM.info,
				api: global.expressServer.schema.info
			};
			console.log(JSON.stringify(server_info));
			resolve(Service.successResponse(server_info));
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
