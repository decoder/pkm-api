	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				if(skip) options.skip = skip;
				if(limit) options.limit = limit;
				pkm.get_executable_binary_files(dbName, query, options).then((executable_binary_files) =>
				{
					let response = [];
					executable_binary_files.forEach((executable_binary_file) =>
					{
						response.push(executable_binary_file.export());
					});
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
