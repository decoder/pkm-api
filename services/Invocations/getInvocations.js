	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options =
				{
					sort : { timestampRequest: -1 }
				};
				if(invocationStatus) query.invocationStatus = invocationStatus;
				if(skip) options.skip = skip;
				if(limit) options.limit = limit;
				pkm.get_invocations(dbName, query, options).then((invocation_documents) =>
				{
					resolve(Service.successResponse(invocation_documents));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
