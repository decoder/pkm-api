	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename: filename
				};
				pkm.get_any_files(dbName, query).then((files) =>
				{
					const path = require('path');
					resolve(Service.successResponse(files[0].content, 200, { contentType : files[0].mime_type, attachment : { filename : path.basename(files[0].rel_path) } }));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
