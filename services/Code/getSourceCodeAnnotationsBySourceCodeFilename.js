	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_code_annotations(dbName, query, options).then((source_code_annotations_documents) =>
				{
					resolve(Service.successResponse(source_code_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
