	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query['annotations.id'] = id;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_cpp_annotations(dbName, query, options).then((cpp_annotation_artefact_documents) =>
				{
					let result = cpp_annotation_artefact_documents.map((cpp_annotation_artefact_document) => cpp_annotation_artefact_document.annotations).reduce((acc, val) => acc.concat(val), []);
					resolve(Service.successResponse(result));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
