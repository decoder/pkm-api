	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query.id = id;
				if(kind !== undefined) query.kind = kind;
				if(path !== undefined) query.path = path;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.find_in_cpp_source(dbName, query, options).then((cpp_source_code_artefacts_documents) =>
				{
					resolve(Service.successResponse(cpp_source_code_artefacts_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
