	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'type', typename).then((c_types_documents) =>
				{
					resolve(Service.successResponse(c_types_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
