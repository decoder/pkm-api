	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { rowRole : 'source' };
				if(fromRow !== undefined) options.fromRow = fromRow;
				if(fromColumn !== undefined) options.fromColumn = fromColumn;
				if(toRow !== undefined) options.toRow = toRow;
				if(toColumn !== undefined) options.toColumn = toColumn;
				if(rowRole !== undefined) options.rowRole = rowRole;
				pkm.get_traceability_matrix(dbName, query, options).then((traceability_matrix) =>
				{
					resolve(Service.successResponse(traceability_matrix));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
