	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.get_testar_state_models(dbName, query).then((testar_state_models) =>
				{
					// it should be one and unique JSON document (_id)
					resolve(Service.successResponse(testar_state_models[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});
