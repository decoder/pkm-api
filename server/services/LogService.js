/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete a Log document from the database
*
* dbName String Database name
* artefactId String the ID of the Log document
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteLog = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					id: artefactId
				};
				pkm.delete_logs(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					const error = require('../../core/error');
					if(error instanceof error.PKM_NotFound)
					{
						// uses _id instead of id for very old databases
						
						// Convert the artefact ID into a MongoDB ID
						const ObjectID = require('mongodb').ObjectID;
						try
						{
							query =
							{
								_id : new ObjectID(artefactId.toString())
							};
						}
						catch(err)
						{
							reject(Service.rejectResponse(new String('Not Found'), 404));
							return;
						}
						pkm.delete_logs(dbName, query).then(() =>
						{
							resolve(Service.successResponse(''));
						}).catch((err) =>
						{
							reject(Service.rejectResponse(new String(err.message), err.code));
						});
					}
					else
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Log documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteLogs = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_logs(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a Log document from the database
*
* dbName String Database name
* artefactId String the ID of the Log document
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint (optional)
* returns PkmLog
* */
const getLog = ({ dbName, artefactId, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					id: artefactId
				};
				let options = {};
				if(abbrev) options.projection = { messages : 0, warnings : 0, errors : 0, details : 0, content : 0 };
				pkm.get_logs(dbName, query, options).then((log_documents) =>
				{
					resolve(Service.successResponse(log_documents[0]));
				}).catch((err) =>
				{
					const error = require('../../core/error');
					if(error instanceof error.PKM_NotFound)
					{
						// uses _id instead of id for very old databases
						
						// Convert the artefact ID into a MongoDB ID
						const ObjectID = require('mongodb').ObjectID;
						try
						{
							query =
							{
								_id : new ObjectID(artefactId.toString())
							};
						}
						catch(err)
						{
							reject(Service.rejectResponse(new String('Not Found'), 404));
							return;
						}
						pkm.get_logs(dbName, query).then((log_documents) =>
						{
							resolve(Service.successResponse(log_documents[0]));
						}).catch((err) =>
						{
							reject(Service.rejectResponse(new String(err.message), err.code));
						});
					}
					else
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Log documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without messages, warnings, errors, and details properties)/full response, useful for lowering response footprint (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getLogs = ({ dbName, key, abbrev, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options =
				{
					sort : { 'end running time': -1 }
				};
				if(abbrev) options.projection = { messages : 0, warnings : 0, errors : 0, details : 0, content : 0 };
				if(skip !== undefined) options.skip = skip;
				if(limit != undefined) options.limit = limit;
				pkm.get_logs(dbName, query, options).then((log_documents) =>
				{
					resolve(Service.successResponse(log_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Post one or more Log documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Log documents
* returns List
* */
const postLogs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const log_documents = body;
				pkm.insert_logs(dbName, log_documents).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) one or more Log documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Log documents
* returns List
* */
const putLogs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const log_documents = body;
				pkm.update_logs(dbName, log_documents).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 200));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteLog,
  deleteLogs,
  getLog,
  getLogs,
  postLogs,
  putLogs,
};

