/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete traceability matrix (all traceability matrix cell documents) from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTraceabilityMatrix = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_traceability_matrix(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete a traceability matrix cell document from the database
*
* dbName String Database name
* artefactId String the ID of traceability matrix cell
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTraceabilityMatrixCell = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.delete_traceability_matrix(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get traceability matrix (all traceability matrix cell documents) or a part of it with row number in range [ fromRow, toRow ] and column number in range [ fromColumn, toColumn ] from the database
*
* dbName String Database name
* key String Access key to the PKM
* fromRow Integer number of rows to skip, useful for lowering response footprint (optional)
* fromColumn Integer number of columns to skip, useful for lowering response footprint (optional)
* toRow Integer last row (should be >= fromRow otherwise resulting matrix is empty), useful for lowering response footprint (optional)
* toColumn Integer last column (should be >= fromColumn otherwise resulting matrix is empty), useful for lowering response footprint (optional)
* rowRole String role of row (optional)
* returns List
* */
const getTraceabilityMatrix = ({ dbName, key, fromRow, fromColumn, toRow, toColumn, rowRole }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { rowRole : 'source' };
				if(fromRow !== undefined) options.fromRow = fromRow;
				if(fromColumn !== undefined) options.fromColumn = fromColumn;
				if(toRow !== undefined) options.toRow = toRow;
				if(toColumn !== undefined) options.toColumn = toColumn;
				if(rowRole !== undefined) options.rowRole = rowRole;
				pkm.get_traceability_matrix(dbName, query, options).then((traceability_matrix) =>
				{
					resolve(Service.successResponse(traceability_matrix));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a traceability matrix cell document from the database
*
* dbName String Database name
* artefactId String the ID of traceability matrix cell
* key String Access key to the PKM
* returns PkmTraceabilityMatrix
* */
const getTraceabilityMatrixCell = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.get_traceability_matrix(dbName, query).then((traceability_matrix) =>
				{
					resolve(Service.successResponse(traceability_matrix[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Post one or more traceability matrix cells into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Traceability Matrix cell documents
* returns List
* */
const postTraceabilityMatrix = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const traceability_matrix_documents = body;
				pkm.insert_traceability_matrix(dbName, traceability_matrix_documents).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) one or more traceability matrix cells into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Traceability Matrix cell documents
* returns List
* */
const putTraceabilityMatrix = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const traceability_matrix_documents = body;
				pkm.update_traceability_matrix(dbName, traceability_matrix_documents).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 200));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteTraceabilityMatrix,
  deleteTraceabilityMatrixCell,
  getTraceabilityMatrix,
  getTraceabilityMatrixCell,
  postTraceabilityMatrix,
  putTraceabilityMatrix,
};

