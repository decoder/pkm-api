/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete ASFM documents present in the database
*
* dbName String Database name
* key String Access key to the PKM
* doc String ASFM document name as a bare character string or a /regular expression/ (optional)
* filename String related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/ (optional)
* no response value expected for this operation
* */
const deleteDocs = ({ dbName, key, doc, filename }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(doc !== undefined) query.name = doc;
				if(filename !== undefined) query.sourceFile = filename;
				pkm.delete_docs(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete GSL documents present in the database
*
* dbName String Database name
* key String Access key to the PKM
* _class String GSL class name (optional)
* object String GSL object name (optional)
* no response value expected for this operation
* */
const deleteGraphicalDocs = ({ dbName, key, _class, object }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(_class !== undefined) query.class = _class;
				if(object !== undefined) query.object = object;
				pkm.delete_graphical_docs(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* delete a Documentation file from the database
*
* dbName String Database name
* filename String Documentation filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteRawDoc = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				pkm.delete_doc_files(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Documentation files present in the database together with documents which originate from these Documentation files
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteRawDocs = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_doc_files(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get ASFM artefacts from the database
*
* dbName String Database name
* key String Access key to the PKM
* id Integer artefact identifier (optional)
* doc String ASFM document name as bare character string or a /regular expression/ (optional)
* filename String related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/ (optional)
* unit String unit name as bare character string or a /regular expression/ (optional)
* _class String class name as bare character string or a /regular expression/ (optional)
* invariant String invariant name as bare character string or a /regular expression/ (optional)
* field String field name as bare character string or a /regular expression/ (optional)
* method String method name as bare character string or a /regular expression/ (optional)
* type String type name as bare character string or a /regular expression/ (optional)
* macro String macro name as bare character string or a /regular expression/ (optional)
* constant String constant name as bare character string or a /regular expression/ (optional)
* member String member name (either invariant, field, method, type, macro, or constant name) as bare character string or a /regular expression/ (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getDocArtefacts = ({ dbName, key, id, doc, filename, unit, _class, invariant, field, method, type, macro, constant, member, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query.id = id;
				if(doc !== undefined) query.doc = doc;
				if(filename !== undefined) query.filename = filename;
				if(unit !== undefined) query.unit = unit;
				if(_class !== undefined) query.class = _class;
				if(invariant !== undefined) query.invariant = invariant;
				if(field !== undefined) query.field = field;
				if(method !== undefined) query.method = method;
				if(type !== undefined) query.type = type;
				if(macro !== undefined) query.macro = macro;
				if(constant !== undefined) query.constant = constant;
				if(member !== undefined) query.member = member;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.find_in_doc(dbName, query, options).then((doc_documents) =>
				{
					resolve(Service.successResponse(doc_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get ASFM documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* doc String ASFM document name as a bare character string or a /regular expression/ (optional)
* filename String related documentation filename (e.g. .docx file) as a bare character string or a /regular expression/ (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getDocs = ({ dbName, key, doc, filename, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(doc !== undefined) query.name = doc;
				if(filename !== undefined) query.sourceFile = filename;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_docs(dbName, query, options).then((doc_documents) =>
				{
					resolve(Service.successResponse(doc_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get GSL documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* _class String GSL class name (optional)
* object String GSL object name (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getGraphicalDocs = ({ dbName, key, _class, object, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(_class !== undefined) query.class = _class;
				if(object !== undefined) query.object = object;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_graphical_docs(dbName, query, options).then((doc_documents) =>
				{
					resolve(Service.successResponse(doc_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a single Documentation file from the database
*
* dbName String Database name
* filename String Documentation filename
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* returns PkmFile
* */
const getRawDoc = ({ dbName, filename, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				pkm.get_doc_files(dbName, query, options).then((doc_files) =>
				{
					const doc_file = doc_files[0];
					resolve(Service.successResponse(doc_file.export()));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Documentation files from the database
*
* dbName String Database name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getRawDocs = ({ dbName, key, abbrev, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_doc_files(dbName, query, options).then((doc_files) =>
				{
					let response = [];
					doc_files.forEach((doc_file) =>
					{
						response.push(doc_file.export());
					});
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some ASFM documents (which are not yet present) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List ASFM documents
* returns String
* */
const postDocs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const doc_documents = body;
				pkm.insert_docs(dbName, doc_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some GSL documents (which are not yet present) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List GSL documents
* returns String
* */
const postGraphicalDocs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const graphical_doc_documents = body;
				pkm.insert_graphical_docs(dbName, graphical_doc_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some Documentation files (which are not yet present) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Documentation files
* returns String
* */
const postRawDocs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var doc_files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					doc_files.push(File.from(body_file));
				});
				pkm.insert_doc_files(dbName, doc_files).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some ASFM documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List ASFM documents
* returns String
* */
const putDocs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const doc_documents = body;
				pkm.update_docs(dbName, doc_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some GSL documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List GSL documents
* returns String
* */
const putGraphicalDocs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const graphical_doc_documents = body;
				pkm.update_graphical_docs(dbName, graphical_doc_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some Documentation files into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Documentation files
* no response value expected for this operation
* */
const putRawDocs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var doc_files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					doc_files.push(File.from(body_file));
				});
				
				pkm.update_doc_files(dbName, doc_files).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteDocs,
  deleteGraphicalDocs,
  deleteRawDoc,
  deleteRawDocs,
  getDocArtefacts,
  getDocs,
  getGraphicalDocs,
  getRawDoc,
  getRawDocs,
  postDocs,
  postGraphicalDocs,
  postRawDocs,
  putDocs,
  putGraphicalDocs,
  putRawDocs,
};

