/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Initialize PKM. Create Role 'user' in users database, then create a PKM administrator in users database.
*
* body InlineObject3 
* returns String
* */
const postInitPkm = ({ body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			const User = require('../../core/user');
			const pkm_admin_user = new User(
				body.pkm_admin_user.name,                // user's name
				body.pkm_admin_user.password,            // user's password
				body.pkm_admin_user.first_name,          // first name
				body.pkm_admin_user.last_name,           // last name
				body.pkm_admin_user.email,               // email
				body.pkm_admin_user.phone,               // phone
				body.pkm_admin_user.roles,               // roles
				body.pkm_admin_user.git_user_credentials // Git user's credentials
			);
			
			PKM.initialize(body.superuser_name, body.superuser_auth_db, body.superuser_password, pkm_admin_user).then((pkm) =>
			{
				resolve(Service.successResponse(new String('Created'), 201));
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  postInitPkm,
};

