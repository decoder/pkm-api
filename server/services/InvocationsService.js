/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* delete a Invocation document from the database
*
* dbName String Database name
* invocationID String Invocation ID
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteInvocation = ({ dbName, invocationID, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					invocationID : invocationID
				};
				pkm.delete_invocations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Invocation documents present in the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteInvocations = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_invocations(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Invocation documents by Invocation ID from the database
*
* dbName String Database name
* invocationID String Invocation ID
* key String Access key to the PKM
* returns PkmInvocation
* */
const getInvocation = ({ dbName, invocationID, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					invocationID: invocationID
				};
				pkm.get_invocations(dbName, query).then((invocation_documents) =>
				{
					const invocation_document = invocation_documents[0];
					resolve(Service.successResponse(invocation_document));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Invocation documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* invocationStatus String invocation status (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getInvocations = ({ dbName, key, invocationStatus, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options =
				{
					sort : { timestampRequest: -1 }
				};
				if(invocationStatus) query.invocationStatus = invocationStatus;
				if(skip) options.skip = skip;
				if(limit) options.limit = limit;
				pkm.get_invocations(dbName, query, options).then((invocation_documents) =>
				{
					resolve(Service.successResponse(invocation_documents));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some Invocation documents (which are not yet present) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Invocation documents
* returns String
* */
const postInvocations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const invocation_documents = body;
				pkm.insert_invocations(dbName, invocation_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some Invocation documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Invocation documents
* returns String
* */
const putInvocations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const invocation_documents = body;
				pkm.update_invocations(dbName, invocation_documents).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteInvocation,
  deleteInvocations,
  getInvocation,
  getInvocations,
  postInvocations,
  putInvocations,
};

