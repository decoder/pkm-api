/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete TESTAR settings
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTESTARSettings = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_testar_settings(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete Specific TESTAR StateModel JSON Artefact from the database
*
* dbName String Database name
* artefactId String ArtefactId
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTESTARStateModel = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.delete_testar_state_models(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete Specific TESTAR TestResults JSON Artefact from the database
*
* dbName String Database name
* artefactId String ArtefactId
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTESTARTestResults = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.delete_testar_test_results(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get All TESTAR StateModel JSON Artefacts from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getAllTESTARStateModel = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_testar_state_models(dbName, query, options).then((all_testar_state_model) =>
				{
					resolve(Service.successResponse(all_testar_state_model));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get All TESTAR TestResults JSON Artefacts from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getAllTESTARTestResults = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_testar_test_results(dbName, query, options).then((all_testar_test_results) =>
				{
					resolve(Service.successResponse(all_testar_test_results));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get TESTAR settings
*
* dbName String Database name
* key String Access key to the PKM
* returns Object
* */
const getTESTARSettings = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.get_testar_settings(dbName).then((testar_settings_document) =>
				{
					resolve(Service.successResponse(testar_settings_document));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Specific TESTAR StateModel JSON Artefact from the database
*
* dbName String Database name
* artefactId String ArtefactId
* key String Access key to the PKM
* returns TestarStateModel
* */
const getTESTARStateModel = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.get_testar_state_models(dbName, query).then((testar_state_models) =>
				{
					// it should be one and unique JSON document (_id)
					resolve(Service.successResponse(testar_state_models[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Specific TESTAR TestResults JSON Artefact from the database
*
* dbName String Database name
* artefactId String ArtefactId
* key String Access key to the PKM
* returns TestarTestResults
* */
const getTESTARTestResults = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.get_testar_test_results(dbName, query).then((testar_test_results) =>
				{
					// it should be one and unique JSON document (_id)
					resolve(Service.successResponse(testar_test_results[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert TESTAR settings
*
* dbName String Database name
* key String Access key to the PKM
* body Object TESTAR settings
* returns String
* */
const postTESTARSettings = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const testar_settings_document = body;
				pkm.insert_testar_settings(dbName, testar_settings_document).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some StateModel JSON Artefacts into the database
*
* dbName String Database name
* key String Access key to the PKM
* body TestarStateModel TESTAR StateModel JSON Artefact
* returns pkm-testar-response
* */
const postTESTARStateModel = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const testar_state_model = body;
				pkm.insert_testar_state_models(dbName, [ testar_state_model ]).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some TestResults JSON Artefacts into the database
*
* dbName String Database name
* key String Access key to the PKM
* body TestarTestResults TESTAR TestResults JSON Artefact
* returns pkm-testar-response
* */
const postTESTARTestResults = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const testar_test_results = body;
				pkm.insert_testar_test_results(dbName, [ testar_test_results ]).then((artefactId) =>
				{
					resolve(Service.successResponse(artefactId[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert TESTAR settings or update existing ones
*
* dbName String Database name
* key String Access key to the PKM
* body Object TESTAR settings
* no response value expected for this operation
* */
const putTESTARSettings = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const testar_settings_document = body;
				pkm.update_testar_settings(dbName, testar_settings_document).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteTESTARSettings,
  deleteTESTARStateModel,
  deleteTESTARTestResults,
  getAllTESTARStateModel,
  getAllTESTARTestResults,
  getTESTARSettings,
  getTESTARStateModel,
  getTESTARTestResults,
  postTESTARSettings,
  postTESTARStateModel,
  postTESTARTestResults,
  putTESTARSettings,
};

