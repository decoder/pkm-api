/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* User's access. PKM access key validity is checked and session lifetime is extended until another session timeout (see session_timeout in PKM configuration)
*
* key String Access key to the PKM
* no response value expected for this operation
* */
const access = ({ key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				resolve(Service.successResponse(''));
				pkm.release();
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* delete user
*
* userName String user's name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteUser = ({ userName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.drop_user(userName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Duplicate user's session, returns a new access key to the PKM.
*
* key String Access key to the PKM
* returns Object
* */
const dup = ({ key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const new_pkm = PKM.from(pkm);
				resolve(Service.successResponse({ key : new_pkm.key}));
				pkm.release();
				new_pkm.release();
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get current user
*
* key String Access key to the PKM
* returns PkmUser
* */
const getCurrentUser = ({ key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.get_current_user().then((user) =>
				{
					resolve(Service.successResponse(user));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get user. WARNING! user can only view own Git user's credentials even if he's an administrator.
*
* userName String user's name
* key String Access key to the PKM
* returns PkmUser
* */
const getUser = ({ userName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.get_user(userName).then((user) =>
				{
					resolve(Service.successResponse(user));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a user project
*
* userName String user's name
* projectName String project name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint (optional)
* returns PkmProject
* */
const getUserProject = ({ userName, projectName, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let options = { project_name : projectName };
				if(abbrev) options.abbrev = true;
				pkm.get_user_projects(userName, options).then((projects) =>
				{
					resolve(Service.successResponse(projects[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get user's projects
*
* userName String user's name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (with only project name and members)/full response, useful for lowering response footprint (optional)
* returns List
* */
const getUserProjects = ({ userName, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.get_user(userName).then((user) =>
				{
					let options = {};
					if(abbrev) options.abbrev = true;
					pkm.get_user_projects(user, options).then((projects) =>
					{
						resolve(Service.successResponse(projects));
					}).catch((err) =>
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}).finally(() =>
					{
						pkm.release();
					});
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* User's login, returns an access key to the PKM
*
* body PkmLoginInfo user's login informations
* returns Object
* */
const login = ({ body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.login(body.user_name, body.user_password).then((pkm) =>
			{
				resolve(Service.successResponse({ key : pkm.key}));
				pkm.release();
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* User's logout, invalidates an access key to the PKM
*
* key String Access key to the PKM
* no response value expected for this operation
* */
const logout = ({ key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.logout(key).then((pkm) =>
			{
				resolve(Service.successResponse(''));
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Create a new user. WARNING! an administrator can set Git user's credentials at user creation but cannot alter them with later putUser operation.
*
* key String Access key to the PKM
* body PkmUser user's information
* returns String
* */
const postUser = ({ key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const User = require('../../core/user');
				
				const user_name = body.name;
				const user_password = body.password;
				
				if(user_name && user_password)
				{
					const user = new User(
						user_name,                // user's name
						user_password,            // user's password
						body.first_name,          // first name
						body.last_name,           // last name
						body.email,               // email
						body.phone,               // phone
						body.roles,               // roles
						body.git_user_credentials // Git user's credentials
					);
					pkm.create_user(user).then(() =>
					{
						resolve(Service.successResponse(new String('Created'), 201));
					}).catch((err) =>
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}).finally(() =>
					{
						pkm.release();
					});
				}
				else
				{
					const msg = 'expecting a user\'s name and password in POST user request body';
					console.warn(msg);
					reject(respondWithCode(400, new String(msg)));
					pkm.release();
				}
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Update a user. WARNING! user can only change own Git user's credentials even if he's an administrator.
*
* key String Access key to the PKM
* body PkmUser user's information
* returns String
* */
const putUser = ({ key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const User = require('../../core/user');
				
				const user_name = body.name;
				if(user_name)
				{
					const user = new User(
						user_name,                // user's name
						body.password,            // user's password
						body.first_name,          // first name
						body.last_name,           // last name
						body.email,               // email
						body.phone,               // phone
						body.roles,               // roles
						body.git_user_credentials // Git user's credentials
					);
					pkm.update_user(user).then(() =>
					{
						resolve(Service.successResponse(''));
					}).catch((err) =>
					{
						reject(Service.rejectResponse(new String(err.message), err.code));
					}).finally(() =>
					{
						pkm.release();
					});
				}
				else
				{
					const msg = 'expecting a user\'s name in PUT user request body';
					console.warn(msg);
					reject(respondWithCode(400, new String(msg)));
					pkm.release();
				}
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  access,
  deleteUser,
  dup,
  getCurrentUser,
  getUser,
  getUserProject,
  getUserProjects,
  login,
  logout,
  postUser,
  putUser,
};

