/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete Reviews
*
* dbName String Database name
* key String Access key to the PKM
* reviewID String review identifier (optional)
* reviewAuthor String review author (optional)
* reviewStatus String review status (optional)
* no response value expected for this operation
* */
const deleteReviews = ({ dbName, key, reviewID, reviewAuthor, reviewStatus }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(reviewID !== undefined) query.reviewID = reviewID;
				if(reviewAuthor !== undefined) query.reviewAuthor = reviewAuthor;
				if(reviewStatus !== undefined) query.reviewStatus = reviewStatus;
				pkm.delete_reviews(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Reviews
*
* dbName String Database name
* key String Access key to the PKM
* reviewID String review identifier (optional)
* reviewAuthor String review author (optional)
* reviewStatus String review status (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getReviews = ({ dbName, key, reviewID, reviewAuthor, reviewStatus, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(reviewID !== undefined) query.reviewID = reviewID;
				if(reviewAuthor !== undefined) query.reviewAuthor = reviewAuthor;
				if(reviewStatus !== undefined) query.reviewStatus = reviewStatus;
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_reviews(dbName, query, options).then((review_documents) =>
				{
					resolve(Service.successResponse(review_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert Reviews
*
* dbName String Database name
* key String Access key to the PKM
* body List Reviews
* returns List
* */
const postReviews = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const reviews_documents = body;
				pkm.insert_reviews(dbName, reviews_documents).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert Reviews or update existing ones
*
* dbName String Database name
* key String Access key to the PKM
* body List Reviews
* returns List
* */
const putReviews = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const review_documents = body;
				pkm.update_reviews(dbName, review_documents).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 200));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteReviews,
  getReviews,
  postReviews,
  putReviews,
};

