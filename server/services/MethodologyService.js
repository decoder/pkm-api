/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete Methodology status
*
* dbName String Database name
* key String Access key to the PKM
* id String phase identifier (optional)
* name String phase name (optional)
* phaseNumber Integer phase number (optional)
* no response value expected for this operation
* */
const deleteMethodologyStatus = ({ dbName, key, id, name, phaseNumber }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query.id = id;
				if(name !== undefined) query.name = name;
				if(phaseNumber !== undefined) query.phaseNumber = phaseNumber;
				pkm.delete_methodology_status(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Methodology status
*
* dbName String Database name
* key String Access key to the PKM
* id String phase identifier (optional)
* name String phase name (optional)
* phaseNumber Integer phase number (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getMethodologyStatus = ({ dbName, key, id, name, phaseNumber, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(id !== undefined) query.id = id;
				if(name !== undefined) query.name = name;
				if(phaseNumber !== undefined) query.phaseNumber = phaseNumber;
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_methodology_status(dbName, query, options).then((methodology_status_documents) =>
				{
					resolve(Service.successResponse(methodology_status_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert Methodology status
*
* dbName String Database name
* key String Access key to the PKM
* body List Methodology status
* returns String
* */
const postMethodologyStatus = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const methodology_status_documents = body;
				pkm.insert_methodology_status(dbName, methodology_status_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert Methodology status or update existing one
*
* dbName String Database name
* key String Access key to the PKM
* body List Methodology status
* no response value expected for this operation
* */
const putMethodologyStatus = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const methodology_status_documents = body;
				pkm.update_methodology_status(dbName, methodology_status_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteMethodologyStatus,
  getMethodologyStatus,
  postMethodologyStatus,
  putMethodologyStatus,
};

