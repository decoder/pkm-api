/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete all C annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCAnnotations = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_c_annotations(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the C annotations documents related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCAnnotationsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_c_annotations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all C comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCComments = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_c_comments(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the C comments documents related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCCommentsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_c_comments(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all C++ annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCPPAnnotations = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_cpp_annotations(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the C++ annotations documents related to a C++ source code file from the database
*
* dbName String Database name
* filename String C++ Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCPPAnnotationsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_cpp_annotations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all C++ comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCPPComments = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_cpp_comments(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the C++ comments documents related to a C++ source code file from the database
*
* dbName String Database name
* filename String C++ Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCPPCommentsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_cpp_comments(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all C++ source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCPPSourceCodes = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_cpp_source_codes(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the C++ source code documents (Abstract Syntax Tree) related to a C++ source code file from the database
*
* dbName String Database name
* filename String C++ Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCPPSourceCodesBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_cpp_source_codes(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all C source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCSourceCodes = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_c_source_codes(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the C source code documents (Abstract Syntax Tree) related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCSourceCodesBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_c_source_codes(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Java annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteJavaAnnotations = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_java_annotations(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the Java annotations documents related to a Java source code file from the database
*
* dbName String Database name
* filename String Java Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteJavaAnnotationsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_java_annotations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Java comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteJavaComments = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_java_comments(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the Java comments documents related to a Java source code file from the database
*
* dbName String Database name
* filename String Java Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteJavaCommentsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_java_comments(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Java source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteJavaSourceCodes = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_java_source_codes(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the Java source code documents (Abstract Syntax Tree) related to a Java source code file from the database
*
* dbName String Database name
* filename String Java Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteJavaSourceCodesBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_java_source_codes(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* delete a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteRawSourceCode = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				pkm.delete_source_files(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all source code files present in the database together with source code ASTs, annotations and comments documents which originate from these source code files
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteRawSourceCodes = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_source_files(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all source code annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteSourceCodeAnnotations = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_source_code_annotations(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the source code annotations documents related to a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteSourceCodeAnnotationsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_source_code_annotations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all source code comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteSourceCodeComments = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_source_code_comments(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the source code comments documents related to a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteSourceCodeCommentsBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_source_code_comments(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteSourceCodes = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_source_codes(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete the source code documents (Abstract Syntax Tree) related to a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteSourceCodesBySourceCodeFilename = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				pkm.delete_source_codes(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCAnnotations = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_c_annotations(dbName, query, options).then((c_annotations_documents) =>
				{
					resolve(Service.successResponse(c_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the C annotations documents related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCAnnotationsBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_c_annotations(dbName, query, options).then((c_annotations_documents) =>
				{
					resolve(Service.successResponse(c_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCComments = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_c_comments(dbName, query, options).then((c_comments_documents) =>
				{
					resolve(Service.successResponse(c_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the C comments documents related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCCommentsBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_c_comments(dbName, query, options).then((c_comments_documents) =>
				{
					resolve(Service.successResponse(c_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C functions
*
* dbName String Database name
* key String Access key to the PKM
* returns List
* */
const getCFunctions = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'function').then((c_functions_documents) =>
				{
					resolve(Service.successResponse(c_functions_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get C functions by name
*
* dbName String Database name
* funcname String Function name
* key String Access key to the PKM
* returns List
* */
const getCFunctionsByName = ({ dbName, funcname, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'function', funcname).then((c_functions_documents) =>
				{
					resolve(Service.successResponse(c_functions_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a C++ annotation artefacts
*
* dbName String Database name
* key String Access key to the PKM
* id Integer artefact identifier (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns Object
* */
const getCPPAnnotationArtefacts = ({ dbName, key, id, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query['annotations.id'] = id;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_cpp_annotations(dbName, query, options).then((cpp_annotation_artefact_documents) =>
				{
					let result = cpp_annotation_artefact_documents.map((cpp_annotation_artefact_document) => cpp_annotation_artefact_document.annotations).reduce((acc, val) => acc.concat(val), []);
					resolve(Service.successResponse(result));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C++ annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCPPAnnotations = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_cpp_annotations(dbName, query, options).then((cpp_annotations_documents) =>
				{
					resolve(Service.successResponse(cpp_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the C++ annotations documents related to a C++ source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCPPAnnotationsBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_cpp_annotations(dbName, query, options).then((cpp_annotations_documents) =>
				{
					resolve(Service.successResponse(cpp_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get fields from a C++ class
*
* dbName String Database name
* className String class name
* key String Access key to the PKM
* returns List
* */
const getCPPClassFields = ({ dbName, className, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_cpp_class(dbName, className, 'field').then((cpp_fields_documents) =>
				{
					resolve(Service.successResponse(cpp_fields_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get methods from a C++ class
*
* dbName String Database name
* className String class name
* key String Access key to the PKM
* returns List
* */
const getCPPClassMethods = ({ dbName, className, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_cpp_class(dbName, className, 'method').then((cpp_methods_documents) =>
				{
					resolve(Service.successResponse(cpp_methods_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a C++ comment artefacts
*
* dbName String Database name
* key String Access key to the PKM
* id Integer artefact identifier (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns Object
* */
const getCPPCommentArtefacts = ({ dbName, key, id, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query['comments.id'] = id;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_cpp_comments(dbName, query, options).then((cpp_comment_artefact_documents) =>
				{
					let result = cpp_comment_artefact_documents.map((cpp_comment_artefact_document) => cpp_comment_artefact_document.comments).reduce((acc, val) => acc.concat(val), []);
					resolve(result);
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C++ comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCPPComments = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_cpp_comments(dbName, query, options).then((cpp_comments_documents) =>
				{
					resolve(Service.successResponse(cpp_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the C++ comments documents related to a C++ source code file from the database
*
* dbName String Database name
* filename String C++ Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCPPCommentsBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_cpp_comments(dbName, query, options).then((cpp_comments_documents) =>
				{
					resolve(Service.successResponse(cpp_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a C++ source code artefacts
*
* dbName String Database name
* key String Access key to the PKM
* id Integer artefact identifier (optional)
* kind String kind of C++ artefacts as a bare character string or a /regular expression/ (optional)
* path String abstract path of C++ artefacts as a bare character string or a /regular expression/ (optional)
* name String name of C++ artefacts as a bare character string or a /regular expression/ (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns Object
* */
const getCPPSourceCodeArtefacts = ({ dbName, key, id, kind, path, name, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(id !== undefined) query.id = id;
				if(kind !== undefined) query.kind = kind;
				if(path !== undefined) query.path = path;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.find_in_cpp_source(dbName, query, options).then((cpp_source_code_artefacts_documents) =>
				{
					resolve(Service.successResponse(cpp_source_code_artefacts_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C++ source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCPPSourceCodes = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_cpp_source_codes(dbName, query, options).then((cpp_source_code_documents) =>
				{
					resolve(Service.successResponse(cpp_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the C++ source code documents (Abstract Syntax Tree) related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCPPSourceCodesBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_cpp_source_codes(dbName, query, options).then((cpp_source_code_documents) =>
				{
					resolve(Service.successResponse(cpp_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCSourceCodes = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_c_source_codes(dbName, query, options).then((c_source_code_documents) =>
				{
					resolve(Service.successResponse(c_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the C source code documents (Abstract Syntax Tree) related to a C source code file from the database
*
* dbName String Database name
* filename String C Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getCSourceCodesBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_c_source_codes(dbName, query, options).then((c_source_code_documents) =>
				{
					resolve(Service.successResponse(c_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C types
*
* dbName String Database name
* key String Access key to the PKM
* returns List
* */
const getCTypes = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'type').then((c_types_documents) =>
				{
					resolve(Service.successResponse(c_types_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get C types by name
*
* dbName String Database name
* typename String Type name
* key String Access key to the PKM
* returns List
* */
const getCTypesByName = ({ dbName, typename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'type', typename).then((c_types_documents) =>
				{
					resolve(Service.successResponse(c_types_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all C variables
*
* dbName String Database name
* key String Access key to the PKM
* returns List
* */
const getCVariables = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'variable').then((c_variables_documents) =>
				{
					resolve(Service.successResponse(c_variables_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get C variables by name
*
* dbName String Database name
* varname String Variable name
* key String Access key to the PKM
* returns List
* */
const getCVariablesByName = ({ dbName, varname, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_c_source(dbName, 'variable', varname).then((c_variables_documents) =>
				{
					resolve(Service.successResponse(c_variables_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Java annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getJavaAnnotations = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_java_annotations(dbName, query, options).then((java_annotations_documents) =>
				{
					resolve(Service.successResponse(java_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the Java annotations documents related to a Java source code file from the database
*
* dbName String Database name
* filename String Java Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getJavaAnnotationsBySourceCodeFilename = ({ dbName, filename, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_java_annotations(dbName, query, options).then((java_annotations_documents) =>
				{
					resolve(Service.successResponse(java_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all fields from a Java class
*
* dbName String Database name
* className String class name
* key String Access key to the PKM
* returns List
* */
const getJavaClassFields = ({ dbName, className, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_java_class(dbName, className, 'field').then((java_fields_documents) =>
				{
					resolve(Service.successResponse(java_fields_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all methods from a Java class
*
* dbName String Database name
* className String class name
* key String Access key to the PKM
* returns List
* */
const getJavaClassMethods = ({ dbName, className, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_java_class(dbName, className, 'method').then((java_methods_documents) =>
				{
					resolve(Service.successResponse(java_methods_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Java classes
*
* dbName String Database name
* key String Access key to the PKM
* returns List
* */
const getJavaClasses = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.find_in_java_source(dbName, 'class').then((java_classes_documents) =>
				{
					resolve(Service.successResponse(java_classes_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Java comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getJavaComments = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_java_comments(dbName, query, options).then((java_comments_documents) =>
				{
					resolve(Service.successResponse(java_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the Java comments documents related to a Java source code file from the database
*
* dbName String Database name
* filename String Java Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getJavaCommentsBySourceCodeFilename = ({ dbName, filename, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_java_comments(dbName, query, options).then((java_comments_documents) =>
				{
					resolve(Service.successResponse(java_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Java source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getJavaSourceCodes = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_java_source_codes(dbName, query, options).then((java_source_code_documents) =>
				{
					resolve(Service.successResponse(java_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the Java source code documents (Abstract Syntax Tree) related to a Java source code file from the database
*
* dbName String Database name
* filename String Java Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getJavaSourceCodesBySourceCodeFilename = ({ dbName, filename, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_java_source_codes(dbName, query, options).then((java_source_code_documents) =>
				{
					resolve(Service.successResponse(java_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a single source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* returns PkmFile
* */
const getRawSourceCode = ({ dbName, filename, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				pkm.get_source_files(dbName, query, options).then((source_files) =>
				{
					const source_file = source_files[0];
					resolve(Service.successResponse(source_file.export()));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all source code files from the database
*
* dbName String Database name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getRawSourceCodes = ({ dbName, key, abbrev, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_source_files(dbName, query, options).then((source_files) =>
				{
					let response = [];
					source_files.forEach((source_file) =>
					{
						response.push(source_file.export());
					});
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all source code annotations documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getSourceCodeAnnotations = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_code_annotations(dbName, query, options).then((source_code_annotations_documents) =>
				{
					resolve(Service.successResponse(source_code_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the source code annotations documents related to a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getSourceCodeAnnotationsBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_code_annotations(dbName, query, options).then((source_code_annotations_documents) =>
				{
					resolve(Service.successResponse(source_code_annotations_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all source code comments documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getSourceCodeComments = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_code_comments(dbName, query, options).then((source_code_comments_documents) =>
				{
					resolve(Service.successResponse(source_code_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the source code comments documents related to a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getSourceCodeCommentsBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_code_comments(dbName, query, options).then((source_code_comments_documents) =>
				{
					resolve(Service.successResponse(source_code_comments_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all source code documents (Abstract Syntax Tree) from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getSourceCodes = ({ dbName, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_codes(dbName, query, options).then((source_code_documents) =>
				{
					resolve(Service.successResponse(source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the source code documents (Abstract Syntax Tree) related to a source code file from the database
*
* dbName String Database name
* filename String Source code filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* merge Boolean enable/disable merging according to file (optional)
* returns List
* */
const getSourceCodesBySourceCodeFilename = ({ dbName, filename, key, skip, limit, merge }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					sourceFile : filename
				};
				let options = { merge : true };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				if(merge !== undefined) options.merge = merge;
				pkm.get_source_codes(dbName, query, options).then((source_code_documents) =>
				{
					resolve(Service.successResponse(source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some Java source code (AST)
*
* dbName String Database name
* key String Access key to the PKM
* body List Java source code documents
* no response value expected for this operation
* */
const postJavaSourceCodes = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const java_source_code_documents = body;
				pkm.insert_java_source_code(dbName, java_source_code_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some source code files (which are not yet present) into the database. The related documents (source code AST, comments and annotations) which originate from the source code files are invalidated.
*
* dbName String Database name
* key String Access key to the PKM
* body List Source code files
* returns String
* */
const postRawSourceCodes = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var source_code_files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					source_code_files.push(File.from(body_file));
				});
				pkm.insert_source_files(dbName, source_code_files).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some C annotations documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List 
* no response value expected for this operation
* */
const putCAnnotations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const c_annotations_documents = body;
				pkm.update_c_annotations(dbName, c_annotations_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some C comments documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List C comments documents
* no response value expected for this operation
* */
const putCComments = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const c_comments_documents = body;
				pkm.update_c_comments(dbName, c_comments_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some C++ annotations documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List 
* no response value expected for this operation
* */
const putCPPAnnotations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const cpp_annotations_documents = body;
				pkm.update_cpp_annotations(dbName, cpp_annotations_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some C++ comments documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List C++ comments documents
* no response value expected for this operation
* */
const putCPPComments = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const cpp_comments_documents = body;
				pkm.update_cpp_comments(dbName, cpp_comments_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some C++ source code documents (Abstract Syntax Tree) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List C++ source code documents
* no response value expected for this operation
* */
const putCPPSourceCodes = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const cpp_source_code_documents = body;
				pkm.update_cpp_source_code(dbName, cpp_source_code_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some C source code documents (Abstract Syntax Tree) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List C source code documents
* no response value expected for this operation
* */
const putCSourceCodes = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const c_source_code_documents = body;
				pkm.update_c_source_code(dbName, c_source_code_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some Java annotations documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List 
* no response value expected for this operation
* */
const putJavaAnnotations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const java_annotations_documents = body;
				pkm.update_java_annotations(dbName, java_annotations_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some Java comments documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Java comments documents
* no response value expected for this operation
* */
const putJavaComments = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const java_comments_documents = body;
				pkm.update_java_comments(dbName, java_comments_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some Java source code (AST)
*
* dbName String Database name
* key String Access key to the PKM
* body List Java source code documents
* no response value expected for this operation
* */
const putJavaSourceCodes = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const java_source_code_documents = body;
				pkm.update_java_source_code(dbName, java_source_code_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some source code files into the database. The related documents (source code AST, comments and annotations) which originate from the source code files are invalidated.
*
* dbName String Database name
* key String Access key to the PKM
* body List Source code files
* no response value expected for this operation
* */
const putRawSourceCodes = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var source_code_files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					source_code_files.push(File.from(body_file));
				});
				
				pkm.update_source_files(dbName, source_code_files).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteCAnnotations,
  deleteCAnnotationsBySourceCodeFilename,
  deleteCComments,
  deleteCCommentsBySourceCodeFilename,
  deleteCPPAnnotations,
  deleteCPPAnnotationsBySourceCodeFilename,
  deleteCPPComments,
  deleteCPPCommentsBySourceCodeFilename,
  deleteCPPSourceCodes,
  deleteCPPSourceCodesBySourceCodeFilename,
  deleteCSourceCodes,
  deleteCSourceCodesBySourceCodeFilename,
  deleteJavaAnnotations,
  deleteJavaAnnotationsBySourceCodeFilename,
  deleteJavaComments,
  deleteJavaCommentsBySourceCodeFilename,
  deleteJavaSourceCodes,
  deleteJavaSourceCodesBySourceCodeFilename,
  deleteRawSourceCode,
  deleteRawSourceCodes,
  deleteSourceCodeAnnotations,
  deleteSourceCodeAnnotationsBySourceCodeFilename,
  deleteSourceCodeComments,
  deleteSourceCodeCommentsBySourceCodeFilename,
  deleteSourceCodes,
  deleteSourceCodesBySourceCodeFilename,
  getCAnnotations,
  getCAnnotationsBySourceCodeFilename,
  getCComments,
  getCCommentsBySourceCodeFilename,
  getCFunctions,
  getCFunctionsByName,
  getCPPAnnotationArtefacts,
  getCPPAnnotations,
  getCPPAnnotationsBySourceCodeFilename,
  getCPPClassFields,
  getCPPClassMethods,
  getCPPCommentArtefacts,
  getCPPComments,
  getCPPCommentsBySourceCodeFilename,
  getCPPSourceCodeArtefacts,
  getCPPSourceCodes,
  getCPPSourceCodesBySourceCodeFilename,
  getCSourceCodes,
  getCSourceCodesBySourceCodeFilename,
  getCTypes,
  getCTypesByName,
  getCVariables,
  getCVariablesByName,
  getJavaAnnotations,
  getJavaAnnotationsBySourceCodeFilename,
  getJavaClassFields,
  getJavaClassMethods,
  getJavaClasses,
  getJavaComments,
  getJavaCommentsBySourceCodeFilename,
  getJavaSourceCodes,
  getJavaSourceCodesBySourceCodeFilename,
  getRawSourceCode,
  getRawSourceCodes,
  getSourceCodeAnnotations,
  getSourceCodeAnnotationsBySourceCodeFilename,
  getSourceCodeComments,
  getSourceCodeCommentsBySourceCodeFilename,
  getSourceCodes,
  getSourceCodesBySourceCodeFilename,
  postJavaSourceCodes,
  postRawSourceCodes,
  putCAnnotations,
  putCComments,
  putCPPAnnotations,
  putCPPComments,
  putCPPSourceCodes,
  putCSourceCodes,
  putJavaAnnotations,
  putJavaComments,
  putJavaSourceCodes,
  putRawSourceCodes,
};

