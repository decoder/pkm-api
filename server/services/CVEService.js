/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete a CVE document from the database
*
* dbName String Database name
* id String the ID of the CVE document
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCVE = ({ dbName, id, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = { 'CVE_data_meta.ID' : id };
				pkm.delete_cves(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all CVE documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCVEs = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_cves(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all CVE documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* id String CVE document ID (optional)
* state String CVE state (optional)
* assigner String CVE assigner (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getCVEs = ({ dbName, key, id, state, assigner, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(id !== undefined) query['CVE_data_meta.ID'] = id;
				if(state !== undefined) query['CVE_data_meta.STATE'] = state;
				if(assigner !== undefined) query['CVE_data_meta.ASSIGNER'] = assigner;
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_cves(dbName, query, options).then((cve_documents) =>
				{
					resolve(Service.successResponse(cve_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Post one or more CVE documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List CVE documents (optional)
* no response value expected for this operation
* */
const postCVEs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const cve_documents = body;
				pkm.insert_cves(dbName, cve_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) one or more CVE documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List CVE documents (optional)
* no response value expected for this operation
* */
const putCVEs = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const cve_documents = body;
				pkm.update_cves(dbName, cve_documents).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteCVE,
  deleteCVEs,
  getCVEs,
  postCVEs,
  putCVEs,
};

