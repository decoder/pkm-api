/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* delete a file in the Git working tree associated to the database.
*
* dbName String Database name
* gitWorkingTree String Git working tree
* filename String filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteGitFile = ({ dbName, gitWorkingTree, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				pkm.get_git_files(dbName, gitWorkingTree, query).then((files) =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* delete files in the Git working tree associated to the database.
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteGitFiles = ({ dbName, gitWorkingTree, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_git_files(dbName, gitWorkingTree).then((files) =>
				{
					let response = files.map((file) => file.export());
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* delete a Git Working Tree document from the database
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* dontDeletePkmFiles Boolean a flag to control deletion of files in the PKM together with the Git working trees. (optional)
* no response value expected for this operation
* */
const deleteGitWorkingTree = ({ dbName, gitWorkingTree, key, dontDeletePkmFiles }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					directory : gitWorkingTree
				};
				let options = {};
				if(dontDeletePkmFiles) options.dont_delete_pkm_files = true;
				pkm.delete_git_working_trees(dbName, query, options).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Git Working Tree documents present in the database
*
* dbName String Database name
* key String Access key to the PKM
* dontDeletePkmFiles Boolean a flag to control deletion of files in the PKM together with the Git working trees. (optional)
* no response value expected for this operation
* */
const deleteGitWorkingTrees = ({ dbName, key, dontDeletePkmFiles }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(dontDeletePkmFiles) options.dont_delete_pkm_files = true;
				pkm.delete_git_working_trees(dbName, query, options).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* returns inline_response_200
* */
const getGitConfig = ({ dbName, gitWorkingTree, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.get_git_config(dbName, gitWorkingTree).then((git_config) =>
				{
					let response =
					{
						git_config : git_config
					};
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* get a file in the Git working tree associated to the database.
*
* dbName String Database name
* gitWorkingTree String Git working tree
* filename String filename
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* encoding String text encoding (optional)
* returns PkmFile
* */
const getGitFile = ({ dbName, gitWorkingTree, filename, key, abbrev, encoding }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				let options = {};
				if(abbrev) options.abbrev = true;
				if(encoding !== undefined) options.encoding = encoding;
				pkm.get_git_files(dbName, gitWorkingTree, query, options).then((files) =>
				{
					let response = files[0].export();
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* get files in the Git working tree associated to the database.
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* encoding String text encoding (optional)
* returns List
* */
const getGitFiles = ({ dbName, gitWorkingTree, key, abbrev, encoding }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(abbrev) options.abbrev = true;
				if(encoding !== undefined) options.encoding = encoding;
				pkm.get_git_files(dbName, gitWorkingTree, query, options).then((files) =>
				{
					let response = files.map((file) => file.export());
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Git Job. Getting a finished or failed job, unpublish it (it is no longer available))
*
* jobId Integer Job identifier
* key String Access key to the PKM
* returns GitJob
* */
const getGitJob = ({ jobId, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const Job = require('../../util/job');
			
			const job = Job.find(jobId);
			if(job !== undefined)
			{
				if(job.parameters.key == key)
				{
					if((job.state == 'failed') || (job.state == 'finished'))
					{
						job.unpublish();
					}
					resolve(Service.successResponse(job, 200));
				}
				else
				{
					reject(Service.rejectResponse(new String('Forbidden'), 403));
				}
			}
			else
			{
				resolve(Service.rejectResponse(new String('Not Found'), 404));
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}
/**
* Get Git Working Tree documents by Git working tree from the database
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* returns PkmGitWorkingTree
* */
const getGitWorkingTree = ({ dbName, gitWorkingTree, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					directory : gitWorkingTree
				};
				pkm.get_git_working_trees(dbName, query).then((git_working_tree_documents) =>
				{
					const git_working_tree_document = git_working_tree_documents[0];
					resolve(Service.successResponse(git_working_tree_document));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Git Working Tree documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getGitWorkingTrees = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options;
				if(skip || limit)
				{
					options = {};
					
					if(skip) options.skip = skip;
					if(limit) options.limit = limit;
				}
				pkm.get_git_working_trees(dbName, query, options).then((git_working_tree_documents) =>
				{
					resolve(Service.successResponse(git_working_tree_documents));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* post some files in the Git working tree associated to the database.
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* body List Files
* returns String
* */
const postGitFiles = ({ dbName, gitWorkingTree, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const File = require('../../util/file');
				let files = body.map((body_file) => File.from(body_file));
				pkm.insert_git_files(dbName, gitWorkingTree, files).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Run Git commands
*
* dbName String Database name
* key String Access key to the PKM
* body InlineObject 
* asynchronous Boolean flag to control asynchronous/synchronous execution of Git job (optional)
* returns GitJob
* */
const postGitRun = ({ dbName, key, body, asynchronous }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const git_commands = body.git_commands || [];
				let options = Object.assign({}, body.options);
				const debug = pkm.config.debug;
				
				const Job = require('../../util/job');
				
				let stripped_body = { ...body };
				delete stripped_body.git_user_credentials;
				let job = new Job('git', Object.assign({ dbName : dbName, key : key }, stripped_body), { debug : debug });
				
				if(asynchronous)
				{
					resolve(Service.successResponse(job, 201));
				}
				
				let promise = job.run(new Promise((resolve, reject) =>
				{
					let error;
					let status = true;
					
					pkm.git(dbName, git_commands, { ...options, logger : job }).catch((err) =>
					{
						// delay the promise rejection until the logs in the database have been updated
						error = err;
						status = false;
					}).finally(() =>
					{
						// Update the Logs in the database
						let log_document =
						{
							tool: 'Git',
							'nature of report' : 'Git',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors: job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						pkm.insert_logs(dbName, [ log_document ]).then(() =>
						{
							if(debug)
							{
								job.log('Put a Log document');
							}
							
							if(status)
							{
								resolve();
							}
							else
							{
								reject(error);
							}
						}).catch((post_logs_err) =>
						{
							const { deep_copy } = require('../../util/deep_copy');
							const msg = 'can\'t insert Log documents: ' + JSON.stringify(deep_copy(post_logs_err));
							job.error(msg);
							reject(new Error(msg));
						}).finally(() =>
						{
							pkm.release();
						});
					});
				}));
				
				if(!asynchronous)
				{
					promise.then((job) =>
					{
						resolve(Service.successResponse(job, 201));
					}).catch((job) =>
					{
						reject(Service.rejectResponse(job, 500));
					}).finally(() =>
					{
						job.unpublish();
					});
				}
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Update the Git configuration file of the Git directory (usually .git/config) associated to a Git working tree
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* body InlineObject1 
* no response value expected for this operation
* */
const putGitConfig = ({ dbName, gitWorkingTree, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const git_config = body.git_config;
				pkm.update_git_config(dbName, gitWorkingTree, git_config).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* put some files in the Git working tree associated to the database.
*
* dbName String Database name
* gitWorkingTree String Git working tree
* key String Access key to the PKM
* body List Files
* no response value expected for this operation
* */
const putGitFiles = ({ dbName, gitWorkingTree, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const File = require('../../util/file');
				let files = body.map((body_file) => File.from(body_file));
				pkm.update_git_files(dbName, gitWorkingTree, files).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteGitFile,
  deleteGitFiles,
  deleteGitWorkingTree,
  deleteGitWorkingTrees,
  getGitConfig,
  getGitFile,
  getGitFiles,
  getGitJob,
  getGitWorkingTree,
  getGitWorkingTrees,
  postGitFiles,
  postGitRun,
  putGitConfig,
  putGitFiles,
};

