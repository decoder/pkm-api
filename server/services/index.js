const AnnotationsService = require('./AnnotationsService');
const CVEService = require('./CVEService');
const CodeService = require('./CodeService');
const CompileCommandService = require('./CompileCommandService');
const DbService = require('./DbService');
const DocService = require('./DocService');
const ExecutableBinaryService = require('./ExecutableBinaryService');
const FilesService = require('./FilesService');
const FiniService = require('./FiniService');
const GitService = require('./GitService');
const IOService = require('./IOService');
const InitService = require('./InitService');
const InvocationsService = require('./InvocationsService');
const LogService = require('./LogService');
const MethodologyService = require('./MethodologyService');
const ProjectService = require('./ProjectService');
const ReviewsService = require('./ReviewsService');
const ServerInfoService = require('./ServerInfoService');
const TESTARService = require('./TESTARService');
const ToolsService = require('./ToolsService');
const TraceabilityMatrixService = require('./TraceabilityMatrixService');
const UMLService = require('./UMLService');
const UserService = require('./UserService');

module.exports = {
  AnnotationsService,
  CVEService,
  CodeService,
  CompileCommandService,
  DbService,
  DocService,
  ExecutableBinaryService,
  FilesService,
  FiniService,
  GitService,
  IOService,
  InitService,
  InvocationsService,
  LogService,
  MethodologyService,
  ProjectService,
  ReviewsService,
  ServerInfoService,
  TESTARService,
  ToolsService,
  TraceabilityMatrixService,
  UMLService,
  UserService,
};
