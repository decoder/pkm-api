/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Returns information about the PKM instance
*
* returns PkmServerInfo
* */
const getServerInfo = () => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			const server_info =
			{
				server: PKM.info,
				api: global.expressServer.schema.info
			};
			console.log(JSON.stringify(server_info));
			resolve(Service.successResponse(server_info));
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  getServerInfo,
};

