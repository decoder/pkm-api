/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* delete a file from the database
*
* dbName String Database name
* filename String filename
* key String Access key to the PKM
* returns PkmFile
* */
const deleteFile = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = { filename : filename };
				pkm.delete_any_files(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a file from the database
*
* dbName String Database name
* filename String filename
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* returns PkmFile
* */
const getFile = ({ dbName, filename, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = { filename : filename };
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				pkm.get_any_files(dbName, query, options).then((files) =>
				{
					resolve(Service.successResponse(files[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get files from the database
*
* dbName String Database name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* type String type of file (optional)
* gitWorkingTree String Git working tree (optional)
* gitDirty Boolean A dirty flag intended for the PKM Git service to indicate that a PKM File has been modified since the last synchronization with the Git working tree. (optional)
* gitUnmerged Boolean A flag intended for the user provided by the PKM Git service to indicate that a PKM File is unmerged (merge conflict ?) since the last synchronization with the Git working tree. (optional)
* returns List
* */
const getFiles = ({ dbName, key, abbrev, skip, limit, type, gitWorkingTree, gitDirty, gitUnmerged }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(type) query.fileType = type;
				if(gitWorkingTree) query.gitWorkingTree = gitWorkingTree;
				if(gitDirty) query.gitDirty = gitDirty;
				if(gitUnmerged) query.gitUnmerged = gitUnmerged;
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_any_files(dbName, query, options).then((files) =>
				{
					let response = files.map((file) => file.export());
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some files (which are not yet present) into the database. The related documents (source code AST, comments and annotations, etc. ) which originate from the files are invalidated.
*
* dbName String Database name
* key String Access key to the PKM
* body List Files
* returns String
* */
const postFiles = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					files.push(File.from(body_file));
				});
				pkm.insert_any_files(dbName, files).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some files into the database. The related documents (source code AST, comments and annotations, etc. ) which originate from the files are invalidated.
*
* dbName String Database name
* key String Access key to the PKM
* body List Files
* no response value expected for this operation
* */
const putFiles = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					files.push(File.from(body_file));
				});
				pkm.update_any_files(dbName, files).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteFile,
  getFile,
  getFiles,
  postFiles,
  putFiles,
};

