/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* delete the compile command document related to a file from the database
*
* dbName String Database name
* filename String the filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCompileCommand = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					file : filename
				};
				pkm.delete_compile_commands(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all compile command documents in the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteCompileCommands = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_compile_commands(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get the compile commands documents related to a file from the database
*
* dbName String Database name
* filename String the filename
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getCompileCommand = ({ dbName, filename, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					file : filename
				};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_compile_commands(dbName, query, options).then((compile_commands) =>
				{
					resolve(Service.successResponse(compile_commands));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all compile command documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getCompileCommands = ({ dbName, key, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_compile_commands(dbName, query, options).then((c_source_code_documents) =>
				{
					resolve(Service.successResponse(c_source_code_documents));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Post some compile commands into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Compile command documents
* no response value expected for this operation
* */
const postCompileCommands = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const compile_commands = body;
				pkm.insert_compile_commands(dbName, compile_commands).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Put some compile commands into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Compile command documents
* no response value expected for this operation
* */
const putCompileCommands = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const compile_commands = body;
				pkm.update_compile_commands(dbName, compile_commands).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteCompileCommand,
  deleteCompileCommands,
  getCompileCommand,
  getCompileCommands,
  postCompileCommands,
  putCompileCommands,
};

