/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete all Executable Binary files present in the database together with documents which originate from these Executable Binary files
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteExecutableBinaries = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_executable_binary_files(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* delete a Executable Binary file from the database
*
* dbName String Database name
* filename String Executable Binary filename
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteExecutableBinary = ({ dbName, filename, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				pkm.delete_executable_binary_files(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Executable Binary files from the database
*
* dbName String Database name
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getExecutableBinaries = ({ dbName, key, abbrev, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				if(skip) options.skip = skip;
				if(limit) options.limit = limit;
				pkm.get_executable_binary_files(dbName, query, options).then((executable_binary_files) =>
				{
					let response = [];
					executable_binary_files.forEach((executable_binary_file) =>
					{
						response.push(executable_binary_file.export());
					});
					resolve(Service.successResponse(response));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get a single Executable Binary file from the database
*
* dbName String Database name
* filename String Executable Binary filename
* key String Access key to the PKM
* abbrev Boolean toggle abbreviated response (without content)/full response, useful for lowering response footprint (optional)
* returns PkmFile
* */
const getExecutableBinary = ({ dbName, filename, key, abbrev }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					filename : filename
				};
				let options = {};
				if(abbrev) options.projection = { filecontent : 0 };
				pkm.get_executable_binary_files(dbName, query, options).then((executable_binary_files) =>
				{
					const executable_binary_file = executable_binary_files[0];
					resolve(Service.successResponse(executable_binary_file.export()));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some Executable Binary files (which are not yet present) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Executable Binary files (optional)
* returns String
* */
const postExecutableBinaries = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var executable_binary_files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					executable_binary_files.push(File.from(body_file));
				});
				pkm.insert_executable_binary_files(dbName, executable_binary_files).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some Executable Binary files into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Executable Binary files (optional)
* no response value expected for this operation
* */
const putExecutableBinaries = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				var executable_binary_files = [];
				const File = require('../../util/file');
				body.forEach((body_file) =>
				{
					executable_binary_files.push(File.from(body_file));
				});
				
				pkm.update_executable_binary_files(dbName, executable_binary_files).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteExecutableBinaries,
  deleteExecutableBinary,
  getExecutableBinaries,
  getExecutableBinary,
  postExecutableBinaries,
  putExecutableBinaries,
};

