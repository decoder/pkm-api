/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Finalize PKM. Drop Role 'user' in users database, then drop all users in users database.
*
* body InlineObject4 
* no response value expected for this operation
* */
const postFiniPkm = ({ body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.finalize(body.superuser_name, body.superuser_auth_db, body.superuser_password).then((pkm) =>
			{
				resolve(Service.successResponse(new String('Created'), 201));
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  postFiniPkm,
};

