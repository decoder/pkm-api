/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Delete annotation with the given ID in the given project
*
* dbName String Database name
* artefactId String the ID of the annotation
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteAnnotation = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.delete_annotations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete annotations concerning the given dbName
*
* dbName String Database name
* key String Access key to the PKM
* path String the url-coded path in the pkm the retrieved annotations are about (optional)
* access String json path allowing to retrieve the text to analyze in the retrieved data (optional)
* no response value expected for this operation
* */
const deleteAnnotations = ({ dbName, key, path, access }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(path !== undefined) query.path = path;
				if(access !== undefined) query.access = access;
				pkm.delete_annotations(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get annotation with the given ID in the given project
*
* dbName String Database name
* artefactId String the ID of the annotation
* key String Access key to the PKM
* returns PkmAnnotations
* */
const getAnnotation = ({ dbName, artefactId, key }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				// Convert the artefact ID into a MongoDB ID
				const ObjectID = require('mongodb').ObjectID;
				try
				{
					query._id = new ObjectID(artefactId.toString());
				}
				catch(err)
				{
					reject(Service.rejectResponse(new String('Not Found'), 404));
					return;
				}
				pkm.get_annotations(dbName, query).then((annotations) =>
				{
					resolve(Service.successResponse(annotations[0]));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get annotations in the given project
*
* dbName String Database name
* key String Access key to the PKM
* path String the url-coded path in the pkm the retrieved annotations are about (optional)
* access String json path allowing to retrieve the text to analyze in the retrieved data (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getAnnotations = ({ dbName, key, path, access, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{	
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				if(path !== undefined) query.path = path;
				if(access !== undefined) query.access = access;
				let options = {};
				if(skip !== undefined) options.skip = skip;
				if(limit !== undefined) options.limit = limit;
				pkm.get_annotations(dbName, query, options).then((annotations) =>
				{
					resolve(Service.successResponse(annotations));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert annotations in the given project
*
* dbName String Database name
* key String Access key to the PKM
* body List Annotations
* returns List
* */
const postAnnotations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const annotations = body;
				pkm.insert_annotations(dbName, annotations).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 201));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some annotations into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Annotations
* returns List
* */
const putAnnotations = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const annotations = body;
				pkm.update_annotations(dbName, annotations).then((artefactIds) =>
				{
					resolve(Service.successResponse(artefactIds, 200));
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
				}).finally(() =>
				{
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteAnnotation,
  deleteAnnotations,
  getAnnotation,
  getAnnotations,
  postAnnotations,
  putAnnotations,
};

