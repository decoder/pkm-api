/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* delete a Tool document from the database
*
* dbName String Database name
* toolID String Tool ID
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTool = ({ dbName, toolID, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					toolID : toolID
				};
				pkm.delete_tools(dbName, query).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Delete all Tool documents present in the database
*
* dbName String Database name
* key String Access key to the PKM
* no response value expected for this operation
* */
const deleteTools = ({ dbName, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				pkm.delete_tools(dbName).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get Tool documents by Tool ID from the database
*
* dbName String Database name
* toolID String Tool ID
* key String Access key to the PKM
* returns PkmTool
* */
const getTool = ({ dbName, toolID, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query =
				{
					toolID: toolID
				};
				pkm.get_tools(dbName, query).then((tool_documents) =>
				{
					const tool_document = tool_documents[0];
					resolve(Service.successResponse(tool_document));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Get all Tool documents from the database
*
* dbName String Database name
* key String Access key to the PKM
* phase String phase (optional)
* task String task (optional)
* skip Integer number of elements to skip, useful for lowering response footprint (optional)
* limit Integer maximum number of elements in the response (zero means no limit), useful for lowering response footprint (optional)
* returns List
* */
const getTools = ({ dbName, key, phase, task, skip, limit }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				let query = {};
				let options;
				if(phase) query.phases = { $in: [ phase ] };
				if(task) query.tasks = { $in: [ task ] };
				if(skip || limit)
				{
					options = {};
					
					if(skip) options.skip = skip;
					if(limit) options.limit = limit;
				}
				pkm.get_tools(dbName, query, options).then((tool_documents) =>
				{
					resolve(Service.successResponse(tool_documents));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert some Tool documents (which are not yet present) into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Tool documents (optional)
* returns String
* */
const postTools = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const tool_documents = body;
				pkm.insert_tools(dbName, tool_documents).then(() =>
				{
					resolve(Service.successResponse(new String('Created'), 201));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}
/**
* Insert (if not yet present) or update (if already present) some Tool documents into the database
*
* dbName String Database name
* key String Access key to the PKM
* body List Tool documents (optional)
* returns String
* */
const putTools = ({ dbName, key, body }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const PKM = require('../../core/pkm');
			
			PKM.access(key).then((pkm) =>
			{
				const tool_documents = body;
				pkm.update_tools(dbName, tool_documents).then(() =>
				{
					resolve(Service.successResponse(''));
					pkm.release();
				}).catch((err) =>
				{
					reject(Service.rejectResponse(new String(err.message), err.code));
					pkm.release();
				});
			}).catch((err) =>
			{
				reject(Service.rejectResponse(new String(err.message), err.code));
			});
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500));
		}
	});

}

module.exports = {
  deleteTool,
  deleteTools,
  getTool,
  getTools,
  postTools,
  putTools,
};

