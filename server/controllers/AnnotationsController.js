/**
 * The AnnotationsController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/AnnotationsService');
const deleteAnnotation = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteAnnotation);
};

const deleteAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteAnnotations);
};

const getAnnotation = async (request, response) => {
  await Controller.handleRequest(request, response, service.getAnnotation);
};

const getAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.getAnnotations);
};

const postAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.postAnnotations);
};

const putAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.putAnnotations);
};


module.exports = {
  deleteAnnotation,
  deleteAnnotations,
  getAnnotation,
  getAnnotations,
  postAnnotations,
  putAnnotations,
};
