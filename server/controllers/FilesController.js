/**
 * The FilesController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/FilesService');
const deleteFile = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteFile);
};

const getFile = async (request, response) => {
  await Controller.handleRequest(request, response, service.getFile);
};

const getFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.getFiles);
};

const postFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.postFiles);
};

const putFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.putFiles);
};


module.exports = {
  deleteFile,
  getFile,
  getFiles,
  postFiles,
  putFiles,
};
