/**
 * The ToolsController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/ToolsService');
const deleteTool = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTool);
};

const deleteTools = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTools);
};

const getTool = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTool);
};

const getTools = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTools);
};

const postTools = async (request, response) => {
  await Controller.handleRequest(request, response, service.postTools);
};

const putTools = async (request, response) => {
  await Controller.handleRequest(request, response, service.putTools);
};


module.exports = {
  deleteTool,
  deleteTools,
  getTool,
  getTools,
  postTools,
  putTools,
};
