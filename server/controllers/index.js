const AnnotationsController = require('./AnnotationsController');
const CVEController = require('./CVEController');
const CodeController = require('./CodeController');
const CompileCommandController = require('./CompileCommandController');
const DbController = require('./DbController');
const DocController = require('./DocController');
const ExecutableBinaryController = require('./ExecutableBinaryController');
const FilesController = require('./FilesController');
const FiniController = require('./FiniController');
const GitController = require('./GitController');
const IOController = require('./IOController');
const InitController = require('./InitController');
const InvocationsController = require('./InvocationsController');
const LogController = require('./LogController');
const MethodologyController = require('./MethodologyController');
const ProjectController = require('./ProjectController');
const ReviewsController = require('./ReviewsController');
const ServerInfoController = require('./ServerInfoController');
const TESTARController = require('./TESTARController');
const ToolsController = require('./ToolsController');
const TraceabilityMatrixController = require('./TraceabilityMatrixController');
const UMLController = require('./UMLController');
const UserController = require('./UserController');

module.exports = {
  AnnotationsController,
  CVEController,
  CodeController,
  CompileCommandController,
  DbController,
  DocController,
  ExecutableBinaryController,
  FilesController,
  FiniController,
  GitController,
  IOController,
  InitController,
  InvocationsController,
  LogController,
  MethodologyController,
  ProjectController,
  ReviewsController,
  ServerInfoController,
  TESTARController,
  ToolsController,
  TraceabilityMatrixController,
  UMLController,
  UserController,
};
