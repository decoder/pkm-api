/**
 * The TraceabilityMatrixController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/TraceabilityMatrixService');
const deleteTraceabilityMatrix = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTraceabilityMatrix);
};

const deleteTraceabilityMatrixCell = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTraceabilityMatrixCell);
};

const getTraceabilityMatrix = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTraceabilityMatrix);
};

const getTraceabilityMatrixCell = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTraceabilityMatrixCell);
};

const postTraceabilityMatrix = async (request, response) => {
  await Controller.handleRequest(request, response, service.postTraceabilityMatrix);
};

const putTraceabilityMatrix = async (request, response) => {
  await Controller.handleRequest(request, response, service.putTraceabilityMatrix);
};


module.exports = {
  deleteTraceabilityMatrix,
  deleteTraceabilityMatrixCell,
  getTraceabilityMatrix,
  getTraceabilityMatrixCell,
  postTraceabilityMatrix,
  putTraceabilityMatrix,
};
