/**
 * The TESTARController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/TESTARService');
const deleteTESTARSettings = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTESTARSettings);
};

const deleteTESTARStateModel = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTESTARStateModel);
};

const deleteTESTARTestResults = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteTESTARTestResults);
};

const getAllTESTARStateModel = async (request, response) => {
  await Controller.handleRequest(request, response, service.getAllTESTARStateModel);
};

const getAllTESTARTestResults = async (request, response) => {
  await Controller.handleRequest(request, response, service.getAllTESTARTestResults);
};

const getTESTARSettings = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTESTARSettings);
};

const getTESTARStateModel = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTESTARStateModel);
};

const getTESTARTestResults = async (request, response) => {
  await Controller.handleRequest(request, response, service.getTESTARTestResults);
};

const postTESTARSettings = async (request, response) => {
  await Controller.handleRequest(request, response, service.postTESTARSettings);
};

const postTESTARStateModel = async (request, response) => {
  await Controller.handleRequest(request, response, service.postTESTARStateModel);
};

const postTESTARTestResults = async (request, response) => {
  await Controller.handleRequest(request, response, service.postTESTARTestResults);
};

const putTESTARSettings = async (request, response) => {
  await Controller.handleRequest(request, response, service.putTESTARSettings);
};


module.exports = {
  deleteTESTARSettings,
  deleteTESTARStateModel,
  deleteTESTARTestResults,
  getAllTESTARStateModel,
  getAllTESTARTestResults,
  getTESTARSettings,
  getTESTARStateModel,
  getTESTARTestResults,
  postTESTARSettings,
  postTESTARStateModel,
  postTESTARTestResults,
  putTESTARSettings,
};
