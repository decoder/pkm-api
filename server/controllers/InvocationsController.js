/**
 * The InvocationsController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/InvocationsService');
const deleteInvocation = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteInvocation);
};

const deleteInvocations = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteInvocations);
};

const getInvocation = async (request, response) => {
  await Controller.handleRequest(request, response, service.getInvocation);
};

const getInvocations = async (request, response) => {
  await Controller.handleRequest(request, response, service.getInvocations);
};

const postInvocations = async (request, response) => {
  await Controller.handleRequest(request, response, service.postInvocations);
};

const putInvocations = async (request, response) => {
  await Controller.handleRequest(request, response, service.putInvocations);
};


module.exports = {
  deleteInvocation,
  deleteInvocations,
  getInvocation,
  getInvocations,
  postInvocations,
  putInvocations,
};
