/**
 * The ReviewsController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/ReviewsService');
const deleteReviews = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteReviews);
};

const getReviews = async (request, response) => {
  await Controller.handleRequest(request, response, service.getReviews);
};

const postReviews = async (request, response) => {
  await Controller.handleRequest(request, response, service.postReviews);
};

const putReviews = async (request, response) => {
  await Controller.handleRequest(request, response, service.putReviews);
};


module.exports = {
  deleteReviews,
  getReviews,
  postReviews,
  putReviews,
};
