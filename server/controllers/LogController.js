/**
 * The LogController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/LogService');
const deleteLog = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteLog);
};

const deleteLogs = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteLogs);
};

const getLog = async (request, response) => {
  await Controller.handleRequest(request, response, service.getLog);
};

const getLogs = async (request, response) => {
  await Controller.handleRequest(request, response, service.getLogs);
};

const postLogs = async (request, response) => {
  await Controller.handleRequest(request, response, service.postLogs);
};

const putLogs = async (request, response) => {
  await Controller.handleRequest(request, response, service.putLogs);
};


module.exports = {
  deleteLog,
  deleteLogs,
  getLog,
  getLogs,
  postLogs,
  putLogs,
};
