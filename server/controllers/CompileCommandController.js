/**
 * The CompileCommandController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/CompileCommandService');
const deleteCompileCommand = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCompileCommand);
};

const deleteCompileCommands = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCompileCommands);
};

const getCompileCommand = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCompileCommand);
};

const getCompileCommands = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCompileCommands);
};

const postCompileCommands = async (request, response) => {
  await Controller.handleRequest(request, response, service.postCompileCommands);
};

const putCompileCommands = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCompileCommands);
};


module.exports = {
  deleteCompileCommand,
  deleteCompileCommands,
  getCompileCommand,
  getCompileCommands,
  postCompileCommands,
  putCompileCommands,
};
