/**
 * The CodeController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/CodeService');
const deleteCAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCAnnotations);
};

const deleteCAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCAnnotationsBySourceCodeFilename);
};

const deleteCComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCComments);
};

const deleteCCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCCommentsBySourceCodeFilename);
};

const deleteCPPAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCPPAnnotations);
};

const deleteCPPAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCPPAnnotationsBySourceCodeFilename);
};

const deleteCPPComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCPPComments);
};

const deleteCPPCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCPPCommentsBySourceCodeFilename);
};

const deleteCPPSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCPPSourceCodes);
};

const deleteCPPSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCPPSourceCodesBySourceCodeFilename);
};

const deleteCSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCSourceCodes);
};

const deleteCSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCSourceCodesBySourceCodeFilename);
};

const deleteJavaAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteJavaAnnotations);
};

const deleteJavaAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteJavaAnnotationsBySourceCodeFilename);
};

const deleteJavaComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteJavaComments);
};

const deleteJavaCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteJavaCommentsBySourceCodeFilename);
};

const deleteJavaSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteJavaSourceCodes);
};

const deleteJavaSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteJavaSourceCodesBySourceCodeFilename);
};

const deleteRawSourceCode = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteRawSourceCode);
};

const deleteRawSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteRawSourceCodes);
};

const deleteSourceCodeAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteSourceCodeAnnotations);
};

const deleteSourceCodeAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteSourceCodeAnnotationsBySourceCodeFilename);
};

const deleteSourceCodeComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteSourceCodeComments);
};

const deleteSourceCodeCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteSourceCodeCommentsBySourceCodeFilename);
};

const deleteSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteSourceCodes);
};

const deleteSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteSourceCodesBySourceCodeFilename);
};

const getCAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCAnnotations);
};

const getCAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCAnnotationsBySourceCodeFilename);
};

const getCComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCComments);
};

const getCCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCCommentsBySourceCodeFilename);
};

const getCFunctions = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCFunctions);
};

const getCFunctionsByName = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCFunctionsByName);
};

const getCPPAnnotationArtefacts = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPAnnotationArtefacts);
};

const getCPPAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPAnnotations);
};

const getCPPAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPAnnotationsBySourceCodeFilename);
};

const getCPPClassFields = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPClassFields);
};

const getCPPClassMethods = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPClassMethods);
};

const getCPPCommentArtefacts = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPCommentArtefacts);
};

const getCPPComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPComments);
};

const getCPPCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPCommentsBySourceCodeFilename);
};

const getCPPSourceCodeArtefacts = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPSourceCodeArtefacts);
};

const getCPPSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPSourceCodes);
};

const getCPPSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCPPSourceCodesBySourceCodeFilename);
};

const getCSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCSourceCodes);
};

const getCSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCSourceCodesBySourceCodeFilename);
};

const getCTypes = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCTypes);
};

const getCTypesByName = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCTypesByName);
};

const getCVariables = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCVariables);
};

const getCVariablesByName = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCVariablesByName);
};

const getJavaAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaAnnotations);
};

const getJavaAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaAnnotationsBySourceCodeFilename);
};

const getJavaClassFields = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaClassFields);
};

const getJavaClassMethods = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaClassMethods);
};

const getJavaClasses = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaClasses);
};

const getJavaComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaComments);
};

const getJavaCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaCommentsBySourceCodeFilename);
};

const getJavaSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaSourceCodes);
};

const getJavaSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getJavaSourceCodesBySourceCodeFilename);
};

const getRawSourceCode = async (request, response) => {
  await Controller.handleRequest(request, response, service.getRawSourceCode);
};

const getRawSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.getRawSourceCodes);
};

const getSourceCodeAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.getSourceCodeAnnotations);
};

const getSourceCodeAnnotationsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getSourceCodeAnnotationsBySourceCodeFilename);
};

const getSourceCodeComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.getSourceCodeComments);
};

const getSourceCodeCommentsBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getSourceCodeCommentsBySourceCodeFilename);
};

const getSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.getSourceCodes);
};

const getSourceCodesBySourceCodeFilename = async (request, response) => {
  await Controller.handleRequest(request, response, service.getSourceCodesBySourceCodeFilename);
};

const postJavaSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.postJavaSourceCodes);
};

const postRawSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.postRawSourceCodes);
};

const putCAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCAnnotations);
};

const putCComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCComments);
};

const putCPPAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCPPAnnotations);
};

const putCPPComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCPPComments);
};

const putCPPSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCPPSourceCodes);
};

const putCSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCSourceCodes);
};

const putJavaAnnotations = async (request, response) => {
  await Controller.handleRequest(request, response, service.putJavaAnnotations);
};

const putJavaComments = async (request, response) => {
  await Controller.handleRequest(request, response, service.putJavaComments);
};

const putJavaSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.putJavaSourceCodes);
};

const putRawSourceCodes = async (request, response) => {
  await Controller.handleRequest(request, response, service.putRawSourceCodes);
};


module.exports = {
  deleteCAnnotations,
  deleteCAnnotationsBySourceCodeFilename,
  deleteCComments,
  deleteCCommentsBySourceCodeFilename,
  deleteCPPAnnotations,
  deleteCPPAnnotationsBySourceCodeFilename,
  deleteCPPComments,
  deleteCPPCommentsBySourceCodeFilename,
  deleteCPPSourceCodes,
  deleteCPPSourceCodesBySourceCodeFilename,
  deleteCSourceCodes,
  deleteCSourceCodesBySourceCodeFilename,
  deleteJavaAnnotations,
  deleteJavaAnnotationsBySourceCodeFilename,
  deleteJavaComments,
  deleteJavaCommentsBySourceCodeFilename,
  deleteJavaSourceCodes,
  deleteJavaSourceCodesBySourceCodeFilename,
  deleteRawSourceCode,
  deleteRawSourceCodes,
  deleteSourceCodeAnnotations,
  deleteSourceCodeAnnotationsBySourceCodeFilename,
  deleteSourceCodeComments,
  deleteSourceCodeCommentsBySourceCodeFilename,
  deleteSourceCodes,
  deleteSourceCodesBySourceCodeFilename,
  getCAnnotations,
  getCAnnotationsBySourceCodeFilename,
  getCComments,
  getCCommentsBySourceCodeFilename,
  getCFunctions,
  getCFunctionsByName,
  getCPPAnnotationArtefacts,
  getCPPAnnotations,
  getCPPAnnotationsBySourceCodeFilename,
  getCPPClassFields,
  getCPPClassMethods,
  getCPPCommentArtefacts,
  getCPPComments,
  getCPPCommentsBySourceCodeFilename,
  getCPPSourceCodeArtefacts,
  getCPPSourceCodes,
  getCPPSourceCodesBySourceCodeFilename,
  getCSourceCodes,
  getCSourceCodesBySourceCodeFilename,
  getCTypes,
  getCTypesByName,
  getCVariables,
  getCVariablesByName,
  getJavaAnnotations,
  getJavaAnnotationsBySourceCodeFilename,
  getJavaClassFields,
  getJavaClassMethods,
  getJavaClasses,
  getJavaComments,
  getJavaCommentsBySourceCodeFilename,
  getJavaSourceCodes,
  getJavaSourceCodesBySourceCodeFilename,
  getRawSourceCode,
  getRawSourceCodes,
  getSourceCodeAnnotations,
  getSourceCodeAnnotationsBySourceCodeFilename,
  getSourceCodeComments,
  getSourceCodeCommentsBySourceCodeFilename,
  getSourceCodes,
  getSourceCodesBySourceCodeFilename,
  postJavaSourceCodes,
  postRawSourceCodes,
  putCAnnotations,
  putCComments,
  putCPPAnnotations,
  putCPPComments,
  putCPPSourceCodes,
  putCSourceCodes,
  putJavaAnnotations,
  putJavaComments,
  putJavaSourceCodes,
  putRawSourceCodes,
};
