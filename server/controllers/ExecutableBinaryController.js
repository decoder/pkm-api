/**
 * The ExecutableBinaryController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/ExecutableBinaryService');
const deleteExecutableBinaries = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteExecutableBinaries);
};

const deleteExecutableBinary = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteExecutableBinary);
};

const getExecutableBinaries = async (request, response) => {
  await Controller.handleRequest(request, response, service.getExecutableBinaries);
};

const getExecutableBinary = async (request, response) => {
  await Controller.handleRequest(request, response, service.getExecutableBinary);
};

const postExecutableBinaries = async (request, response) => {
  await Controller.handleRequest(request, response, service.postExecutableBinaries);
};

const putExecutableBinaries = async (request, response) => {
  await Controller.handleRequest(request, response, service.putExecutableBinaries);
};


module.exports = {
  deleteExecutableBinaries,
  deleteExecutableBinary,
  getExecutableBinaries,
  getExecutableBinary,
  postExecutableBinaries,
  putExecutableBinaries,
};
