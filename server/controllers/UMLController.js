/**
 * The UMLController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/UMLService');
const deleteRawUML = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteRawUML);
};

const deleteRawUMLs = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteRawUMLs);
};

const deleteUMLClassDiagrams = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteUMLClassDiagrams);
};

const deleteUMLStateMachines = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteUMLStateMachines);
};

const getRawUML = async (request, response) => {
  await Controller.handleRequest(request, response, service.getRawUML);
};

const getRawUMLs = async (request, response) => {
  await Controller.handleRequest(request, response, service.getRawUMLs);
};

const getUMLClass = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUMLClass);
};

const getUMLClassDiagram = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUMLClassDiagram);
};

const getUMLClassDiagrams = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUMLClassDiagrams);
};

const getUMLStateMachine = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUMLStateMachine);
};

const getUMLStateMachines = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUMLStateMachines);
};

const postRawUMLs = async (request, response) => {
  await Controller.handleRequest(request, response, service.postRawUMLs);
};

const postUMLClassDiagrams = async (request, response) => {
  await Controller.handleRequest(request, response, service.postUMLClassDiagrams);
};

const postUMLStateMachines = async (request, response) => {
  await Controller.handleRequest(request, response, service.postUMLStateMachines);
};

const putRawUMLs = async (request, response) => {
  await Controller.handleRequest(request, response, service.putRawUMLs);
};

const putUMLClassDiagrams = async (request, response) => {
  await Controller.handleRequest(request, response, service.putUMLClassDiagrams);
};

const putUMLStateMachines = async (request, response) => {
  await Controller.handleRequest(request, response, service.putUMLStateMachines);
};


module.exports = {
  deleteRawUML,
  deleteRawUMLs,
  deleteUMLClassDiagrams,
  deleteUMLStateMachines,
  getRawUML,
  getRawUMLs,
  getUMLClass,
  getUMLClassDiagram,
  getUMLClassDiagrams,
  getUMLStateMachine,
  getUMLStateMachines,
  postRawUMLs,
  postUMLClassDiagrams,
  postUMLStateMachines,
  putRawUMLs,
  putUMLClassDiagrams,
  putUMLStateMachines,
};
