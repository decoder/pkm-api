/**
 * The CVEController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/CVEService');
const deleteCVE = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCVE);
};

const deleteCVEs = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteCVEs);
};

const getCVEs = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCVEs);
};

const postCVEs = async (request, response) => {
  await Controller.handleRequest(request, response, service.postCVEs);
};

const putCVEs = async (request, response) => {
  await Controller.handleRequest(request, response, service.putCVEs);
};


module.exports = {
  deleteCVE,
  deleteCVEs,
  getCVEs,
  postCVEs,
  putCVEs,
};
