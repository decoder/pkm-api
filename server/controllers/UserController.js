/**
 * The UserController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/UserService');
const access = async (request, response) => {
  await Controller.handleRequest(request, response, service.access);
};

const deleteUser = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteUser);
};

const dup = async (request, response) => {
  await Controller.handleRequest(request, response, service.dup);
};

const getCurrentUser = async (request, response) => {
  await Controller.handleRequest(request, response, service.getCurrentUser);
};

const getUser = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUser);
};

const getUserProject = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUserProject);
};

const getUserProjects = async (request, response) => {
  await Controller.handleRequest(request, response, service.getUserProjects);
};

const login = async (request, response) => {
  await Controller.handleRequest(request, response, service.login);
};

const logout = async (request, response) => {
  await Controller.handleRequest(request, response, service.logout);
};

const postUser = async (request, response) => {
  await Controller.handleRequest(request, response, service.postUser);
};

const putUser = async (request, response) => {
  await Controller.handleRequest(request, response, service.putUser);
};


module.exports = {
  access,
  deleteUser,
  dup,
  getCurrentUser,
  getUser,
  getUserProject,
  getUserProjects,
  login,
  logout,
  postUser,
  putUser,
};
