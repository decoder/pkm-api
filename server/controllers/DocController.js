/**
 * The DocController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/DocService');
const deleteDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteDocs);
};

const deleteGraphicalDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteGraphicalDocs);
};

const deleteRawDoc = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteRawDoc);
};

const deleteRawDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteRawDocs);
};

const getDocArtefacts = async (request, response) => {
  await Controller.handleRequest(request, response, service.getDocArtefacts);
};

const getDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.getDocs);
};

const getGraphicalDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGraphicalDocs);
};

const getRawDoc = async (request, response) => {
  await Controller.handleRequest(request, response, service.getRawDoc);
};

const getRawDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.getRawDocs);
};

const postDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.postDocs);
};

const postGraphicalDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.postGraphicalDocs);
};

const postRawDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.postRawDocs);
};

const putDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.putDocs);
};

const putGraphicalDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.putGraphicalDocs);
};

const putRawDocs = async (request, response) => {
  await Controller.handleRequest(request, response, service.putRawDocs);
};


module.exports = {
  deleteDocs,
  deleteGraphicalDocs,
  deleteRawDoc,
  deleteRawDocs,
  getDocArtefacts,
  getDocs,
  getGraphicalDocs,
  getRawDoc,
  getRawDocs,
  postDocs,
  postGraphicalDocs,
  postRawDocs,
  putDocs,
  putGraphicalDocs,
  putRawDocs,
};
