/**
 * The GitController file is a very simple one, which does not need to be changed manually,
 * unless there's a case where business logic routes the request to an entity which is not
 * the service.
 * The heavy lifting of the Controller item is done in Request.js - that is where request
 * parameters are extracted and sent to the service, and where response is handled.
 */

const Controller = require('./Controller');
const service = require('../services/GitService');
const deleteGitFile = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteGitFile);
};

const deleteGitFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteGitFiles);
};

const deleteGitWorkingTree = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteGitWorkingTree);
};

const deleteGitWorkingTrees = async (request, response) => {
  await Controller.handleRequest(request, response, service.deleteGitWorkingTrees);
};

const getGitConfig = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGitConfig);
};

const getGitFile = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGitFile);
};

const getGitFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGitFiles);
};

const getGitJob = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGitJob);
};

const getGitWorkingTree = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGitWorkingTree);
};

const getGitWorkingTrees = async (request, response) => {
  await Controller.handleRequest(request, response, service.getGitWorkingTrees);
};

const postGitFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.postGitFiles);
};

const postGitRun = async (request, response) => {
  await Controller.handleRequest(request, response, service.postGitRun);
};

const putGitConfig = async (request, response) => {
  await Controller.handleRequest(request, response, service.putGitConfig);
};

const putGitFiles = async (request, response) => {
  await Controller.handleRequest(request, response, service.putGitFiles);
};


module.exports = {
  deleteGitFile,
  deleteGitFiles,
  deleteGitWorkingTree,
  deleteGitWorkingTrees,
  getGitConfig,
  getGitFile,
  getGitFiles,
  getGitJob,
  getGitWorkingTree,
  getGitWorkingTrees,
  postGitFiles,
  postGitRun,
  putGitConfig,
  putGitFiles,
};
