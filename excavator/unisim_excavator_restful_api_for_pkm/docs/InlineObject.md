# UnisimExcavatorApiForPkm.InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**binary** | **String** | ELF executable binary file with DWARF debugging informations to analyze | 
**sources** | **[String]** | the list of compilation units to process; if not specified or empty, it means all compilation units | [optional] 
**suppress_types** | **[String]** | the list of types to suppress while analysis, e.g. types to be considered as compiler builtins | [optional] 
**suppress_functions** | **[String]** | the list of functions to suppress while analysis, e.g. functions to be considered as compiler builtins | [optional] 
**output_dir** | **String** | the output directory of the analysis; the result of the analysis is a reversed engineered C source codes | [optional] [default to &#39;&#39;]


