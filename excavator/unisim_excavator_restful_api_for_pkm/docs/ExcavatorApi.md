# UnisimExcavatorApiForPkm.ExcavatorApi

All URIs are relative to *http://pkm-api_excavator_1:8087*

Method | HTTP request | Description
------------- | ------------- | -------------
[**excavator**](ExcavatorApi.md#excavator) | **POST** /excavator/{dbName} | 
[**getJob**](ExcavatorApi.md#getJob) | **GET** /excavator/jobs/{jobId} | 



## excavator

> Job excavator(dbName, key, body, opts)



Parses DWARF debugging information from an ELF executable binary and generates an equivalent (interfaces and code skeletons only) compilable C source code

### Example

```javascript
var UnisimExcavatorApiForPkm = require('unisim_excavator_api_for_pkm');

var apiInstance = new UnisimExcavatorApiForPkm.ExcavatorApi();
var dbName = "dbName_example"; // String | The project name from where to get the ELF executable binary and put analysis results (compilable C source code)
var key = "key_example"; // String | Access key to the PKM (optional, depending on server configuration)
var body = new UnisimExcavatorApiForPkm.InlineObject(); // InlineObject | 
var opts = {
  'asynchronous': false, // Boolean | flag to control asynchronous/synchronous execution of Excavator job
  'invocationID': "invocationID_example" // String | invocation identifier (optional)
};
apiInstance.excavator(dbName, key, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dbName** | **String**| The project name from where to get the ELF executable binary and put analysis results (compilable C source code) | 
 **key** | **String**| Access key to the PKM (optional, depending on server configuration) | 
 **body** | [**InlineObject**](InlineObject.md)|  | 
 **asynchronous** | **Boolean**| flag to control asynchronous/synchronous execution of Excavator job | [optional] [default to false]
 **invocationID** | **String**| invocation identifier (optional) | [optional] 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getJob

> Job getJob(jobId, key)



Get Excavator Job. Getting a finished or failed job, unpublish it (it is no longer available)

### Example

```javascript
var UnisimExcavatorApiForPkm = require('unisim_excavator_api_for_pkm');

var apiInstance = new UnisimExcavatorApiForPkm.ExcavatorApi();
var jobId = 56; // Number | Job identifier
var key = "key_example"; // String | Access key to the PKM (optional, depending on server configuration)
apiInstance.getJob(jobId, key).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters



Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| Job identifier | 
 **key** | **String**| Access key to the PKM (optional, depending on server configuration) | 

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

