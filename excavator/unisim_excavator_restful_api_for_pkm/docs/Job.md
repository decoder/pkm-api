# UnisimExcavatorApiForPkm.Job

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | job identifier | [optional] 
**service_name** | **String** | name of the service running the job | [optional] 
**parameters** | [**JobParameters**](JobParameters.md) |  | [optional] 
**state** | **String** | job state | [optional] 
**start_date** | **String** | date (GMT) when job started | [optional] 
**end_date** | **String** | date (GMT) when job ended | [optional] 
**logs** | **String** | log messages | [optional] 
**warnings** | **String** | warning messages | [optional] 
**err** | **Object** | error | [optional] 



## Enum: StateEnum


* `pending` (value: `"pending"`)

* `running` (value: `"running"`)

* `failed` (value: `"failed"`)

* `finished` (value: `"finished"`)




