/**
 * UnisimExcavatorApiForPkm
 * API of UNISIM Excavator for PKM
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: decoder@decoder-project.eu
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 *
 * OpenAPI Generator version: 5.3.0
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.UnisimExcavatorApiForPkm) {
      root.UnisimExcavatorApiForPkm = {};
    }
    root.UnisimExcavatorApiForPkm.InlineObject = factory(root.UnisimExcavatorApiForPkm.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';



  /**
   * The InlineObject model module.
   * @module model/InlineObject
   * @version 1.0.0
   */

  /**
   * Constructs a new <code>InlineObject</code>.
   * @alias module:model/InlineObject
   * @class
   * @param binary {String} ELF executable binary file with DWARF debugging informations to analyze
   */
  var exports = function(binary) {
    var _this = this;

    _this['binary'] = binary;
  };

  /**
   * Constructs a <code>InlineObject</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/InlineObject} obj Optional instance to populate.
   * @return {module:model/InlineObject} The populated <code>InlineObject</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('binary')) {
        obj['binary'] = ApiClient.convertToType(data['binary'], 'String');
      }
      if (data.hasOwnProperty('sources')) {
        obj['sources'] = ApiClient.convertToType(data['sources'], ['String']);
      }
      if (data.hasOwnProperty('suppress_types')) {
        obj['suppress_types'] = ApiClient.convertToType(data['suppress_types'], ['String']);
      }
      if (data.hasOwnProperty('suppress_functions')) {
        obj['suppress_functions'] = ApiClient.convertToType(data['suppress_functions'], ['String']);
      }
      if (data.hasOwnProperty('output_dir')) {
        obj['output_dir'] = ApiClient.convertToType(data['output_dir'], 'String');
      }
    }
    return obj;
  }

  /**
   * ELF executable binary file with DWARF debugging informations to analyze
   * @member {String} binary
   */
  exports.prototype['binary'] = undefined;
  /**
   * the list of compilation units to process; if not specified or empty, it means all compilation units
   * @member {Array.<String>} sources
   */
  exports.prototype['sources'] = undefined;
  /**
   * the list of types to suppress while analysis, e.g. types to be considered as compiler builtins
   * @member {Array.<String>} suppress_types
   */
  exports.prototype['suppress_types'] = undefined;
  /**
   * the list of functions to suppress while analysis, e.g. functions to be considered as compiler builtins
   * @member {Array.<String>} suppress_functions
   */
  exports.prototype['suppress_functions'] = undefined;
  /**
   * the output directory of the analysis; the result of the analysis is a reversed engineered C source codes
   * @member {String} output_dir
   * @default ''
   */
  exports.prototype['output_dir'] = '';



  return exports;
}));


