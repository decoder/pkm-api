/* eslint-disable no-unused-vars */
const Service = require('./Service');

/**
* Parses DWARF debugging information from an ELF executable binary and generates an equivalent (interfaces and code skeletons only) compilable C source code
*
* dbName String The project name from where to get the ELF executable binary and put analysis results (compilable C source code)
* key String Access key to the PKM (optional, depending on server configuration)
* body InlineObject 
* asynchronous Boolean flag to control asynchronous/synchronous execution of Excavator job (optional)
* invocationID String invocation identifier (optional) (optional)
* returns Job
* */
const excavator = ({ dbName, key, body, asynchronous, invocationID }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const mode = (body.mode !== undefined) ? body.mode : { parser : true };
			const binary = body.binary;
			const sources = body.sources;
			const suppress_types = body.suppress_types;
			const suppress_functions = body.suppress_functions;
			let output_dir = body.output_dir;
			if(output_dir && output_dir.startsWith('/')) output_dir.slice(1); // make sure output directory is relative when specified
			const debug = (global.server_config !== undefined) && global.server_config.debug;
			const do_not_remove_temporary_root_directory = (global.server_config === undefined) || global.server_config.do_not_remove_temporary_root_directory;
			const tmp_dir = (global.server_config !== undefined) && global.server_config.tmp_dir;
			if(invocationID !== undefined) asynchronous = false;
			
			const Job = require('../../../util/job');
			
			let job = new Job('excavator', Object.assign({ dbName : dbName, key : key }, body), { debug : debug });
			
			if(asynchronous)
			{
				resolve(Service.successResponse(job, 201));
			}
			
			let promise = job.run(new Promise((resolve, reject) =>
			{
				const PkmRestfulApi = require('pkm_restful_api');
				let error;
				let status = true;
				let end_date;
				let invocation;
				
				((invocationID === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					invocations_api.getInvocation(dbName, invocationID, key).then((invocation_obj) =>
					{
						const Invocation = require('../../../util/invocation');
						invocation = new Invocation(invocation_obj);
						resolve();
					}).catch((get_invocation_err) =>
					{
						const msg = 'can\'t get invocation with invocationID \'' + invocationID + '\'' + (get_invocation_err.status ? (': PKM server returned error status ' + get_invocation_err.status) : '');
						job.error(msg);
						reject(new Error(msg));
					});
				})).then(() => new Promise((resolve, reject) =>
				{
					const execute_binary_api = new PkmRestfulApi.ExecutableBinaryApi();
					const code_api = new PkmRestfulApi.CodeApi();
					const File = require('../../../util/file.js');
					const FileSystem = require('../../../util/file_system');
					const excavator = require('../../../util/excavator').excavator;
					
					// create a temporary file system
					const file_system = new FileSystem({ debug : debug, logger : job, ...((tmp_dir !== undefined) ? { tmp_dir : tmp_dir } : {}) });
					file_system.make_temporary_root_directory().then(() =>
					{
						// a temporary file system was created
						new Promise((resolve, reject) =>
						{
							// get the executable binary
							execute_binary_api.getExecutableBinary(dbName, binary, key).then((binary_file) =>
							{
								// write the content of the executable binary file on the host file system
								file_system.writeFile(File.from(binary_file)).then(() =>
								{
									// the content of the executable binary file was written on the host file system
									resolve();
								}).catch((write_file_err) =>
								{
									// an error occurred while writting the content of the executable binary on the host file system
									reject(write_file_err);
								});
							}).catch((get_binary_err) =>
							{
								// an error occurred while getting the executable binary file
								reject(get_binary_err);
							});
						}).then(() => new Promise((resolve, reject) =>
						{
							let excavator_options =
							{
								debug : debug,
								logger : job,
								sources : sources,
								suppress_types : suppress_types,
								suppress_functions : suppress_functions,
								output_dir : (binary + '_out')
							};
							
							// run UNISIM Excavator
							excavator(file_system, binary, excavator_options).then((excavator_result) =>
							{
								const c_source_code_filenames = excavator_result.c_source_code_filenames;
								
								if(debug && c_source_code_filenames)
								{
									job.log(c_source_code_filenames.length + ' C source code files generated');
								}
								
								((!c_source_code_filenames) ? Promise.resolve() : new Promise((resolve, reject) =>
								{
									c_source_code_filenames.forEach((c_source_code_filename) =>
									{
										file_system.readFile(c_source_code_filename, { encoding : 'utf8' }).then((source_code_file) =>
										{
											// remove excavator output directory prefix and /
											source_code_file.rel_path = source_code_file.rel_path.slice(excavator_options.output_dir.length + 1);
											
											// add optional user's output directory prefix
											if(output_dir)
											{
												const path = require('path');
												source_code_file.rel_path = path.posix.join(output_dir, source_code_file.rel_path);
											}
											
											if(debug)
											{
												job.log('putting a C source code file named \'' + source_code_file.rel_path + '\'');
											}
											
											code_api.putRawSourceCodes(dbName, key, [ source_code_file.export() ]).then(() =>
											{
												if(invocation !== undefined) invocation.pushRawSourceCodes(dbName,  [ source_code_file ]);
												
												resolve();
											}).catch((put_c_source_code_file_err) =>
											{
												const msg = 'can\'t put C source code file named \'' + source_code_file.rel_path + '\'' + (put_c_source_code_file_err.status ? (': PKM server returned error status ' + put_c_source_code_file_err.status) : '');
												job.error(msg);
												reject(new Error(msg));
											});
										}).catch((read_c_source_code_file_err) =>
										{
											reject(read_c_source_code_file_err);
										});
									});
								})).then(() =>
								{
									// all the source code files were updated in the database
									resolve()
								}).catch((err) =>
								{
									// an error occurred while updating the database
									reject(err);
								});
							}).catch((err) =>
							{
								// an error occurred while running UNISIM Excavator
								reject(err);
							});
						})).catch((err) =>
						{
							// delay the promise rejection until temporary file system has been deleted
							error = err;
							status = false;
						}).finally(() =>
						{
							// delete previously created temporary file system
							(do_not_remove_temporary_root_directory ? Promise.resolve() : file_system.remove_temporary_root_directory()).catch((err) =>
							{
								// an error occurred while deleting the temporary file system
								const { deep_copy } = require('../../../util/deep_copy');
								job.error(JSON.stringify(deep_copy(err)));
								
								if(status)
								{
									error = err;
									status = false;
								}
							}).finally(() =>
							{
								if(status)
								{
									resolve();
								}
								else
								{
									reject(error);
								}
							});
						});
					}).catch((err) =>
					{
						// an error occurred while creating the temporary file system
						reject(err);
					});
				})).catch((err) =>
				{
					// delay the promise rejection until the logs in the database have been updated
					error = err;
					status = false;
				}).finally(() =>
				{
					end_date = new Date();
					
					return new Promise((resolve, reject) =>
					{
						// Update the Logs in the database
						const log_api = new PkmRestfulApi.LogApi();
						
						let log_document =
						{
							tool: 'Excavator',
							'nature of report' : 'Reverse engineering',
							'start running time' : job.start_date,
							'end running time': new Date().toUTCString(),
							messages: job.messages,
							warnings: job.warnings,
							errors : job.errors,
							status: status,
							details:
							{
								parameters: job.parameters
							}
						};
						
						if(debug)
						{
							job.log('putting a Log document');
						}
						log_api.postLogs(dbName, key, [ log_document ]).then((artefactsIds) =>
						{
							if(invocation !== undefined) invocation.pushLogs(dbName, artefactsIds);
							
							resolve();
						}).catch((post_logs_err) =>
						{
							const msg = 'can\'t post Log documents' + (post_logs_err.status ? (': PKM server returned error status ' + post_logs_err.status) : '');
							job.error(msg);
							if(status)
							{
								error = new Error(msg);
								status = false;
							}
						}).finally(() =>
						{
							resolve();
						});
					});
				}).finally(() => (invocation === undefined) ? Promise.resolve() : new Promise((resolve, reject) =>
				{
					// Update the invocation in the database
						
					invocation.invocationStatus = status ? 'COMPLETED' : 'FAILED';
					invocation.setTimestampCompleted(end_date);
					
					const invocations_api = new PkmRestfulApi.InvocationsApi();
					
					if(debug)
					{
						job.log('putting invocation with invocationID \'' + invocation.invocationID + '\'');
					}
					invocations_api.putInvocations(dbName, key, [ invocation ]).catch((put_invocations_err) =>
					{
						const msg = 'can\'t put invocation with invocationID \'' + invocation.invocationID + '\'' + (put_invocations_err.status ? (': PKM server returned error status ' + put_invocations_err.status) : '');
						job.error(msg);
						if(status)
						{
							error = new Error(msg);
							status = false;
						}
					}).finally(() =>
					{
						resolve();
					});
				})).finally(() =>
				{
					if(status)
					{
						resolve();
					}
					else
					{
						reject(error);
					}
				});
			}));
			
			if(!asynchronous)
			{
				promise.then((job) =>
				{
					resolve(Service.successResponse(job, 201));
				}).catch((job) =>
				{
					reject(Service.rejectResponse(job, 500));
				}).finally(() =>
				{
					job.unpublish();
				});
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}
/**
* Get Excavator Job. Getting a finished or failed job, unpublish it (it is no longer available)
*
* jobId Integer Job identifier
* key String Access key to the PKM (optional, depending on server configuration)
* returns Job
* */
const getJob = ({ jobId, key }) => {
	return new Promise((resolve, reject) =>
	{
		try
		{
			const Job = require('../../../util/job');
			
			const job = Job.find(jobId);
			if(job !== undefined)
			{
				if(job.parameters.key == key)
				{
					if((job.state == 'failed') || (job.state == 'finished'))
					{
						job.unpublish();
					}
					resolve(Service.successResponse(job, 200));
				}
				else
				{
					reject(Service.rejectResponse(new String('Forbidden'), 403));
				}
			}
			else
			{
				resolve(Service.rejectResponse(new String('Not Found'), 404));
			}
		}
		catch(err)
		{
			console.warn(err);
			reject(Service.rejectResponse(new String("Internal Server Error"), 500)); // Internal Server Error
		}
	});

}

module.exports = {
  excavator,
  getJob,
};

