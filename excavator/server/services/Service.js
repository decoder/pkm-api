class Service {
  static rejectResponse(error, code = 500) {
    return { error, code };
  }

  static successResponse(payload, code = 200, options) {
    return { payload, code, options };
  }
}

module.exports = Service;
