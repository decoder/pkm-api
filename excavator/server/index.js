const config = require('./config');
const logger = require('./logger');
const ExpressServer = require('./expressServer');
const fs = require('fs');
const path = require('path');
try
{
  server_config = JSON.parse(fs.readFileSync(path.resolve(path.join(__dirname, 'server_config.json'))));
}
catch(err)
{
  console.warn(err);
  server_config = {};
}
console.log('UNISIM Excavator for PKM: server configuration:\n' + JSON.stringify(server_config, null, 2));

const launchServer = async () => {
  try {
    expressServer = new ExpressServer(config.URL_PORT, config.OPENAPI_YAML);
    expressServer.launch();
    logger.info('Express server running');
  } catch (error) {
    logger.error('Express Server failure', error.message);
    await this.close();
  }
};

launchServer().catch(e => logger.error(e));
