#!/bin/bash

echo "Excavator for PKM start script."

VOLUME_CONFIG=/pkm-api/volume_config
[ -f "${VOLUME_CONFIG}/excavator/server_config.json" ] && \
    cp -f "${VOLUME_CONFIG}/excavator/server_config.json" /pkm-api/excavator && \
    cp -f "${VOLUME_CONFIG}/excavator/server_config.json" /pkm-api/excavator/server
  
runuser -u excavator -- /pkm-api/excavator/server/start_server.sh
